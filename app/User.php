<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract {

    use Authenticatable, Authorizable;

    // TODO: Review fields and roles
    protected $fillable
        = [
            'username',
            'email',
            'first',
            'last',
            'role',
            'allow_access',
            'password',
        ];

    protected $hidden
        = [
            'password',
        ];

    public static function authenticate($username, $password) {
        $user   = User::where('username', $username)->first();
        $hasher = app()->make('hash');

        if ($user && $hasher->check($password, $user->password)) {
            return $user;
        }

        return false;
    }
}
