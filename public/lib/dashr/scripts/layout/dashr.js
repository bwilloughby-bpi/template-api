'use strict';

if (typeof Dashr === 'undefined') {
	var Dashr = {};
}

Dashr.scriptLoader = {
	scripts: [],
	progress: {
		pending: 0,
		loaded: 0,
		callBack: null
	},

	onLoaded: null,

	pageLoaded: false,

	setup: function() {
		this.addEvent();
		return this;
	},

	add: function(scriptObj, index) {
		var createObj = function(obj) {
			return {
				src: obj.src || '',
				type: obj.type || '',
				async: obj.async || false,
				defer: (obj.async || obj.defer === false) ? false : true,
				callBack: obj.callBack || '',
				status: 'not-loaded'
			};
		};

		var script = createObj(scriptObj),
			scripts = this.scripts;

		if (typeof index !== 'undefined') {
			if (index === 'first') {
				scripts.unshift(script);
			}
			else if (!isNaN(index)) {
				scripts.splice(index, 0, script);
			}
			else {
				scripts.push(script);
			}
		}
		else {
			scripts.push(script);
		}

		this.progress.pending++;

		if (this.pageLoaded === true) {
			this.createScript(script);
		}

		return this;
	},

	updateProgress: function() {
		var progress = this.progress;
		var percent = Math.round(progress.loaded / progress.pending * 100);

		var callBack = this.progress.callBack;
		if (typeof callBack === 'function') {
			callBack(percent);
		}

		if (percent >= 100) {
			if (typeof this.onLoaded === 'function') {
				this.onLoaded();
			}
		}
	},

	remove: function(src) {
		var scripts = this.scripts;
		for (var i = 0; i < scripts.length; i++) {
			var obj = scripts[i];
			if (obj.src === src) {
				this.scripts.splice(i, 1);
			}
		}

		return this;
	},

	createScript: function(obj) {
		if (obj && typeof obj === 'object') {
			var d = document;
			var s = d.createElement('script');

			s.async = (obj.async === true) ? true : false;
			s.defer = (obj.defer === true) ? true : false;

			var self = this;
			s.onload = function() {
				self.progress.loaded++;
				self.updateProgress();
				obj.status = 'loaded';

				if (typeof obj.callBack === 'function') {
					obj.callBack();
				}
			};

			s.src = obj.src;

			var node = d.getElementsByTagName('head')[0];
			node.appendChild(s);
		}

		return this;
	},

	loadScripts: function() {
		var scripts = this.scripts;
		for (var i = 0, maxLength = scripts.length; i < maxLength; i++) {
			var obj = scripts[i];
			this.createScript(obj);
		}
	},

	addEvent: function() {
		var self = this;

		var callBackFn = function() {
			self.pageLoaded = true;
			self.loadScripts();
		};

		if (typeof window.addEventListener === 'function') {
			window.addEventListener('load', callBackFn, false);
		}
		else {
			window.attachEvent('onload', callBackFn);
		}

		return this;
	}
}.setup();

Dashr.loadModules = function(basePath, modules) {
	modules = modules || ['base', 'core', 'controls', 'alerts', 'graphs', 'modals', 'panels', 'subs', 'main'];
	if (modules.length > 0) {
		basePath = basePath || '/';
		var dir = basePath + 'dashr/scripts/' + 'layout/';
		var loader = Dashr.scriptLoader;

		var add = function(src, defer) {
			defer = defer === false ? false : true;
			loader.add({ src: src, defer: defer });
		};

		for (var i = 0, maxLength = modules.length; i < maxLength; i++) {
			var module = modules[i];
			switch (module) {
				case 'base':
					add(dir + 'base-1.5.js', false);
					break;
				case 'core':
					add(dir + 'login-panel.js');
					add(dir + 'nav-settings-option.js');
					add(dir + 'navigation.js');
					add(dir + 'background-panel.js');
					break;
				case 'controls':
					add(dir + 'controls/selector.js');
					add(dir + 'controls/tab-panel.js');
					add(dir + 'controls/option-group-panel.js');
					add(dir + 'controls/toggle.js');
					add(dir + 'controls/page-panel.js');
					add(dir + 'controls/list.js');
					add(dir + 'controls/select-list.js');
					add(dir + 'controls/calendar.js');

					add(dir + 'controls/tab.js');
					add(dir + 'controls/tabs-container.js');
					add(dir + 'controls/collapse-search.js');
					add(dir + 'controls/filter-component.js');
					add(dir + 'controls/module.js');
					break;
				case 'alerts':
					add(dir + 'alerts/alert.js');
					add(dir + 'alerts/flash-panel.js');
					break;
				case 'graphs':
					add(dir + 'graphs/graph.js');
					add(dir + 'graphs/graph-bar.js');
					add(dir + 'graphs/graph-line.js');
					add(dir + 'graphs/radial-progress.js');
					add(dir + 'graphs/radial-graph.js');
					break;
				case 'modals':
					add(dir + 'modals/modal.js');
					add(dir + 'modals/edit-modal.js');
					add(dir + 'modals/data-modal.js');
					add(dir + 'modals/search-modal.js');
					add(dir + 'modals/file-upload-modal.js');
					break;
				case 'subs':
					add(dir + 'subs/modal-sub-panel.js');
					break;
				case 'panels':
					add(dir + 'panels/main-panel.js');
					add(dir + 'panels/list-panel.js');
					add(dir + 'panels/fixed-panel.js');
					add(dir + 'panels/settings-panel.js');
					add(dir + 'panels/modules-panel.js');
					break;
				case 'main':
					add(dir + 'main-controller.js');
					break;
			}
		}
	}
}; 
