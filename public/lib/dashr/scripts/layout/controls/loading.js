"use strict";

var loadingPanelArray = []; 

/* 
	LoadingPanel 
	
	this will create a loading panel. 
*/
var LoadingPanel = function(message,container)
{ 
	this.init('LoadingPanel');    
	
	this.title = ''; 
	this.message = message;   
	this.container = (container)? container: document.body;   
	
	this.timer = null; 
}; 

/* we want to extend the base html builder class and 
html builder class and the class methods to the 
prototype */ 
base.Component.extend( 
{ 
	constructor: LoadingPanel, 
	
	setup: function()
	{ 
		this.addToLoadingArray(this); 
		
		this.render(); 
		this.align(); 
		this.setupEvents(); 
		this.addEvent();    
	}, 
	
	reset: function()
	{ 
	
	}, 
	
	render: function()
	{ 
		var id = this.id; 
		var frag = this.createDocFragment(); 
		this.addElement('div',id + '_shadow','loading_shadow fadeIn','',this.container);
	
		var panel = this.addElement('div',id,'loading_panel','',frag); 
		
		var titleContainer = this.addElement('div','', 'title_container', '', panel); 
		this.addElement('div','', 'title', this.title, titleContainer);
		
		var loadingCenter = this.addElement('div','', 'center_container', '', panel); 
		var loadingCenterArea = this.addElement('div','', 'center_area', '', loadingCenter); 
		var imageContainer = this.addElement('div','', 'image_holder', '', loadingCenterArea); 
		
		this.addElement('div','', 'image rotateCircle', '', imageContainer);
		this.addElement('div','', 'message left light', this.message, loadingCenterArea);  
		
		this.addElement('div','', 'button_holder', '', panel);  
		this.append(this.container, frag); 
	}, 
	
	/*remove alert*/ 
	remove: function()
	{  
		//remove panel and shadow 
		var panel = document.getElementById(this.id); 
		if(panel) 
		{ 
			this.removeElement(panel); 
		} 
		
		panel = document.getElementById(this.id + '_shadow'); 
		if(panel) 
		{ 
			this.removeElement(panel); 
		} 
	}, 
	
	/* self will add to the loading panel array to 
	show when it has been removed */ 
	addToLoadingArray: function(panel) 
	{ 
		/* we want to  clear out any previous loading 
		panels that may still be loading */ 
		if(this.checkPreviousPanels()) 
		{ 
			this.stopPreviousPanels(); 
		} 
		// add panel to loading panel array 
		loadingPanelArray.push(panel); 
	}, 
	
	/* self will check if there are previous loading 
	panels still pending */ 
	checkPreviousPanels: function() 
	{ 
		if(loadingPanelArray.length >= 1) 
		{ 
			return true; 
		} 
		else 
		{ 
			return false; 
		} 
	}, 
	
	/* self will clear out all loading panels */ 
	stopPreviousPanels: function() 
	{ 
		for(var i = 0, maxLength = loadingPanelArray.length; i < maxLength; i++) 
		{ 
			var loadingPanel = loadingPanelArray[i]; 
			if(loadingPanel) 
			{ 
				loadingPanel.stop.call(loadingPanel); 
			}
		} 
	}, 
	
	/* self will remove the loading panel from the loading panel array */ 
	removeFromLoadingPanel: function(panel) 
	{ 
		var index = base.inArray(loadingPanelArray, panel); 
		if(index !== -1) 
		{  
			loadingPanelArray.splice(index, 1);
		} 
	}, 
	
	/*stop alert*/ 
	stop: function()
	{ 
		this.removeEvent(); 
		//remove panel from loading panel array 
		this.removeFromLoadingPanel(this);  
		this.remove();   
	},  
	
	/* align alert panel to center of the window */ 
	align: function()
	{ 
		var panel = document.getElementById(this.id); 
		var windowSize = base.getWindowSize(); 
		
		var top = (windowSize.height / 2) - (panel.offsetHeight / 2);
		var left = (windowSize.width / 2) - (panel.offsetWidth / 2); 
		
		panel.style.top = top + 'px'; 
		panel.style.left = left + 'px'; 
	}, 
	
	setupEvents: function() 
	{ 
		var callBack = base.bind(this, this.align); 
		
		this.addEvent = function()
		{  
			base.addListener('onresize', window, callBack); 
		}; 
		
		this.removeEvent = function()
		{  
			base.removeListener('onresize', window, callBack); 
		}; 
	}, 
	
	/* add and remove events */ 
	addEvent: null, 
	
	removeEvent: null    
}); 