'use strict';

var NestedModel = BaseModel.extend({

	xhr: {
		parentObjectType: null,

		index: function(parentId, filter, skip, take, callback) {
			var url = '/api/' + this.parentObjectType + '/' + parentId + '/' + this.objectType;

			filter = filter || {};
			skip = !isNaN(skip) ? skip : 0;
			take = !isNaN(take) ? take : 50;

			var params = {
				filter: base.prepareJsonUrl(filter),
				skip: skip,
				take: take
			};

			this._get(url, params, callback);
		},

		store: function(parentId, callback) {
			var url = '/api/' + this.parentObjectType + '/' + parentId + '/' + this.objectType;

			var params = this.model.get();

			return this._post(url, params, callback);
		},

		show: function(parentId, id, callback) {
			var url = '/api/' + this.parentObjectType + '/' + parentId + '/' + this.objectType + '/' + id;

			var self = this;
			this._get(url, {}, function(response) {
				var obj = self.getObject(response);
				self.model.set(obj);

				if (typeof callback === 'function') {
					callback(response);
				}
			});
		},

		update: function(parentId, callback) {
			if (this.isValid() === false)
				return false;

			var id = this.model.get('id');
			var url = '/api/' + this.parentObjectType + '/' + parentId + '/' + this.objectType + '/' + id;

			var params = this.model.get();

			return this._put(url, params, callback);
		},

		destroy: function(parentId, callback) {
			var id = this.model.get('id');
			var url = '/api/' + this.parentObjectType + '/' + parentId + '/' + this.objectType + '/' + id;

			return this._delete(url, callback);
		}
	}
});
