"use strict";

/* 
	List 
	
	this will create a list that can iterate through 
	the options and can be updated and extended. 
	
	to use this the list should be extended and the methods 
	to create list rows, list row titles, and update should 
	be overridden. 
	 
	@param (string) container = the parent container id that 
	will receive the new panel 
*/
var List = function(multiSelect, container) 
{ 
	/* this will setup to have mutliple instances of the 
	panel in one project without issue */ 
	this.init('List');  
	
	this.multiselect = (multiSelect == true)? true : false; 
	this.container = container; 
}; 
	
/* we want to extend the html builder class and the 
class methods to the prototype */ 
base.Component.extend( 
{ 
	/* we want to reset the constructor */ 
	constructor: List,  
	
	status: 'not_setup',   

	lastSelectedOption: null,  
	optionsArray: [],   
	
	/* this will reset the list and list options */ 
	resetList: function() 
	{  
		this.optionsArray = []; 
		
		var container = (typeof this.container === 'object')? this.container : document.getElementById(this.container); 
		if(container) 
		{ 
			this.removeAll(container);  
		}
	}, 
	
	/* this will setup a list from a list array. 
	@param (array) list = the list array */ 
	setupList: function(listArray) 
	{ 
		/* we want to clear  the old list before 
		adding anything new */ 
		this.resetList(); 
		
		/* we want to add the new rows to a document fragement 
		and then add them back to the original container */ 
		var previousContainer = this.container; 
		this.container = this.createDocFragment(); 
		
		/* we need to create the list title */ 
		var title = this.createListOption('title'); 
		
		if(listArray && typeof listArray === 'object' && listArray.length > 0) 
		{ 			
			/* create the option array and setup the options */ 
			for(var i = 0, maxLength = listArray.length; i < maxLength; i++) 
			{ 
				var option = listArray[i]; 
				this.addOption(option);     
			}  
		} 
		else 
		{ 
			this.createListOption('default'); 
		} 
		
		this.append(previousContainer, this.container); 
		this.container = previousContainer; 
		
		this.status = 'setup'; 
	}, 
	
	addOption: function(option) 
	{ 
		var number = this.optionsArray.length; 

		/* setup option settings */ 
		option.optionNumber = number; 
		option.optionRowId = this.id + '_option_number' + '_' + number; 
		option.checkboxId = this.id + '_checkbox' + '_' + number;
		option.selected = 'no';      
				
		this.optionsArray.push(option); 
		
		/* add a new option to the list panel */ 
		this.createListOption('row', option); 
	}, 
	
	createListOption: function(type, option) 
	{ 
		switch(type)
		{ 
			case 'row': 
				this.createListRow(option); 
				break; 
			case 'title': 
				this.createListRowTitle(); 
				break; 
			default: 
				this.createListRowDefault();
		}
	}, 
	
	createListRow: function(option) 
	{ 
		var fadeInClass = (this.status === 'not_setup')? 'fade-in-med' : '';
		var row = this.addButton('div', option.optionRowId, 'row-list ' + fadeInClass, '', '', this.container); 
		
		var checkBox = this.addElement('div', '', 'col-check dark center', '', row);  
		
		/* we need to bind the method to the 
		object to use in the closure */ 
		var bind = base.bind(this, this.selectOption); 
		var input = this.addCheckbox(option.checkboxId, '', function(){ bind(option); }, this.isChecked(option), checkBox, true);  
		
		var col = this.addElement('div', '', 'col center', base.getValue(option, 'id'), row);  
		col = this.addElement('div', '', 'col left', base.getValue(option, 'name'), row); 
		col = this.addElement('div', '', 'col left', base.getValue(option, 'address'), row);
		col = this.addElement('div', '', 'col center', base.getValue(option, 'city') + ', ' + base.getValue(option, 'state') + ' ' + base.getValue(option, 'zip'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'email'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'mobilePhone'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'dob'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'status'), row); 
	}, 
	
	createListRowTitle: function() 
	{ 
		var row = this.addButton('div', '', 'row-list row-title light left', '', '', this.container); 
			
		var checkBox = this.addElement('div','','col-check dark center','',row);
		var tmpCheck = this.addCheckbox(this.id + '_title_checkbox', '', base.bind(this, this.changeCheckAll), '', checkBox); 
		
		var col = this.addElement('div','','col light center', 'ID',row);
		col = this.addElement('div','','col light left',' Name',row); 
		col = this.addElement('div','','col light left',' Address',row);
		col = this.addElement('div','','col light center','C/S/Z',row); 
		col = this.addElement('div','','col light center','Email',row);
		col = this.addElement('div','','col light center','Cell Phone',row); 
		col = this.addElement('div','','col light center','Birthday',row);
		col = this.addElement('div','','col light center','Status',row); 
	}, 
	
	createListRowDefault: function() 
	{ 
		//var row = this.addElement('div', '', 'row-list row-default dark center', '', this.container); 
	}, 
	
	/* this will get checked mode */ 
	changeCheckAll: function() 
	{ 
		var checkbox = document.getElementById(this.id + '_title_checkbox'); 
		if(checkbox.checked === true) 
		{ 
			this.selectOptions();  
		} 
		else 
		{ 
			this.unselectOption('');  
		} 
	}, 
	
	/* this will get checked mode */ 
	isChecked: function(option) 
	{ 
		return (option.selected === 'yes')? 1 : 0; 
	}, 
	
	selectOption: function(option)
	{ 
		var object = document.getElementById(option.optionRowId);  
		if(option.selected === 'no') 
		{  
			option.selected = 'yes'; 
			this.setLastSelectedOption(option);  
		} 
		else 
		{  
			option.selected = 'no'; 
		} 
		
		object.className = this.getOptionStyle(option); 
		
		/* check if the panel allows multiple selections */ 
		if(this.multiselect != true) 
		{ 
			/* unselectall other options */ 
			this.unselectOption(option); 
		} 
	}, 
	
	/* this will save the last selected option */ 
	setLastSelectedOption: function(option) 
	{ 
		this.lastSelectedOption = option; 
	}, 
	
	/* we can use the get and select next options to 
	move through the list 
	@return (object) the next option to select */
	getNextOption: function() 
	{ 
		/* we want to get the last option and setup the next option 
		number be incremeneting the last option number */ 
		var lastOption = this.lastSelectedOption; 
		var lastNumber = (lastOption !== null)? lastOption.optionNumber : 0; 
		var nextNumber = ++lastNumber; 
		 
		/* we need to check if we are at the end of the list */ 
		var options = this.optionsArray; 
		var index = (nextNumber < options.length)? nextNumber : 0; 
		
		return options[index];
	},  
	
	/* this will select the next option */ 
	selectNextOption: function() 
	{ 
		var nextOption = this.getNextOption(); 
		if(nextOption) 
		{ 
			this.selectOption(nextOption); 
		} 
	},  
	
	/* we can use the get and select previous options to 
	move in reverse through the list 
	@return (object) the previous option to select */  
	getPreviousOption: function() 
	{ 
		/* we want to get the last option and setup the previous option 
		number be decremeneting the last option number */ 
		var lastOption = this.lastSelectedOption;
		var lastNumber = (lastOption !== null)? lastOption.optionNumber : 0; 
		var previousNumber = --lastNumber; 
		
		/* we need to check if we have reached the begining of the list */ 
		var options = this.optionsArray; 
		var index = (previousNumber >= 0)? previousNumber : (options.length - 1);  
		
		return options[index];
	},  
	
	/* this will select the previous option */ 
	selectPreviousOption: function() 
	{ 
		var previousOption = this.getPreviousOption(); 
		if(previousOption) 
		{ 
			this.selectOption(previousOption); 
		} 
	},
	
	/* this will unselect all options except the last selected option */ 
	unselectOption: function(selectedOption) 
	{ 
		var options = this.optionsArray; 
		for(var j = 0, maxLength = options.length; j < maxLength; j++) 
		{ 
			var option = options[j];  
			if(option !== selectedOption) 
			{ 
				/* unselect any option that is selected */ 
				if(option.selected === 'yes') 
				{ 
					option.selected = 'no'; 
					this.updatedOptionStyle(option, false); 
				} 
			} 
		} 
	},  
	
	/* this will select all options */ 
	selectOptions: function() 
	{ 
		var options = this.optionsArray;
		for(var j = 0, maxLength = options.length; j < maxLength; j++) 
		{  
			var option = options[j]; 
			if(option) 
			{ 
				option.selected = 'yes'; 
				this.updatedOptionStyle(option, true);  
			} 
		} 
	}, 
	
	updatedOptionStyle: function(option, checked)
	{ 
		var doc = document;
		doc.getElementById(option.optionRowId).className = this.getOptionStyle(option); 
		doc.getElementById(option.checkboxId).checked = checked;
	}, 
	
	/* this will get the style of the option */ 
	getOptionStyle: function(option) 
	{ 
		return (option.selected === 'yes')? 'row-list selected' : 'row-list';   
	},      
	
	getSelectedOptions: function() 
	{ 
		var selectedOptions = []; 
		
		var options = this.optionsArray;
		for(var i = 0, maxLength = options.length; i < maxLength; i++) 
		{ 
			var option = options[i]; 
			if(option.selected === 'yes') 
			{  
				selectedOptions.push(option); 
			} 
		} 
		
		return selectedOptions; 
	}, 
	
	/* we want to save our  xhr incase we want 
	to stop it later */ 
	xhr: null, 
	
	update: function() 
	{ 
		/* example */ 
		/* we want to check if their is a previous xhr 
		that we want to stop before we create a new 
		xhr */ 
		/* 
		if(this.xhr !== null) 
		{ 
			this.xhr.abort(); 
			this.removeMiniLoading();
		} 
		
		this.addMiniLoading(); 
		
		var startNumber = this.pages.getLimit('start'), 
		stopNumber = this.pages.getLimit('stop'); 
		
		var params = "op=getClientPatientsByLimit"+
					 "&appCode=" + encodeURI(app.client.app)+
					 "&option=" + encodeURI(this.optionCategory.value)+ 
					 "&start=" + encodeURI(startNumber)+ 
					 "&stop=" + encodeURI(stopNumber); 
	
		this.xhr = base.ajax("/api/patient/", params, base.bind(this, this.updateResponse));*/ 
	}, 
	
	updateResponse: function(response) 
	{ 
		/* example 
		this.removeMiniLoading(); 
		this.xhr = null; 
		
		if(response) 
		{ 
			var result = response; 
			this.pages.checkToUpdate(result.rowCount);
			
			this.setupList(result.patients); 
		} 
		else 
		{ 
			var label = this.personLabel; 
			var mpalert = new Alert('alert', 'Error','There was an error getting the ' + label + ' list.<br> Message: ' + result.message); 
			mpalert.start();
		}*/ 
	} 
}); 