var Tab = function() {
	this.init();

	this.title = 'Tab';
};

base.Component.extend({
	constructor: Tab,

	setup: function(container) {
		this.render(container);
	},

	render: function(container) {
		var layout = {
			id: this.id
		};

		base.parseLayout(layout, container);
	},

	update: function() {

	}
});