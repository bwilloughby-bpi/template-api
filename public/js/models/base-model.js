'use strict';

var BaseModel = base.Model.extend({

	xhr: {
		index: function(filter, skip, take, callback) {
			var url = '/api/' + this.objectType;

			filter = filter || {};
			skip = !isNaN(skip) ? skip : 0;
			take = !isNaN(take) ? take : 50;

			var params = {
				filter: base.prepareJsonUrl(filter),
				skip: skip,
				take: take
			};

			this._get(url, params, callback);
		},

		store: function(callback) {
			var url = '/api/' + this.objectType;

			var params = this.model.get();

			return this._post(url, params, callback);
		},

		show: function(id, callback) {
			var url = '/api/' + this.objectType + '/' + id;

			var self = this;
			this._get(url, {}, function(response) {
				var obj = self.getObject(response);
				self.model.set(obj);

				if (typeof callback === 'function') {
					callback(response);
				}
			});
		},

		update: function(callback) {
			if (this.isValid() === false)
				return false;

			var id = this.model.get('id');
			var url = '/api/' + this.objectType + '/' + id;

			var params = this.model.get();

			return this._put(url, params, callback);
		},

		destroy: function(callback) {
			var id = this.model.get('id');
			var url = '/api/' + this.objectType + '/' + id;

			return this._delete(url, {}, callback);
		},

		destroyAll: function(ids, callback) {
			var url = '/api/' + this.objectType;

			return this._delete(url, { ids: ids }, callback);
		},

		search: function(term, callback) {
			var url = '/api/' + this.objectType + '/search';

			var params = {
				term: term
			};

			return this._get(url, params, callback);
		}
	}
});
