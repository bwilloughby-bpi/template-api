'use strict';

/* Modal */
var Modal = function(container) {
	this.init();

	this.option = {};

};

base.Component.extend({
	constructor: Modal,

	optionPanel: null,
	fullScreen: true,
	minimize: null,
	miniLoading: null,
	loading: false,

	setup: function() {
		this.render();
		this.setupHeaderOptions();
	},

	setupHeaderOptions: function() {
		this.setupOptions();
		this.setupMiniLoading();
		this.setupMinimize();
	},

	remove: function() {
		var id = this.id;
		var doc = document;
		var panel = doc.getElementById(id);
		if (panel) {
			this.removeElement(panel);
		}

		panel = doc.getElementById(id + '_shadow');
		if (panel) {
			this.removeElement(panel);
		}
	},

	setupMinimize: function(cancelSetup) {
		var self = this;

		/* this will allow the panel to minimize and collapse all
		content but the title bar and send to the main minimize
		panel and allow to restore the panel from the minimize
		panel */
		this.minimize = {
			setup: function() {
				this.addButtons();
			},
			addButtons: function() {
				this.createMinimizeButton();
				this.createCloseButton();
			},
			createMinimizeButton: function() {
				var button = self.addButton('button', self.id + '_option_button_minimize', 'title-bttn minimize', '<span class="minimize"></span>', base.bind(this, this.expandOrCollapsePanel), self.id + '_title_button_group');
			},
			createCloseButton: function() {
				var button = self.addButton('button', self.id + '_option_button_close', 'title-bttn close', '<span></span><span></span>', base.bind(this, this.closePanel), self.id + '_title_button_group');
			},
			expandOrCollapsePanel: function() {
				var panel = document.getElementById(self.id),
					shdw = document.getElementById(self.id + '_shadow');
				if (self.fullScreen === true && base.hasClass(panel, 'minimized') === false) {
					base.addClass(panel, 'minimized');
					base.animate.out(shdw, 'fadeOut', 800);
					self.fullScreen = false;

					/* we want to add thepanel to the
					minimized container */
					app.backgroundPanel.minimize.add(panel);
				}
				else {
					base.removeClass(panel, 'minimized');
					base.animate._in(shdw, 'fadeIn', 500);
					self.fullScreen = true;

					/* we want to remove the panel from
					the minimize container */
					app.backgroundPanel.minimize.remove(panel);
				}
			},
			closePanel: function() {
				var panel = document.getElementById(self.id);
				app.backgroundPanel.minimize.remove(panel);
				self.decline();
			}
		};

		if (cancelSetup != true) {
			this.minimize.setup();
		}
	},

	setupMiniLoading: function(listContainer) {
		var self = this;
		/* this will create and remove mini loading panels and
		track if the panel is loading */
		self.miniLoading =
			{
				listContainer: null,

				setup: function() {
					this.setupListContainer();

					/* we want to check if the loading panel
					is already created*/
					if (self.loading === false) {
						/* we wantto create a new loading panel */
						this.setupMiniLoading();
					}

					this.startLoadingMode();
				},

				setupListContainer: function() {
					if (listContainer !== null) {
						this.listContainer = (typeof listContainer !== 'object') ? document.getElementById(listContainer) : listContainer;
					}
				},

				/* this will setup a miniloading panel */
				setupMiniLoading: function() {
					var loadingContainer = self.addElement('div', self.id + '_loading_container', 'title-loading-container', '', self.id + '_title_container');
					self.addElement('div', self.id + '_loading_image', 'loading-circle loading-circle-small-white', '', loadingContainer);
					/* we want to save that loading has started */
					self.loading = true;
				},

				remove: function() {
					var panel = document.getElementById(self.id + '_loading_container');
					if (panel) {
						self.removeElement(panel);
					}
					/* we want to save that loading has stopped */
					self.loading = false;

					this.endLoadingMode();
				},

				startLoadingMode: function() {
					var id = this.listContainer;
					if (id) {
						var panel = this.listContainer;
						panel.style.opacity = '.6';
					}
				},

				endLoadingMode: function() {
					var id = this.listContainer;
					if (id) {
						var panel = this.listContainer;
						panel.style.opacity = '1';
					}
				}
			};
	},

	addMiniLoading: function() {
		this.miniLoading.setup();
	},

	removeMiniLoading: function() {
		this.miniLoading.remove();
	},

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'modal edit-panel', '', frag);

		this.createHeader(panel);

		var container = this.addElement('div', id + '_list_container', 'body-container', '', panel);
		this.createBodyPanel(container);

		this.createFooter(panel);
		this.append(this.container, frag);
	},

	createHeader: function(container) {
		var id = this.id;
		var title = this.addElement('header', id + '_title_container', 'title-container', '', container);
		var titleLabel = this.addElement('div', id + '_title', 'title dark', this.getTitle(), title);
		var titleButtonGroup = this.addElement('div', id + '_title_button_group', 'title-button-group', '', title);
	},

	addHeader: function() {
		var id = this.id;
		return {
			node: 'header',
			id: id + '_title_container',
			className: 'title-container',
			titleLabel: {
				id: id + '_title',
				className: 'title dark',
				textContent: this.getTitle()
			},
			titleButtonGroup: {
				id: id + '_title_button_group',
				className: 'title-button-group'
			}
		};
	},

	createFooter: function(container) {
		var id = this.id;
		var buttons = this.addElement('footer', id + '_button_container', 'button-container', '', container);
		var button1 = this.addButton('button', id + '_button_1', 'bttn-red', 'Cancel', base.bind(this, this.decline), buttons);
		var button2 = this.addButton('button', id + '_button_2', 'bttn-green', 'Save', base.bind(this, this.accept), buttons);
	},

	addFooter: function() {
		var id = this.id;
		return {
			node: 'footer',
			id: id + '_button_container',
			className: 'button-container',
			decline: {
				id: id + '_button_1',
				className: 'bttn-red',
				textContent: 'Cancel',
				onclick: base.bind(this, this.decline)
			},
			accept: {
				id: id + '_button_2',
				className: 'bttn-green',
				textContent: 'Save',
				onclick: base.bind(this, this.accept)
			}
		};
	},

	getTitle: function() {
		return 'Edit Panel';
	},

	createRowInput: function(type, id, label, value, container, callBackFn, validate) {
		/* we want to make the callback optional */
		callBackFn = callBackFn || '';
		var className = (validate == true) ? 'val' : '';

		var tmpRow = this.addElement('div', '', 'row light left', '', container),
			tmpLabel = this.addElement('label', '', '', label, tmpRow),
			field = this.addInput(type, id, className, value, callBackFn, '', tmpRow);

		return field;
	},

	createRowInputDate: function(type, id, label, value, container, callBackFn, validate) {
		/* we want to make the callback optional */
		callBackFn = callBackFn || '';

		var className = (validate == true) ? 'val' : '';
		var tmpRow = this.addElement('div', '', 'row light left', '', container),
			tmpLabel = this.addElement('label', '', '', label, tmpRow),
			field = this.addInput(type, id, 'date ' + className, value, callBackFn, '', tmpRow);

		var parent = this;

		/* we want to add a call to the calendar method
		in a closure to bind the parent and add the
		field to the method params */
		var button = this.addButton('button', '', 'bttn date', '', function() { parent.calendar(field); }, tmpRow);

		return field;
	},

	createRowText: function(id, label, value, container) {
		var tmpRow = this.addElement('div', '', 'row text dark left', '', container),
			tmpLabel = this.addElement('div', '', 'text-label light', label, tmpRow),
			field = this.addElement('div', id, 'text-value dark', value, tmpRow);

		return field;
	},

	createRowTextarea: function(id, label, value, container, callBackFn, validate) {
		/* we want to make the callback optional */
		callBackFn = callBackFn || '';
		var className = (validate == true) ? 'val' : '';

		var tmpRow = this.addElement('div', '', 'row light left', '', container),
			tmpLabel = this.addElement('label', '', '', label, tmpRow),
			field = this.addTextarea(id, 'small ' + className, value, callBackFn, tmpRow);

		return field;
	},

	calendar: function(field) {
		if (field !== null) {
			var calendar = new Calendar(field);
			calendar.setup();
		}
	},

	createRowSelect: function(id, label, value, container, callBackFn) {
		/* we want to make the callback optional */
		callBackFn = callBackFn || '';

		var tmpRow = this.addElement('div', '', 'row light left', '', container),
			tmpLabel = this.addElement('label', '', '', label, tmpRow),
			field = this.addSelect(id, '', value, callBackFn, tmpRow);

		return field;
	},

	createBodyPanel: function(container) {
		var h2 = this.addElement('h2', '', 'left dark', 'Sales Settings', container);

		var panel = this.addElement('div', '', 'content-panel', '', container);

		var field = this.createRowInput('text', this.id + '_contact_name', 'Contact Name', base.getValue(this.option, 'contactName'), panel);
		field.focus();

		field = this.createRowInput('text', this.id + '_contact_name', 'Address', base.getValue(this.option, 'address'), panel);

		var subTitle = this.addElement('div', '', 'sub-title white', 'Goals', panel);

		field = this.createRowInput('text', this.id + '_sales_monthly_goal', 'Monthly Goal', base.getValue(this.option, 'salesMonthlyGoal'), panel);

		field = this.createRowInput('text', this.id + '_sales_yearly_goal', 'Yearly Goal', base.getValue(this.option, 'salesYearlyGoal'), panel);
	},

	setupOptions: function() {
		var optionsArray = [
			{ label: 'message', tmpFunction: '' },
			{ label: 'Delete', tmpFunction: '' }
		];

		var panel = this.setupEditOptionsPanel(optionsArray);
	},

	setupEditOptionsPanel: function(optionsArray) {
		var parent = this;

		this.optionPanel = new ModalTitleOptionPanel(parent, this.id + '_title_button_group');
		this.optionPanel.setup(optionsArray);
	},

	checkToClose: function() {
		if (this.fullScreen === true) {
			this.decline();
		}
	},

	returnCallBack: function(data) {
		var callBack = this.callbackFunction;
		if (typeof this.callbackFunction === 'function') {
			callBack(data);
		}
	},

	removeAndCallBack: function(data) {
		this.returnCallBack(data);
		window.setTimeout(base.bind(this, this.display), 200);
	},

	accept: function() {
		this.removeAndCallBack();
	},

	decline: function() {
		this.display();
	},

	display: function(center) {
		this.toggleDisplay();

		if (this.toggleMode === 'block') {
			if (center) {
				this.align();
			}
		}
	},

	toggleMode: null,

	toggleDisplay: function() {
		var obj = document.getElementById(this.id);
		var display = obj.style.display;

		if (display === '' || display === 'none') {
			obj.style.display = 'block';
			this.toggleMode = 'block';
			this.createShadow();
		}
		else {
			obj.style.display = 'none';
			this.toggleMode = 'none';
			this.remove();
		}
	},

	createShadow: function() {
		var self = this;
		this.addButton('div', this.id + '_shadow', 'panel-shadow modal-shadow fadeIn', '', function() {self.checkToClose();}, self.container);
	},

	align: function() {
		var panel = document.getElementById(this.id),
			windowSize = base.getWindowSize();

		var top = (windowSize.height / 2) - (panel.offsetHeight / 2),
			left = (windowSize.width / 2) - (panel.offsetWidth / 2);

		panel.style.top = top + 'px';
		panel.style.left = left + 'px';
	},

	setupEvents: function() {
		var callBack = base.bind(this, this.align);

		this.addEvent = function() {
			base.on('resize', window, callBack);
		};

		this.removeEvent = function() {
			base.off('resize', window, callBack);
		};
	},

	/* add and remove events */
	addEvent: null,

	removeEvent: null
});

/* this will create and add the option panel to 
the title bar */
var ModalTitleOptionPanel = function(parent, container) {
	this.init('ModalTitleOptionPanel');

	this.parent = parent;
	this.optionsArray = [];
	this.lastSelectedOption = null;
	this.container = container;
};

base.Component.extend(
	{
		constructor: ModalTitleOptionPanel,

		setup: function(optionsArray) {
			this.render();
			this.setupEvents();
			this.setupOptions(optionsArray);
			this.toggleButton();
		},

		render: function() {
			var id = this.id;
			this.addButton('button', id + '_option_button', 'title-bttn nav', '<span></span><span></span><span></span>', base.bind(this, this.expandOrCollapse), this.container);

			this.addElement('div', id + '_edit_option_container', 'title-option-container fadeFlipX dark left', '', this.container);
		},

		toggleButton: function() {
			var bttn = document.getElementById(this.id + '_option_button');
			bttn.style.display = (this.optionsArray.length) ? 'block' : 'block';
		},

		expandOrCollapse: function() {
			var obj = document.getElementById(this.id + '_edit_option_container');
			var btn = document.getElementById(this.id + '_option_button');
			var display = obj.style.display;
			if (display === '' || display === 'none') {
				//show panel
				obj.style.display = 'block';
				base.addClass(btn, 'selected');
				this.addEvent();
			}
			else {
				//do not display
				obj.style.display = 'none';
				base.removeClass(btn, 'selected');
				this.removeEvent();
			}
		},

		checkEvent: function(e) {
			if (e.srcElement && e.srcElement.id !== this.id + '_option_button') {
				this.expandOrCollapse();
			}
		},

		setupEvents: function() {
			var callback = base.bind(this, this.checkEvent);

			this.addEvent = function() {
				base.addListener('mouseup', window, callback);
			};

			this.removeEvent = function() {
				base.removeListener('mouseup', window, callback);
			};
		},

		addEvent: null,

		removeEvent: null,

		createOption: function(option) {
			var tmpFunction = (option.tmpFunction) ? option.tmpFunction : '';
			var button = this.addButton('li', option.nameId, 'option', base.getValue(option, 'label'), tmpFunction, this.id + '_edit_option_container');
		},

		setupOptions: function(optionsArray) {
			if (typeof optionsArray === 'object') {
				for (var i = 0, length = optionsArray.length; i < length; i++) {
					this.addOption(optionsArray[i]);
				}
			}
		},

		update: function(optionsArray) {
			this.resetOptions();
			this.setupOptions(optionsArray);
		},

		resetOptions: function() {
			var container = document.getElementById(this.id + '_edit_option_container');
			this.removeAll(container);

			this.optionsArray = [];
			this.optionNumber = 0;

			this.toggleButton();
		},

		optionNumber: 0,

		addOption: function(option) {
			if (option && typeof option === 'object') {
				option.number = this.optionNumber++;
				option.nameId = this.id + '_option_' + option.number;
				option.selected = 'no';

				this.optionsArray.push(option);
				this.createOption(option);
			}
		},

		/* select option */
		selectOption: function(option) {
			var object = document.getElementById(option.nameId);

			if (option.selected === 'no') {
				option.selected = 'yes';
				this.setLastSelectedOption(option);

				var callBack = option.tmpFunction;
				if (typeof callBack === 'function') {
					callBack.call();
				}
			}
			else {
				option.selected = 'no';
			}

			object.className = this.getOptionStyle(option);

			/* unselectall other options */
			this.unselectOption(option);
		},

		/* this will get the style of the option */
		getOptionStyle: function(option) {
			return (option.selected === 'yes') ? 'option selected' : 'option';
		},

		/* we can use the get and select
		next options to move in reverse
		through the list */
		getNextOption: function(lastOptionNumber) {
			/* we want to get the last option and setup the next option
			number be incremeneting the last option number */
			var last = this.lastSelectedOption;
			var lastNumber = (last !== null) ? last.optionNumber : 0;
			var nextNumber = ++lastNumber;

			/* we need to check if we are at the end of the list */
			var options = this.optionsArray;
			var index = (nextNumber <= options.length) ? nextNumber : 0;
			return options[index];
		},

		selectNextOption: function() {
			var nextOption = this.getNextOption();
			if (nextOption) {
				this.selectOption(nextOption);
			}
		},

		/* we can use the get and select
		previous options to move in reverse
		through the list */
		getPreviousOption: function(lastOptionNumber) {
			/* we want to get the last option and setup the previous option
			number be decremeneting the last option number */
			var last = this.lastSelectedOption;
			var lastNumber = (last !== null) ? last.optionNumber : 0;
			var previousNumber = --lastNumber;

			/* we need to check if we have reached the begining of the list */
			var options = this.optionsArray;
			var index = (previousNumber >= 1) ? previousNumber : (options.length - 1);
			return options[index];
		},

		selectPreviousOption: function() {
			var previousOption = this.getPreviousOption();
			if (previousOption) {
				this.selectOption(previousOption);
			}
		},

		/* this will save the last selected option */
		setLastSelectedOption: function(option) {
			this.lastSelectedOption = option;
		},

		/* this will unselect all options except the last selected option */
		unselectOption: function(tmpOption) {
			var options = this.optionsArray;
			for (var j = 0, maxLength = options.length; j < maxLength; j++) {
				var option = options[j];
				if (option !== tmpOption) {
					if (option.selected === 'yes') {
						option.selected = 'no';
						var panel = document.getElementById(option.nameId);
						if (panel) {
							panel.className = this.getOptionStyle(option);
						}
					}
				}
			}
		}
	});