var Module = function() {
	this.init();

	this.moduleSize = 'large';
};

base.Component.extend({
	constructor: Module,

	setup: function(container) {
		var layout = this.getLayout();
		base.parseLayout(layout, container);
	},

	getLayout: function() {
		var layout = {
			class: 'module module-' + this.moduleSize, textContent: 'Module Base Class'
		};

		return layout;
	},

	update: function(data) {
		console.log(this.name, data);
	}
});