'use strict';

var FixedPanel = function(container) {
	this.init();

	this.label = 'List';
	this.panelTitle = this.label;
	this.mainPanelClass = '';

	this.autoUpdate = false;

	this.iconUrl = '../dashr/images/icons/locations.svg';
	this.model = new Model();

	this.container = container;
};

ListPanel.extend({
	constructor: FixedPanel,

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();

		var panelClass = this.mainPanelClass || '';
		var panel = this.addElement('div', id, 'main-panel list-panel fixed-panel ' + panelClass, '', frag);

		this.createBody(panel);

		this.append(this.container, frag);
	},

	createBody: function(container) {
		var main = this.addElement('div', this.id + '_body_wrapper', 'main-wrapper', '', container);

		this.createHeader(main);
		var contentContainer = this.addElement('div', this.id + '_content_container', 'content-container', '', main);
		this.createSettings(contentContainer);

		this.addElement('div', this.id + '_main_list_container', 'main-settings-container list-container', '', contentContainer);
		this.addElement('footer', this.id + '_page_container', 'settings-button-container', '', container);
	},

	setupScrollChecker: function() {
		var id = this.id;

		this.scrollChecker = {
			settingsContainer: document.getElementById(id + '_settings_container'),
			listContainer: document.getElementById(id + '_main_list_container'),
			container: document.getElementById(id + '_body_wrapper'),

			enabled: false,
			scrolled: false,
			stickySet: false,

			addEvents: null,
			eventsAdded: false,

			setup: function() {
				this.enabled = true;
				this.setupEvents();
			},

			checkScrollPosition: function() {
				var titleContainer = this.settingsContainer;
				var container = this.container;
				var scrollPosition = base.getScrollPosition(container);

				var scrollTop = scrollPosition.top;
				if (scrollTop > 0) {
					if (this.scrolled === false) {
						this.scrolled = true;
						base.addClass(titleContainer, 'minify');
					}
				}
				else {
					if (this.scrolled === true) {
						this.scrolled = false;
						base.removeClass(titleContainer, 'minify');
					}
				}

				if (scrollTop > 54) {
					if (this.stickySet === false) {
						this.stickySet = true;
						base.addClass(titleContainer, 'sticky');
						base.addClass(this.listContainer, 'add-buffer');
					}
				}
				else {
					if (this.stickySet === true) {
						this.stickySet = false;
						base.removeClass(titleContainer, 'sticky');
						base.removeClass(this.listContainer, 'add-buffer');
					}
				}
			},

			setupEvents: function() {
				var panel = this.container;
				var callBack = base.bind(this, this.checkScrollPosition);

				this.addEvents = function() {
					if (this.eventsAdded !== true) {
						this.eventsAdded = true;
						base.addListener('scroll', panel, callBack);
					}
				};

				this.removeEvents = function() {
					this.eventsAdded = false;
					base.removeEvent('scroll', panel, callBack);
				};

				this.addEvents();
			}
		};

		this.scrollChecker.setup();
	},

	scrollToTop: function() {
		var panel = document.getElementById(this.id + '_body_wrapper');
		if (panel) {
			panel.scrollTop = 0;
		}
	}
});