'use strict';

/* ListPanel */
var ListPanel = function(container) {
	this.init('ListPanel');

	this.container = container;

	this.label = 'List';
	this.panelTitle = this.label;
	this.mainPanelClass = '';

	this.autoUpdate = false;

	this.iconUrl = '../dashr/images/icons/locations.svg';
	this.model = new Model();
};

MainPanel.extend({
	constructor: ListPanel,

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();

		var panelClass = this.mainPanelClass || '';
		var panel = this.addElement('div', id, 'main-panel list-panel ' + panelClass, '', frag);

		this.createHeader(panel);
		this.createBody(panel);
		this.append(this.container, frag);
	},

	createBody: function(container) {
		var id = this.id;
		var contentContainer = this.addElement('div', id + '_content_container', 'content-container', '', container);

		this.createSettings(contentContainer);

		this.addElement('div', id + '_main_list_container', 'main-settings-container list-container', '', contentContainer);
		this.addElement('footer', id + '_page_container', 'settings-button-container', '', container);
	},

	getLayout: function() {
		var id = this.id;
		var panelClass = this.mainPanelClass || '';

		return {
			id: id,
			class: 'main-panel list-panel ' + panelClass,
			header: this.addHeader(),
			body: this.addBody(),
			footer: { id: id + '_page_container', class: 'settings-button-container' }
		};
	},

	addBody: function() {
		var id = this.id;
		return {
			id: id + '_content_container',
			class: 'content-container',
			settings: this.addSettingsContainer(),
			content: { id: id + '_main_list_container', class: 'main-settings-container list-container' }
		};
	},

	addSettingsContainer: function() {
		var id = this.id;
		return {
			id: id + '_settings_container',
			class: 'settings-container',
			buttons: {
				id: id + '_settings_button_container',
				class: 'button-container',
				children: this.addSettingsButtons()
			}
		};
	},

	addSettingsButtons: function() {
		var id = this.id;
		return [
			{ node: 'button', class: 'bttn white', textContent: 'Add', onclick: base.bind(this, this.add) }
		];
	},

	setup: function() {
		/* the search panel must be setup before the panel
		is created */
		this.setupSearch();

		this.render();
		this.setupPanelDefault();
	},

	setupFilters: function() {

	},

	setupPanelDefault: function() {
		this.setupPagePanel();
		this.setupMiniLoading();
		this.setupFilters();
		this.setupList(this.id + '_main_list_container');
		this.setupScrollChecker();
	},

	/* this will allow the panel to start any services
	in this method when the panel activates */
	activate: function() {

	},

	/* this will allow the panel to update any services
	in this method when the panel is active */
	update: function() {
		this.getList();
	},

	/* this will allow the panel to stop any services
	when the panel becomes deactivated */
	deactivate: function() {

	},

	add: function() {
		this.openModal('add');
	},

	edit: function(option) {
		this.openModal('edit', option);
	},

	openModal: function(mode, option) {

	},

	/* this will store the list object */
	list: null,

	setupList: function(listContainer) {
		this.list = new List(listContainer);
	},

	searchTitleLabel: '',

	/* this will setup the search panel by overriding the search
	function and setting up the callback function */
	setupSearch: function() {
		var self = this;
		/* we need to create a new search panel object and
		override the panel search settings */
		var searchPanel = function() {};
		searchPanel.prototype = this.addSearchPanel();

		searchPanel.prototype.search = function(tmpsearch) {
			/* we want to check to cancel prevous xhr before settings
			up a thenext request */
			this.cancelPreviousXhr();

			this.xhr = self.model.xhr.search('', base.bind(this, this.searchResults), tmpsearch);
		};

		var callBack = base.bind(this, this.getSearchSettings);

		/* initialize new search panel with search title
		and callback fn */
		var panel = this.searchPanel = new searchPanel();
		panel.setup(this.searchTitleLabel, callBack);
	},

	getSearchSettings: function(id) {

	},

	getList: function() {
		this.list.update();
	}
});

/* we want to extend the list class to add our row
and update methods */
var PanelList = function(parent, multiSelect, container) {
	this.init('PanelList');

	this.multiselect = (multiSelect == true) ? true : false;
	this.parent = parent;
	this.container = container;
};

/* we want to extend the html builder class and the
class methods to the prototype */
List.extend(
	{
		/* we want to reset the constructor */
		constructor: PanelList,

		createListRow: function(option) {

		},

		createListRowTitle: function() {

		},

		/* we want to save our  xhr incase we want
		to stop it later */
		xhr: null,

		update: function() {
			var parent = this.parent;
			/* we want to check if their is a previous xhr
			that we want to stop before we create a new
			xhr */
			if (this.xhr !== null) {
				this.xhr.abort();
				parent.removeMiniLoading();
			}

			parent.addMiniLoading();

			var startNumber = parent.pages.getLimit('start'),
				stopNumber = parent.pages.getLimit('stop');

			var filter = this.setupFilter();

			this.xhr = parent.model.xhr.index('', base.bind(this, this.updateResponse), startNumber, stopNumber, filter);
		},

		setupFilter: function() {
			return {};
		},

		updateResponse: function(response) {
			var parent = this.parent;
			parent.removeMiniLoading();
			this.xhr = null;

			if (response) {
				var result = response;
				parent.pages.checkToUpdate(result.rowCount);

				this.setupList(result.rows);
			}
			else {
				var mpalert = new Alert('alert', 'Error', 'There was an error getting the list.');
				mpalert.start();
			}
		}
	});