"use strict"; 

var SelectList = function(multiSelect, container) 
{ 
	/* this will setup to have mutliple instances of the 
	panel in one project without issue */ 
	this.init('SelectList');  
	this.selected = []; 
	this.optionsArray = []; 
	this.lastSelectedOption = null; 
	this.status = 'not_setup'; 
	this.multiselect = (multiSelect === true)? true : false; 
	this.selectedCallBack = null; 
	
	this.container = container; 
}; 
	
/* we want to extend the html builder class and the 
class methods to the prototype */ 
List.extend( 
{ 
	/* we want to reset the constructor */ 
	constructor: SelectList, 
	
	addSelectedOption: function(option)
	{ 
		if(option)
		{ 
			this.selected.push(option); 
			this.performSelectedCallBack(); 
		}
	}, 
	
	performSelectedCallBack: function()
	{ 
		var callBack = this.selectedCallBack; 
		if(typeof callBack === 'function')
		{ 
			callBack(this.selected.length); 
		}
	}, 
	
	isSelected: function(option)
	{ 
		var options = this.selected, 
		length = options.length; 
		if(option && length > 0)
		{ 
			for(var j = 0; j < length; j++) 
			{ 
				/* this will check to match the previous selection 
				to the new options */ 
				var selectedOption = options[j];  
				if(option && option.id === selectedOption.id) 
				{ 
					/* we want to override the selected option 
					with the new version of the selected option */ 
					options[j] = option; 
					return true; 
				} 
			}
		}
		return false; 
	}, 
	
	removeSelectedOption: function(option)
	{ 
		if(option)
		{ 
			var options = this.selected; 
			var index = base.inArray(options, option); 
			if(index > -1)
			{ 
				options.splice(index, 1); 
				this.performSelectedCallBack();
			}
		}
	}, 
	
	resetSelectedOption: function()
	{ 
		this.selected = []; 
		this.performSelectedCallBack();
	},
	
	/* this will setup a list from a list array. 
	@param (array) list = the list array */ 
	setupList: function(listArray) 
	{ 
		/* we want to clear  the old list before 
		adding anything new */ 
		this.resetList(); 
		
		/* we want to add the new rows to a document fragement 
		and then add them back to the original container */ 
		var previousContainer = this.container; 
		this.container = this.createDocFragment(); 
		
		/* we need to create the list title */ 
		this.createListOption('title'); 
		
		if(listArray && typeof listArray === 'object' && listArray.length > 0) 
		{ 			
			/* create the option array and setup the options */ 
			for(var i = 0, maxLength = listArray.length; i < maxLength; i++) 
			{ 
				var option = listArray[i]; 
				this.addOption(option);     
			}  
		} 
		else 
		{ 
			this.createListOption('default'); 
		} 
		
		this.append(previousContainer, this.container); 
		this.container = previousContainer; 
		
		this.status = 'setup'; 
	}, 
	
	addOption: function(option) 
	{ 
		var options = this.optionsArray; 
		var number = options.length; 

		var id = this.id; 
		/* setup option settings */ 
		option.optionNumber = number; 
		option.optionRowId = id + '_option_number' + '_' + number; 
		option.checkboxId = id + '_checkbox' + '_' + number;
		option.selected = this.isSelected(option) === true? 'yes' : 'no'; 
				
		options.push(option); 
		
		/* add a new option to the list panel */ 
		this.createListOption('row', option); 
	}, 
	
	createListOption: function(type, option) 
	{ 
		switch(type)
		{ 
			case 'row': 
				this.createListRow(option); 
				break; 
			case 'title': 
				this.createListRowTitle(); 
				break; 
			default: 
				this.createListRowDefault();
		}
	}, 
	
	createListRow: function(option) 
	{ 
		var fadeInClass = (this.status === 'not_setup')? 'fade-in-med' : '';
		var rowClassName = this.getOptionStyle(option); 
		var row = this.addButton('div', option.optionRowId, rowClassName + ' ' + fadeInClass, '', '', this.container); 
		
		var checkBox = this.addElement('div', '', 'col-check dark center', '', row);  
		
		/* we need to bind the method to the 
		object to use in the closure */ 
		var bind = base.bind(this, this.selectOption); 
		var input = this.addCheckbox(option.checkboxId, '', function(){ bind(option); }, this.isChecked(option), checkBox, true);  
		
		var col = this.addElement('div', '', 'col center', base.getValue(option, 'id'), row);  
		col = this.addElement('div', '', 'col left', base.getValue(option, 'name'), row); 
		col = this.addElement('div', '', 'col left', base.getValue(option, 'address'), row);
		col = this.addElement('div', '', 'col center', base.getValue(option, 'city') + ', ' + base.getValue(option, 'state') + ' ' + base.getValue(option, 'zip'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'email'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'mobilePhone'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'dob'), row); 
		col = this.addElement('div', '', 'col center', base.getValue(option, 'status'), row); 
	},  
	
	selectOption: function(option)
	{ 
		var object = document.getElementById(option.optionRowId);  
		if(option.selected === 'no') 
		{  
			option.selected = 'yes'; 
			this.setLastSelectedOption(option); 
			this.addSelectedOption(option); 
		} 
		else 
		{  
			option.selected = 'no'; 
			this.removeSelectedOption(option);
			this.lastSelectedOption = null; 
		} 
		
		object.className = this.getOptionStyle(option); 
		
		/* check if the panel allows multiple selections */ 
		if(this.multiselect !== true) 
		{ 
			/* unselectall other options */ 
			this.unselectOption(option); 
		} 
	},
	
	/* this will unselect all options except the last selected option */ 
	unselectOption: function(selectedOption) 
	{ 
		var options = this.selected.slice(0); 
		for(var j = 0, maxLength = options.length; j < maxLength; j++) 
		{ 
			var option = options[j];  
			if(option !== selectedOption) 
			{ 
				option.selected = 'no'; 
				this.updatedOptionStyle(option, false); 
				this.removeSelectedOption(option); 
			} 
		} 
	},  
	
	/* this will select all options */ 
	selectOptions: function() 
	{ 
		var options = this.optionsArray;
		for(var j = 0, maxLength = options.length; j < maxLength; j++) 
		{  
			var option = options[j]; 
			if(option) 
			{ 
				option.selected = 'yes'; 
				this.addSelectedOption(option);
				this.updatedOptionStyle(option, true);  
			} 
		} 
	}, 
	
	updatedOptionStyle: function(option, checked)
	{ 
		var doc = document;
		doc.getElementById(option.optionRowId).className = this.getOptionStyle(option); 
		doc.getElementById(option.checkboxId).checked = checked;
	},       
	
	getSelectedOptions: function() 
	{ 
		return this.selected; 
	} 
}); 