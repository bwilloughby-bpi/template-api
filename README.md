# TEMPLATE-API #

### What is this repository for? ###

* Starting point for writing Dashr projects with Lumen
* V0.1

### How do I get set up? ###

* Unzip template-api.zip, rename resulting folder to your project's name (my-project for example)
    * `move template my-project`
    * `cd my-project`
* Run `composer install` to install dependencies
* Configure local apache instance to run this webapp
    * Edit `C:\Windows\System32\drivers\etc\hosts` (requires admin mode) and add this line:
        ```
        127.0.0.1 local.my-project.com
        ```
    * Notice the instances of 'my-project'
    * Edit `%APACHE_DIR%\conf\extra\httpd-vhosts.conf` and add this block:
        ```
        <VirtualHost *>  
            DocumentRoot "C:/dev/bpi/my-project/public"  
            ServerName local.my-project.com  
            <Directory "C:/dev/bpi/my-project/public">  
                AllowOverride all  
                Options Indexes FollowSymLinks Includes ExecCGI  
                Allow from all  
                Require all granted  
            </Directory>  
        </VirtualHost>
        ```
    * Notice the instances of 'my-project'  
* Create the database and database user
* Copy `.env.example` to `.env` and fill out info
    * `APP_KEY` is for encryption, just generate a random length 32 string (http://localhost/api/key)
    * `DB_DATABASE` is the database you just created
    * `DB_USERNAME` is the database user you just created
    * `DB_PASSWORD` is the database pass you just created
* Customize Backend
    * Update `/database/migrations/*_users.php` with correct fields
    * Update `/App/User` with correct fields
    * Update `/App/Http/Controllers/UserController` with correct fields
    * Check for any remaining TODO comments in PHP files
* Customize Frontend
    * Update `/public/js/TEMPLATE-*.js` files with new project name
        * Rename file `TEMPLATE-background-panel.js` to `my-project-background-panel.js`
        * Rename class from `TEMPLATEBackgroundPanel` to `MyProjectBackgroundPanel`
        * Rename file `TEMPLATE-controller.js` to `my-project-controller.js`
        * Rename class from `TEMPLATEController` to `MyProjectController`
        * Rename file `TEMPLATE-login-panel.js` to `my-project-controller.js`
        * Rename class from `TEMPLATELoginPanel` to `MyProjectLoginPanel`
    * Update references to said js files in `/public/index.php`
        * Includes list
        * Bottom section when it uses the controller
    * Rename `/public/css/TEMPLATE.css` to `my-project.css`
        * Update references to said css file in `/public/index.php`
    * Check for any remaining TODO comments in `/public`
* Run `php artisan migrate --seed` to initialize the database
* Test by navigating to `http://local.my-project.com` in your browser
    * Alternatively check `http://local.my-project.com/api/key` to generate a random 32-character string which can double as your .env APP_KEY
* If everything is set up correctly feel free to remove the `/api/key` endpoint from the `/routes/web.php`
* Create a git repository and you're on your way

### Who do I talk to? ###

* Template:
    * Brandon Willoughby [Email](mailto:brandon.willoughby@businesspromotion.com) [Skype](skype:brandon.willoughby.bpi?chat)
* Dashr
    * Chris Durfee [Email](mailto:chris.durfee@businesspromotion.com) [Skype](skype:chirsdurfeebpi?chat)
