<?php
session_start();
session_write_close();

$user = $_SESSION['user'] ?? false;
$resume = $user && isset($user->resumeLogin) ? $user->resumeLogin : false;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>TEMPLATE &hearts;</title>
    <link type="text/css" rel="stylesheet" href="/lib/dashr/css/dashr.css">
    <!-- TODO: rename css file to project name -->
    <link type="text/css" rel="stylesheet" href="/css/TEMPLATE.css">

    <!-- favicon and touch icons -->
    <link rel="shortcut icon" href="favicon.ico">
    <base href="/">
</head>

<body id="body">
<div id="progress-label" class="progress-label">0%</div>
<div id="progress" class="progress">
    <div class="wipe"></div>
    <div class="wipe"></div>
    <div class="wipe"></div>
</div>

<script>
	'use strict';
</script>

<script src="lib/dashr/scripts/layout/dashr.js"></script>

<script>
	'use strict';
	Dashr.loadModules('lib/');

	var dir = 'js/';
	var loader = Dashr.scriptLoader;

	var add = function(src, defer) {
		defer = defer !== false;
		loader.add({ src: src, defer: defer });
	};

	// TODO: Rename template to project name
	add(dir + 'TEMPLATE-background-panel.js');
	add(dir + 'TEMPLATE-login-panel.js');

	// models
	add(dir + 'models/base-model.js');
	add(dir + 'models/nested-model.js');
	add(dir + 'models/auth-model.js');
	add(dir + 'models/user-model.js');

	// TODO: add panels and controls used in the project here

	// TODO: Rename template to project name
	add(dir + 'TEMPLATE-controller.js');

	var app;

	var progress = document.getElementById('progress');
	var progressLabel = document.getElementById('progress-label');

	loader.progress.callBack = function(percent) {
		progressLabel.textContent = Math.round(percent) + '%';
		progress.style.width = percent + '%';

		if (percent >= 100) {
			document.body.classList.add('loaded');
			progressLabel.classList.add('complete');

			window.setTimeout(function() {
				progress.classList.add('complete');

				var resume = <?php echo $resume === true ? 'true' : 'false'; ?>;

				app = new TEMPLATEController(resume);
				app.setupOp();
			}, 400);
		}
	};
</script>
</body>
</html>
