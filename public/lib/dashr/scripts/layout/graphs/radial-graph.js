"use strict";

/* 
	radialGraph 
	
	this will create an animated radial graph in canvas. 
	
	@param (mixed) width = the width (int) of the graph 
	container or 'auto'
	@param (mixed) height = the height (int) of the graph 
	container or 'auto' 
	@param (array) data = an array of objects 
	@param (string) container = the id of the container to 
	contain the new graph 
	@param [(string)] animation = the animation name (spread, 
	fan, none) 
*/ 
function RadialGraph(width, height, data, container, animation)
{ 
	this.init('RadialGraph');  
	
	/* this will hold the canvas size */ 
	this.canvas = null; 
	this.ctx = null; 
	this.height = height; 
	this.width = width; 
	this.size = { width: 0, height: 0 }; 
	
	/* thiswill store the inital data 
	to start the graph */ 
	this.data = data; 
	
	/* this will hold the graph itemsafter 
	they have been setup */ 
	this.items = []; 
	this.itemNumber = 0; 
	
	/* this will allow the canvas to animate 
	and stop when done */ 
	this.status = 'stopped'; 
	this.timer; 
	this.fps = 60; 
	
	this.animation = (typeof animation !== 'undefined')? animation : 'spread'; 
	
	this.container = container; 
}; 

Graph.extend( 
{ 
	constructor: RadialGraph, 
	
	setup: function()
	{ 
		this.setupAnimationFrame(); 
		this.render(); 
		this.setItems(); 
		this.resize();   
	}, 
	
	/* this will create a new graph canvas */  
	render: function()
	{ 
		var canvas = this.canvas = document.createElement('canvas'); 
		this.ctx = canvas.getContext("2d"); 
		
		canvas.id = this.id; 
		var containerSize = this.getContainerSize(); 
		
		/* we want to setup the width and height */ 
		if(this.width === 'auto')
		{ 
			canvas.width = containerSize.width; 
			
			/* if the graph isset to auto we want to 
			add a resize listener */ 
			this.setupEvents();  
		}
		else
		{ 
			canvas.width = this.width; 
		} 
		
		canvas.height = (this.height === 'auto')? containerSize.height : this.height;  		
		
		document.getElementById(this.container).appendChild(canvas); 
	}, 
	
	//create all items 
	setItems: function()
	{ 
		var data = this.data;
		for(var i = 0, maxLength = data.length; i < maxLength; i++)
		{ 
			this.createObject(tdata[i]); 
		} 
	}, 
	
	//get total of items 
	getTotal: function()
	{ 
		var myTotal = 0;
		var data = this.data;
		for (var j = 0, maxLength = data.length; j < maxLength; j++)
		{
			myTotal += data[j].number;
		}
		return myTotal; 
	}, 
	
	//create item object 
	createObject: function(tmpObj)
	{ 
		var tmpItem = { 
			id: this.itemNumber++, 
			name: tmpObj.label, 
			currentSize: 0, 
			endingSize: (tmpObj.number / this.getTotal()), 
			number: tmpObj.number, 
			color: (tmpObj.color)? tmpObj.color : this.getRandomColor(this.itemNumber), 
			selected: 'no' 
		}; 
		
		this.items.push(tmpItem); 
	}, 
	
	getRandomColor: function(number) 
	{ 
		var colorNumber = Math.round(255 / number); 
		var color = "hsla(" + colorNumber + ", 80%, 60%, 1)"; 
		return color; 
	}, 
	
	update: function(data)
	{ 
		this.reset(); 
		this.data = data; 
		this.setItems(); 
		this.resize();  
	},  
	
	reset: function()
	{ 
		this.itemNumber = 0; 
		this.items = []; 
	}, 
	
	startTimer: function()
	{  
		this.stopTimer(); 
		this.draw();  
		this.status = 'started'; 
	}, 
	
	stopTimer: function()
	{ 
		window.cancelAnimationFrame(this.timer);  
		this.status = 'stopped';
	}, 
	
	setColor: function(number)
	{ 
		var colors = [
			  
			  '#7600FF', //medium colors 
			  '#0013FF', 
			  '#0092FF',  
			  '#00F3FF', 
			  '#5EFF00', 
			  '#FFF100', 
			  '#FF6B06', 
			  '#FF0013', 
			  '#DF00FF', 
			  
			  //dark colors 
			  '#400299', //purples 
			  '#03148C', //blues 
			  '#04619B', //light blue 
			  '#07ADAD', //cyan 
			  '#49B702', //green 
			  '#FFDA00', //yellow 
			  '#C95302', //orange 
			  '#A00224', //red 
			  '#9604AF', //magenta 
			  
			  '#BB8FF9', //light colors 
			  '#8392FC', 
			  '#8AD2FF', 
			  '#9CFFFC', 
			  '#B7FF88', 
			  '#FFF685', 
			  '#FFC9A1', 
			  '#FF647E', 
			  '#F099FF'  
		];
		
		//setup colors  
		if(number <= 9)
		{ 
			var color = number = colors[number];
		}
		else if(number <= 18)
		{ 
			var color = number = colors[number];
		}
		else if(number <= 27)
		{ 
			var color = number = colors[number];
		}
		else if(number <= colors.length)
		{ 
			var color = number = colors[number]; 
		}
		else
		{ 
			var color = Math.random() * colors.length; 
		}
		
		return color; 
	}, 
	
	getGraphItemSize: function(graphItem) 
	{ 
		var pause = true; 
		
		if(this.animation === 'spread')
		{ 
			/*this will evenly distribute the animation to end at the same time*/ 
			if((graphItem.currentSize / this.getTotal()) < graphItem.endingSize)
			{
				//next distance 
				var arcStep = (graphItem.number / this.getTotal()) * (this.fps * 4); 
				//remaining distance 
				var arcRemaining = (graphItem.endingSize - (graphItem.currentSize / this.getTotal())) * this.getTotal(); 
			
				//if the remaining distance is less than the next distance 
				//use the remaining distance as the next distance 
				if(arcStep > arcRemaining)
				{ 
					//the remaining distance is used because the next distance is too large 
					graphItem.currentSize += arcRemaining; 
				}else{ 
					//the remaining distance is greater than the next distance 
					graphItem.currentSize += arcStep; 
				} 
				
				//do not pause 
				pause = false; 
			}
		} 
		else if(this.animation === 'fan')
		{ 
			/*this will evenly distribute the animation to end at the same time*/ 
			if((graphItem.currentSize / this.getTotal()) < graphItem.endingSize)
			{ 
				//next distance 
				var arcStep = (graphItem.number / this.getTotal()) * (this.fps * 4); 
				//remaining distance 
				var arcRemaining = (graphItem.endingSize - (graphItem.currentSize / this.getTotal())) * this.getTotal(); 
			
				//if the remaining distance is less than the next distance 
				//use the remaining distance as the next distance 
				if(arcStep > arcRemaining)
				{ 
					//the remaining distance is used because the next distance is too large 
					graphItem.currentSize += arcRemaining; 
				}else{ 
					//the remaining distance is greater than the next distance 
					graphItem.currentSize += arcStep; 
				} 
				
				//do not pause 
				pause = false; 
			}
		}
		else 
		{ 
			/*this will set the cureent size to the ending size with no animation*/ 
			if(graphItem.currentSize < graphItem.endingSize)
			{  
				graphItem.currentSize = graphItem.endingSize; 
				//do not pause 
				pause = false;
			} 	
		} 
		
		return pause; 
	}, 
	
	selectItemByName: function(name) 
	{ 
		for (var i = 0, maxLength = this.items.length; i < maxLength; i++) 
		{ 
			var graphItem = this.items[i]; 
			if(graphItem.name === name) 
			{ 
				graphItem.selected = (graphItem.selected !== 'yes')? 'yes' : 'no';  
			} 
			else 
			{ 
				graphItem.selected = 'no'; 
			} 
		} 
		
		/* we needto check to start the graph if the 
		status is not started */ 
		if(this.status !== 'started') 
		{ 
			this.draw(); 
		} 
	}, 
	
	/* this will store the math for a full circle in radians */ 
	fullCircle: (Math.PI * 2), 
	
	getGraphItemArcEnd: function(graphItem) 
	{ 
		var arcEnd; 
		
		if(this.animation === 'spread')
		{
			arcEnd = this.fullCircle * (graphItem.endingSize); 
		} 
		else if(this.animation === 'fan')
		{ 
			arcEnd = this.fullCircle * (graphItem.currentSize / this.getTotal()); 
		}
		else 
		{ 
			arcEnd = this.fullCircle * graphItem.currentSize; 	
		} 
		return arcEnd; 
	}, 
	
	draw: function()
	{ 
		var canvas = this.canvas, 
		ctx = this.ctx, 
		
		width = this.size.width, 
		halfWidth = width / 2, 
		
		height = this.size.height, 
		halfHeight = height / 2; 
		
		//reset canvas 
		ctx.clearRect(0, 0, width, height);

		//reset last ending item 
		var lastend = 0;  
		
		//setup the graph to pause when done  
		var pause = true; 
		var graphRenderStyle = 'stroke'; 
		
		/* we want to check if the total is zero because we 
		will just draw a blank gray circle and stop the 
		timer */ 
		if(this.getTotal() == 0) 
		{ 
			ctx.beginPath(); 
			ctx.strokeStyle = 'rgba(0,0,0,.2)';
			ctx.lineWidth = (width * 0.15); 
			
			/* we need to remove the width of the line from the size of the arc */ 
			var arcWidth = ((width - (ctx.lineWidth)) /2); 
			var arcHeight = ((height - (ctx.lineWidth)) /2)
			
			//arc 
			ctx.arc(halfWidth , halfHeight , arcHeight, 0, this.fullCircle, false); 
			
			ctx.stroke(); 
			ctx.closePath(); 
			this.stopTimer();
			
			return 0; 
		}
		
		for (var i = 0, maxLength = this.items.length; i < maxLength; i++) 
		{ 
			var graphItem = this.items[i]; 
			
			//setup color 
			ctx.strokeStyle = graphItem.color; 
			ctx.fillStyle = graphItem.color; 
			
			if(graphRenderStyle === 'fill') 
			{ 
				ctx.beginPath(); 
				//move to center of canvas 
				ctx.moveTo(halfWidth, halfHeight); 
				
				/* the grapg item needs to  be updated by the animation 
				to get the current size and check to pause */ 
				var itemUpdatePause = this.getGraphItemSize(graphItem); 
				if(itemUpdatePause === false) 
				{ 
					pause = false; 
				} 
				
				//arc 
				var arcEnd = lastend + (this.fullCircle * (graphItem.currentSize / this.getTotal())); 
				ctx.arc(halfWidth, halfHeight, halfHeight, lastend, arcEnd, false); 
				
				//move to center of canvas 
				ctx.lineTo(halfWidth, halfHeight); 
				ctx.fill(); 
				ctx.closePath(); 
				
			} 
			else if(graphRenderStyle === 'stroke') 
			{ 
				ctx.beginPath(); 
				
				if(graphItem.selected === 'yes') 
				{ 
					ctx.lineWidth = (width * 0.20); 
				} 
				else 
				{ 
					ctx.lineWidth = (width * 0.15); 
				}
				
				/* the grapg item needs to  be updated by the animation 
				to get the current size and check to pause */ 
				var itemUpdatePause = this.getGraphItemSize(graphItem); 
				if(itemUpdatePause === false) 
				{ 
					pause = false; 
				} 
				
				/* we need to remove the width of the line from the size of the arc */ 
				var arcWidth = ((width - (ctx.lineWidth)) /2); 
				var arcHeight = ((height - (ctx.lineWidth)) /2); 
				
				//arc 
				var arcEnd = lastend + (this.fullCircle * (graphItem.currentSize / this.getTotal())); 
				ctx.arc(halfWidth, halfHeight, arcHeight, lastend, arcEnd,false); 
				ctx.stroke(); 
				ctx.closePath(); 
			}  
			
			//set last arc position 
			lastend += this.getGraphItemArcEnd(graphItem);
		} 
		
		//pause if the rendering is done 
		if(pause)
		{ 
			this.stopTimer(); 
		} 
		else 
		{ 
			this.timer = window.requestAnimationFrame(this.draw.bind(this)); 
		} 
	} 
}); 