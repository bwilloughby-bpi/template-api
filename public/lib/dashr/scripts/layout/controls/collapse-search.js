/*
 *
 */
var CollapseSearch = function(searchFunction, callbackFunction, container) {
	this.init();

	this.searchFunction = searchFunction;
	this.callbackFunction = callbackFunction;
	this.container = container;

	this.timeoutId = null;
};

CollapseSearch.prototype = base.Component.extend({
	constructor: CollapseSearch,

	setup: function() {
		this.createPanel();
	},

	createPanel: function() {
		var layout = {
			class: 'collapse-search',
			input: {
				node: 'input', type: 'text', id: this.id + '_text',
				keyup: base.bind(this, this.onKeyPress),
				onfocus: base.bind(this, this.onFocus),
				onblur: base.bind(this, this.onBlur)
			}
		};

		base.parseLayout(layout, this.container);

		this.textField = document.getElementById(this.id + '_text');
	},

	getSearchText: function() {
		return (typeof this.textField !== 'undefined' ? this.textField.value : '');
	},

	onKeyPress: function(keyCode, e) {
		var searchText = this.textField.value;

		if (typeof this.searchFunction === 'function' && searchText.length > 0) {
			this.searchFunction(searchText);
		}
		else if (typeof this.callbackFunction === 'function' && searchText.length == 0) {
			this.callbackFunction();
		}
	},

	onFocus: function() {
		if (this.timeoutId !== null) {
			clearTimeout(this.timeoutId);
			this.timeoutId = null;
		}
		this.expand();
	},

	onBlur: function() {
		if (this.textField.value === '') {
			setTimeout(base.bind(this, this.finish), 500);
		}
	},

	isSearching: function() {
		return this.textField.value !== '';
	},

	finish: function() {
		this.timeoutId = null;
		this.textField.value = '';
		this.collapse();

		if (typeof this.callbackFunction === 'function') {
			this.callbackFunction();
		}
	},

	expand: function() {
		base.addClass(this.textField, 'expanded');
	},

	collapse: function() {
		base.removeClass(this.textField, 'expanded');
	}
});