"use strict";

/* 
	PagePanel 
	
	this will create a page panel that will allow lists 
	to be paginated and keep track of the current page 
	and allow the next and previous pages to be 
	selected. 
	
	@param (array) options = the page options array 
	@param (int) perPage = thenumber per page 
	@param (function) callBackFn = the call back 
	function to use when the page is changed 
	@param (string) container = the container id 
*/ 
var PagePanel = function(options, perPage, callBackFn, container) 
{ 
	/* this will setup to have mutliple instances of the 
	panel in one project without issue */ 
	this.init('PagePanel');     
	
	this.options = options, 
	this.perPage = perPage;  
	this.pages; 
	
	this.callBackFn = callBackFn; 
	
	this.lastSelectedOption;   
	this.optionsArray = [];   
	
	this.container = container;   
}; 

/* we want to extend the html builder class and the 
class methods to the prototype */
base.Component.extend( 
{ 
	/* we want to reset the constructor */ 
	constructor: PagePanel, 
	
	/* this will create the page panel */ 
	render: function() 
	{ 
		var id = this.id; 
		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'page-panel', '', frag); 
		
		var pageNavContainer = this.addElement('div', id + '_nav_container', 'page-nav-container', '', panel); 
		var previous = this.createListOption('previous', '', pageNavContainer); 
		var next = this.createListOption('next', '', pageNavContainer);
		var container = this.list = this.addElement('div', id + '_list', 'page-container', '', pageNavContainer);  
		this.append(this.container, frag); 
		
		this.createListByPages(); 
	},    
	
	/* this will create the options  array by the page count */ 
	createListByPages: function() 
	{ 
		this.reset(); 
		
		var pages = this.pages; 
		var listContainer = this.list;  
		
		if(pages > 0) 
		{ 			
			var tmpNumber = 1; 
			
			/* create the option array and setup the options */ 
			for(var i = 0; i < pages; i++) 
			{ 
				var tmpOption = { 
					optionNumber: tmpNumber,  
					nameId: this.id + '_number' + '_' + tmpNumber++,  
					selected: 'no', 
					label: (i + 1) 
				}; 
								
				/* check if we have a selected page and that it hasn't been removed */ 
				if(this.lastSelectedOption) 
				{ 
					var lastSelectedNumber = this.lastSelectedOption.optionNumber; 
					/* we need to select the previous selected page if its still available */ 
					if(lastSelectedNumber <= this.pages) 
					{ 
						/* check if this option is the previous selected option */ 
						if(tmpOption.optionNumber === lastSelectedNumber) 
						{ 
							tmpOption.selected = 'yes'; 
							this.setLastSelectedOption(tmpOption);	 
						} 
					} 
					else if(i == 0) 
					{ 
						/* select first page */ 
						tmpOption.selected = 'yes'; 
						this.setLastSelectedOption(tmpOption);   
					} 
				} 
				else 
				{ 
					/* show first pages selected */ 
					if(i == 0) 
					{ 
						tmpOption.selected = 'yes'; 
						this.setLastSelectedOption(tmpOption); 
					} 
				}
				
				// add to optionsArray 
				this.addOption(tmpOption);   
				
				/* add a new option to the list panel */ 
				var option = this.createListOption('number', tmpOption, listContainer);  
			} 
			
			/* update list option styles */ 
			this.updateListOptionStyle(); 
		} 
		else 
		{ 
			var option = this.createListOption('default', '', listContainer);
		}  
	}, 
	
	/* this will set the page count */ 
	setupPageCount: function() 
	{ 
		/* we need to setup the number of pages */ 
		this.pages = Math.ceil(this.options / this.perPage); 
	}, 
	
	/* this will setup the panel */ 
	setup: function() 
	{   
		this.setupPageCount(); 
		this.render();   
	}, 
	
	/* this updates the page panel settings and recreates the panel to the new settings */ 
	updatePageSettings: function(options, perPage) 
	{ 
		this.options = options; 
		this.perPage = perPage; 
		this.setupPageCount(); 
		this.createListByPages(); 
	}, 
	
	/* this will get the start and stop limit numbers */ 
	getLimitNumber: function(mode) 
	{ 
		var numberOfOptions = this.optionsArray.length; 
		var currentPage = this.getCurrentPage(); 
		var number = 0; 
		
		if(mode === 'start') 
		{ 
			/* we need to check if its the first page because we 
			dont need to get more than one */ 
			if(this.isFirstPage()) 
			{ 
				number = 0;  
			} 
			else 
			{
				/* we need to multiply per page number times current 
				page and subtract the per page number mnus 1 to get the start */ 
				number = (this.perPage * currentPage) - (this.perPage); 
			} 
		} 
		else if(mode === 'stop') 
		{ 
			/* the stop is the number of options per page */ 
			number = this.perPage;  
		} 
		
		return number; 
	}, 
			
	/* this will return the current page */ 
	getCurrentPage: function() 
	{ 
		return (this.lastSelectedOption)? this.lastSelectedOption.optionNumber : 1;   
	}, 
	
	/* this will check if the current page is the last page */ 
	isLastPage: function() 
	{ 
		var currentPage = this.getCurrentPage(); 
		return (currentPage === (this.pages))? true : false;  
	}, 
	
	/* this will check if the current page is the first page */ 
	isFirstPage: function() 
	{ 
		var currentPage = this.getCurrentPage(); 
		return (currentPage == '1')? true : false;  
	},
	
	// this will reset the whole page panel 
	reset: function() 
	{ 
		// reset optionsArray and nav  
		this.optionsArray = []; 
		
		this.removeAll(this.list); 
	}, 
	
	resetLastSelection: function() 
	{ 
		this.lastSelectedOption = null; 
	}, 
	
	/* reset html object list */ 
	resetList: function() 
	{ 
		this.removeAll(this.list); 
	},  
	
	/* this will create the options for the page panel list */ 
	createListOption: function(type, tmpOption, container) 
	{ 
		if(type === 'number') 
		{ 
			var self = this; 
			var optionContainer = this.addButton('button', tmpOption.nameId, 'bttn', tmpOption.label, function(){ self.selectOption(tmpOption); }, container); 
		}  
		else if(type === 'previous') 
		{ 
			var optionContainer = this.addButton('button', tmpOption.nameId, 'bttn previous', '', base.bind(this, this.selectPreviousOption), container); 
		} 
		else if(type === 'next') 
		{ 
			var optionContainer = this.addButton('button', tmpOption.nameId, 'bttn next', '', base.bind(this, this.selectNextOption), container); 
		} 
		else if(type === 'blank') 
		{ 
			var optionContainer = this.addElement('div', '', 'option-spacer', '..', container); 
		}
		else 
		{ 
			var optionContainer = this.addButton('button', tmpOption.nameId, 'bttn', '1', '', container); 
		}
	},     		
	
	/* this adds a new option to the list panel */ 
	addOption: function(option) 
	{ 
		this.optionsArray.push(option); 
	}, 
	
	/* this will update the option style */ 
	updateListOptionStyle: function() 
	{ 
		this.createPageNumbers(); 
		
		var doc = document; 
		var options = this.optionsArray;
		for(var i = 0, maxLength = options.length; i < maxLength; i++) 
		{ 
			var tmpOption = options[i]; 
			var element = doc.getElementById(tmpOption.nameId); 
			if(element) 
			{ 
				element.className = this.getOptionStyle(tmpOption);   
			} 
		} 
	}, 
	
	/* this will reduce the number of pages to only show a few pages */ 
	createPageNumbers: function() 
	{ 
		this.resetList();  
		var options = this.optionsArray, 
		length = options.length; 
		if(length > 0) 
		{ 
			var frag = this.createDocFragment();  
			var tmpNumber = 0; 			

			var id = this.id; 
			/* create the option array and setup the options */ 
			for(var i = 0; i < length; i++) 
			{  
				var option = options[i]; 
				/* get a new id because we deleted the old id when 
				we reset the list*/ 
				option.nameId = id + '_number' + '_' + tmpNumber++; 
				
				var optionType = this.getPageOptionType(option); 
				if(optionType === 'number') 
				{ 
					var option = this.createListOption('number', option, frag); 
				} 
				else if(optionType === 'blank') 
				{ 

					var option = this.createListOption('blank', '',frag); 
				}				  
			} 
			this.append(this.list, frag); 
		}  
	},  
	
	getPageOptionType: function(tmpOption) 
	{ 
		var options = this.optionsArray;
		var lastSelectedOption = this.lastSelectedOption;
		/* we want to show pages near start, select, and end */ 
		if(tmpOption.optionNumber <= 2) 
		{ 
			/* theseare the starting pages */ 
			return 'number';  
		} 
		else if(tmpOption.optionNumber >= (options.length - 1) && tmpOption.optionNumber <= (options.length)) 
		{ 
			/* these are the ending pages */ 
			return 'number';  
		} 
		else if(tmpOption.optionNumber >= (lastSelectedOption.optionNumber - 1) && tmpOption.optionNumber <= (lastSelectedOption.optionNumber + 1)) 
		{ 
			/* these are the selection pages */ 
			return 'number';  
		}  
		else if((tmpOption.optionNumber == 3 && options.length > 3 ) || (tmpOption.optionNumber == lastSelectedOption.optionNumber + 2 )) 
		{ 
			/* theseare the dots to show numbers have been hidden */ 
			return 'blank';  
		} 
	}, 
	
	/* select option */ 
	selectOption: function(tmpOption)
	{ 
		var object = document.getElementById(tmpOption.nameId);  

		if(tmpOption.selected === 'no') 
		{  
			tmpOption.selected = 'yes'; 
		} 
		
		this.setLastSelectedOption(tmpOption); 
		this.view(tmpOption); 
		
		if(object) 
		{ 
			object.className = this.getOptionStyle(tmpOption); 
		} 
		
		/* check if the panel allows multiple selections */ 
		if(this.multiselect !== 'yes') 
		{ 
			/* unselectall other options */ 
			this.unselectOption(tmpOption); 
		} 
		
		this.updateListOptionStyle(); 
	},  
		
	/* we can use the get and select next options to 
	move through the list 
	@return (object) the next option to select */
	getNextOption: function() 
	{ 
		/* we want to get the last option and setup the next option 
		number be incremeneting the last option number */ 
		var lastSelectedOption = (this.lastSelectedOption)? this.lastSelectedOption.optionNumber : 0; 
		var nextOptionNumber = ++lastSelectedOption; 
		 
		var options = this.optionsArray;
		/* we need to check if we are at the end of the list */ 
		if(nextOptionNumber <= options.length) 
		{ 
			/* return next option */ 
			return options[--nextOptionNumber]; 
		} 
		else 
		{ 
			return options[0];  
		} 
	},  
	
	/* this will select the next option */ 
	selectNextOption: function() 
	{ 
		var nextOption = this.getNextOption(); 
		if(nextOption) 
		{ 
			this.selectOption(nextOption); 
		} 
	},  
	
	/* we can use the get and select previous options to 
	move in reverse through the list 
	@return (object) the previous option to select */  
	getPreviousOption: function() 
	{ 
		/* we want to get the last option and setup the previous option 
		number be decremeneting the last option number */ 
		var lastSelectedOption = (this.lastSelectedOption)? this.lastSelectedOption.optionNumber : 0; 
		var previousOptionNumber = --lastSelectedOption; 
		
		var options = this.optionsArray;
		/* we need to check if we have reached the begining of the list */ 
		if(previousOptionNumber >= 1) 
		{ 
			/* return next option */
			return options[--previousOptionNumber]; 
		} 
		else 
		{ 
			return options[(options.length - 1)]; 
		} 
	},  
	
	/* this will select the previous option */ 
	selectPreviousOption: function() 
	{ 
		var previousOption = this.getPreviousOption(); 
		if(previousOption) 
		{ 
			this.selectOption(previousOption); 
		} 
	},
	
	/* this will save the last selected option */ 
	setLastSelectedOption: function(option) 
	{ 
		this.lastSelectedOption = option; 
	}, 
	
	/* this will unselect all options except the last selected option */ 
	unselectOption: function(tmpOption) 
	{ 
		var options = this.optionsArray;
		for(var j = 0, maxLength = options.length; j < maxLength; j++) 
		{ 
			var option = options[j]; 
			/* if the option is not the last selected option */ 
			if(option !== tmpOption) 
			{ 
				/* unselect any option that is selected */ 
				if(option.selected === 'yes') 
				{ 
					option.selected = 'no'; 
					var option = document.getElementById(option.nameId); 
					if(option) 
					{ 
						option.className = this.getOptionStyle(option); 
					} 
				} 
			} 
		} 
	},
	
	/* this will get the style of the option */ 
	getOptionStyle: function(tmpOption) 
	{ 
		return (tmpOption.selected === 'yes')? 'bttn selected' : 'bttn';   
	},  
	
	/* this selects the page to view */ 
	view: function(tmpOption) 
	{ 
		if(this.callBackFn) 
		{ 
			this.callBackFn(tmpOption.label); 
		} 
	} 
}); 