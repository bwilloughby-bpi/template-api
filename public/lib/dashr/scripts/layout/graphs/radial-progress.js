"use strict";

/* 
	radialProgress 
	
	this will create an animated radial graph in canvas. 
	
	@param (mixed) width = the width (int) of the graph 
	container or 'auto'
	@param (mixed) height = the height (int) of the graph 
	container or 'auto'  
	@param (string) container = the id of the container to 
	contain the new graph 
*/ 
var RadialProgress = function(width, height, progressColor, textColor, container)
{ 
	this.init('RadialProgress');  
	
	/* this will hold the canvas size */ 
	this.canvas = null; 
	this.ctx = null; 
	this.height = height; 
	this.width = width; 
	this.size = { width: 0, height: 0 }; 
	
	this.progressColor = progressColor; 
	this.textColor = textColor; 
	
	this.progress = null;  
	
	/* this will allow the canvas to animate 
	and stop when done */ 
	this.status = 'stopped'; 
	this.timer; 
	this.fps = 60;  
	
	this.container = container; 
}; 

Graph.extend( 
{ 	
	constructor: RadialProgress,  
	
	setup: function(percent)
	{ 
		this.setupAnimationFrame(); 
		this.render();  
		this.createProgressObject(percent); 
		this.resize();  
	},  
	
	/* this will create a new canvas */  
	render: function()
	{ 
		var canvas = this.canvas = document.createElement('canvas'); 
		canvas.id = this.id;
		
		this.ctx = canvas.getContext("2d");  
		var containerSize = this.getContainerSize(); 
		
		/* we want to setup the width and height */ 
		if(this.width === 'auto')
		{ 
			canvas.width = containerSize.width; 
			
			/* if the canvas is set to auto we want to 
			add a resize listener */ 
			this.setupEvents();  
		}
		else
		{ 
			canvas.width = this.width; 
		} 
		
		canvas.height = (this.height === 'auto')? containerSize.height : this.height;  		
		
		document.getElementById(this.container).appendChild(canvas); 
	},  
	 
	getTotal: function()
	{ 
		return 100;  
	}, 
	
	//create item object 
	createProgressObject: function(percent)
	{ 
		var progress = { 
			id: this.itemNumber++,  
			currentSize: 0, 
			endingSize: (percent / this.getTotal()), 
			number: percent, 
			color: (this.progressColor)? this.progressColor : this.getRandomColor(this.itemNumber), 
			selected: 'no' 
		}; 
		
		this.progress = progress; 
	}, 
	
	getRandomColor: function(number) 
	{ 
		var colorNumber = Math.round(255 / number); 
		var color = "hsla(" + colorNumber + ", 80%, 60%, 1)"; 
		return color; 
	}, 
	
	update: function(percent)
	{  
		//update 
		this.progress.number = percent; 
		var endNUmber = (percent !== 0)? (percent / this.getTotal()) : .000001; 
		this.progress.endingSize = endNUmber; 
		//start drawing graph 
		this.resize();  
	}, 
	
	startTimer: function()
	{ 
		this.stopTimer(); 
		/* we want to start the draw and setup the timer */ 
		this.draw(); 
		this.status = 'started'; 
	}, 
	
	stopTimer: function()
	{ 
		window.cancelAnimationFrame(this.timer); 
		this.status = 'stopped';
	}, 
	
	getArcSize: function() 
	{ 
		var pause = true, 
		progress = this.progress; 
		
		//next distance 
		var arcStep = (progress.endingSize / progress.number) * (this.fps); 
		//remaining distance 
		var arcRemaining = (progress.endingSize - (progress.currentSize / this.getTotal())) * this.getTotal();
		
		if((progress.currentSize / this.getTotal()) < progress.endingSize)
		{
			//if the remaining distance is less than the next distance 
			//use the remaining distance as the next distance 
			if(arcStep > arcRemaining)
			{ 
				//the remaining distance is used because the next distance is too large 
				progress.currentSize += arcRemaining; 
			}
			else
			{ 
				//the remaining distance is greater than the next distance 
				progress.currentSize += arcStep; 
			} 
			
			//do not pause 
			pause = false; 
		} 
		else if((progress.currentSize / this.getTotal()) > progress.endingSize) 
		{ 
			/* we need to the remaining amount to be positive */ 
			var compare = (arcRemaining < 0)? arcRemaining * -1 : arcRemaining; 
			if(arcStep > compare)
			{ 
				//the remaining distance is used because the next distance is too large 
				progress.currentSize -= compare; 
			}
			else
			{ 
				//the remaining distance is greater than the next distance 
				progress.currentSize -= arcStep; 
			}  
			
			//do not pause 
			pause = false; 
		}
		
		return pause; 
	}, 
	
	fullCircle: (Math.PI * 2),   
	
	drawBgArc: function(ctx) 
	{ 
		var canvas = this.canvas, 
		
		size = this.size, 
		width = size.width, 
		height = size.height; 
		
		ctx.beginPath(); 
		ctx.strokeStyle = 'rgba(0, 0, 0, .05)';
		ctx.lineWidth = (width * .095); 
		
		/* we need to remove the width of the line 
		from the size of the arc */ 
		var arcWidth = ((width - (ctx.lineWidth)) /2); 
		if(arcWidth < 0) 
		{ 
			arcWidth = 0; 
		}
		
		//arc 
		ctx.arc(width / 2, height / 2, arcWidth, 0, this.fullCircle, false); 
		
		ctx.stroke(); 
		ctx.closePath(); 
	}, 
	
	drawText: function(ctx) 
	{ 
		var canvas = this.canvas, 
		
		size = this.size, 
		width = size.width, 
		height = size.height; 
		
		ctx.font = "30px 'open_sans_condensed_light'";  		
		ctx.fillStyle = this.textColor;  
		ctx.textAlign = 'center'; 
		ctx.fillText(Math.round(this.progress.number) + '%', width / 2, height / 2 + 10);
		
		ctx.stroke(); 
		ctx.closePath(); 
	},
	
	drawProgressArc: function(ctx) 
	{ 
		var pause = true, 
		progress = this.progress, 
		startAngle = -1.5;   
		
		var size = this.size, 
		width = size.width, 
		height = size.height; 
		
		//setup color 
		ctx.strokeStyle = progress.color;  
		ctx.beginPath(); 
			
		if(progress.selected === 'yes') 
		{ 
			ctx.lineWidth = (width * .20); 
		} 
		else 
		{ 
			ctx.lineWidth = (width * .095); 
		}
		
		/* the progress needs to  be updated by the animation 
		to get the current size and check to pause */ 
		var itemUpdatePause = this.getArcSize(); 
		if(itemUpdatePause === false) 
		{ 
			pause = false; 
		} 
		
		/* we need to remove the width of the line from the size of the arc */ 
		var arcWidth = ((width - (ctx.lineWidth)) /2); 
		if(arcWidth < 0) 
		{ 
			arcWidth = 0; 
		}
		
		//arc 
		ctx.arc(width / 2, height / 2, arcWidth, startAngle, startAngle + (this.fullCircle * (progress.currentSize / this.getTotal())), false); 
		
		ctx.stroke(); 
		ctx.closePath(); 
		
		return pause; 
	},
	
	draw: function()
	{ 
		var ctx = this.ctx; 
		
		var size = this.size, 
		width = size.width, 
		height = size.height;

		//reset canvas 
		ctx.clearRect(0, 0, width, height); 
		
		//setup the canvas to pause when done  
		var pause = true;  
		
		/* we want to draw a bg arc to show the whole 
		arc behind the progress */ 
		this.drawBgArc(ctx);  
		this.drawText(ctx); 
			
		var progressStopped = this.drawProgressArc(ctx); 
		if(progressStopped === false) 
		{ 
			pause = false; 
		}
		
		//pause if the rendering is done 
		if(pause)
		{ 
			this.stopTimer(); 
		} 
		else 
		{ 
			this.timer = window.requestAnimationFrame(this.draw.bind(this)); 
		}
	}
}); 