// JavaScript Document

/* base framework */
(function(global) {
	'use strict';

	/* this will check to stop setup if already setup */
	if (global.base) {
		return false;
	}

	/* this will set all settings of the base framework and
	allow all panels to be extended from the base */
	var Base = function() {
		/* the version number */
		this.version = '1.5.4';
	};

	/* we want to add the intial methods to the
	base framework as prototypes so they can be inherited */
	Base.prototype = {

		/* reset the constructor */
		constructor: Base,

		/* this will augement the base framework
        with new functionality
        @param (object) settings = the new functionality
		@return (object) an instance of the object */
		augment: function(settings) {
			if (settings && typeof settings === 'object') {
				var prototype = this.constructor.prototype;
				for (var property in settings) {
					if (settings.hasOwnProperty(property)) {
						prototype[property] = settings[property];
					}
				}
			}
			return this;
		}
	};

	/* this will create an auto initializing function to return
	the base framework prototype to allow new panels to be
	added
	@return (object) the base prototype */
	Base.prototype.extend = (function() {
		return Base.prototype;
	})();

	/* we need to create a new instance of the base framework,
	save it to the global object. shorthand and long name */
	var base = global._b = global.base = new Base();

	/* we want to add the intial methods to the
	base framework as prototypes so they can be inherited */
	base.augment({

		/* this will return an object with the params
		from the url or false if none found */
		parseQueryString: function() {
			var str = window.location.search,
				objURL = {},
				regExp = /([^?=&]+)(=([^&]*))?/g;

			str.replace(regExp, function(a, b, c, d) {
				/* we want to save the key and the
				value to the objURL */
				objURL[b] = d;
			});

			/* we want to check if the url has any params */
			return (this.isEmpty(objURL) === false) ? objURL : false;
		},

		/* this is an alias for parse query string and will
		 return an object with the paramsfrom the url */
		getParams: function() {
			return this.parseQueryString();
		},

		/* this will check if an object is empty.
		@param (object) obj = the object to check
		@return (bool) true or false if empty */
		isEmpty: function(obj) {
			if (obj && typeof obj === 'object') {
				/* we want to loop through each property and
				check if it belongs to the object directly */
				for (var key in obj) {
					if (obj.hasOwnProperty(key) === true) {
						return false;
					}
				}
			}
			return true;
		},

		/* this will select an html element by id.
		@param (string) id = the id of the element
		@return (mixed) the element or false on error */
		getById: function(id) {
			if (typeof id !== 'undefined') {
				var obj = document.getElementById(id);
				return (obj) ? obj : false;
			}
			return false;
		},

		/* this will select html elements by name and return
		a list.
		@param (string) name = the name of the elements
		@return (mixed) the element array or false on error */
		getByName: function(name) {
			if (typeof name !== 'undefined') {
				var obj = document.getElementsByName(name);
				return (obj) ? this.listToArray(obj) : false;
			}
			return false;
		},

		/* this will select html elements by css selector.
		@param (string) name = the name of the elements
		@param [(int)] single = setup to only select the
		first element
		@return (mixed) the element or array or false */
		getBySelector: function(selector, single) {
			if (typeof selector !== 'undefined') {
				/* we want to check if we are only selecting
				the first element or all elements */
				single = single || false;
				if (single == true) {
					var obj = document.querySelector(selector);
					return (obj) ? obj : false;
				} else {
					var elements = document.querySelectorAll(selector);
					if (elements) {
						/* if there is only one result just return the
						first element in the node list */
						return (elements.length == 1) ? elements[0] : this.listToArray(elements);
					}
				}
			}
			return false;
		},

		/* this will convert a nodelist into an array.
		@param (nodeList) list = the list to convert
		@return (array) */
		listToArray: function(list) {
			return Array.prototype.slice.call(list);
		},

		/* this will either set the css style value
		to an object or return the value of the style property.
		@param object obj = the object to use
		@param (string) property = the css property name
		@param [0ptional](string) value = the value to
		add to the attribute
		@return (mixed) if the value is being set a
		reference to the base object will be return, if the
		value is being accessed the value will be returned */
		css: function(obj, property, value) {
			if (obj && typeof obj === 'object' && typeof property !== 'undefined') {
				/* we want to check if we are getting the
				value or setting the value */
				if (typeof value !== 'undefined') {
					property = this.uncamelCase(property);
					obj.style[property] = value;

					return this;
				} else {
					property = this.uncamelCase(property);
					var css = obj.style[property];
					if (css === '') {
						/* we want to check if we have an inherited
						value */
						css = obj.currentStyle ? obj.currentStyle[property] : window.getComputedStyle(obj, null)[property];
					}
					return css;
				}
			}
		},

		/* this will remove the attribute of an object
		@param object obj = the object to use
		@param (string) property = the attribut property name
		@return (object) a reference to the base object
		to allow chaining */
		removeAttr: function(obj, property) {
			if (obj && typeof obj === 'object') {
				if (obj.removeAttribute !== 'undefined') {
					try {
						obj.removeAttribute(property);
					} catch (e) {

					}
				} else {
					/* we cannot remove the attr through the remove
					attr method so we want to null the value.
					we want to camel caps the propety */
					property = this.camelCase(property);
					obj.property = null;
				}
			}

			return this;
		},

		/* this will either set the attribute and value
		to an object or return the value of an attribute.
		@param object obj = the object to use
		@param (string) property = the attribut property name
		@param [0ptional](string) value = the value to
		add to the attribute
		@return (mixed) if the value is being set a
		reference to the base object will be return, if the
		value is being accessed the value will be returned */
		attr: function(obj, property, value) {
			if (obj && typeof obj === 'object') {
				/* we want to check if we are getting the
				value or setting the value */
				if (typeof value !== 'undefined') {
					/* we want to check to set the value */
					if (obj.setAttribute !== 'undefined') {
						obj.setAttribute(property, value);
					} else {
						obj[property] = value;
					}

					return this;
				} else {
					return obj.getAttribute(property);
				}
			}
		},

		/* this will either set the data attribute and value
		to an object or return the data value.
		@param object obj = the object to use
		@param (string) property = the attribut property name
		@param [0ptional](string) value = the value to
		add to the attribute
		@return (mixed) if the value is being set a
		reference to the base object will be return, if the
		value is being accessed the value will be returned
		*/
		data: function(obj, property, value) {
			/* this will check if the property is not
			prefixed with data for loder browsers */
			var checkPrefix = function(prop) {
				if (typeof prop !== 'undefined') {
					/* we want to de camelcase if set */
					prop = Base.prototype.uncamelCase(prop);
					if (prop.substring(0, 5) !== 'data-') {
						prop = 'data-' + prop;
					}
				}
				return prop;
			};

			/* this will return the property without the data prefix */
			var removePrefix = function(prop) {
				if (typeof prop !== 'undefined') {
					if (prop.substring(0, 5) === 'data-') {
						prop = prop.substring(5);
					}
				}
				return prop;
			};

			if (obj && typeof obj === 'object') {
				if (typeof value !== 'undefined') {
					if (typeof obj.dataset !== 'undefined') {
						/* we want to check to remove the data prefix and
						camel caps the title */
						property = removePrefix(property);
						property = this.camelCase(property);

						obj.dataset[property] = value;
					} else {
						/* we need to check the prop prefix */
						property = checkPrefix(property);
						obj.setAttribute(property, value);
					}

					return this;
				} else {
					/* we need to check the prop prefix */
					property = checkPrefix(property);
					return obj.getAttribute(property);
				}
			}
		},

		/* this will find child elements in an element.
		@param object obj = the object to use
		@param string queryString = the query string
		@return (mixed) a nodeList or false on error */
		find: function(obj, queryString) {
			var result = false;

			if (obj && typeof obj === 'object' && typeof queryString === 'string') {
				try {
					result = obj.querySelectorAll(queryString);
				} catch (e) {

				}
			}
			return result;
		},

		/* this will display an element.
		@param object obj = the object to use
		@return (object) a referenceto the base object
		to allow method chaining */
		show: function(obj) {
			if (obj && typeof obj === 'object') {
				/* we want to get the previous display style
				from the data-style-display attr */
				var previous = this.data(obj, 'style-display'),
					value = (typeof previous === 'string') ? previous : '';

				this.css(obj, 'display', value);
			}

			return this;
		},

		/* this will hide an element.
		@param object obj = the object to use
		@return (object) a reference to the base object
		to allow chaining */
		hide: function(obj) {
			if (obj && typeof obj === 'object') {
				/* we want to set the previous display style
				on the element as a data attr */
				var previous = this.css(obj, 'display');
				if (previous !== 'none' && previous != '') {
					this.data(obj, 'style-display', previous);
				}

				this.css(obj, 'display', 'none');
			}

			return this;
		},

		/* this will display or hide an element.
		@param object obj = the object to use
		@return (object) a reference to the base object
		to allow chaining */
		toggle: function(obj) {
			if (obj && typeof obj === 'object') {
				var mode = this.css(obj, 'display');
				if (mode !== 'none') {
					this.hide(obj);
				} else {
					this.show(obj);
				}
			}

			return this;
		},

		/* this will camelcase a string that is separated
		with a space, hyphen, or underscore and retun.
		@param (string) str = the string to camelcase */
		camelCase: function(str) {
			if (typeof str === 'string') {
				var regExp = /(-|\s|\_)+\w{1}/g;
				return str.replace(regExp, function(match) {
					return match[1].toUpperCase();
				});
			}
			return false;
		},

		/* this will uncamelcase a string and separate with a delimter
		and retun.
		@param (string) str = the string to camelcase
		@param [(string)] delimiter = the string delimiter */
		uncamelCase: function(str, delimiter) {
			if (typeof str === 'string') {
				delimiter = delimiter || '-';

				var regExp = /([A-Z]{1,})/g;
				return str.replace(regExp, function(match) {
					return delimiter + match.toLowerCase();
				}).toLowerCase();
			}
			return false;
		},

		/* this will return an object with the width and height
		of an object.
		@param object obj = the object to get the
		width and height
		@return (mixed) an object with the width and size or
		false on error */
		getSize: function(obj) {
			var size = {
				width: 0,
				height: 0
			};

			/* we want to check if the object is not supplied */
			if (obj && typeof obj === 'object') {
				size.width = this.getWidth(obj);
				size.height = this.getHeight(obj);

				return size;
			} else {
				return false;
			}
		},

		/* this will return the width of an object.
		@param object obj = the object to get the
		width
		@return (mixed) the width int or false on error */
		getWidth: function(obj) {
			/* we want to check if the object is not supplied */
			if (obj && typeof obj === 'object') {
				return obj.offsetWidth;
			} else {
				return false;
			}
		},

		/* this will return the height of an object.
		@param object obj = the object to get the
		height
		@return (mixed) the height int or false on error */
		getHeight: function(obj) {
			/* we want to check if the object is not supplied */
			if (obj && typeof obj === 'object') {
				return obj.offsetHeight;
			} else {
				return false;
			}
		},

		/* this will get the scroll position of an object and
		return an object with top and left scroll position or
		false on error.
		@param (optional) object obj = the object to get the
		scroll position, if blank it will return the document
		scroll position
		@return (mixed) an object with the left and top position
		 or false on error */
		getScrollPosition: function(obj) {
			var position = {
				left: 0,
				top: 0
			};

			/* we wantto check if the object is not supplied */
			if (typeof obj === 'undefined') {
				/* we want to use the document body */
				var doc = document.documentElement;
				position.left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
				position.top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

				return position;
			} else {
				/* we want to use the object */
				if (obj && typeof obj === 'object') {
					position.left = (obj.scrollLeft) - (obj.clientLeft || 0);
					position.top = (obj.scrollTop) - (obj.clientTop || 0);

					return position;
				} else {
					return false;
				}
			}
		},

		/* this will return the scroll top position
		of an object or false if not found.
		@param (optional) object obj = the object to get the
		scroll position, if blank it will return the document
		scroll position
		@return (mixed) the top position int or false on error */
		getScrollTop: function(obj) {
			var position = this.getScrollPosition(obj);
			return (position !== false) ? position.top : false;
		},

		/* this will return the left scroll position
		of an object or false if not found.
		@param (optional) object obj = the object to get the
		scroll position, if blank it will return the document
		scroll position
		*/
		getScrollLeft: function(obj) {
			var position = this.getScrollPosition(obj);
			return (position !== false) ? position.left : false;
		},

		/* this will get the window size and return
		an object with the height and width */
		getWindowSize: function() {
			var w = window,
				d = document,
				e = d.documentElement,
				g = d.getElementsByTagName('body')[0],
				x = w.innerWidth || e.clientWidth || g.clientWidth,
				y = w.innerHeight || e.clientHeight || g.clientHeight;

			return {
				width: x,
				height: y
			};
		},

		/* this will return the window height */
		getWindowHeight: function() {
			var size = this.getWindowSize();
			return size.height;
		},

		/* this will return the window width */
		getWindowWidth: function() {
			var size = this.getWindowSize();
			return size.width;
		},

		/* this will return the document size */
		getDocumentSize: function() {
			var size = {
				width: 0,
				height: 0
			};

			var doc = document,
				body = doc.body,
				html = doc.documentElement;

			size.height = Math.max(body.scrollHeight, body.offsetHeight,
				html.clientHeight, html.scrollHeight, html.offsetHeight);

			size.width = Math.max(body.scrollWidth, body.offsetWidth,
				html.clientWidth, html.scrollWidth, html.offsetWidth);

			return size;
		},

		/* this will return the document height */
		getDocumentHeight: function() {
			var size = this.getDocumentSize();
			return size.height;
		},

		/* this class will add, remove and track active active
		event listeners */
		events: {

			/* this will store all active events */
			activeEvents: [],

			/* this will  create an object to use through the event
			tracker.
			@param string event = the name of the event to listen
			@param object obj = obj to add the listener
			@param function fn = function to perform on event
			@param bool capture = event capture
			@param bool swapped = tells if the even return was
			swapped with a standardized function.
			@param function originalFn = the original function
			return function was swapped.
			*/
			createEventObj: function(event, obj, fn, capture, swapped, originalFn) {
				/* we want to check if the swapped param was set */
				swapped = (typeof swapped !== 'undefined') ? swapped : false;

				var activeEvent = {
					event: event,
					obj: obj,
					fn: fn,
					capture: capture,
					swapped: swapped,
					originalFn: originalFn
				};
				return activeEvent;
			},

			/* this will and an event listener and add
			to the event tracker.
			@param string event = the name of the event to listen
			@param object obj = obj to add the listener
			@param function fn = function to perform on event
			@param bool capture = event capture
			@param bool swapped = tells if the even return was
			swapped with a standardized function.
			@param function originalFn = the original function
			return function was swapped.
			*/
			add: function(event, obj, fn, capture, swapped, data) {
				capture = (typeof capture !== 'undefined') ? capture : false;
				var activeEvent = this.createEventObj(event, obj, fn, capture, swapped, data);

				this.activeEvents.push(activeEvent);

				if (obj && typeof obj === 'object') {
					if (typeof obj.addEventListener === 'function') {
						try {
							// modern browsers
							obj.addEventListener(event, fn, capture);
						} catch (error) {
							return error.message;
						}
					} else {
						try {
							// older versions of IE
							obj.attachEvent('on' + event, fn);
						} catch (error) {
							return error.message;
						}
					}
				}
			},

			/* this remove the event and remove from the tracker.
			@param string event = the name of the event to listen
			@param object obj = obj to add the listener
			@param function fn = function to perform on event
			@param bool capture = event capture
			*/
			remove: function(event, obj, fn, capture) {
				capture = (typeof capture !== 'undefined') ? capture : false;

				/* we want to select the event from the active events array */
				var searchEvent = this.getActiveEvent(event, obj, fn, capture);
				if (searchEvent !== false) {
					var listener = searchEvent;
					if (typeof listener === 'object') {
						/* we want to use the remove event method and just
						pass the listener object */
						this.removeEvent(listener);
					}
				}
			},

			/* this will remove an event object and the listener
			from the active events array.
			@param (object) listener = the listener object from
			the active events array */
			removeEvent: function(listener) {
				if (typeof listener === 'object') {
					var object = listener.obj;
					if (typeof object.removeEventListener === 'function') {
						// modern browsers
						object.removeEventListener(listener.event, listener.fn, listener.capture);
					} else {
						// older versions of IE
						object.detachEvent('on' + listener.event, listener.fn);
					}
				}

				/* we want to remove the event from the active events array */
				//var index = this.activeEvents.indexOf(searchEvent);
				var index = Base.prototype.inArray(this.activeEvents, listener);
				if (index >= 0) {
					this.activeEvents.splice(index, 1);
				}
			},

			/* this will search for an return an active event or return
			false if nothing found.
			@param string event = the name of the event to listen
			@param object obj = obj to add the listener
			@param function fn = function to perform on event
			@param bool capture = event capture
			@return (mixed) the event object or false if not found */
			getActiveEvent: function(event, obj, fn, capture) {
				/* we want to create an object to search by */
				var listener = this.createEventObj(event, obj, fn, capture);
				var searchEvent = this.searchEvent(listener);

				/* if the search returns anything but false we
				found our active event */
				return (searchEvent !== false) ? searchEvent : false;
			},

			/* this will search for an return an active event or return
			false if nothing found.
			@param object eventObj = the listener object to search
			*/
			searchEvent: function(eventObj) {
				if (typeof eventObj === 'object') {
					/* we want to get the event listener from
					the active event array */
					var searchEvent = this.searchAllEvents(eventObj);

					/* if the search returns anything but false we
					found our active event */
					if (searchEvent !== false) {
						return searchEvent;
					} else {
						/* we want to check if the event was swapped for
						a standardized return function */
						var searchSwappedEvent = this.searchAllEvents(eventObj, true);

						/* if the search returns anything but false we
						found our active event */
						if (searchSwappedEvent !== false) {
							return searchSwappedEvent;
						}
					}
				}

				return false;
			},

			/* this will search for an return an active event or return
			false if nothing found.
			@param object eventObj = the listener object to search
			@param bool swappedSearch = will enable checking of swapped events
			*/
			searchAllEvents: function(eventObj, swappedSearch) {
				if (typeof eventObj === 'object') {
					/* check if the swapped search was setup */
					swappedSearch = (typeof swappedSearch !== 'undefined') ? swappedSearch : false;

					/* we want to check if the search is a swapped search to
					verfiy the event type is swappable */
					if (swappedSearch == true) {
						var swappable = this.isSwappedEventType(eventObj.event);
						if (swappable == false) {
							/* if the event type is not swappable we need to return
							without checking */
							return false;
						}
					}

					var events = this.activeEvents;
					for (var i = 0, maxLength = events.length; i < maxLength; i++) {
						var listener = events[i];

						/* we want to check if we are search by swapped
						events or normal events */
						if (swappedSearch == false) {
							/* we want to check the listener event with the
							 event object */
							if (listener.event === eventObj.event && listener.obj === eventObj.obj && listener.fn === eventObj.fn) {
								return listener;
							}
						} else {
							if (listener.swapped == true) {
								/* we want to check the listener event using
								its swapped settings with the event object */
								if (listener.event === eventObj.event && listener.obj === eventObj.obj && listener.originalFn === eventObj.fn) {
									return listener;
								}
							}
						}
					}
				}

				return false;
			},

			/* this will remove all events added to an element through
			the base framework.
			@param (object) obj = the element object */
			removeElementEvents: function(obj) {
				if (obj && typeof obj === 'object') {
					var events = this.activeEvents,
						listener, i, length,
						removeEvents = [];

					length = events.length;
					for (i = 0; i < length; i++) {
						listener = events[i];
						if (listener && listener.obj === obj) {
							removeEvents.push(listener);
						}
					}

					length = removeEvents.length;
					for (i = 0; i < length; i++) {
						listener = removeEvents[i];
						if (listener) {
							this.removeEvent(listener);
						}
					}
				}
			},

			/* this will check if an event type could be swapped.
			@param string event = the type of event
			 */
			isSwappedEventType: function(event) {
				/* these event types can have standarized return
				functions*/
				var swappedEvents = [
					'keydown',
					'keyup',
					'keypress',
					'DOMMouseScroll',
					'mousewheel',
					'mousemove',
					'popstate'
				];

				/* we want to check if the event type is in the
				swapped event array */
				var index = Base.prototype.inArray(swappedEvents, event);
				if (index >= 0) {
					/* we have found an event that may have been swapped
					by our standardized return functions */
					return true;
				}

				return false;
			}
		},

		/* this will add an event listener.
		@param string event = the name of the event to listen
		@param object obj = obj to add the listener
		@param function fn = function to perform on event
		@param bool capture = event capture
		@return (object) a reference to the base object */
		addListener: function(event, obj, fn, capture) {
			/* we want to add this to the active events */
			this.events.add(event, obj, fn, capture);

			return this;
		},

		/* this will add an event listener.
		@param mixed event = the name of the event to listen
		@param object obj = obj to add the listener
		@param function fn = function to perform on event
		@param bool capture = event capture
		@return (object) a reference to the base object */
		on: function(event, obj, fn, capture) {
			var events = this.events;
			if (this.isArray(event) === true) {
				for (var i = 0, length = event.length; i < length; i++) {
					var evt = event[i];
					events.add(evt, obj, fn, capture);
				}
			} else {
				events.add(event, obj, fn, capture);
			}
			return this;
		},

		/* this will remove an event listener.
		@param mixed event = the name of the event to listen
		@param object obj = obj to remove the listener
		@param function fn = function to perform on event
		@param bool capture = event capture
		@return (object) a reference to the base object */
		off: function(event, obj, fn, capture) {
			var events = this.events;
			if (this.isArray(event) === true) {
				for (var i = 0, length = event.length; i < length; i++) {
					var evt = event[i];
					events.remove(evt, obj, fn, capture);
				}
			} else {
				events.remove(event, obj, fn, capture);
			}
			return this;
		},

		/* this will remove an event listener.
		@param string event = the name of the event to listen
		@param object obj = obj to add the listener
		@param function fn = function to perform on event
		@param bool capture = event capture
		@return (object) a reference to the base object */
		removeListener: function(event, obj, fn, capture) {
			/* we want to remove this from the active events */
			this.events.remove(event, obj, fn, capture);

			return this;
		},

		/* this will create an event object that can be used
		to dispatch events. this supports html mouse and cu-
		stom events.
		@param string event = the name of the event
		@param object obj = obj to trigger the listener
		@param object params = event params object
		@param object options = event settings options
		@return (object | false) the event or false */
		createEvent: function(event, obj, params, options) {
			if (obj && typeof obj === 'object') {
				var settings = {
					pointerX: 0,
					pointerY: 0,
					button: 0,
					view: window,
					detail: 1,
					screenX: 0,
					screenY: 0,
					clientX: 0,
					clientY: 0,
					ctrlKey: false,
					altKey: false,
					shiftKey: false,
					metaKey: false,
					bubbles: true,
					cancelable: true,
					relatedTarget: null
				};

				if (options && typeof options === 'object') {
					settings = base.extendObject(settings, options);
				}
				var eventTypes = {
					'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
					'MouseEvents': /^(?:click|dblclick|mouse(?:down|up|over|move|out))$/
				};

				var eventType = 'CustomEvent';
				for (var prop in eventTypes) {
					if (eventTypes.hasOwnProperty(prop) === true) {
						var value = eventTypes[prop];
						if (event.match(value)) {
							eventType = prop;
							break;
						}
					}
				}

				var e;
				if (('CustomEvent' in window) === true) {
					if (eventType === 'HTMLEvents') {
						e = new Event(event);
					} else if (eventType === 'MouseEvents') {
						e = new MouseEvent(event, settings);
					} else {
						e = new CustomEvent(event, params);
					}
				} else if (('createEventObject' in document) === true) {
					e = document.createEventObject();
					e.eventType = event;
				} else {
					e = document.createEvent(eventType);
					if (eventType === 'HTMLEvents') {
						obj.initEvent(event, settings.bubbles, settings.cancelable);
					} else if (eventType === 'MouseEvents') {
						e.initMouseEvent(
							event,
							settings.canBubble,
							settings.cancelable,
							settings.view,
							settings.detail,
							settings.screenX,
							settings.screenY,
							settings.clientX,
							settings.clientY,
							settings.ctrlKey,
							settings.altKey,
							settings.shiftKey,
							settings.metaKey,
							settings.button,
							settings.relatedTarget
						);
					} else if (eventType === 'CustomEvent') {
						e.initCustomEvent(event, settings.bubbles, settings.cancelable, params);
					}
				}
				return e;
			}
			return false;
		},

		/* this will trigger an event.
		@param string event = the name of the event
		@param object obj = obj to trigger the listener
		@param object params = event params object
		@return (object) a reference to the base object */
		trigger: function(event, obj, params) {
			if (obj && typeof obj === 'object') {
				/* this will create our event object that
				will be used to dispatch */
				var e = this.createEvent(event, obj, params);

				if (('createEvent' in document) === true) {
					obj.dispatchEvent(e);
				} else {
					obj.fireEvent('on' + event, e);
				}
			}
			return this;
		},

		onLoad: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('load', obj, callBackFn, capture);

			return this;
		},

		onClick: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('click', obj, callBackFn, capture);

			return this;
		},

		onDoubleClick: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('dblclick', obj, callBackFn, capture);

			return this;
		},

		onMouseOver: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('mouseover', obj, callBackFn, capture);

			return this;
		},

		onMouseOut: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('mouseout', obj, callBackFn, capture);

			return this;
		},

		onMouseDown: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('mousedown', obj, callBackFn, capture);

			return this;
		},

		onMouseUp: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('mouseup', obj, callBackFn, capture);

			return this;
		},

		onMouseMove: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			this.events.add('mousemove', obj, callBackFn, capture);

			return this;
		},

		/* this will enable the mouse tracking to be enabled
		and disabled and the position can be set to page,
		client, or screen */
		mouse: {
			/* this will store the mouse position */
			position: { x: null, y: null },

			/* the mouse can be tracked by different modes
			and this will control the tracking position mode */
			mode: 'page',

			/* this will update the mouse position mode.
			@param (string) mode = the position mode */
			updatePositionMode: function(mode) {
				switch (mode) {
					case 'client':
						this.mode = mode;
						break;
					case 'screen':
						this.mode = mode;
						break;
					default:
						this.mode = 'page';
				}
			},

			/* this will add onmousemove event listener and return
			standardized return of position then event.
			@param [(function)] callBackFn = the callback function
			@param [(object)] obj = the object to bind the listener
			@param [(bool)] capture = event capture mode */
			enableTracking: function(callBackFn, obj, capture) {
				if (typeof obj === 'undefined') {
					obj = window;
				}

				var basePrototype = Base.prototype;
				/* we want to update mouse position and
				standardize the return */
				var mouseResults = function(e) {
					// cross-browser result
					e = e || window.event;

					var position = this.position = basePrototype.getMousePosition(e, basePrototype.mouse.mode);

					/* we can now send the mouse wheel results to
					the call back function */
					if (typeof callBackFn === 'function') {
						callBackFn.call(null, position, e);
					}
				};

				basePrototype.events.add('mousemove', obj, mouseResults, capture, true, callBackFn);
			},

			/* this will remove onmousemove event listener.
			@param [(function)] callBackFn = the callback function
			@param [(object)] obj = the object to bind the listener
			@param [(bool)] capture = event capture mode */
			removeTracking: function(callBackFn, obj, capture) {
				if (typeof obj === 'undefined') {
					obj = window;
				}

				Base.prototype.removeListener('mousemove', obj, callBackFn, capture);
			}
		},

		/* this will get the current position of the
		mouse position. You must first be tracking the
		mouse to get the position.
		@param (event) e = the event response
		@param [(string)] = the request mode can be set
		to client */
		getMousePosition: function(e, requestMode) {
			var position = { x: 0, y: 0 };

			e = e || window.event;
			if (e) {
				/* we need to check if the mode is set to
				client or return page position */
				switch (requestMode) {
					case 'client':
						position.x = e.clientX || e.pageX;
						position.y = e.clientY || e.pageY;
						break;
					case 'screen':
						position.x = e.clientX || e.pageX;
						position.y = e.clientY || e.pageY;
						break;
					default:
						position.x = e.pageX;
						position.y = e.pageY;
				}
			}

			return position;
		},

		onMouseWheel: function(callBackFn, obj, cancelDefault, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			var self = this;

			/* we want to return the mousewheel data
			to this private callback function before
			returning to the call back function*/
			var mouseWeelResults = function(e) {
				// cross-browser wheel delta
				e = window.event || e;
				var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));

				/* we can now send the mouse wheel results to
				the call back function */
				if (typeof callBackFn === 'function') {
					callBackFn(delta, e);
				}

				/* we want to check to cancel default */
				if (typeof cancelDefault !== 'undefined' && cancelDefault == true) {
					self.preventDefault(e);
				}
			};

			/* we need to check to add firefox support */
			if (typeof obj.addEventListener === 'function') {
				this.events.add('DOMMouseScroll', obj, mouseWeelResults, capture, true, callBackFn);
			}

			this.events.add('mousewheel', obj, mouseWeelResults, capture, true, callBackFn);

			return this;
		},

		/* this will prevent the default action
		of an event */
		preventDefault: function(e) {
			e = e || window.event;
			if (typeof e.preventDefault !== 'undefined') {
				e.preventDefault();
			} else {
				e.returnValue = false;
			}

			return this;
		},

		/* this will cancel the propagation of an event. */
		stopPropagation: function(e) {
			e = e || window.event;
			if (typeof e.stopPropagation === 'function') {
				e.stopPropagation();
			} else {
				e.cancelBubble = true;
			}

			return this;
		},

		/* this will add onKeyUp event listener and return
		standardized return of keycode then event.
		@param (function) callBackFn = the callback function
		@param [(object)] obj = the object to bind the listener
		@param [(bool)] capture = event capture mode */
		onKeyUp: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			/* we want to standardize the return */
			var keyCodeResults = function(e) {
				e = e || window.event;

				/* we can now send the mouse wheel results to
				the call back function */
				if (typeof callBackFn === 'function') {
					callBackFn(e.keyCode, e);
				}
			};

			this.events.add('keyup', obj, keyCodeResults, capture, true, callBackFn);

			return this;
		},

		/* this will add onKeyDown event listener and return
		standardized return of keycode then event.
		@param (function) callBackFn = the callback function
		@param [(object)] obj = the object to bind the listener
		@param [(bool)] capture = event capture mode */
		onKeyDown: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			var keyCodeResults = function(e) {
				e = e || window.event;

				/* we can now send the mouse wheel results to
				the call back function */
				if (typeof callBackFn === 'function') {
					callBackFn(e.keyCode, e);
				}
			};

			this.events.add('keydown', obj, keyCodeResults, capture, true, callBackFn);

			return this;
		},

		/* this will add onKeyPress event listener and return
		standardized return of keycode then event.
		@param (function) callBackFn = the callback function
		@param [(object)] obj = the object to bind the listener
		@param [(bool)] capture = event capture mode */
		onKeyPress: function(callBackFn, obj, capture) {
			if (typeof obj === 'undefined') {
				obj = window;
			}

			var keyCodeResults = function(e) {
				e = e || window.event;

				/* we can now send the mouse wheel results to
				the call back function */
				if (typeof callBackFn === 'function') {
					callBackFn(e.keyCode, e);
				}
			};

			this.events.add('keypress', obj, keyCodeResults, capture, true, callBackFn);

			return this;
		},

		/* this is an alias for getProperty. this will check to return
		the property value from an object or return a default value or
		return empty string.
		@param (object) obj = the object to check for the value
		@param (string) property = the property name to check return
		@param [(string)] defaultText = if no property value is found
		it will return this */
		getText: function(obj, property, defaultText) {
			return this.getProperty(obj, property, defaultText);
		},

		/* this is an alias for getProperty. this will check to return
		the property value from an object or return a default value or
		return empty string.
		@param (object) obj = the object to check for the value
		@param (string) property = the property name to check return
		@param [(string)] defaultText = if no property value is found
		it will return this */
		getValue: function(obj, property, defaultText) {
			return this.getProperty(obj, property, defaultText);
		},

		/* this will check to return the property value from an
		object or return a default value or return empty
		string.
		@param (object) obj = the object to check for the value
		@param (string) property = the property name to check return
		@param [(string)] defaultText = if no property value is found
		it will return this */
		getProperty: function(obj, property, defaultText) {
			if (obj && typeof obj === 'object') {
				if (obj.hasOwnProperty(property)) {
					return obj[property];
				}
			}

			if (typeof defaultText !== 'undefined') {
				return defaultText;
			} else {
				return '';
			}
		},

		/* this will get the offset position of an object.
		@params object obj = the object that is requesting the offset
		*/
		getPosition: function(obj) {
			var position = { x: 0, y: 0 };

			if (obj && typeof obj === 'object') {
				while (obj) {
					position.x += (obj.offsetLeft + obj.clientLeft);
					position.y += (obj.offsetTop + obj.clientTop);
					obj = obj.offsetParent;
				}
			}
			return position;
		},

		/* this will get the offset position of an objectin a parent.
		@params object obj = the object that is requesting the offset
		*/
		position: function(obj) {
			var position = { x: 0, y: 0 };

			if (obj && typeof obj === 'object') {
				position.x += (obj.offsetLeft - obj.scrollLeft + obj.clientLeft);
				position.y += (obj.offsetTop - obj.scrollTop + obj.clientTop);
			}
			return position;
		},

		addClass: function(obj, tmpClassName) {
			if (obj && typeof obj === 'object' && typeof tmpClassName !== 'undefined') {
				if (typeof obj.classList !== 'undefined') {
					try {
						obj.classList.add(tmpClassName);
					} catch (e) {
						console.log(e.message);
					}
				} else {
					obj.className = obj.className + ' ' + tmpClassName;
				}
			}

			return this;
		},

		removeClass: function(obj, tmpClassName) {
			if (obj && typeof obj === 'object' && typeof tmpClassName !== 'undefined') {
				/* if no className was specified we will remove all classes from object */
				if (typeof tmpClassName === 'undefined') {
					obj.className = '';
				} else {
					if (typeof obj.classList !== 'undefined') {
						try {
							obj.classList.remove(tmpClassName);
						} catch (e) {
							console.log(e.message);
						}
					} else {
						/* we want to get the object classes in an array */
						var classNames = obj.className.split(' ');

						for (var i = 0, maxLength = classNames.length; i < maxLength; i++) {
							/* if the class matches the tmpClassName we want to remove it */
							if (classNames[i] === tmpClassName) {
								classNames.splice(i, 1);
							}
						}

						obj.className = classNames.join(' ');
					}
				}
			}

			return this;
		},

		removeClasses: function(obj, classNameString) {
			if (obj && typeof obj === 'object' && classNameString != '') {
				if (classNameString) {
					var removing = classNameString.split(' ');
					for (var i = 0, maxLength = removing.length; i < maxLength; i++) {
						this.removeClass(obj, removing[i]);
					}
				}
			}

			return this;
		},

		hasClass: function(obj, tmpClassName) {
			var check = false;
			if (obj && typeof obj !== 'undefined' && tmpClassName) {
				if (typeof obj.classList !== 'undefined') {
					try {
						check = obj.classList.contains(tmpClassName);
					} catch (e) {
						console.log(e.message);
						check = false;
					}
				} else {
					/* we want to get the object classes in an array */
					var classNames = obj.className.split(' ');

					for (var i = 0, maxLength = classNames.length; i < maxLength; i++) {
						/* if the class matches the tmpClassName we want to remove it */
						if (classNames[i] === tmpClassName) {
							check = true;
							break;
						}
					}
				}
			}

			return check;
		},

		/* this will toggle a class on an element. if the element
		has the class it will be removed if not added.
		@param (object) obj = the element object
		@param (string) tmpClassName = the class name
		@return (object) the base object to allow chaining */
		toggleClass: function(obj, tmpClassName) {
			if (typeof obj === 'object') {
				var hasClass = this.hasClass(obj, tmpClassName);
				if (hasClass === true) {
					this.removeClass(obj, tmpClassName);
				} else {
					this.addClass(obj, tmpClassName);
				}
			}

			return this;
		},

		/* this will check if an object is an array.
		@param (mixed) array = the array to check
		@return (bool) true or false if array is an array */
		isArray: function(array) {
			if (typeof Array.isArray === 'function') {
				// modern browsers
				return Array.isArray(array);
			} else {
				if (array instanceof Array) {
					return true;
				}
				return false;
			}
		},

		/* this will search an array and return the index number of an array
		or a -1 if the element is not found.
		@param (array) array = the array to search
		@param (mixed) element = the element to search in the array
		@param [(int)] fromIndex = the index number to start searching from
		@return (int) the index position or -1 if not found */
		inArray: function(array, element, fromIndex) {
			if (array && typeof array === 'object') {
				fromIndex = fromIndex || 0;
				if (Array.prototype.indexOf) {
					try {
						return array.indexOf(element, fromIndex);
					} catch (e) {
						console.log(array);
					}
				} else {
					var length = (array.length),
						start = (!isNaN(fromIndex)) ? fromIndex : 0;

					for (var i = start; i < length; i++) {
						if (element === array[i]) {
							return i;
						}
					}
				}
			}

			return -1;
		},

		/* this will extend an object and return the extedned
		object or false.
		@param (object) sourceObj = the original object
		@param (object) targetObj = the target object */
		extendObject: function(sourceObj, targetObj) {
			if (typeof sourceObj !== 'undefined' && typeof targetObj !== 'undefined') {
				for (var property in sourceObj) {
					if (sourceObj.hasOwnProperty(property) === true && typeof targetObj[property] === 'undefined') {
						targetObj[property] = sourceObj[property];
					}
				}

				return targetObj;
			}
			return false;
		},

		/* this will compare two values or objects
		and return true or false if they match.
		@param (mixed) option1 = the first option to compare
		@param (mixed) option2 = the second option to compare
		@return (bool) true or false if equal */
		equals: function(option1, option2) {
			/* this will count object properties and compare values */
			var propertyCountAndCompare = function(obj1, obj2) {
				var same = true;

				/* this will count the properties of an object
				and any child object properties */
				var countProperty = function(obj) {
					var count = 0;
					/* we want to count each property of the object */
					for (var property in obj) {
						if (obj.hasOwnProperty(property) === true) {
							count++;
							/* we want to do a recursive count to get
							any child properties */
							if (typeof obj[property] === 'object') {
								count += countProperty(obj[property]);
							}
						}
					}
					return count;
				};

				var matchProperties = function(obj1, obj2) {
					var matched = true;

					if (typeof obj1 === 'object' && typeof obj2 === 'object') {
						/* we want to check each object1 property to the
						object 2 property */
						for (var property in obj1) {
							/* we want to check if the property is owned by the
							object andthat they have matching types */
							if (obj1.hasOwnProperty(property) === true && obj2.hasOwnProperty(property) === true && typeof obj1[property] === typeof obj2[property]) {
								/* we want to check if the type is an object */
								if (typeof obj1[property] === 'object') {
									/* this will do a recursive check to the
									child properties */
									matched = matchProperties(obj1[property], obj2[property]);

									/* if a property did not match we can stop
									the camparison */
									if (matched === false) {
										break;
									}
								} else {
									if (obj1[property] !== obj2[property]) {
										matched = false;
										break;
									}
								}
							} else {
								matched = false;
								break;
							}
						}
					} else {
						matched = false;
					}

					return matched;
				};

				/* we want to check if they have the same number of
				properties */
				var option1Count = countProperty(obj1),
					option2Count = countProperty(obj2);

				if (option1Count === option2Count) {
					/* we want to check if the properties match */
					var result = matchProperties(obj1, obj2);
					if (result === false) {
						same = false;
					}
				} else {
					same = false;
				}

				return same;
			};

			var same = true;

			/* we want to check if there types match */
			var option1Type = typeof option1,
				option2Type = typeof option2;
			if (option1Type === option2Type) {
				/* we need to check if the options are objects
				because we will want to match all the
				properties */
				if (option1Type === 'object' && option2Type === 'object') {
					var matchResult = propertyCountAndCompare(option1, option2);
					if (matchResult === false) {
						same = false;
					}
				} else {
					if (option1 !== option2) {
						same = false;
					}
				}
			} else {
				same = false;
			}

			return same;
		},

		/* this will extend an object by creating a clone object and
		returning the new object with the added object or false.
		@param (mixed) sourceClass = the original (class function
		constructor or class prototype) unextended
		@param (mixed) addingClass = the (class function constructor
		or class prototype) to add to the original */
		extendClass: function(sourceClass, targetClass) {
			/* if we are using a class constructor function
			we want to get the class prototype object */
			var source = (typeof sourceClass === 'function') ? sourceClass.prototype : sourceClass,
				target = (typeof targetClass === 'function') ? targetClass.prototype : targetClass;

			if (typeof source === 'object' && typeof target === 'object') {
				/* we want to create a new object and add the source
				prototype to the new object */
				var obj = this.createObject(source);

				/* we need to add any settings from the source that
				are not on the prototype */
				this.extendObject(source, obj);

				/* we want to add any additional properties from the
				target class to the new object */
				for (var prop in target) {
					obj[prop] = target[prop];
				}

				return obj;
			}
			return false;
		},

		/* this will extend an object by creating a clone object and
		returning the new object with the added object or false.
		@param (object) sourceObj = the original object
		@param (object) targetObj = the target object */
		extendObj: function(source, target) {
			if (source && typeof source === 'object' && target && typeof target === 'object') {
				target.parent = source;
				for (var prop in source) {
					if (typeof target[prop] === 'undefined') {
						target[prop] = source[prop];
					}
				}
				return target;
			}
			return false;
		},

		/* this will extend an object by creating a clone object and
		returning the new object with the added object or false.
		@param (object) sourceObj = the original object
		@param (object) targetObj = the target object */
		extendPrototype: function(source, target) {
			target.prototype = new source();
			target.prototype.parent = source;
			target.prototype.constructor = target;

			return target;
		},

		/* this will return a new object and extend it if an object it supplied.
		@param [(object)] object = the extending object
		@return (object) this will return a new object with the
		inherited object */
		createObject: function(object) {
			if (typeof Object.create !== 'function') {
				var obj = function() {};
				obj.prototype = object;
				return new obj();
			} else {
				return Object.create(object);
			}
		},

		/* this will bind the method to an object to return
		the bound method.
		@param (object) obj = the object to bind the method
		@param (function) method = the method to bind
		@param [(array)] argArray = the method args
		@param [(bool)] addArgs = this will set the curried
		args to be added to the original args
		@return a bound method or false on error */
		createCallBack: function(obj, method, argArray, addArgs) {
			if (typeof method === 'function') {
				return function() {
					argArray = argArray || [];

					if (addArgs === true) {
						var args = Base.prototype.listToArray(arguments);
						return method.apply(obj, argArray.concat(args));
					} else {
						return method.apply(obj, argArray);
					}
				};
			} else {
				return false;
			}
		},

		/* this will bind the method to an object to return
		the bound method.
		@param (object) obj = the object to bind the method
		@param (function) method = the method to bind */
		bind: function(obj, method) {
			if (typeof Function.prototype.bind !== 'function') {
				return function() {
					return method.apply(obj, arguments);
				};
			} else {
				return method.bind(obj);
			}
		},

		/* this will prepare a json object to be used in an
		xhr request to sanitize the uri compontents and json
		encode the object.
		@param (object) obj = the object json encode
		@return (mixed) a json string or false */
		prepareJsonUrl: function(obj) {
			var escapeChars = function(str) {
				if (typeof str !== 'string') {
					// str = String(str);
				}

				var newLine = /\n/g,
					returnCarriage = /\r/g,
					tab = /\t/g;

				if (typeof str === 'string') {
					str = str.replace(newLine, '\\n').replace(returnCarriage, '\\r').replace(tab, '\\t');
				}

				return str;
			};

			var sanitize = function(text) {
				/* we needto escape chars and encode the uri
				components */
				text = escapeChars(text);
				if (typeof text === 'string') {
					text = encodeURIComponent(text);

					var pattern = /\%22/g;
					text = text.replace(pattern, '"');
				}

				return text;
			};

			var prepareUrl = function(data) {
				var type = typeof data;
				if (type !== 'undefined') {
					if (type === 'object') {
						for (var prop in data) {
							if (data.hasOwnProperty(prop) && data[prop] !== null) {
								var childType = typeof data[prop];
								if (childType) {
									data[prop] = prepareUrl(data[prop]);
								} else {
									data[prop] = sanitize(data[prop]);
								}
							}
						}
					} else {
						data = sanitize(data);
					}
				}
				return data;
			};

			var before = obj;

			/* we want to check to clone object so we wont modify the
			original object */
			if (typeof before === 'object') {
				before = JSON.parse(JSON.stringify(obj));
			}
			var after = prepareUrl(before);
			return this.jsonEncode(after);
		},

		/* this will decode a json string.
		@param (string) data = a json encoded string */
		jsonDecode: function(data) {
			if (typeof data !== 'undefined' && data.length > 0) {
				try {

					return JSON.parse(data);
				} catch (e) {
					return data;
				}
			}

			return false;
		},

		/* this will encode an object or array of objects.
		@param (string) data = an object or array of objects */
		jsonEncode: function(data) {
			if (typeof data !== 'undefined') {
				try {
					return JSON.stringify(data);
				} catch (e) {
					return false;
				}
			}

			return false;
		},

		/* this will parse an xml string.
		@param (string) data = an xml string */
		xmlParse: function(data) {
			if (typeof data !== 'undefined') {
				var response;

				if (window.DOMParser) {
					var parser = new DOMParser();
					response = parser.parseFromString(data, 'text/xml');
				} else {
					var xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
					xmlDoc.async = false;
					response = xmlDoc.loadXML(data);
				}

				return response;
			} else {
				return false;
			}
		}

	});

	/*
		Class

		this will allow class object to be extend
		from a single factory.

	*/
	var Class = function() {

	};

	Class.prototype = {
		constructor: Class,

		super: function() {
			var args = base.listToArray(arguments);
			return this.callParent.apply(this, args);
		},

		callParent: function() {
			var parent = this.parent;
			if (parent) {
				var args = base.listToArray(arguments),

					// this will remove the first arg as the method
					method = args.shift();
				if (method) {
					var func = parent[method];
					if (typeof func === 'function') {
						return func.apply(this, args);
					}
				}
			}
			return false;
		}
	};

	/* this will allow the classes to be extened.
	@param (object) child = the child object to extend
	@return (mixed) the new child prototype or false */
	Class.extend = function(child) {
		/* the child constructor must be set to set
		the parent static methods on the child */
		var constructor = child && child.constructor ? child.constructor : false;
		if (constructor) {
			/* this will add the parent class to the
			child class */
			var parent = this.prototype;
			constructor.prototype = base.extendClass(parent, child);
			constructor.prototype.parent = parent;

			/* this will add the static methods from the parent to
			the child constructor. */
			base.extendObject(this, constructor);
			return constructor.prototype;
		}
		return false;
	};

	/* this will add a reference to the Class
	object */
	base.extend.Class = Class;

})(this);

/* ajax */
(function() {
	'use strict';

	var defaultSettings = {
		url: '',

		dataType: 'json',

		type: 'POST',

		fixedParams: {},

		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},

		async: true,

		crossDomain: false,

		withCredentials: false,

		completed: null,
		failed: null,
		aborted: null,
		progress: null
	};

	base.augment({

		xhrSettings: defaultSettings,

		addFixedParams: function(params) {
			base.xhrSettings.fixedParams = params;
		},

		ajaxSettings: function(settingsObj) {
			if (typeof settingsObj === 'object') {
				base.xhrSettings = base.extendClass(base.xhrSettings, settingsObj);
			}
		},

		resetAjaxSettings: function() {
			base.xhrSettings = defaultSettings;
		}
	});

	/* this will send an ajax request and return
	the xhr object */
	/*
		the params can either be individual params or a settings object

		individual param:
		@param (string) url = the url to connect to
		@param (string) params = the xhr params
		@param (function) callBackFn = the ajax callback function
		@param [(dataType)] dataType = the data type of xhr response
		@param [(type)] dataType = the send type to the server
		(json/xml/text)
		@param [(bool)] async = this will enable or disable async

		settings object:
		-url (string): the ajax file url
		-params (string): the ajax param string
		-type (string): the server request type
		-dataType (string): the type of data to return (text, html, json)
		if the dataType is json or xml the return text will be parsed
		before it is return to the callback function
		-async (bool): default is set to async
		-headers (object): the headers to use
		-crossDomain (bool): set true if performings cors
		-withCredentials (bool): with credentials for cors
		-completed (function): the function to perform
		when completed
		-failed (function): the function to perform
		if failed
		-progress (function): the function to perform
		during progress
		-aborted (function): the function to perform
		if aborted

	*/
	base.extend.ajax = function() {
		var args = base.listToArray(arguments);

		var ajax = new ajaxRquest(args);
		ajax.setup();
		return ajax.xhr;
	};

	var ajaxRquest = function(args) {
		this.args = args;

		this.xhr = null;
		this.settings = null;
	};

	ajaxRquest.prototype = {
		constructor: ajaxRquest,

		setup: function() {
			this.getXhrSettings();

			var xhr = this.xhr = this.createXHR();
			if (xhr !== false) {
				var settings = this.settings;
				var params = this.getParams();

				if (settings.type === 'GET') {
					xhr.open(settings.type, settings.url + '?' + params, settings.async);
				} else {
					xhr.open(settings.type, settings.url, settings.async);
				}

				this.setupHeaders();
				this.addXhrEvents();

				xhr.send(params);
				return true;
			} else {
				return false;
			}
		},

		parseParamString: function(paramStr) {
			var objURL = {};
			var regExp = /([^?=&]+)(=([^&]*))?/g;

			if (typeof paramStr === 'string') {
				paramStr.replace(regExp, function(a, b, c, d) {
					objURL[b] = d;
				});
			}
			return objURL;
		},

		objectToString: function(object) {
			var params = [];
			for (var prop in object) {
				if (object.hasOwnProperty(prop)) {
					var value = object[prop];
					if (typeof value === 'string') {
						params.push(prop + '=' + encodeURIComponent(value));
					} else if (typeof value === 'number' || typeof value === 'boolean') {
						params.push(prop + '=' + value);
					} else if (typeof value === 'object') {
						params.push(prop + '=' + base.prepareJsonUrl(value));
					}
				}
			}
			return params.join('&');
		},

		/* this will setup fixed params with params being 
		sent in the xhr request. this method supports 
		param types of string, FormData, and objects. 
		@return (mixed) string or FormData object */
		getParams: function() {
			var settings = this.settings,
				params = settings.params;

			var self = this;
			var setupParams = function(params, addingParams) {
				var paramsType = typeof params;
				if (addingParams) {
					var addingType = typeof addingParams;
					if (paramsType === 'string') {
						if (addingType !== 'string') {
							addingParams = self.objectToString(addingParams);
						}
						var char = (params === '') ? '?' : '&';
						params += char + addingParams;
					}
					else if (params instanceof FormData) {
						if (addingType === 'string') {
							addingParams = self.parseParamString(addingParams);
						}

						for (var key in addingParams) {
							if (addingParams.hasOwnProperty(key)) {
								params.append(key, addingParams[key]);
							}
						}
					}
					else if (paramsType === 'object') {
						/* we dont want to modify the original object 
						so we need to clone the object before extending */
						params = JSON.parse(JSON.stringify(params));

						if (addingType === 'string') {
							addingParams = self.parseParamString(addingParams);
						}

						params = base.extendObject(addingParams, params);
						params = self.objectToString(params);
					}
				}
				else {
					if ((params instanceof FormData) === false && paramsType === 'object') {
						params = self.objectToString(params);
					}
				}
				return params;
			};

			var fixedParams = settings.fixedParams;
			if (params) {
				params = setupParams(params, fixedParams);
			}
			else if (fixedParams) {
				params = setupParams(fixedParams);
			}

			return params;
		},

		/* this will setup the request settings from the default
		xhr settings and any settings being updated from the
		ajax args */
		getXhrSettings: function() {
			var args = this.args;

			/* we want to create a clone of the default
			settings before adding the new settings */
			var settings = this.settings = base.createObject(base.xhrSettings);

			/* we want to check if we are adding the ajax settings by
			individual args or by a settings object */
			if (args.length >= 2 && typeof args[0] !== 'object') {
				/* we want to add each arg as one of the ajax
				settings */
				for (var i = 0, maxLength = args.length; i < maxLength; i++) {
					var arg = args[i];

					switch (i) {
						case 0:
							settings.url = arg;
							break;
						case 1:
							settings.params = arg;
							break;
						case 2:
							settings.completed = arg;
							settings.failed = arg;
							break;
						case 3:
							settings.dataType = arg || 'json';
							break;
						case 4:
							settings.type = (arg) ? arg.toUpperCase() : 'POST';
							break;
						case 5:
							settings.async = (typeof arg !== 'undefined') ? arg : true;
							break;
					}
				}
			} else {
				/* override the default settings with the args
				settings object */
				settings = this.settings = base.extendClass(this.settings, args[0]);

				/* we want to check to add the completed callback
				as the error and aborted if not set */
				if (typeof settings.completed === 'function') {
					if (typeof settings.failed !== 'function') {
						settings.failed = settings.completed;
					}

					if (typeof settings.aborted !== 'function') {
						settings.aborted = settings.failed;
					}
				}
			}
		},

		createXHR: function() {
			var self = this;
			var createXHRObj = function() {
				var xhr = false;

				if (typeof XMLHttpRequest !== 'undefined') {
					xhr = new XMLHttpRequest();
				} else {
					try {
						xhr = new ActiveXObject('Msxml2.XMLHTTP');
					} catch (e) {
						try {
							xhr = new ActiveXObject('Microsoft.XMLHTTP');
						} catch (err) {

						}
					}
				}

				return xhr;
			};

			var createCorsXHRObj = function() {
				var xhr = false;

				if (typeof XMLHttpRequest !== 'undefined' && typeof XDomainRequest === 'undefined') {
					xhr = new XMLHttpRequest();
				} else if (typeof XDomainRequest !== 'undefined') {
					xhr = new XDomainRequest();
				}

				if (xhr && self.settings.withCredentials === true) {
					try {
						xhr.withCredentials = true;
					} catch (e) {

					}
				}

				return xhr;
			};

			var xhr = (this.settings && this.settings.crossDomain === true) ? createCorsXHRObj() : createXHRObj();

			return (xhr) ? xhr : false;
		},

		setupHeaders: function() {
			for (var header in this.headers) {
				this.xhr.setRequestHeader(header, this.headers[header]);
			}

			var settings = this.settings;
			if (settings && typeof settings.headers === 'object') {
				var headers = settings.headers;
				for (var header in headers) {
					if (headers.hasOwnProperty(header)) {
						this.xhr.setRequestHeader(header, headers[header]);
					}
				}
			}
		},

		/* this will get the server response and check the
		ready state and status to return response to
		request call back function
		@param (object) e = the event object
		@param [(string)] overrideType = the type of event that
		is overriding the default event type. this is used with the older
		style callback for older browsers */
		update: function(e, overrideType) {
			e = e || window.event;

			var xhr = this.xhr;
			var removeEvents = function() {
				var events = base.events;
				events.removeElementEvents(xhr.upload);
				events.removeElementEvents(xhr);
			};

			var settings = this.settings;
			var type = (overrideType) ? overrideType : e.type;
			switch (type) {
				case 'load':
					if (settings && typeof settings.completed === 'function') {
						var response = this.getResponseData();
						settings.completed(response, this.xhr);
					}
					/* the xhr is complete so we need to clean up
					the events */
					removeEvents();
					break;
				case 'error':
					if (settings && typeof settings.failed === 'function') {
						settings.failed(false, this.xhr);
					}
					/* the xhr has errored so we need to clean up
					the events */
					removeEvents();
					break;
				case 'progress':
					if (settings && typeof settings.progress === 'function') {
						settings.progress(e);
					}
					break;
				case 'abort':
					if (settings && typeof settings.aborted === 'function') {
						settings.aborted(false, this.xhr);
					}
					removeEvents();
					break;
			}
		},

		/* this will get the response data and check to decode the data type
		if required.
		@return (mixed) the response data */
		getResponseData: function() {
			var response = this.xhr.responseText;

			/* we want to check to decode the xhr response
			if we have a response */
			if (typeof response === 'string') {
				var encoded = false;
				/* we want to check to decode the response by the type */
				switch (this.settings.dataType.toLowerCase()) {
					case 'json':

						encoded = base.jsonDecode(response);
						if (encoded !== false) {
							response = encoded;
						} else {
							response = response;
							this.error = 'yes';
						}
						break;
					case 'xml':
						encoded = base.xmlParse(response);
						if (encoded !== false) {
							response = encoded;
						} else {
							response = response;
							this.error = 'yes';
						}
						break;
					case 'text':
						break;

				}
			}

			return response;
		},

		checkReadyState: function(e) {
			e = e || window.event;

			/* check the response state */
			if (this.xhr.readyState !== 4) {
				/* the response is not ready */
				return;
			}

			/* check the response status */
			if (this.xhr.status === 200) {
				/* the ajax was successful
				but we want to change the event type to load */
				this.update(e, 'load');
			} else {
				/* there was an error */
				this.update(e, 'error');
			}
		},

		/* this will add the event listeners to the xhr object. */
		addXhrEvents: function() {
			var settings = this.settings;
			if (settings) {
				var xhr = this.xhr;
				/* we need to check if we can add new event listeners or
				if we have to use the old ready state */
				if (typeof xhr.onload !== 'undefined') {
					var callBack = base.bind(this, this.update);
					base.addListener('load', xhr, callBack);
					base.addListener('error', xhr, callBack);
					base.addListener('progress', xhr.upload, callBack);
					base.addListener('abort', xhr, callBack);
				} else {
					var self = this;
					xhr.onreadystatechange = function(e) { self.checkReadyState(e); };
				}
			}
		}
	};
})();

/* base framework module */
/*
	this will create dynamic html to be
	added and modified
*/
(function() {
	'use strict';

	/* this will add a new html builder object that
	can be extended to the object that you want to
	build with */
	var htmlBuilder = function() {

	};

	/* we want to save the methods to the prototype
	to allow inheritance */
	base.Class.extend({
		constructor: htmlBuilder,

		/* this will create an html element by nodeType, add to
		the specified parent container and return the new
		element.
		@param (string) nodeType = the element node type name
		@param (object) attrObject = the node attributes to add
		to the element.
		@param (mixed) container = the parent container element
		or id
		@param (bool) prepend = this will add the element
		to the begining of theparent */
		create: function(nodeName, attrObject, container, prepend) {
			var filterProperty = function(prop) {
				switch (prop) {
					case 'class':
						prop = 'className';
						break;
					case 'for':
						prop = 'htmlFor';
						break;
					case 'readonly':
						prop = 'readOnly';
						break;
					case 'maxlength':
						prop = 'maxLength';
						break;
					case 'cellspacing':
						prop = 'cellSpacing';
						break;
					case 'rowspan':
						prop = 'rowSpan';
						break;
					case 'colspan':
						prop = 'colSpan';
						break;
					case 'tabindex':
						prop = 'tabIndex';
						break;
					case 'cellpadding':
						prop = 'cellPadding';
						break;
					case 'usemap':
						prop = 'useMap';
						break;
					case 'frameborder':
						prop = 'frameBorder';
						break;
					case 'contenteditable':
						prop = 'contentEditable';
						break;
				}

				return prop;
			};

			/* this will return the property without the on prefix */
			var removePrefix = function(prop) {
				if (typeof prop !== 'undefined') {
					if (prop.substring(0, 2) === 'on') {
						prop = prop.substring(2);
					}
				}
				return prop;
			};

			var obj = document.createElement(nodeName);

			/* we want to check if we have attrributes to add */
			if (attrObject && typeof attrObject === 'object') {
				/* we need to add the type if set to stop ie
				from removing the value if set after the value is
				added */
				if (typeof attrObject.type !== 'undefined') {
					base.attr(obj, 'type', attrObject.type);
				}

				/* we want to add each attr to the obj */
				for (var prop in attrObject) {
					if (attrObject.hasOwnProperty(prop) !== true) {
						continue;
					}

					var propName = filterProperty(prop),
						attrPropValue = attrObject[prop];

					/* we have already added the type so we need to
					skip if the prop is type */
					if (prop === 'type') {
						continue;
					}

					/* we want to check to add the attr settings
					 by property name */
					if (prop === 'innerHTML') {
						if (typeof attrPropValue !== 'undefined' && attrPropValue !== '') {
							/* we need to check if we are adding inner
							html content or just a string */
							var pattern = /<[a-z][\s\S]*>/i;
							if (pattern.test(attrPropValue)) {
								/* html */
								obj.innerHTML = attrPropValue;
							} else {
								/* sting */
								obj.textContent = attrPropValue;
							}
						}
					} else if (prop.substring(0, 5) === 'data-') {
						base.data(obj, prop, attrPropValue);
					} else {
						var propType = typeof attrPropValue;
						if (propType !== 'undefined' && attrPropValue != '') {
							/* we want to check to add a value or amn event listener */
							if (propType === 'function') {
								propName = removePrefix(propName);
								base.addListener(propName, obj, attrPropValue);
							} else {
								obj[propName] = attrPropValue;
							}
						}
					}
				}
			}

			try {
				/* we want to check if the new element should be
				added to the begining or end */
				if (prepend === true) {
					this.prepend(container, obj);
				} else {
					this.append(container, obj);
				}
			} catch (e) {
				this.error(e, obj);
			}
			return obj;
		},

		/* this will create a document fragment to allow elements
		to be added to a container element without rendering until the
		fragment gets appended to an element.
		@return (object) the document fragment */
		createDocFragment: function() {
			return document.createDocumentFragment();
		},

		/* this will create an html element and return the new object
		@param (string) nodeName = the new element node Name or tag name
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) html = the element innerHTML content
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addElement: function(nodeName, id, className, html, container, prepend) {
			var attr = { id: id, 'className': className, innerHTML: html };
			return this.create(nodeName, attr, container, prepend);
		},

		/* this will create an html element and add an onclick to the
		callback function and return the new object
		@param (string) nodeName = the new element node Name or tag name
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) html = the element innerHTML content
		@param (function) callBackFn = the callback function
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addButton: function(nodeName, id, className, html, callBackFn, container, prepend) {
			var attr = { id: id, 'className': className, innerHTML: html };

			if (nodeName === 'button') {
				/* this will stop the form from being submitted by default
				when the button is clicked */
				attr.type = 'button';
			}

			if (typeof callBackFn === 'function') {
				attr.onclick = callBackFn;
			}

			return this.create(nodeName, attr, container, prepend);
		},

		/* this will create a checkbox return the new object
		@param (string) id = the node id
		@param (string) className = the element className
		@param (function) callBackFn = the callback function
		@param (bool) checked = when true the element will be checked
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addCheckbox: function(id, className, callBackFn, checked, container, stopPropagation) {
			var attr = { id: id, 'className': className, type: 'checkbox' };

			if (typeof stopPropagation !== 'undefined' && stopPropagation == true) {
				attr.onclick = base.stopPropagation;
			}

			if (typeof callBackFn === 'function') {
				attr.onchange = callBackFn;
			}

			if (checked == true) {
				attr.checked = true;
			}

			return this.create('input', attr, container);
		},

		getCheckboxLayout: function(id, className, callback, checked, stopPropagation) {
			var layout = {
				node: 'input', type: 'checkbox', id: id, class: className
			};

			if (typeof stopPropagation !== 'undefined' && stopPropagation == true) {
				layout.onclick = base.stopPropagation;
			}

			if (typeof callback === 'function') {
				layout.onchange = callback;
			}

			if (checked == true) {
				layout.checked = true;
			}

			return layout;
		},

		/* this will create a radio return the new object
		@param (string) id = the node id
		@param (string) name = the name
		@param (string) className = the element className
		@param (string) value = the element value
		@param (function) callBackFn = the callback function
		@param (bool) checked = when true the element will be checked
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addRadio: function(id, name, className, value, callBackFn, checked, container, stopPropagation) {
			var attr = { id: id, name: name, 'className': className, value: value, type: 'radio' };

			if (typeof stopPropagation !== 'undefined' && stopPropagation == true) {
				attr.onclick = base.stopPropagation;
			}

			if (typeof callBackFn === 'function') {
				attr.onchange = callBackFn;
			}

			if (checked == true) {
				attr.checked = true;
			}

			return this.create('input', attr, container);
		},

		/* this will create an input and return the new object
		@param (string) inputType = the input type
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) value = the element value
		@param (function) callBackFn = the callback function
		@param (string) placeholder = the input placeholder
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addInput: function(inputType, id, className, value, callBackFn, placeholder, container, prepend) {
			var attr = { id: id, 'className': className, value: value, type: inputType };

			attr.onfocus = focusElement;

			if (typeof callBackFn === 'function') {
				var callBack = base.createCallBack(null, callBackFn, [this.value]);
				attr.onkeyup = callBack;
			}

			if (placeholder) {
				attr.placeholder = placeholder;
			}

			return this.create('input', attr, container, prepend);
		},

		/* this will create a textarea and return the new object
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) value = the element value
		@param (function) callBackFn = the callback function
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addTextarea: function(id, className, value, callBackFn, container, prepend) {
			var attr = { id: id, 'className': className, value: value };

			attr.onfocus = focusElement;

			if (typeof callBackFn === 'function') {
				attr.onblur = callBackFn;
				attr.onkeyup = callBackFn;
			}

			return this.create('textarea', attr, container, prepend);
		},

		/* this will create a select and return the new object
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) value = the element value
		@param (function) callBackFn = the callback function
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addSelect: function(id, className, value, callBackFn, container, prepend) {
			var attr = { id: id, 'className': className, value: value };

			if (typeof callBackFn === 'function') {
				var callBack = function() {
					callBackFn.call(null, this.value);
					attr = null;
					container = null;
				};
				attr.onchange = callBack;
			}

			return this.create('select', attr, container, prepend);
		},

		/* this will add options to a select
		@param (mixed) selectElem = the select element or id
		@param (array) optionArray = and array with objects
		to use as options
		@param [(string)] defaultValue = the select default
		value */
		setupSelectOptions: function(selectElem, optionArray, defaultValue) {
			if (selectElem !== null) {
				/* if the select id was sent we need to get
				the element */
				if (typeof selectElem !== 'object') {
					selectElem = document.getElementById(selectElem);
				}

				/* we need to check if we have any options to add */
				if (optionArray && optionArray.length) {
					/* create each option then add it to the select */
					for (var n = 0, maxLength = optionArray.length; n < maxLength; n++) {
						var settings = optionArray[n];
						var option = selectElem.options[n] = new Option(settings.label, settings.value);

						/* we can select an option if a default value
						has been sumbitted */
						if (defaultValue !== null) {
							if (option.value == defaultValue) {
								option.selected = true;
							}
						}
					}
				}
			}
		},

		/* this will create an image and return the new object
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) src = the image src
		@param (string) alt = the image alt
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addImage: function(id, className, src, alt, container, prepend) {
			var attr = { id: id, 'className': className };

			if (typeof alt !== 'undefined') {
				attr.alt = alt;
			}

			if (typeof src !== 'undefined') {
				attr.src = src;
			}

			return this.create('img', attr, container, prepend);
		},

		addForm: function(id, action, method, autocomplete, novalidate, enctype, target, container, prepend) {
			var attr = { id: id };

			if (typeof method !== 'undefined') {
				attr.method = method;
			}

			if (typeof autocomplete !== 'undefined') {
				attr.autocomplete = autocomplete;
			}

			if (typeof novalidate !== 'undefined') {
				attr.novalidate = novalidate;
			}

			if (typeof enctype !== 'undefined') {
				attr.enctype = enctype;
			}

			if (typeof target !== 'undefined') {
				attr.target = target;
			}

			return this.create('form', attr, container, prepend);
		},

		/* this will create an image and return the new object
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) src = the src url
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addIframe: function(id, className, src, container, prepend) {
			var attr = { id: id, 'className': className };

			if (typeof src !== 'undefined') {
				attr.src = src;
			}

			attr.width = '100%';
			attr.height = '100%';
			attr.frameBorder = '0';

			return this.create('iframe', attr, container, prepend);
		},

		/* this will create an a link and return the new object
		@param (string) id = the node id
		@param (string) className = the element className
		@param (string) html = the element inner html
		@param (string) url = the link url
		@param (mixed) container = the element parent id or parent object
		@param (bool) prepend = when true the element will be added
		to the begining */
		addLink: function(id, className, html, url, container, prepend) {
			var attr = { id: id, 'className': className, innerHTML: html };

			if (url !== null) {
				attr.href = url;
			}

			return this.create('a', attr, container, prepend);
		},

		/* this will remove an element from a parent and
		remove all attr and events added to the element and its child
		elements.
		@param (mixed) child = the child element or id */
		removeElement: function(obj) {
			/* stop if no object has been set to be removed */
			if (!obj) {
				return this;
			}

			if (typeof obj === 'string') {
				obj = document.getElementById(obj);
			}

			var baseEvents = base.events;
			var db = base.DataBinder || false;

			/* this will remove the elmenents data and events
			for the elmeent and all child elements.
			@param (object) ele */
			var removeElementData = function(ele) {
				/* we want to do a recursive remove child
				removal */
				var children = ele.childNodes;
				if (children) {
					var length = children.length;
					for (var i = length - 1; i >= 0; i--) {
						var child = children[i];
						if (child) {
							/* this will remove the child elmenet data
							before the parent is removed */
							removeElementData(child);
						}
					}
				}

				/* this will loop though the element attrs to
				check for any event listeners to cancel and
				remove any data binding */
				var attributes = ele.attributes;
				if (attributes) {
					var maxLength = attributes.length;
					for (var j = 0; j < maxLength; j++) {
						var attr = attributes[j];
						if (typeof ele[attr] === 'function') {
							ele[attr] = null;
						} else if (attr.name === 'data-bind' && db !== false) {
							/* this will check to remove any data bindings
							to the element */
							db.unbind(ele);
						}
					}
				}

				/* we want to remove all events aded to the
				element */
				baseEvents.removeElementEvents(ele);
			};

			/* this will remove all element data and binding
			and remove from the parent container */
			removeElementData(obj);
			var container = obj.parentNode;
			if (container) {
				container.removeChild(obj);
			}

			return this;
		},

		/* this is an alias for removeElement. This will remove a
		child element.
		@param (mixed) child = the child element or id */
		removeChild: function(child) {
			this.removeElement(child);
		},

		/* this will remove all child elements.
		@param (mixed) container = the container element or id */
		removeAll: function(container) {
			if (typeof container === 'string') {
				container = document.getElementById(container);
			}

			if (typeof container === 'object') {
				var child;
				while ((child = container.firstChild)) {
					this.removeElement(child);
				}
			}
		},

		changeParent: function(child, newParent) {
			if (typeof child === 'string') {
				child = document.getElementById(child);
			}

			var container = (typeof newParent === 'string') ? document.getElementById(newParent) : newParent;
			container.appendChild(child);
		},

		append: function(parent, child) {
			switch (typeof parent) {
				case 'undefined':
					parent = document.body;
					break;
				case 'string':
					parent = document.getElementById(parent);
					break;
			}

			parent.appendChild(child);
		},

		prepend: function(parent, child) {
			switch (typeof parent) {
				case 'undefined':
					parent = document.body;
					break;
				case 'string':
					parent = document.getElementById(parent);
					break;
			}

			parent.insertBefore(child, parent.firstChild);
		},

		/* this will clone a node and return the
		copy or false */
		clone: function(node, deepCopy) {
			if (node && typeof node === 'object') {
				deepCopy = deepCopy || false;
				return node.cloneNode(deepCopy);
			} else {
				return false;
			}
		},

		error: function(error, object) {
			console.log(error.message, object);
		},

		createRowInputLayout: function(type, label, model, prop, validate, disabled) {
			var className = (validate == true) ? 'val' : '';

			var id = this.id + '_' + base.uncamelCase(prop, '_');

			var layout = {
				class: 'row light left',
				label: { node: 'label', textContent: label },
				input: { node: 'input', type: type, id: id, class: className }
			};

			if (model) {
				layout.input.bind = { model: model, prop: prop };
			}

			if (disabled === true) {
				layout.input.disabled = true;
			}

			return layout;
		},

		createRowSelectLayout: function(label, model, prop, options, disabled, callback) {
			var id = this.id + '_' + base.uncamelCase(prop, '_');

			var layout = {
				class: 'row light left',
				label: { node: 'label', textContent: label },
				select: { node: 'select', id: id, children: options }
			};

			if (model) {
				layout.select.bind = { model: model, prop: prop };

				var value = model.get(prop);
				for (var i = 0; i < options.length; i++) {
					if (value === options[i].value) {
						options[i].selected = true;
						break;
					}
				}
			}

			if (disabled === true) {
				layout.select.disabled = true;
			}

			if (typeof callback === 'function') {
				layout.select.onchange = callback;
			}

			return layout;
		},

		createDatalistFor: function(elementId, options) {

			var element = document.getElementById(elementId);
			if (!element || !element.parentElement)
				return;

			var datalist = document.createElement('datalist');
			datalist.setAttribute('id', elementId + '_datalist');

			for (var i = 0, length = options.length; i < length; i++) {
				var opt = options[i];
				datalist.appendChild(new Option(opt.label, opt.value));
			}

			element.parentElement.appendChild(datalist);
			element.setAttribute('list', elementId + '_datalist');
		}

	});

	/* we want to use a few private functions to add input selection
	to an element and reduce any closures */
	var focusElement = function(e) {
		e = e || window.event;
		var src = e.srcElement || e.target;
		src.select();
	};

	base.extend.htmlBuilder = htmlBuilder;

})();

/* base framework module */
/*
	this will create a layout parser object
	and shortcut functions.
*/
(function() {
	'use strict';

	/*
		LayoutParser

		this will parse json object layouts and
		use the basehtmlBuilder to render the layout
		in html.
	*/
	var LayoutParser = function() {};

	base.htmlBuilder.extend({
		constructor: LayoutParser,

		/* this will parse a layout object.
		@param (object) obj
		@param (mixed) parent = a parent element
		object or parent id string
		@return (object) a doc frag element with the layout */
		parseLayout: function(obj, parent) {
			if (typeof parent === 'string') {
				parent = document.getElementById(parent);
			}

			var fragment = this.createDocFragment();

			if (base.isArray(obj) === true) {
				for (var i = 0, length = obj.length; i < length; i++) {
					var item = obj[i];
					this.parseElement(item, fragment);
				}
			} else {
				this.parseElement(obj, fragment);
			}

			if (parent && typeof parent === 'object') {
				parent.appendChild(fragment);
			}
			return fragment;
		},

		/* this will parse a layout element.
		@param (object) obj
		@param (mixed) parent = a parent element
		object or parent id string */
		parseElement: function(obj, parent) {
			var settings = this.getElementSettings(obj);
			var ele = this.create(settings.node, settings.attr, parent);
			if (typeof obj.onCreated === 'function') {
				obj.onCreated(ele);
			}

			/* this will check to bind the element to
			the prop of a model */
			var bind = obj.bind;
			if (bind && typeof bind === 'object') {
				base.DataBinder.bind(ele, bind.model, bind.prop);
			}

			/* we want to recursively add the children to
			the new element */
			var children = settings.children;
			if (children.length > 0) {
				for (var i = 0, length = children.length; i < length; i++) {
					var child = children[i];
					this.parseElement(child, ele);
				}
			}
		},

		getElementNode: function(obj) {
			var nodeType = 'div';

			if (typeof obj.node !== 'undefined') {
				nodeType = obj.node;
			}

			return nodeType;
		},

		/* this will get the nodeType, attr, and children
		from an element. children can be an array of
		children or an element object.
		@param (object) obj
		@return (object) the attr settings */
		getElementSettings: function(obj) {
			var attr = {},
				children = [];

			for (var key in obj) {
				if (obj.hasOwnProperty(key) === true) {
					if (key === 'node' || key === 'onCreated' || key === 'bind') {
						continue;
					}

					var value = obj[key];
					/* we need to filter the children from the attr
					settings. the children need to keep their order. */
					if (key !== 'children' && typeof value !== 'object') {
						attr[key] = value;
					} else {
						if (key === 'children') {
							children = children.concat(value);
						} else {
							children.push(value);
						}
					}
				}
			}

			var node = this.getElementNode(obj);
			if (node === 'button') {
				attr.type = 'button';
			}

			return {
				node: this.getElementNode(obj),
				attr: attr,
				children: children
			};
		}
	});

	/* this will add a reference to the layout parser
	and add a shortcut method to parse lyout objects */
	base.extend.layoutParser = new LayoutParser();

	/* this will parse a layout object.
	@param (object) obj
	@param (mixed) parent = a parent element
	object or parent id string
	@return (object) a reference to the object */
	base.extend.parseLayout = function(obj, parent) {
		base.layoutParser.parseLayout(obj, parent);
	};
})();

/* base framework module */
(function() {
	'use strict';

	/*
		DataBinder

		this create a data bind module to add
		two way data binding to base models.

		@param [(string)] attr

	*/
	var DataBinder = function(attr) {
		this.attr = attr || 'data-bind';
		/* this will create a new pub sub object
		that will be used to propagate the changes */
		this.pubSub = new DataPubSub();
		this.idCount = 0;
		this.setup();
	};

	DataBinder.prototype = {
		constructor: DataBinder,

		setup: function() {
			this.setupEvents();

			/* this will add a callback to our pubsub object
			to check what has been changed even if the model
			is not added to the pub sub. this will allow our
			models to publish changes without being added
			and requiring them to be removed when deleted */
			this.pubSub.callBack = base.bind(this, this.messageChange);
		},

		/* this will add the data bind attr on an

		element for a model property.
		@param (object) element
		@param (object) model
		@param (string) prop
		@return (object) the instance of the data binder */
		bind: function(element, model, prop) {
			/* this will add the data binding
			attr to out element so it will subscribe to
			the two model changes */
			var modelId = model.getModelId(),
				attr = this.attr;
			base.attr(element, attr, modelId + ':' + prop);

			/* this will add a unique id to the element
			so the model will know when the element has
			been updated */
			var id = 'bs-db-' + this.idCount++;
			base.attr(element, attr + '-id', id);

			/* this will setup the model to update
			from the pubSub element changes */
			this.pubSub.on(id, function(evt, prop, value, committer) {
				model.set(prop, value, committer);
			});

			/* we want to get the starting value of the
			model and set it on our element */
			var value = model.get(prop);
			if (typeof value !== 'undefined') {
				this.set(element, value);
			}
			return this;
		},

		/* this will remove an element from the data binder
		@param (object) element
		@return (object) the instance of the data binder*/
		unbind: function(element) {
			var bindId = base.data(element, this.attr + '-id');
			this.pubSub.remove(bindId);
			return this;
		},

		messageChange: function(message, prop, value, committer) {
			if (typeof message === 'string' && message.indexOf(':') > -1) {
				var parts = message.split(':');
				var modelId = parts[0];

				this.updateElements(modelId, prop, value, committer);

				/*
				var action = parts[1];
				switch(action)
				{
					case 'change':
						this.updateElements(modelId, prop, value, committer);
						break;
					case 'delete':
						this.deleteElements(modelId, prop);
						break;
				}*/
			}
			return this;
		},

		/* this will update all elements with the model id
		attr.
		@param (string) modelId
		@param (string) prop
		@param (mixed) value
		@param (object) committer */
		updateElements: function(modelId, prop, value, committer) {
			var elements = document.querySelectorAll('[' + this.attr + '=\'' + modelId + ':' + prop + '\']');
			if (elements) {
				for (var i = 0, len = elements.length; i < len; i++) {
					var element = elements[i];
					if (committer !== element) {
						this.set(element, value);
					}
				}
			}
		},

		/* this will update all elements with the model id
		attr.
		@param (string) modelId
		@param (string) prop
		@param (mixed) value
		@param (object) committer */
		deleteElements: function(modelId, prop) {
			var elements = document.querySelectorAll('[' + this.attr + '=\'' + modelId + ':' + prop + '\']');
			if (elements) {
				var builder = new base.htmlBuilder();
				for (var i = 0, len = elements.length; i < len; i++) {
					builder.removeElement(elements[i]);
				}
			}
		},

		/* this will set a value on an elememnt.
		@param (object) element
		@param (mixed) value */
		set: function(element, value) {
			if (element && typeof element === 'object') {
				var tagName = element.tagName.toLowerCase();
				if (tagName === 'input' || tagName === 'textarea' || tagName === 'select') {
					var type = element.type;
					if (type && (type === 'checkbox' || type === 'radio')) {
						value = value == 0 ? false : true;
						element.checked = value;
					} else {
						element.value = value;
					}
				} else {
					element.textContent = value;
				}
			}
		},

		/* this will get a value on an elememnt.
		@param (object) element
		@param (mixed) value
		@return (mixed) the element value */
		get: function(element) {
			var value = '';
			if (element && typeof element === 'object') {
				var tagName = element.tagName.toLowerCase();
				if (tagName === 'input' || tagName === 'textarea' || tagName === 'select') {
					var type = element.type;
					if (type && (type === 'checkbox' || type === 'radio')) {
						value = element.checked;
					} else {
						value = element.value;
					}
				} else {
					value = element.textContent;
				}
			}
			return value;
		},

		/* this will publish a change to the data binder.
		@param (string) message e.g id:change
		@param (string) attrName = the model prop name
		@param (mixed) the prop value
		@param (object) the object committing the change */
		publish: function(message, attrName, value, committer) {
			this.pubSub.publish(message, attrName, value, committer);
			return this;
		},

		onChange: function(message, callBack) {
			this.pubSub.on(message, callBack);
			return this;
		},

		/* this will setup the on change handler and
		add the events. this needs to be setup before adding
		the events. */
		changeHandler: null,
		setupEvents: function() {
			var dataAttr = this.attr;
			var isDataBound = function(element) {
				if (element) {
					var value = base.data(element, dataAttr);
					if (value) {
						var parts = value.split(':');
						if (parts.length) {
							return {
								id: parts[0],
								value: parts[1],
								bindId: base.data(element, dataAttr + '-id')
							};
						}
					}
				}
				return false;
			};

			/* this will create a closure scope to the object
			by using a local call back function. */
			var self = this,
				pubSub = this.pubSub;
			this.changeHandler = function(evt) {
				if (evt.type === 'keyup') {
					var blockedKeys = [
						17, //ctrl
						9, //tab
						16, //shift
						18, //alt
						20, //caps lock
						37, //arrows
						38,
						39,
						40
					];
					/* this will check to block ctrl, shift or alt +
					buttons */
					if (evt.ctrlKey !== false || evt.shiftKey !== false || evt.altKey !== false || base.inArray(blockedKeys, evt.keyCode) !== -1) {
						return true;
					}
				}

				var target = evt.target || evt.srcElement;
				var settings = isDataBound(target);
				if (settings) {
					var prop = settings.value;
					if (prop && prop !== '') {
						var value = self.get(target);
						/* this will publish to the ui and to the
						model that subscribes to the element */
						pubSub.publish(settings.bindId, prop, value, target);
						pubSub.publish(settings.id + ':change', prop, value, target);
					}
				}
				evt.stopPropagation();
			};

			this.addEvents();
		},

		/* this will add the binder events */
		addEvents: function() {
			base.on(['change', 'keyup'], document, this.changeHandler, false);
		},

		/* this will remove the binder events */
		removeEvents: function() {
			base.off(['change', 'keyup'], document, this.changeHandler, false);
		}
	};

	/*
		DataPubSub

		this will create a pub sub object
		to allow messages to be subscribed to and
		publish changes that will be pushed to the
		subscribers.
	*/
	var DataPubSub = function() {
		this.callBacks = {};
	};

	DataPubSub.prototype = {
		constructor: DataPubSub,

		/* this will get the subscriber array for the
		message or create a new subscriber array if none
		is setup already.
		@param (string) msg
		@return (array) subscriber array */
		get: function(msg) {
			var callBacks = this.callBacks;
			return (callBacks[msg] = callBacks[msg] || []);
		},

		reset: function() {
			this.callBacks = [];
		},

		on: function(msg, callBack) {
			var list = this.get(msg);
			list.push(callBack);
		},

		off: function(msg, callBack) {
			var list = this.get(msg);
			var index = base.inArray(list, callBack);
			if (index > -1) {
				list.splice(index, 1);
			}
		},

		remove: function(msg) {
			var callBacks = this.callBacks;
			if (callBacks[msg]) {
				delete callBacks[msg];
			}
		},

		callBack: null,

		publish: function(msg) {
			var i, length,
				list = this.callBacks[msg] || false;
			if (list !== false) {
				length = list.length;
				for (i = 0; i < length; i++) {
					list[i].apply(this, arguments);
				}
			}

			if (typeof this.callBack === 'function') {
				/* this will setujp the params array by pushing
				the publish args to the params array */
				var params = [];
				length = arguments.length;
				for (i = 0; i < length; i++) {
					params[i] = arguments[i];
				}
				this.callBack.apply(null, params);
			}
		}
	};

	base.extend.DataPubSub = DataPubSub;
	base.extend.DataBinder = new DataBinder();
})();

/* Model */
(function() {
	'use strict';

	/*
		Model

		this create a model module that can bind
		the data using the dase data binder.

		@param (object) settings = the new model properties
		and methods
	*/
	var Model = function(settings) {
		this._init();

		this.dirty = false;

		this.attributes = {};
		this.stage = {};

		this.eventSub = new base.DataPubSub();

		var attributes = setupAttrSettings(settings);
		this.set(attributes);

		this.bind = true;

		this.initialize();

		this.xhr = null;
	};

	Model.prototype = {
		constructor: Model,

		_init: function() {
			var constructor = this.constructor;
			this._modelNumber = (typeof constructor._modelNumber === 'undefined') ? constructor._modelNumber = 0 : (++constructor._modelNumber);

			var modelId = this.modelTypeId + '-';
			this._id = modelId + this._modelNumber;
		},

		getModelId: function() {
			return this._id;
		},

		remove: function() {

		},

		initialize: function() {

		},

		on: function(attrName, callBack) {
			var message = attrName + ':change';
			this.eventSub.on(message, callBack);
		},

		off: function(attrName, callBack) {
			var message = attrName + ':change';
			this.eventSub.off(message, callBack);
		},

		set: function() {
			var self = this;

			var set = function(attr, val, committer, stopMerge) {
				var setValue = function(obj, attr, val) {
					var test = /(\.|\[)/;
					if (test.test(attr) === true) {
						var pattern = /(\w+)|(?:\[(\d)\))/g;
						var props = attr.match(pattern);
						for (var i = 0, length = props.length - 1; i < length; i++) {
							var prop = props[i];
							var propValue = obj[prop];
							if (propValue === undefined) {
								obj[prop] = !isNaN(prop) ? {} : [];
							}

							obj = propValue;
						}

						obj[props[i]] = val;
					} else {
						obj[attr] = val;
					}
				};

				if (self.bind === true) {
					var binder = base.DataBinder,
						msg = self._id + ':change';

					var publish = function(obj, pathString) {
						pathString = pathString || '';
						if (obj && typeof obj === 'object') {
							var subPath, value;
							if (Array.isArray(obj) === true) {
								var length = obj.length;
								for (var i = 0; i < length; i++) {
									value = obj[i];
									subPath = pathString + '[' + i + ']';
									if (value && typeof value === 'object') {
										publish(value, subPath);
									} else {
										binder.publish(msg, subPath, value, self);
									}
								}
							} else {
								for (var prop in obj) {
									if (obj.hasOwnProperty(prop) === true) {
										value = obj[prop];
										subPath = pathString + '.' + prop;
										if (value && typeof value === 'object') {
											publish(value, subPath);
										} else {
											/* save path and value */
											binder.publish(msg, subPath, value, self);
										}
									}
								}
							}
						} else {
							binder.publish(msg, pathString, obj, self);
						}
						return pathString;
					};

					if (!committer) {
						publish(val, attr);

						if (stopMerge !== true) {
							setValue(self.attributes, attr, val);
						}
						setValue(self.stage, attr, val);
					} else {
						setValue(self.stage, attr, val);

						if (self.dirty === false) {
							self.dirty = true;
						}
					}
				} else {
					if (stopMerge !== true) {
						setValue(self.attributes, attr, val);
					}
					setValue(self.stage, attr, val);
				}

				var message = attr + ':change';
				self.eventSub.publish(message, attr, val);
			};

			var setupObject = function(object, committer, stopMerge) {
				var items = object;
				for (var attr in items) {
					if (items.hasOwnProperty(attr) === true) {
						var item = items[attr];
						if (typeof item === 'function') {
							continue;
						}
						set(attr, item, committer, stopMerge);
					}
				}
			};

			var args = arguments;
			if (typeof args[0] === 'object') {
				setupObject(args[0], args[1], args[2]);
			} else {
				set(args[0], args[1], args[2], args[3]);
			}
		},

		mergeStage: function() {
			this.attributes = JSON.parse(JSON.stringify(this.stage));
			this.dirty = false;
		},

		getModelData: function() {
			this.mergeStage();
			return this.attributes;
		},

		revert: function() {
			this.set(this.attributes);
			this.dirty = false;
		},

		get: function(attrName) {
			if (typeof attrName !== 'undefined') {
				var get = function(obj, attr) {
					var test = /(\.|\[)/;
					if (test.test(attr) === true) {
						var pattern = /(\w+)|(?:\[(\d)\))/g;
						var props = attr.match(pattern);
						for (var i = 0, length = props.length - 1; i < length; i++) {
							var prop = props[i];
							var propValue = obj[prop];
							if (propValue !== undefined) {
								obj = propValue;
							} else {
								break;
							}
						}

						return obj[props[i]];
					} else {
						return obj[attr];
					}
				};
				return get(this.stage, attrName);
			} else {
				return this.getModelData();
			}
		}
	};

	var setupAttrSettings = function(settings) {
		var attributes = {};
		if (settings && typeof settings === 'object') {
			for (var prop in settings) {
				if (settings.hasOwnProperty(prop) === true) {
					var setting = settings[prop];
					if (typeof setting !== 'function') {
						attributes[prop] = setting;
						delete settings[prop];
					}
				}
			}
		}
		return attributes;
	};

	var setupDefaultAttr = function(settings) {
		var attributes = {};
		if (settings && typeof settings === 'object') {
			var defaults = settings.defaults;
			if (defaults) {
				for (var prop in defaults) {
					if (defaults.hasOwnProperty(prop) === true) {
						var attr = defaults[prop];
						if (typeof attr !== 'function') {
							attributes[prop] = attr;
						}
					}
				}
				delete settings.defaults;
			}
		}
		return attributes;
	};

	var getXhr = function(settings) {
		if (settings && typeof settings === 'object') {
			var settingsXhr = settings.xhr;
			if (settingsXhr && typeof settingsXhr === 'object') {
				var xhr = base.createObject(settingsXhr);
				delete settings.xhr;
				return xhr;
			}
		}
		return {};
	};

	var modelTypeNumber = 0;

	Model.extend = function(settings) {
		var parent = this;

		var xhr = getXhr(settings);
		var modelService = this.prototype.xhr.extend(xhr);

		settings = settings || {};

		var defaultAttributes = setupDefaultAttr(settings);
		var model = function(instanceSettings) {
			if (instanceSettings && typeof instanceSettings === 'object') {
				instanceSettings = JSON.parse(JSON.stringify(instanceSettings));
			}

			var instanceAttr = setupAttrSettings(instanceSettings);

			instanceAttr = base.extendObject(defaultAttributes, instanceAttr);
			parent.call(this, instanceAttr);

			this.xhr = new modelService(this);
		};

		model.prototype = base.extendClass(this.prototype, settings);
		model.prototype.constructor = model;
		model.prototype.xhr = modelService;

		model.prototype.modelTypeId = 'bm' + (modelTypeNumber++);

		base.extendObject(parent, model);
		return model;
	};

	base.extend.Model = Model;

	/* ModelService */
	var ModelService = function(model) {
		this.model = model;
		this.url = '';

		this.setup();
	};

	ModelService.prototype = {
		constructor: ModelService,

		setup: function() {
			var model = this.model;
			if (model && model.url) {
				this.url = model.url;
			}
		},

		validateCallBack: null,

		isValid: function() {
			var result = this.validate();
			if (result === false) {
				var callBack = this.validateCallBack;
				if (typeof callBack === 'function') {
					callBack(result);
				}
			}
			return result;
		},

		/* this is a method to use to override to set
		a default validate methode to check the model
		before sending data.
		@return (bool) true or false to submit */
		validate: function() {
			return true;
		},

		/* this is a method to use to override to set
		a default param string to be appended to each
		xhr request
		@return (string) params */
		getDefaultParams: function() {
			return {};
		},

		/* this will add the individual params with the default
		params.
		@param (string) params
		@return (string) the appended params */
		setupParams: function(params) {
			var defaults = this.getDefaultParams();
			params = this.addParams(params, defaults);
			return params;
		},

		parseQueryString: function(str) {
			str = str || '';
			var objURL = {},
				regExp = /([^?=&]+)(=([^&]*))?/g;

			str.replace(regExp, function(a, b, c, d) {
				objURL[b] = d;
			});

			return objURL;
		},

		/* this will add params strings.
		@param (string) params
		@param (string) addingParams
		@return (string) the params combined */
		addParams: function(params, addingParams) {

			params = params || {};
			if (typeof params !== 'object') {
				params = this.parseQueryString(params);
			}

			addingParams = addingParams || {};
			if (typeof addingParams === 'string') {
				addingParams = this.parseQueryString(addingParams);
			}

			return base.extendObject(params, addingParams);
		},

		objectType: 'item',

		getObject: function(response) {
			var object = response['data'] || response[this.objectType] || response;
			return (object) ? object : false;
		},

		request: function(url, params, instanceParams, callback, requestCallBack) {
			params = this.setupParams(params);

			params = this.addParams(params, instanceParams);

			var self = this;
			return base.ajax({
				url: url,
				params: params,
				completed: function(response, xhr) {
					if (typeof requestCallBack === 'function') {
						requestCallBack(response);
					}

					self.getResponse(response, callback, xhr);
				}
			});
		},

		getResponse: function(response, callback, xhr) {
			if (typeof callback === 'function') {
				callback(response, xhr);
			}
		},

		_get: function(url, params, callback) {
			var self = this;

			return base.ajax({
				type: 'GET',
				url: url,
				params: params,
				completed: callback
			});
		},

		_post: function(url, params, callback) {
			params = this.clean(params);

			return base.ajax({
				type: 'POST',
				url: url,
				params: params,
				completed: callback
			});
		},

		_put: function(url, params, callback) {
			params = this.clean(params);

			return base.ajax({
				type: 'PUT',
				url: url,
				params: params,
				completed: callback
			});
		},

		_delete: function(url, params, callback) {

			return base.ajax({
				type: 'DELETE',
				url: url,
				params: params,
				completed: callback
			});
		},

		clean: function(params) {
			var newparams = {};

			for (var attr in params) {
				if (params.hasOwnProperty(attr) && params[attr] !== null) {
					newparams[attr] = params[attr];
				}
			}

			return newparams;
		}
	};

	/* this will setup the service object to extend itself.
	@param (object) settings = the new service settings to
	extend
	@return (object) the new model service */
	ModelService.extend = function(settings) {
		var parent = this;

		/* this will setup a new model service constructor */
		var modelService = function(model) {
			/* this will call the parent contructor */
			parent.call(this, model);
		};

		/* this will extend the model and add the static
		methods to the new object */
		modelService.prototype = base.extendClass(this.prototype, settings);
		modelService.prototype.constructor = modelService;

		base.extendObject(parent, modelService);

		return modelService;
	};

	/* we need to add the service object to the
	model prototype as the xhr service */
	Model.prototype.xhr = ModelService;
})();

/* Component */
(function() {
	'use strict';

	/*
		Component

		this will allow components to be extend
		from a single factory.

		example:

		var QuickFlashPanel = function()
		{
			// this will setup the component id
			this.init();
		};

		base.Component.extend(
		{
			constructor: QuickFlashPanel
		});

	*/
	var Component = function() {
		this.init();
	};

	/* this will extedn the html builder to allow the
	components to build */
	base.htmlBuilder.extend({
		constructor: Component,

		/* this will setup the component number and unique
		instance id for the component elements.
		@param [(string)] overrideName = the component name to
		override the constructor name */
		init: function(overrideName) {
			var constructor = this.constructor;
			this.number = (typeof constructor.number === 'undefined') ? constructor.number = 0 : (++constructor.number);

			var name = overrideName || constructor.name || this.componentTypeId;
			this.id = name + this.number;
		},

		/* this is a method that should be overridden to
		render the component */
		render: function() {

		},

		/* this is a method that should be overridden to
		setup and render the component */
		setup: function() {
			this.render();
		},

		/* this is a method that should be overridden to
		destroy the component */
		destroy: function() {
			this.removeElement(this.id);
		},

		/* this will bind an element to a property
		of a model.
		@param (object) element
		@param (object) model
		@param (string) prop */
		bind: function(element, model, prop) {
			if (element) {
				base.DataBinder.bind(element, model, prop);
			}
		}
	});

	var componentTypeNumber = 0;

	/* this will allow the components to be extened.
	@param (object) child = the child object to extend
	@return (mixed) the new child prototype or false */
	Component.extend = function(child) {
		/* the child constructor must be set to set
		the parent static methods on the child */
		var constructor = child && child.constructor ? child.constructor : false;
		if (constructor) {
			/* this will add the parent class to the
			child class */
			var parent = this.prototype;
			constructor.prototype = base.extendClass(parent, child);
			constructor.prototype.parent = parent;

			/* this will assign a unique id to the type of
			component */
			constructor.prototype.componentTypeId = 'bs-cp-' + (componentTypeNumber++);

			/* this will add the static methods from the parent to
			the child constructor. could use assign but ie doesn't
			support it */
			//Object.assign(constructor, this);
			base.extendObject(this, constructor);
			return constructor.prototype;
		}
		return false;
	};

	/* this will add a reference to the component
	object */
	base.extend.component = base.extend.Component = Component;

})();

/* touch */
(function() {
	'use strict';

	/* this will add an event listener for touch to an object */
	/*
		@param function callBackFn = the function to be performed
		on the event.
		@param object obj = object to receive the event
	*/
	base.extend.touch = function(callBackFn, obj) {
		/* this will be the object that receievedthe touch listeners */
		this.element = (typeof obj !== 'undefined') ? obj : document;

		this.callBackFunction = callBackFn;

		/* we want to track all the touch settings */
		this.fingerCount = 0;
		this.startPosX = 0;
		this.startPosY = 0;
		this.posX = 0;
		this.posY = 0;
		this.minLength = 72;
		this.swipeLength = 0;
		this.swipeAngle = null;
		this.swipeDirection = null;

		var self = this;

		this.add = function() {
			/* check if the browser supports touch */
			if ('ontouchstart' in document.documentElement) {
				base.addListener('touchstart', self.element, self.touchStart);
				base.addListener('touchmove', self.element, self.touchMove);
				base.addListener('touchend', self.element, self.touchEnd);
				base.addListener('touchcancel', self.element, self.touchCancel);

				return true;
			} else {
				return false;
			}
		};

		this.remove = function() {
			base.removeListener('touchstart', self.element, self.touchStart);
			base.removeListener('touchmove', self.element, self.touchMove);
			base.removeListener('touchend', self.element, self.touchEnd);
			base.removeListener('touchcancel', self.element, self.touchCancel);
		};

		this.touchStart = function(e) {
			// cancel default
			base.preventDefault(e);
			// get the total number of fingers touching the screen
			self.fingerCount = e.touches.length;

			/* only track if one finger is being used*/
			if (self.fingerCount == 1) {
				// get the coordinates of the touch
				self.startPosX = e.touches[0].pageX;
				self.startPosY = e.touches[0].pageY;
			} else {
				// more than one finger touched so cancel
				self.touchCancel(e);
			}
		};

		this.touchMove = function(e) {
			// cancel default
			base.preventDefault(e);

			if (e.touches.length == 1) {
				self.posX = e.touches[0].pageX;
				self.posY = e.touches[0].pageY;
			} else {
				self.touchCancel(e);
			}
		};

		this.touchEnd = function(e) {
			// cancel default
			base.preventDefault(e);

			/* we want to check if there was only one finger used and that we have a last position */
			if (self.fingerCount == 1 && self.posX != 0) {
				// use the Distance Formula to determine the length of the swipe
				self.swipeLength = Math.round(Math.sqrt(Math.pow(self.posX - self.startPosX, 2) + Math.pow(self.posY - self.startPosY, 2)));

				//alert('start ' + self.posX + ' end ' + self.posX + ' ' + self.swipeLength + ' ' + self.minLength)
				// if the user swiped more than the minimum length, perform the appropriate action
				if (self.swipeLength >= self.minLength) {
					calculateAngle();
					getSwipeDirection();

					/* call back function */
					if (self.callBackFunction) {
						self.callBackFunction(self.swipeDirection);
					}

					self.touchCancel(e); // reset the variables
				} else {
					self.touchCancel(e);
				}
			} else {
				self.touchCancel(e);
			}
		};

		this.touchCancel = function(e) {
			self.fingerCount = 0;
			self.startPosX = 0;
			self.startPosY = 0;
			self.posX = 0;
			self.posY = 0;
			self.swipeLength = 0;
			self.swipeAngle = null;
			self.swipeDirection = null;
		};

		var getSwipeDirection = function() {
			if ((self.swipeAngle <= 45) && (self.swipeAngle >= 0)) {
				self.swipeDirection = 'left';
			} else if ((self.swipeAngle <= 360) && (self.swipeAngle >= 315)) {
				self.swipeDirection = 'left';
			} else if ((self.swipeAngle >= 135) && (self.swipeAngle <= 225)) {
				self.swipeDirection = 'right';
			} else if ((self.swipeAngle > 45) && (self.swipeAngle < 135)) {
				self.swipeDirection = 'down';
			} else {
				self.swipeDirection = 'up';
			}
		};

		var calculateAngle = function() {
			var positionX = self.startPosX - self.posX,
				positionY = self.posY - self.startPosY;

			/* we need to get the distance */
			var z = Math.round(Math.sqrt(Math.pow(positionX, 2) + Math.pow(positionY, 2)));

			//angle in radians
			var r = Math.atan2(positionY, positionX);

			//angle in degrees
			self.swipeAngle = Math.round(r * 180 / Math.PI);
			if (self.swipeAngle < 0) {
				self.swipeAngle = 360 - Math.abs(self.swipeAngle);
			}
			;
		};

	};
})();

/* animate */
(function() {
	'use strict';

	/* this is a helper fuction to remove a class and hide
	an element.
	@param (object) obj = the element to hide
	@param (string) animationClass = the css class name */
	var removeClassAndHide = function(obj, animationClass, callBack) {
		obj.style.display = 'none';
		removeAnimationClass(obj, animationClass, callBack);
	};

	/* this is a helper fuction to remove an animation
	class after it has been animated.
	@param (object) obj = the element object
	@param (string) animationClass = the css class name */
	var removeAnimationClass = function(obj, animationClass, callBack) {
		if (typeof callBack === 'function') {
			callBack.call();
		}
		base.removeClass(obj, animationClass);
		base.animate.animating.remove(obj);
	};

	var getElement = function(element) {
		return (typeof element === 'string') ? document.getElementById(element) : element;
	};

	/* this will add and remove css animations */
	base.extend.animate = {

		/* this class tracks all objects being animated and can
		add and remove them when completed */
		animating: {
			objects: [],
			add: function(object, className, timer) {
				this.stopPreviousAnimations(object);
				this.addObject(object, className, timer);
			},

			addObject: function(object, className, timer) {
				if (object) {
					var animation = {
						object: object,
						className: className,
						timer: timer
					};

					this.objects.push(animation);
				}
			},

			remove: function(object) {
				this.removeObject(object);
			},

			removeObject: function(object, removeClass) {
				if (object) {
					var animating = this.checkAnimating(object);
					if (animating !== false) {
						var objects = this.objects;
						var animations = animating;
						for (var i = 0, maxLength = animations.length; i < maxLength; i++) {
							var animation = animations[i];
							/* we want to stop the timer */
							this.stopTimer(animation);

							if (removeClass) {
								/* we want to remove the className */
								base.removeClass(animation.object, animation.className);
							}

							/* we want to remove the animation fron the object array */
							//var indexNumber = this.objects.indexOf(animation);
							var indexNumber = base.inArray(objects, animation);
							if (indexNumber > -1) {
								objects.splice(indexNumber, 1);
							}
						}
					}
				}
			},

			stopTimer: function(animation) {
				if (animation) {
					var timer = animation.timer;
					window.clearTimeout(timer);
				}
			},

			checkAnimating: function(obj) {
				var animationArray = [];

				/* we want to get any timers set for our object */
				var objects = this.objects;
				for (var i = 0, maxLength = objects.length; i < maxLength; i++) {
					var animation = objects[i];
					if (animation.object === obj) {
						animationArray.push(animation);
					}
				}

				return (animationArray.length > 0) ? animationArray : false;
			},

			stopPreviousAnimations: function(obj) {
				/* we want to remove the timers and class names
				from the object */
				this.removeObject(obj, 1);
			},

			reset: function() {
				this.objects = [];
			}
		},

		createAnimation: function(obj, animationClass, duration, callBack, endCallBack) {
			var animationCallBack = base.createCallBack(null, callBack, [obj, animationClass, endCallBack]);

			var timer = window.setTimeout(animationCallBack, duration);
			this.animating.add(obj, animationClass, timer);
		},

		/* this will perform an animation on the object and
		then turn the object to display none after the
		duration */
		out: function(object, animationClass, duration, endCallBack) {
			var obj = getElement(object);
			base.addClass(obj, animationClass);

			this.createAnimation(obj, animationClass, duration, removeClassAndHide, endCallBack);
		},

		/* this will dsiplay the object then perform an animation
		on the object and remove the class after the duration */
		_in: function(object, animationClass, duration, endCallBack) {
			var obj = getElement(object);
			base.addClass(obj, animationClass);
			obj.style.display = 'block';

			this.createAnimation(obj, animationClass, duration, removeAnimationClass, endCallBack);
		},

		/* this will add an animation class on the object then
		it will remove the class when the duration is done */
		set: function(object, animationClass, duration, endCallBack) {
			var obj = getElement(object);
			base.addClass(obj, animationClass);

			this.createAnimation(obj, animationClass, duration, removeAnimationClass, endCallBack);
		}
	};
})();

/* date */
(function() {
	'use strict';

	/* this will enable date and time functions */
	base.extend.date = {

		/* this will store all the month names*/
		monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],

		/* this will store all the day names */
		dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

		/* this will return the name of a day or false
		not found.
		@param [(int)] day = the day to use if nothing
		is set it will use the current day
		@param [(bool)] shortenName = set to true to return only
		first 3 chars of name */
		getDayName: function(day, shortenName) {
			day = (typeof month !== 'undefined') ? day : new Date().getDate();

			var dayName = false;
			/* we want to check if the day number is
			less than the array length */
			if (day < this.dayNames.length) {
				/* we want to check to shorten name */
				dayName = (shortenName) ? this.dayNames[day].substring(0, 3) : this.dayNames[day];
			}

			return dayName;
		},

		/* this will check if a year is a leap year
		and return true or false.
		@param (int) year = the year to check */
		leapYear: function(year) {
			var leapYear = false;

			if ((year % 400 === 0) || (year % 100 !== 0 && year % 4 === 0)) {
				leapYear = true;
			}

			return leapYear;
		},

		/* this will return the name of a month or false
		if not found.
		@param [(int)] month = the month to use if nothing
		is set it will use the current month
		@param [(bool)] shortenName = set to true to return only
		first 3 chars of name */
		getMonthName: function(month, shortenName) {
			month = (typeof month !== 'undefined') ? month : new Date().getMonth();

			var monthName = false;
			/* we want to check if the month number is
			less than the array length */
			if (month < this.monthNames.length) {
				/* we want to check to shorten name */
				monthName = (shortenName) ? this.monthNames[month].substring(0, 3) : this.monthNames[month];
			}

			return monthName;
		},

		/* this will return the length of a month.
		@param [(int)] month = the month to use if nothing
		is set it will use the current month
		@param [(int)] year = the year to use if nothing
		is set it will use the current year */
		getMonthLength: function(month, year) {
			/* we want to check to use params or use
			default */
			month = (typeof month !== 'undefined') ? month : new Date().getMonth();
			year = (typeof year !== 'undefined') ? year : new Date().getFullYear();

			/* we need to get the month lengths for
			the year */
			var yearMonthLengths = this.getMonthsLength(year);

			/* we can select the month length from
			the yearMonthLengths array */
			var monthLength = yearMonthLengths[month];

			return monthLength;

		},

		/* this will return an array with all the month
		lengths for a year.
		@params [(int)] year = the year to use if nothing
		is set it will use the current year */
		getMonthsLength: function(year) {
			year = (typeof year !== 'undefined') ? year : new Date().getFullYear();

			/* we needto check if the year is a leap year */
			var isLeapYear = this.leapYear(year);
			var days = (isLeapYear === true) ? [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31] : [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

			return days;
		},

		toYears: function(milliseconds) {
			if (typeof milliseconds === 'number') {
				return Math.floor(milliseconds / (1000 * 60 * 60 * 24 * 365.26));
			}
			return false;
		},

		toDays: function(milliseconds) {
			if (typeof milliseconds === 'number') {
				return Math.floor(milliseconds / (60 * 60 * 1000 * 24) * 1);
			}
			return false;
		},

		toHours: function(milliseconds) {
			if (typeof milliseconds === 'number') {
				return Math.floor((milliseconds % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1);
			}
			return false;
		},

		toMinutes: function(milliseconds) {
			if (typeof milliseconds === 'number') {
				return Math.floor(((milliseconds % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1);
			}
			return false;
		},

		toSeconds: function(milliseconds) {
			if (typeof milliseconds === 'number') {
				return Math.floor((((milliseconds % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1);
			}
			return false;
		},

		/* this will get the difference between two dates
		and return a time object with the difference in
		years, days, hours, minutes, seconds, milliseconds.
		@param (date) startDate = the starting date
		@param (date) endDate = the end date */
		getDifference: function(startDate, endDate) {
			/* we want to convert the dates to objects */
			var start = new Date(startDate),
				end = new Date(endDate);

			/* we want to subtract the start time from the end */
			var difference = (end.getTime() - start.getTime());

			return {
				years: this.toYears(difference),
				days: this.toDays(difference),
				hours: this.toHours(difference),
				minutes: this.toMinutes(difference),
				seconds: this.toSeconds(difference)
			};
		}
	};
})();

/* history */
(function() {
	'use strict';

	/* this will update the page title */
	var updateTitle = function(title) {
		if (typeof title === 'string') {
			document.title = title;
		}
	};

	/* this will check if the user is sending the title
	by the state object because most browsers dont use
	the current title param.
	@param (string) title = the new title
	@param (object) stateObj = the new state object */
	var getStateTitle = function(title, stateObj) {
		return (title === null && (stateObj && stateObj.title)) ? stateObj.title : title;
	};

	/* this will add and remove states from window history */
	base.extend.history = {

		/* this will check if history api is supported and if so
		return true else return false */
		isSupported: function() {
			if ('history' in window && 'pushState' in window.history) {
				return true;
			} else {
				return false;
			}
		},

		/* this will add and event listener for window popstate.
		@param (function) fn = the function to use as callback
		@param [(bool)] capture = event capture */
		addEvent: function(fn, capture) {
			/* this will check if the current state has a
			title property to update thepage title */
			var popEvent = function(e) {
				var state = e.state;
				if (state && state.title) {
					updateTitle(state.title);
				}

				fn(e);
			};

			base.events.add('popstate', window, popEvent, capture, true, fn);
		},

		/* this will remove and event listener for window popstate.
		@param (function) fn = the function to use as callback
		@param [(bool)] capture = event capture */
		removeEvent: function(fn, capture) {
			base.off('popstate', window, fn, capture);
		},

		/* this will add a state to the window history
		@param (object) object = the state object
		@param (string) title = the state page title
		@param (string) url = the state url */
		pushState: function(object, title, url) {
			var history = window.history,
				lastState = history.state;

			/* we want to check if the object is not already
			the last saved state */
			if (!lastState || base.equals(lastState, object) === false) {
				title = getStateTitle(title, object);
				updateTitle(title);

				/* we want to add the new state to the window history*/
				history.pushState(object, title, url);
			}
		},

		/* this will add a state to the window history
		@param (object) object = the state object
		@param (string) title = the state page title
		@param (string) url = the state url */
		replaceState: function(object, title, url) {
			title = getStateTitle(title, object);
			updateTitle(title);

			/* we want to add the new state to the window history*/
			window.history.replaceState(object, title, url);
		},

		/* this will go to the next state in the window history */
		nextState: function() {
			window.history.forward();
		},

		/* this will go to the previous state in the window history */
		prevState: function() {
			window.history.back();
		},

		/* this will take you to a specified number in the history
		index.
		@param (int) indexNumber= the number to go to */
		goTo: function(indexNumber) {
			window.history.go(indexNumber);
		}
	};
})();

/* router */
(function() {
	'use strict';

	var routerNumber = 0;

	/*
		router

		this will add client side routing to allow routes to be
		setup and the router to navigate and activate the routes
		that match the uri.

		@param (string) baseURI = the root of the router uri
		@param (string) title = the root title
	*/
	var router = function(baseURI, title) {
		/* this is the root of the uri for the routing object
		and the base title */
		this.baseURI = baseURI || '/';
		this.title = (typeof title !== 'undefined') ? title : '';
		this.number = routerNumber++;

		/* this will be used to accessour history object */
		this.history = null;

		/* this will store each route added to the
		router. */
		this.routes = [];

		/* this will setup the router and check
		to add the history support */
		this.setup();
	};

	router.prototype = {
		constructor: router,

		/* this will setup our child history object and
		create a closure to allow our child to retain
		the parents scope */
		setupHistory: function() {
			var parent = this;

			this.history = {
				/* this will check if the history api is supported
				and enabled */
				enabled: false,

				locationId: 'base-app-router-' + parent.number,

				callBack: null,

				/* this will check to add history support is
				the browser supports it */
				setup: function() {
					/* we want to check if history is enabled */
					this.enabled = base.history.isSupported();

					/* we want to check to add the history event listener
					that will check the popsate events and select the
					nav option by the history state object */
					if (this.enabled === true) {
						this.callBackLink = base.bind(this, this.checkLink);
						this.callBack = base.bind(this, this.checkHistoryState);
						this.addEvent();
					}
					return this;
				},

				callBackLink: null,

				checkLink: function(evt) {
					var target = evt.target || evt.srcElement;
					var nodeName = target.nodeName.toLowerCase();
					if (nodeName !== 'a') {
						/* this will check to get the parent to check
						if the child is contained in a link */
						target = target.parentNode;
						if (target === null) {
							return true;
						}

						nodeName = target.nodeName.toLowerCase();
						if (nodeName !== 'a') {
							return true;
						}
					}

					if (!base.data(target, 'cancel-route')) {
						var href = target.getAttribute('href');
						if (href) {
							var path = href.replace(parent.baseURI, '');
							parent.navigate(path);

							evt.preventDefault();
							evt.stopPropagation();
							return false;
						}
					}
				},

				/* this will add an event history listener that will
				trigger when the window history is changed */
				addEvent: function() {
					base.on('click', document, this.callBackLink);

					base.history.addEvent(this.callBack);
					return this;
				},

				removeEvent: function() {
					base.off('click', document, this.callBackLink);

					base.history.removeEvent(this.callBack);
					return this;
				},

				checkHistoryState: function(evt) {
					/* we want to check if the event has a state and if the
					state location is from the background */
					var state = evt.state;
					if (state && state.location === this.locationId) {
						base.preventDefault(evt);
						base.stopPropagation(evt);

						parent.checkActiveRoutes(state.uri);
					}
				},

				createStateObject: function(uri, data) {
					var stateObj = {
						location: this.locationId,
						uri: uri
					};

					if (data && typeof data === 'object') {
						stateObj = base.extendObject(stateObj, data);
					}

					return stateObj;
				},

				addState: function(uri, data, replace) {
					if (this.enabled === true) {
						uri = parent.createURI(uri);
						var lastState = window.history.state;

						/* we want to check if the object is not already
						the last saved state */
						if (!lastState || lastState.uri !== uri) {
							var stateObj = this.createStateObject(uri, data);

							/* this will check to push state or
							replace state */
							replace = replace === true ? true : false;
							var method = (replace === false) ? 'pushState' : 'replaceState';
							history[method](stateObj, null, uri);
						}
					}

					parent.activate();
					return this;
				}
			};

			this.history.setup();
		},

		/* this will add a route to the router.
		@param (string) uri = the route uri
		@param (string) template = the template name
		@param [(function)] callBack = the call back function
		@param [(string)] title = the route title
		@param [(string)] id = the route id
		@return (object) reference to the object */
		add: function(uri, template, callBack, title, id) {
			if (typeof uri === 'string') {
				/* this will setup the route uri string to be used in
				a regexp match.
				@param (string) uri = the route uri
				@return (string) the uri to be used in a regexp match */
				var setupMatch = function(uri) {
					var uriQuery = '';
					if (uri) {
						/* we want to setup the wild card and param
						checks to be modified to the route uri string */
						var allowAll = /(\*)/g,
							param = /(:[^\/]*)/g,
							optionalParams = /(\?\/+\*?)/g,
							optionalSlash = /(\/):[^\/]*?\?/g,
							filter = /\//g;
						uriQuery = uri.replace(filter, '\/').replace(allowAll, '.*');

						/* this will setup for optional slashes before the optional params */
						uriQuery = uriQuery.replace(optionalSlash, function(str) {
							var pattern = /\//g;
							return str.replace(pattern, '\/*');
						});

						/* this will setup for optional params and params
						and stop at the last slash or query start */
						uriQuery = uriQuery.replace(optionalParams, '?\/*').replace(param, '([^\/|?]*)');
					}

					/* we want to set and string end if the wild card is not set */
					uriQuery += (uri[uri.length - 1] === '*') ? '' : '$';
					return uriQuery;
				};

				/* this will get the param names from the route uri.
				@param (string) uri = the route uri
				@return (array) an array with the param keys */
				var setupParamKeys = function(uri) {
					var params = [];
					if (uri) {
						var pattern = /:(.[^\/]*)/g,
							matches = uri.match(pattern);
						if (matches) {
							for (var i = 0, maxLength = matches.length; i < maxLength; i++) {
								var param = matches[i];
								if (param) {
									pattern = /(:|\?)/g;
									var filter = /\*/g;
									/* we need to remove the colon, question mark, or asterisk
									 from the key name */
									param = param.replace(pattern, '').replace(filter, '');
									params.push(param);
								}
							}
						}
					}
					return params;
				};

				/* we need to format the uri and setup the uri query
				reg exp that is used to check the route */
				uri = this.removeSlashes(uri);
				var uriQuery = '^' + this.createURI(setupMatch(uri));
				var routes = this.routes;

				var route = {
					uri: uri,
					template: template,
					component: '',
					callBack: callBack,
					title: title,
					uriQuery: uriQuery,
					paramKeys: setupParamKeys(uri),
					params: null,
					id: id || 'bs-rte-' + routes.length,
					setup: false
				};
				routes.push(route);
			}
			return this;
		},

		/* this will get a route by the route settings.
		@param (string) uri = the route uri
		@param (string) template = the template name
		@param [(function)] callBack = the call back function
		@return (mixed) the routeobject or false on error */
		getRoute: function(uri, template, callBack) {
			var routes = this.routes,
				length = routes.length;
			if (length) {
				for (var i = 0; i < length; i++) {
					var route = routes[i];
					if (route.uri === uri && route.template === template && route.callBack === callBack) {
						return route;
					}
				}
			}
			return false;
		},

		/* this will remove a route from the router by the route settings.
		@param (string) uri = the route uri
		@param (string) template = the template name
		@param [(function)] callBack = the call back function
		@return (object) reference to the object */
		remove: function(uri, template, callBack) {
			uri = this.removeSlashes(uri);
			var route = this.getRoute(uri, template, callBack);
			if (route) {
				var index = base.inArray(this.routes, route);
				if (index > -1) {
					this.routes.splice(index, 1);
				}
			}
			return this;
		},

		/* this will setup the history object
		@return (object) reference to the object */
		setup: function() {
			this.setupHistory();

			/* this will route to the first url entered
			when the router loads. this will fix the issue
			that stopped the first endpoint from being
			added to the history */
			var endPoint = this.getEndPoint() || '/';
			this.navigate(endPoint, null, true);
			return this;
		},

		/* this will reset the router
		@return (object) reference to the object */
		reset: function() {
			this.routes = [];
			return this;
		},

		/* this will activate any active routes
		@return (object) reference to the object */
		activate: function() {
			this.checkActiveRoutes();
			return this;
		},

		/* this will navigate the router to the uri.
		@param (string) uri = the relative uri
		@param [(object)] data = a data object that will
		be added to the history state object
		@param [(bool)] replace = settrue to replace state
		@return (object) a reference to the router object */
		navigate: function(uri, data, replace) {
			this.history.addState(uri, data, replace);
			return this;
		},

		/* this will update the window title with the route title.
		@param (object) route = a route that has a title
		@return (object) a reference to the router object */
		updateTitle: function(route) {
			if (route && route.title) {
				var title = route.title,
					parent = this;

				var getTitle = function(title) {

					/* this will uppercase each word in a string
					@param (string) str = the string to uppercase
					@return (string) the uppercase string */
					var toTitleCase = function(str) {
						var pattern = /\w\S*/;
						return str.replace(pattern, function(txt) {
							return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
						});
					};

					/* this will replace the params in the title
					@param (string) str = the route title
					@return (string) the title string */
					var replaceParams = function(str) {
						if (str.indexOf(':') > -1) {
							var params = route.params;
							for (var prop in params) {
								if (params.hasOwnProperty(prop) === true) {
									var param = params[prop];
									var pattern = new RegExp(':' + prop, 'gi');
									str = str.replace(pattern, param);
								}
							}
						}
						return str;
					};

					if (title) {
						/* we want to replace any params in the title
						and uppercase the title */
						title = replaceParams(title);
						var pattern = /-/g;
						title = toTitleCase(title.replace(pattern, ' '));

						/* we want to check to add the base title to the
						to the end of the title */
						if (parent.title != '') {
							title += ' - ' + parent.title;
						}
					}
					return title;
				};

				document.title = getTitle(title);
			}
			return this;
		},

		/* this will get all active routes from a path.
		@param [(string)] path = the path or the current
		url path will be used
		@return (array) an array of active routes */
		checkActiveRoutes: function(path) {
			var active = [],
				routes = this.routes,
				length = routes.length;
			if (length) {
				path = path || this.getPath();

				for (var i = 0; i < length; i++) {
					var route = routes[i];
					if (typeof route !== 'undefined') {
						var check = this.check(route, path);
						if (check !== false) {
							active.push(route);

						} else {
							if (route.template && route.setup === true && route.component) {
								routeComponents.remove(route);
							}
						}
					}
				}
			}
			return active;
		},

		/* this will remove the begining and ending slashes on a url.
		@param (string) uri = the uri to remove
		@return (string) the uri */
		removeSlashes: function(uri) {
			if (typeof uri === 'string') {
				var pattern = /(^\/|\/$)/g;
				uri = uri.toString().replace(pattern, '');
			}

			return uri;
		},

		/* this will create a uri with the base path and
		the route uri.
		@param (string) uri = the uri
		@return (string) the uri */
		createURI: function(uri) {
			var pathURI = '';
			if (this.baseURI !== '/') {
				pathURI += this.baseURI;
			}
			pathURI += ((pathURI[pathURI.length - 1] !== '/') ? '/' : '') + this.removeSlashes(uri);

			return pathURI;
		},

		/* this will check to select a route if it the route uri
		matches the path.
		@param (object) route = the route
		@param [(string)] path = the path. if left null the
		active path will beused
		@return (bool) true or false if active */
		check: function(route, path) {
			var matched = false;

			/* we want to check to use the supplied uri or get the
			current uri if not setup */
			path = path || this.getPath();

			/* we want to check if the route uri matches the path uri */
			var validURI = this.match(route, path);
			if (validURI) {
				matched = true;
				this.select(route);
			}
			return matched;
		},

		/* this will match a route if it the route uri
		matches the path.
		@param (object) route = the route
		@param [(string)] path = the path. if left null the
		active path will beused
		@return (bool) true or false if active */
		match: function(route, path) {
			/* we want to check if the route has been
			deleted from the routes */
			if (!route) {
				return false;
			}

			/* this will get the param names from the route uri.
			@param (string) paramKeys = the route param keys
			@param (array) values = the path param values
			@return (mixed) an empty object or an object with key
			and values of the params */
			var getParams = function(paramKeys, values) {
				var keys = paramKeys,
					params = {};

				if (values && typeof values === 'object') {
					/* we want to add the value to each key */
					if (keys) {
						for (var i = 0, maxL = keys.length; i < maxL; i++) {
							var key = keys[i];
							if (typeof key !== 'undefined') {
								params[key] = values[i];
							}
						}
					}
				}
				return params;
			};

			var matched = false;

			/* we want to check to use the supplied uri or get the
			current uri if not setup */
			path = path || this.getPath();

			/* we need to setup the uri reg expression to match the
			route uri to the path uri */
			var routeURI = route.uriQuery;

			/* we want to check if the route uri matches the path uri */
			var validURI = path.match(routeURI);
			if (validURI) {
				matched = true;

				/* this will remove the first match from the
				the params */
				if (validURI && typeof validURI === 'object') {
					validURI.shift();
					matched = validURI;
				}

				/* this will get the uri params of the route
				and if set will save them to the route */
				var params = getParams(route.paramKeys, validURI);
				if (params) {
					route.params = params;
				}
			}
			return matched;
		},

		/* this will get the param names from the route uri.
		@param (object) route = the route object */
		select: function(route) {
			if (route) {
				var params = route.params || null;
				if (typeof route.callBack === 'function') {
					route.callBack.call({}, params);
				}

				if (route.template) {
					/* this will check to setup the component if
					the component has notbeen setup */
					if (!route.component && route.setup === false) {
						routeComponents.create(route);
					}

					routeComponents.update(route);
				}

				this.updateTitle(route);
			}
		},

		/* this will return the set endpoint not including the
		base uri.
		@return (string) the last endpoint */
		getEndPoint: function() {
			var path = this.getPath();
			return path.replace(this.baseURI, '');
		},

		/* this will get the location pathname.
		@return (string) the location pathname */
		getPath: function() {
			/* we want to get the window location path */
			return window.location.pathname;
		}
	};

	var routeComponents = {
		/* this will create and setup a component
		from a route template.
		@param (object) route */
		create: function(route) {
			route.setup = true;

			if (route.template) {
				var template = window[route.template];
				if (template) {
					var comp = route.component = new template();
					comp.setup();
				}
			}
		},

		/* this will remove a route component
		@param (object) route */
		remove: function(route) {
			route.setup = false;

			var component = route.component;
			if (component) {
				if (typeof component.destroy === 'function') {
					component.destroy();
				}
				route.component = null;
			}
		},

		update: function(route) {
			var component = route.component;
			if (component && typeof component.update === 'function') {
				component.update(route.params);
			}
		}
	};

	base.extend.router = router;

})();

(function() {
	'use strict';

	base.extend.formValidator = {
		/* these are the classes that will display on
		the field when validated */
		errorClass: 'error-val',
		acceptedClass: 'success-val',

		/* this will return true or false if an
		email has valid email syntax.
		@param (string) email = the email address
		to validate */
		isValidEmail: function(email) {
			var regExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return regExp.test(email);
		},

		/* this will return true or false if a
		phone is the filled out.
		@param (mixed) phone = the phone number
		to validate */
		isValidPhone: function(phone) {
			var pattern = /[^0-9]/g;
			/* we want to convert to string and remove any marks */
			phone = phone.toString().replace(pattern, '');

			/* we want to check if the phone is a
			number */
			if (!isNaN(phone)) {
				/* we want to check to remove the leading 1 */
				if (phone.substr(0, 1) === '1') {
					phone = phone.substring(1);
				}

				/* we want check the length and block any 555
				area code numbers */
				if (phone.length === 10 && phone.substr(0, 3) !== '555') {
					return true;
				}
			}

			return false;
		},

		/* this will return true or false if a
		date is the filled out.
		@param (string) date = the date
		to validate */
		isValidDate: function(date) {
			if (typeof date !== 'undefined') {
				var result = new Date(date).toDateString();
				if (result !== 'Invalid Date') {
					return true;
				}
			}
			return false;
		},

		/* this will return true or false if an
		radio group is checked.
		@param (string) groupName = the radio
		group name */
		isRadioChecked: function(groupName) {
			if (typeof groupName !== 'undefined') {
				var radios = document.getElementsByName(groupName);
				if (radios && radios.length) {
					for (var i = 0, maxLength = radios.length; i < maxLength; i++) {
						var radio = radios[i];
						if (radio.type === 'radio' && radio.checked) {
							return true;
						}
					}
				}
			}
			return false;
		},

		/* this will return true or false if a field
		has a value.
		@param (mixed) val = the value to validate */
		isValidField: function(val) {
			if (typeof val !== 'undefined' && val != '') {
				return true;
			}
			return false;
		},

		/* this will validate a form and return an error object
		with the number of errors and an error message.
		@param (object) form = the form to validate
		@param [(string)] uniqueFormId = specify any unique prefix that
		should be remove from the error message
		@return (object) a response object with the error
		number and message */
		validateForm: function(form, uniqueFormId) {
			uniqueFormId = uniqueFormId || '';
			/* we want to save a reference of our object
			to use with any call backs */
			var self = this;

			/* this will save the errors and the error message */
			var errors = {
				number: 0,
				message: ''
			};

			/* this will update add a field to the error object
			@param (object) node = the node element object */
			var updateError = function(node) {
				/* this will uppercase each word */
				var upperCaseWords = function(str) {
					var pattern = /\w\S*/g;
					return str.replace(pattern, function(txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
				};

				/* we want to get something to identify the field */
				var fieldName = base.attr(node, 'placeholder') || base.attr(node, 'name') || base.attr(node, 'id');
				if (fieldName) {
					if (uniqueFormId !== '') {
						fieldName = fieldName.replace(uniqueFormId, '');
					}
					var pattern = /[^a-zA-Z1-9]/g;
					fieldName = fieldName.replace(pattern, ' ');
					fieldName = upperCaseWords(fieldName);
				}
				/* we want to increase the error count
				and save an error message for the field */
				errors.number++;
				errors.message += '<br>' + fieldName + ' is empty or invaid.';
			};

			if (typeof form === 'object') {
				/* we want to track a radio so we do keep tracking
				the same group after the first check */
				var previousRadios = [];

				var nodes = base.find(form, 'input.val, select.val, textarea.val');
				if (nodes) {
					for (var i = 0, maxLength = nodes.length; i < maxLength; i++) {
						/* we want to cache the object and settings */
						var node = nodes[i],
							nodeName = node.nodeName.toLowerCase();

						/* we want to check if the input is a radio */
						if (nodeName === 'input' && base.attr(node, 'type') === 'radio') {
							var groupName = node.name;

							/* we want to check if the radio has already
							been checked */
							if (base.inArray(previousRadios, groupName) == '-1') {
								/* we want to add this group to the previous
								radio array to stop future checks */
								previousRadios.push(groupName);

								/* we want to validate the field and check
								the status */
								var validField = self.isRadioChecked(groupName);
								if (validField == false) {
									updateError(node);
								}
							}
						} else {
							/* we want to validate the field and check
							the status */
							var validField = self.validateField(node);
							if (validField == false) {
								updateError(node);
							}
						}
					}
				}
			}

			return errors;
		},

		/* this will validate a field and return true or false.
		@param (object) field = the field element */
		validateField: function(field) {
			var self = this;

			/* this will set a field to show a validated style
			after the field is validated.
			@param (object) field = the field object
			@param (bool) isValid = set true if the field is valid */
			var showValidateStyle = function(field, isValid) {
				/* we want to check if the field was valid */
				if (isValid == true) {
					/* we want to update the class to reflect the
					valid status */
					base.removeClass(field, self.errorClass);
					base.addClass(field, self.acceptedClass);
					return true;
				} else {
					/* we want to show the field is invalid
					and save */
					base.addClass(field, self.errorClass);
					base.removeClass(field, self.acceptedClass);

					return false;
				}
			};

			var returnValue = false;

			if (field) {
				var val = field.value,
					type = base.attr(field, 'type'),
					placeholder = base.attr(field, 'placeholder') || base.attr(field, 'alt');

				if (type === 'checkbox') {
					var validField = field.checked;
					returnValue = showValidateStyle(field, validField);
				} else if (!self.isValidField(val) || val === placeholder) {
					/* if the field is empty or has  same value as the
					placeholder text we can set the style to error */
					returnValue = showValidateStyle(field, false);
				} else if (type === 'email') {
					/* we need to validate the email */
					var validField = self.isValidEmail(val);
					returnValue = showValidateStyle(field, validField);

				} else if (type === 'tel') {
					/* we need to validate the phone */
					var validField = self.isValidPhone(val);
					returnValue = showValidateStyle(field, validField);
				} else if (type === 'date') {
					/* we need to validate the phone */
					var validField = self.isValidDate(val);
					returnValue = showValidateStyle(field, validField);
				} else {
					returnValue = showValidateStyle(field, true);
				}
			}

			return returnValue;
		},

		/* this will remove all the validation styles from a form
		@param (object) form = the form element */
		resetForm: function(form) {
			if (form && typeof form === 'object') {
				var elements = form.elements;
				if (elements) {
					for (var i = 0, maxLength = elements.length; i < maxLength; i++) {
						var element = elements[i];
						this.removeStyles(element);
					}
				}
			}
		},

		/* this will remove the styles from a field
		@param (object) field = the element object */
		removeStyles: function(field) {
			if (field) {
				base.removeClass(field, this.errorClass);
				base.removeClass(field, this.acceptedClass);
			}
		}

	};
})();
