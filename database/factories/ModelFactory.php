<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// TODO: Review fields and roles
$factory->define(App\User::class, function (Faker\Generator $faker) {
    $hasher = app()->make('hash');

    $first = $faker->firstName;
    $last  = $faker->lastName;

    $username = strtolower("$first.$last");

    return [
        'username'     => $username,
        'email'        => "$username@" . $faker->domainName,
        'first'        => $first,
        'last'         => $last,
        'role'         => $faker->randomElement(['restricted', 'user', 'admin']),
        'allow_access' => true,
        'password'     => $hasher->make('secret'),
    ];
});
