var UserList = function(parent, container) {
	this.init();

	this.parent = parent;
	this.container = container;

	this.multiselect = true;
};

List.extend({
	constructor: UserList,

	createListRowTitle: function() {
		var layout = {
			class: 'row-list row-title light left',
			children: [
				{
					class: 'col-check dark center',
					chk: {
						node: 'input', type: 'checkbox', id: this.id + '_title_checkbox', onchange: base.bind(this, this.changeCheckAll)
					}
				},
				{ class: 'col light left', textContent: 'ID' },
				{ class: 'col light left', textContent: 'Username' },
				{ class: 'col light left', textContent: 'Email' },
				{ class: 'col light left', textContent: 'Name' },
				{ class: 'col light left', textContent: 'Role' },
				{ class: 'col light center', textContent: 'Allow Access' }
			]
		};

		base.parseLayout(layout, this.container);
	},

	createListRow: function(user) {
		var parent = this.parent;

		var bind = base.bind(this, this.selectOption);
		var editBind = base.bind(parent, parent.edit);

		var name = base.getValue(user, 'first') + ' ' + base.getValue(user, 'last');
		var allow = base.getValue(user, 'allow_access');

		var allowClass = allow === true ? 'allow' : 'dont-allow';

		var layout = {
			id: user.optionRowId, class: 'row-list', onclick: function() { editBind(user); },
			children: [
				{
					class: 'col-check dark center',
					cb: parent.getCheckboxLayout(user.checkboxId, '', function() { bind(user); }, this.isChecked(user), true)
				},
				{ class: 'col', textContent: base.getValue(user, 'id') },
				{ class: 'col', textContent: base.getValue(user, 'username') },
				{ class: 'col', textContent: base.getValue(user, 'email') },
				{ class: 'col', textContent: name },
				{ class: 'col capitalize', textContent: base.getValue(user, 'role') },
				{ class: 'col capitalize center ' + allowClass, textContent: allow === true ? 'Yes' : 'No' }
			]
		};

		base.parseLayout(layout, this.container);
	}
});
