'use strict';

/* EditModal */
var EditModal = function(container) {
	this.init();

	this.container = container;
};

Modal.extend({
	constructor: EditModal,

	setup: function() {
		this.render();
		this.setupHeaderOptions();
	},

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'modal modal-edit-panel', '', frag);

		this.createHeader(panel);

		var container = this.addElement('div', id + '_list_container', 'body-container', '', panel);

		this.createFooter(panel);
		this.append(this.container, frag);
	},

	createRowInputMedium: function(type, id, label, value, container, callBackFn, validate) {
		/* we want to make the callback optional */
		callBackFn = callBackFn || '';

		var className = (validate == true) ? 'val' : '';
		var tmpRow = this.addElement('div', '', 'row light left', '', container),
			tmpLabel = this.addElement('label', '', '', label, tmpRow),
			field = this.addInput(type, id, 'medium ' + className, value, callBackFn, '', tmpRow);

		return field;
	},

	createRowInputCsz: function(label, cityId, cityValue, stateId, stateValue, zipId, zipValue, container, validate) {
		var className = (validate == true) ? 'val' : '';
		var tmpRow = this.addElement('div', '', 'row light left', '', container),
			tmpLabel = this.addElement('label', '', '', label, tmpRow),
			field = this.addInput('text', cityId, 'city ' + className, cityValue, '', '', tmpRow);
		field = this.addInput('text', stateId, 'state ' + className, stateValue, '', '', tmpRow);
		field = this.addInput('text', zipId, 'zip ' + className, zipValue, '', '', tmpRow);

		return field;
	},

	createRowSelectTime: function(label, hourId, hourValue, minuteId, minuteValue, meridiemId, meridiemValue, container, validate) {
		var className = (validate == true) ? 'val' : '';
		var tmpRow = this.addElement('div', '', 'row light left', '', container),
			tmpLabel = this.addElement('label', '', '', label, tmpRow),
			field = this.addSelect(hourId, 'hour', hourValue, '', tmpRow);
		field = this.addSelect(minuteId, 'hour', minuteValue, '', tmpRow);
		field = this.addSelect(meridiemId, 'hour', meridiemValue, '', tmpRow);

		return field;
	},

	addSearchPanel: function() {
		return new ModalSearchPanelController();
	},

	checkAccessLevel: function() {
		return app.hasAccessLevel(this.accessLevel);
	}
});

var ModalSearchPanelController = function() {
	this.panel = null;

	this.callBackFn = null;
	this.titleLabel = '';
	this.xhr = null;
};

base.Class.extend({
	constructor: ModalSearchPanelController,

	setup: function(titleLabel, callBackFn) {
		if (typeof callBackFn === 'function') {
			this.callBackFn = callBackFn;
		}

		if (typeof titleLabel === 'string') {
			this.titleLabel = titleLabel;
		}
	},

	createPanel: function() {
		this.panel = null;

		this.panel = new ModalSearchPanel(this.titleLabel, 'no', base.bind(this, this.search), base.bind(this, this.callBackFn), app.backgroundPanel.id);
		this.panel.setup();
	},

	search: function(tmpSearch) {
	},

	searchResults: function(response) {
		this.xhr = null;
		if (typeof response !== 'string') {
			var result = response;
			this.panel.updateOptions(result);
		}
	},

	cancelPreviousXhr: function() {
		if (this.xhr !== null) {
			this.xhr.abort();
		}
	},

	display: function() {
		this.createPanel();
		this.panel.display();
	}
});
