var LoginPanel = function(resume, iconURL, callBackFn, container) {
	this.init();

	this.resume = resume;
	this.iconURL = iconURL;
	this.callBackFn = callBackFn;
	this.container = container;

	this.state = null;
	this.secured = true;
	this.model = new AuthModel();
};

base.Component.extend({
	constructor: LoginPanel,

	render: function() {
		var id = this.id;

		var frag = this.createDocFragment();

		var panel = this.panel = this.addElement('div', id, 'login-panel', '', frag);
		var panelContainer = this.addElement('div', id + '_main_panel', 'panel-container', '', panel);
		var sectionLogo = this.addElement('section', '', 'logo-container section-container', '', panelContainer);
		var iconContainer = this.addElement('div', '', 'icon-container', '', sectionLogo);
		var iconImage = this.addImage('', 'logo fadeIn', this.iconURL, '', iconContainer);
		var sectionLogin = this.addElement('section', '', 'section-container main-login-container', '', panelContainer);
		var animation = this.secured ? 'pullLeftIn' : '';
		var loginCol = this.addElement('section', '', 'login-col login-container ' + animation, '', sectionLogin);
		var loginContainer = this.addElement('div', id + '_login_area', 'login-area', '', loginCol);
		if (this.resume !== true) {
			base.animate.set(loginContainer, 'flipInY', 3000);
		}
		var loadingContainer = this.loadingContainer = this.addElement('div', id + '_loading_container', 'loading loading-circle', '', loginContainer);

		this.createLoginArea(loginContainer);
		this.createCopyRightPanel(panelContainer);
		this.append(this.container, frag);
	},

	createCopyRightPanel: function(container) {
		var sectionCopyright = this.addElement('section', '', 'section-container copyright-container', '', container);
		var copyrightTable = this.addElement('section', '', 'copyright-table', '', sectionCopyright);
		var copyrightRow = this.addElement('div', '', 'copyright-row', '', copyrightTable);
		copyrightRow = this.addElement('div', '', 'copyright-row', '', copyrightTable);
		var copyrightPanel = this.addElement('section', '', 'copyright-panel pullUp', '', copyrightRow);
		var iconContainer = this.addElement('div', '', 'icon-container', '', copyrightPanel);
		var iconImage = this.addElement('div', '', 'logo fadeIn', '', iconContainer);
		var copyright = this.addElement('div', '', 'copyright', 'Copyright \u00a9' + (new Date).getFullYear() + ' Business Promotion. All rights Reserved.', copyrightPanel);
	},

	createRow: function(container) {
		return this.addElement('div', '', 'row', '', container);
	},

	createInputRow: function(label, model, prop, callBack, container) {
		var row = this.createRow(container);
		var label = this.addElement('label', '', 'left dark', label, row);
		row = this.createRow(container);
		var input = this.addInput('text', '', '', '', '', '', row);
		this.bind(input, model, prop);
		if (typeof callBack === 'function') {
			base.onKeyUp(callBack, input);
		}
		return input;
	},

	createLoginArea: function(container) {
		var textContainer = this.loginContainer = this.addElement('div', this.id + '_details_container', 'text-container', '', container);
		var form = this.addForm(this.id + '_form', 'off', 'POST', '', true, '', '', textContainer);

		var callBack = base.bind(this, this.checkReturnKey);
		var field = this.username = this.createInputRow('Username', this.model, 'username', callBack, form);
		field.name = 'username';
		field.focus();

		field = this.password = this.createInputRow('Password', this.model, 'password', callBack, form);
		field.type = 'password';
		field.name = 'password';

		var row = this.addElement('div', '', 'button-row', '', form);
		var button = this.addButton('button', this.id + '_button', 'bttn bttn-green', 'Sign In', base.bind(this, this.request), row);

		if (this.guestLogin === true) {
			button = this.addButton('button', this.id + '_button', 'bttn bttn-red', 'Cancel', base.bind(this, this.pullOut), row);
		}
	},

	changeState: function(state) {
		this.state = state;
		this.updateState();
	},

	updateState: function() {
		var id = this.id;
		var loginContainer = this.loginContainer;
		var loadingContainer = this.loadingContainer;
		var username = this.username;

		switch (this.state) {
			case 'setup':
				loadingContainer.style.display = 'block';
				loginContainer.style.display = 'none';
				break;
			case 'resume':
				loadingContainer.style.display = 'block';
				loginContainer.style.display = 'none';
				break;
			case 'login':
				loadingContainer.style.display = 'none';
				loginContainer.style.display = 'block';
				username.focus();
				break;
			case 'request':
				loadingContainer.style.display = 'block';
				loginContainer.style.display = 'none';
				break;
		}
	},

	checkReturnKey: function(keyCode) {
		if (keyCode == 13) {
			this.request();
		}
	},

	remove: function() {
		var panel = this.panel;
		if (panel) {
			this.removeElement(panel);
		}
	},

	setup: function() {
		this.render();
		this.changeState('setup');
		this.getResume();
		if (!this.secured) {
			this.changeState('request');
		}
	},

	getResume: function() {
		if (this.resume === true) {
			this.verifyResume();
		} else {
			this.reset();
		}
	},

	request: function() {
		this.changeState('request');

		this.model.xhr.login(base.bind(this, this.checkLoginResponse));
	},

	verifyResume: function() {
		this.changeState('resume');

		this.model.xhr.resume(base.bind(this, this.checkLoginResponse));
	},

	checkLoginResponse: function(response) {
		if (!response) {
			this.error();
		} else if (response.success === false || response.allowAccess === false) {
			this.decline();
		} else {
			this.accept(response.user);
		}
	},

	reset: function() {
		this.changeState('login');
	},

	accept: function(user) {
		this.loadSettings(user);

		window.setTimeout(base.bind(this, this.pullOut), 1000);
	},

	loadSettings: function(user) {
		var callback = this.callBackFn;
		if (typeof callback === 'function') {
			if (this.resume === true) {
				window.setTimeout(function() {
					callback(user);
				}, 800);
			} else {
				callback(user);
			}
		}
	},

	decline: function() {
		var mainPanel = document.getElementById(this.id + '_main_panel');
		base.animate.set(mainPanel, 'fadeInOutRed', 2000);
		var loginArea = document.getElementById(this.id + '_login_area');
		base.animate.set(loginArea, 'flipY', 2000);
		this.reset();
	},

	error: function() {
		var loginArea = document.getElementById(this.id + '_main_panel');
		base.animate.set(loginArea, 'fadeInOutOrange', 2000);
		this.reset();
	},

	pullIn: function() {
		var obj = this.panel;
		obj.style.zIndex = 100;
		base.animate._in(obj, 'pullLeftIn', 800);
	},

	pullOut: function() {
		var obj = this.panel;
		obj.style.zIndex = 100;
		base.animate.out(obj, 'pullLeft', 1000);
	},

	confirmLogin: function() {
		var mpalert = new Alert('confirm', 'Confirm this action', 'Do you really want to do this?', base.bind(this, this.request));
		mpalert.start();
	}
});