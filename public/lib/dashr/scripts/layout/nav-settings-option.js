'use strict';

var NavSettingsOption = function(container) {
	/* we want to be able to build multiple panels in 
	the same system so we need to give each instance 
	a unique id */
	this.init('NavSettingsOption');

	this.label = '';
	this.buttonClass = '';
	this.iconPath = '';
	this.container = container;
};

base.Component.extend(
	{
		constructor: NavSettingsOption,

		setup: function() {
			this.update();
		},

		render: function() {
			var panel = this.addElement('div', this.id, 'nav-overlay-container', '', this.container);
		},

		remove: function() {
			var panel = document.getElementById(this.id);
			if (panel) {
				this.removeElement(panel);
			}
		},

		reset: function() {
			var panel = document.getElementById(this.id);
			if (panel) {
				this.removeAll(panel);
			}
		},

		update: function() {
			this.reset();
			this.render();
		}
	});

var UserNavOption = function(container) {
	this.init('UserNavOption');

	this.label = 'User';
	this.buttonClass = 'user';
	this.iconPath = '';
	this.container = container;
};

NavSettingsOption.extend({
	constructor: UserNavOption,

	render: function() {
		var panel = this.addElement('div', this.id, 'nav-overlay-container user-container', '', this.container);
		var imageContainer = this.addElement('div', this.id + '_user_image_container', 'user-image-container', '', panel);

		var user = appData.user;
		if (user) {
			var image = user.get('image');
			if (image === 'string') {
				var image = this.addImage('', '', '/directory/images/uploads/employee/' + image, 'user image', imageContainer);
			}

			var textContainer = this.addElement('div', '', 'user-name-container', '', panel);
			this.createLabelRow(user, 'first', textContainer);
			this.createLabelRow(user, 'last', textContainer);
		}

		this.addSignOutButton();
	},

	createLabelRow: function(model, prop, container) {
		var label = this.addElement('div', '', 'user-name-row', '', container);
		this.bind(label, model, prop);
		return label;
	},

	addSignOutButton: function() {
		var container = this.container;
		if (typeof app.user === 'object') {
			var loginSystem = app.backgroundPanel.loginSystem;
			var button = this.addButton('button', this.id + '_user_button', 'bttn', 'Sign Out', base.bind(loginSystem, loginSystem.confirmLogout), this.id);
		}
	},

	createLoginPanel: function() {
		app.createLoginPanel(false);
	}
});
