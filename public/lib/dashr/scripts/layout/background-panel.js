var BackgroundPanel = function(container) {
	this.init('BackgroundPanel');
	this.loginStatus = false;
	this.container = container;
};

base.Component.extend({
	constructor: BackgroundPanel,

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'background-panel', '', frag);
		this.createNavContainer(panel);
		var mainPanel = this.addElement('main', id + '_main_panel', 'main-panel-container', '', panel);
		this.createMinimizePanel(mainPanel);
		this.createPanelContainer(mainPanel);
		this.append(this.container, frag);
	},

	createNavContainer: function(container) {
		var id = this.id;
		var navContainer = this.addElement('div', id + '_nav_container', 'nav-container', '', container);
		this.createNavSettingsPanel(navContainer);
		var navOptionsContainer = this.addElement('nav', id + '_option_container', 'options-container', '', navContainer);
		var ul = this.addElement('ul', id + '_option_ul', '', '', navOptionsContainer);
	},

	createNavSettingsPanel: function(container) {
		var id = this.id;
		var userTopContainer = this.addElement('div', id + '_nav_top_container', 'nav-top-container', '', container);
		var navSettingsContainer = this.addElement('div', id + '_nav_settings_container', 'nav-settings-container', '', userTopContainer);
	},

	createMinimizePanel: function(container) {
		var id = this.id;
		var minimizeContainer = this.addElement('div', id + '_minimize_container', 'minimize-container', '', container);
		var minimizeTitle = this.addElement('div', '', 'title-label light', 'Minimized Panels', minimizeContainer);
	},

	createPanelContainer: function(container) {
		var id = this.id;
		var panelContainer = this.addElement('div', id + '_panel_container', 'panel-container', '', container);
		this.createPanelSettings(panelContainer);
	},

	createPanelSettings: function(container) {
		var id = this.id;
		var settingsContainer = this.addElement('div', id + '_panel_settings', 'panel-settings-container', '', container);
		var settingsColContainer = this.addElement('div', id + '_col_container', 'col-container', '', settingsContainer);
		var colA = this.addElement('div', '', 'col-left', '', settingsColContainer);
		var listOption = this.addButton('button', id + '_list_option', 'nav-list-option bttn', '<span></span><span></span><span></span>', base.bind(this.navContainer, this.navContainer.display), colA);
		var colB = this.addElement('div', '', 'col', '', settingsColContainer);
		var titleContainer = this.addElement('div', id + '_panel_title', 'panel-title-container left white', 'Settings', colB);
		var colC = this.addElement('div', '', 'col-right', '', settingsColContainer);
	},

	previousTitle: null,

	mainTitleElement: null,

	updatePanelTitle: function(title) {
		if (title !== this.previousTitle) {
			this.mainTitleElement = this.mainTitleElement || document.getElementById(this.id + '_panel_title');
			this.mainTitleElement.textContent = title;
			this.previousTitle = title;
		}
	},

	addRoutes: function() {
		var callBack = base.bind(this, this.selectNavOptionByRoute);
		app.addRoute('/:option/:sub?/*', '', callBack, ':option');
	},

	navContainer: null,

	nav: null,

	setupNavContainer: function() {
		var self = this;
		this.navContainer = {
			hidden: true,

			collapseByMouse: false,

			setup: function() {
				this.addEvents();
				this.resize();
			},

			setupNav: function() {
				var id = self.id;
				var callBack = base.bind(this, this.onSelectCallBack);
				self.nav = new Navigation(callBack, id + '_option_ul', id + '_panel_container');
			},

			addEvents: function() {
				var nav = document.getElementById(self.id + '_nav_container');
				base.on('mouseover', nav, base.bind(this, this.addCollapseClass));
				base.on('mouseout', nav, base.bind(this, this.removeCollapseClass));
				base.on('resize', window, base.bind(this, this.resize));
			},

			resize: function() {
				this.width = base.getWindowWidth();
			},

			mobileWidth: 900,

			isMobile: function() {
				return this.width <= this.mobileWidth;
			},

			canTouch: function() {
				return 'ontouchstart' in document.documentElement;
			},

			addCollapseClass: function() {
				var id = self.id;
				var doc = document;
				var panel = doc.getElementById(id + '_main_panel');
				if (this.canTouch() === false && !base.hasClass(panel, 'collapse')) {
					var nav = doc.getElementById(id + '_nav_container');
					base.addClass(nav, 'active');
					base.addClass(panel, 'collapse');
					this.collapseByMouse = true;
				}
			},

			removeCollapseClass: function() {
				var id = self.id;
				var doc = document;
				var panel = doc.getElementById(id + '_main_panel');
				if (base.hasClass(panel, 'collapse') && this.collapseByMouse === true) {
					this.collapseByMouse = false;
					base.removeClass(panel, 'collapse');
					var nav = doc.getElementById(id + '_nav_container');
					base.removeClass(nav, 'active');
				}
			},

			display: function() {
				var id = self.id;
				var nav = document.getElementById(id + '_nav_container');
				var panel = document.getElementById(id + '_main_panel');
				var button = document.getElementById(id + '_list_option');
				if (this.hidden === true) {
					this.hidden = false;
					base.addClass(button, 'active');
					base.addClass(panel, 'collapse');
					base.addClass(nav, 'active');
					self.navShadow.add();
				} else {
					this.hidden = true;
					base.removeClass(button, 'active');
					base.removeClass(panel, 'collapse');
					base.removeClass(nav, 'active');
					self.navShadow.remove();
				}
			},

			onSelectCallBack: function(option) {
				if (this.isMobile() && this.hidden === false) {
					this.display();
				}
				self.updatePanelTitle(option.panelTitle);
				self.backgroundUpdater.setup();
			}
		};
		this.navContainer.setupNav();
	},

	addOption: function(option) {
		this.nav.addOption(option);
	},

	selectNavOptionByLabel: function(label) {
		this.nav.selectByLabel(label);
	},

	selectNavOptionByRoute: function(params) {
		if (params) {
			var suppressAction = params.sub !== '' ? true : false;
			var label = params.option;
			this.nav.selectByUri(label, suppressAction);
		}
	},

	refreshSelectedOption: function() {
		var option = this.nav.lastSelectedOption;
		if (option) {
			this.nav.selectOption(option, false);
		}
	},

	selectNextOption: function() {
		this.nav.selectNextOption();
	},

	selectPreviousOption: function() {
		this.nav.selectPreviousOption();
	},

	selectPrimaryOption: function(option) {
		this.nav.selectPrimaryOption(option);
	},

	navShadow: null,

	setupNavShadow: function() {
		var self = this;
		this.navShadow = {
			id: self.id + '_shadow',

			container: self.id + '_main_panel',

			status: 'closed',

			updateStatus: function(status) {
				this.status = status;
			},

			add: function() {
				if (this.status === 'closed') {
					self.addButton('div', this.id, 'nav-shadow', '', base.bind(self.navContainer, self.navContainer.display), this.container);
					this.updateStatus('added');
				}
			},

			remove: function() {
				if (this.status === 'added') {
					var panel = document.getElementById(this.id);
					if (panel) {
						self.removeElement(panel);
						this.updateStatus('closed');
					}
				}
			}
		};
	},

	backgroundUpdater: null,

	setupBackgroundUpdater: function() {
		var self = this;
		this.backgroundUpdater = {
			timer: null, interval: 60000, status: 'idle',

			setup: function() {
				this.timerStop();
				var update = this.checkOptionForUpdates();
				if (update === true) {
					this.timerStart();
				} else {
					this.timerStop();
				}
			},

			checkOptionForUpdates: function() {
				var option = self.nav.getSelectedOption();
				if (option && option.update === true) {
					return true;
				} else {
					return false;
				}
			},

			updateOption: function() {
				var option = self.nav.getSelectedOption();
				if (option != null) {
					if (option.update === true && typeof option.callBackFn === 'function') {
						option.callBackFn.call();
					}
				}
			},

			timerStart: function() {
				var child = this;
				this.timerStop();
				this.timer = window.setInterval(function() {
					child.updateOption();
				}, this.interval);
				this.status = 'started';
			},

			timerStop: function() {
				window.clearInterval(this.timer);
				this.status = 'stopped';
			}
		};
	},

	minimize: null,

	setupMinimize: function() {
		var self = this;
		this.minimize = {
			minimizedPanels: [],

			add: function(panel) {
				if (panel !== null) {
					var tmpOption = this.createPanelObj(panel);
					this.minimizedPanels.push(tmpOption);
					this.changeParent(tmpOption);
				}
				this.displayContainer();
				return false;
			},

			createPanelObj: function(panel) {
				var panelObj = { panel: panel, originalParent: panel.parentNode };
				return panelObj;
			},

			remove: function(panel) {
				if (panel !== null) {
					var option = this.getPanelObj(panel);
					if (option !== false) {
						this.changeParent(option, 'removing');
						var index = base.inArray(this.minimizedPanels, option);
						if (index > -1) {
							this.minimizedPanels.splice(index, 1);
						}
					}
				}
				this.displayContainer();
				return false;
			},

			checkCount: function() {
				if (this.minimizedPanels.length) {
					return true;
				} else {
					return false;
				}
			},

			displayContainer: function() {
				var panelContainer = document.getElementById(self.id + '_panel_container');
				if (this.checkCount()) {
					base.addClass(panelContainer, 'minimize');
				} else {
					base.removeClass(panelContainer, 'minimize');
				}
			},

			getPanelObj: function(panel) {
				if (panel) {
					var options = this.minimizedPanels;
					for (var i = 0, maxLength = options.length; i < maxLength; i++) {
						var option = options[i];
						if (option.panel === panel) {
							return option;
						}
					}
				}
				return false;
			},

			changeParent: function(panelObj, direction) {
				var container = document.getElementById(self.id + '_minimize_container');
				direction = typeof direction === 'undefined' ? 'adding' : direction;
				if (direction === 'adding') {
					container.appendChild(panelObj.panel);
				} else {
					container = panelObj.originalParent;
					container.appendChild(panelObj.panel);
				}
			}
		};
	},

	setup: function() {
		this.addRoutes();
		this.setupKeyboard();
		this.setupLoginSystem();
		var navOptions = ['UserNavOption', 'LocationNavOption'];
		this.setupNavSettings(navOptions);
		this.setupNavContainer();
		this.setupNavShadow();
		this.setupBackgroundUpdater();
		this.setupMinimize();
		this.render();
		this.navContainer.setup();
		this.loadSettings();
	},

	loadSettings: function() {
	},

	addAppModules: function() {
		app.addModules();
	},

	navSettings: null,

	setupNavSettings: function(optionTypes) {
		var self = this;
		this.navSettings = {
			container: self.id + '_nav_settings_container', panelContainer: self.id + '_nav_top_container', optionsArray: [], optionTypes: optionTypes,

			setup: function() {
				this.update();
				this.setupEvents();
			},

			reset: function() {
				var options = this.optionsArray, count = options.length;
				if (count > 0) {
					for (var i = 0; i < count; i++) {
						var option = options[i];
						if (option && typeof option === 'object') {
							option.remove();
						}
					}
				}
				this.optionNumber = 0;
				this.optionsArray = [];
				var panel = document.getElementById(this.container);
				if (panel) {
					self.removeAll(panel);
				}
			},

			createPanel: function() {
				var imageContainer = self.addElement('div', self.id + '_nav_logo_container', 'nav-logo-container', '', this.container);
				var optionContainer = self.addElement('div', self.id + '_nav_settings', 'nav-settings', '', this.container);
				this.setupOptions();
			},

			setupOptions: function() {
				var optionList = this.optionTypes;
				if (typeof optionList === 'object' && optionList.length > 0) {
					for (var i = 0, maxLength = optionList.length; i < maxLength; i++) {
						this.createOption(optionList[i]);
					}
				}
			},

			createOption: function(optionType) {
				if (optionType && typeof optionType === 'string') {
					var navOption = window[optionType];
					if (navOption) {
						var option = new navOption(this.panelContainer);
						option.setup();
						this.createButton(option);
						this.optionsArray.push(option);
					}
				}
				return false;
			},

			optionNumber: 0,

			createButton: function(option) {
				if (option) {
					var number = this.optionNumber++;
					option.selected = false;
					option.optionNumber = number;
					option.settingId = self.id + '_nav_settings_option_' + number;
					var child = this, button = self.addButton('button', option.settingId, 'bttn circle ' + option.buttonClass, '', function() {
						child.selectOption(option);
					}, self.id + '_nav_settings');
					var panel = document.getElementById(option.id);
					if (panel && panel.style.display !== 'none') {
						panel.style.display = 'none';
					}
				}
			},

			selectOption: function(option) {
				if (option) {
					if (option.selected === false) {
						option.selected = true;
						var panel = document.getElementById(option.id);
						if (panel) {
							panel.style.display = 'block';
							panel.style.zIndex = 2;
							base.animate._in(panel, 'fadeFlipX', 500);
						}
						var button = document.getElementById(option.settingId);
						if (button) {
							base.addClass(button, 'selected');
						}
						this.unselectOptions(option);
						this.activate();
					} else {
						this.deactivate();
					}
				}
			},

			unselectOptions: function(option) {
				if (typeof this.optionsArray !== 'undefined') {
					var optionList = this.optionsArray;
					for (var i = 0, maxLength = optionList.length; i < maxLength; i++) {
						var button = optionList[i];
						if (button !== option) {
							if (button.selected === true) {
								var panel = document.getElementById(button.id);
								if (panel) {
									panel.style.zIndex = 1;
									base.animate.out(panel, 'flipOutX', 500);
								}
								var setting = document.getElementById(button.settingId);
								if (setting) {
									base.removeClass(setting, 'selected');
								}
							}
							button.selected = false;
						}
					}
				}
			},

			update: function() {
				this.reset();
				this.createPanel();
			},

			eventsAdded: false,

			setupEvents: function() {
				var mouseover = base.bind(this, this.deactivate), panel = document.getElementById(self.id + '_main_panel');
				if (this.eventsAdded === false) {
					this.removeEvents = function() {
						if (panel) {
							base.removeListener('mouseover', panel, mouseover);
							this.eventsAdded = false;
						}
					};
					this.addEvents = function() {
						if (this.eventsAdded === false) {
							this.removeEvents();
							if (panel) {
								base.addListener('mouseover', panel, mouseover);
								this.eventsAdded = true;
							}
						}
					};
				}
			},

			activate: function() {
				this.addEvents();
			},

			deactivate: function() {
				this.removeEvents();
				this.unselectOptions();
			},

			removeEvents: null, addEvents: null
		};
	},

	signIn: function() {
		this.loginSystem.signIn();
	},

	signOut: function() {
		this.loginSystem.signOut();
	},

	loginSystem: null,

	setupLoginSystem: function() {
		var self = this;
		this.loginSystem = {
			model: new AuthModel,

			signIn: function() {
				this.updateStatus(true);
				self.nav.reset();
				this.session.startTimer();
				this.displayLogout();
				this.updatePanelClass();
			},

			signOut: function() {
				this.updateStatus(false);
				this.removeLogout();
				this.logoutUser();
			},

			updatePanelClass: function(direction) {
				var panel = document.getElementById(self.id);
				direction = typeof direction === 'undefined' ? 'in' : direction;
				if (direction === 'in') {
					base.addClass(panel, 'active');
				} else {
					base.removeClass(panel, 'active');
				}
			},

			logoutUser: function() {
				this.model.xhr.logout(base.bind(this, this.logoutUserResults));
			},

			logoutUserResults: function(response) {
				console.log('logoutUserResults', response);

				this.updateStatus(false);
				self.keyboard.removeKeySupport();
				this.session.stopTimer();
				window.location.reload();
			},

			confirmLogout: function() {
				var child = this;
				var mpalert = new Alert('confirm', 'Sign Out', 'Do you really want to sign out?', function() {
					child.signOut();
				});
				mpalert.start();
			},

			updateStatus: function(status) {
				if (typeof status !== 'undefined') {
					self.loginStatus = status;
				} else {
					if (self.loginStatus === false) {
						self.loginStatus = true;
					} else {
						self.loginStatus = false;
					}
				}
			},

			displayLogout: function() {
				self.navSettings.setup();
			},

			removeLogout: function() {
			},

			session: {
				timer: null, interval: 120000,

				model: new AuthModel,

				startTimer: function() {
					var callBack = base.bind(this, this.update);
					this.stopTimer();
					this.timer = window.setInterval(callBack, this.interval);
				},

				stopTimer: function() {
					var session = self.loginSystem.session;
					window.clearInterval(session.timer);
				},

				update: function() {
					var session = self.loginSystem.session;
					if (self.loginStatus === true) {
						this.model.xhr.updateSession(session.updateResults);
					} else {
						session.stopTimer();
					}
				},

				updateResults: function(response) {
					if (response) {
						if (response.allowAccess === true) {
							app.updateUser(response.user);
						} else {
							self.loginSystem.signOut();
						}
					}
				}
			}
		};
	},

	keyboard: null,

	setupKeyboard: function() {
		var self = this;
		this.keyboard = {
			activeKeys: [],

			setup: function() {
				this.setupEvents();
				this.addKeySupport();
			},

			eventsAdded: false,

			setupEvents: function() {
				var keyPress = base.bind(this, this.keyPress), keyUp = base.bind(this, this.keyUp), panel = document.getElementById(self.id + '_main_panel');
				if (this.eventsAdded === false) {
					this.removeKeySupport = function() {
						base.removeListener('keydown', window, keyPress);
						base.removeListener('keyup', window, keyUp);
						this.eventsAdded = false;
					};
					this.addKeySupport = function() {
						if (this.eventsAdded === false) {
							this.removeKeySupport();
							base.onKeyDown(keyPress, window);
							base.onKeyUp(keyUp, window);
							this.eventsAdded = true;
						}
					};
				}
			},

			addKeySupport: null,

			keyPress: function(keyCode, e) {
				if (base.inArray(this.activeKeys, keyCode) === -1) {
					this.activeKeys.push(keyCode);
				}
				switch (this.activeKeys.toString()) {
					case '16,37':
						self.nav.selectPreviousOption();
						base.preventDefault(e);
						break;
					case '16,38':
						self.nav.selectPreviousOption();
						base.preventDefault(e);
						break;
					case '16,39':
						self.nav.selectNextOption();
						base.preventDefault(e);
						break;
					case '16,40':
						self.nav.selectNextOption();
						base.preventDefault(e);
						break;
					default:
				}
			},

			keyUp: function(keyCode) {
				var index = base.inArray(this.activeKeys, keyCode);
				if (index > -1) {
					this.activeKeys.splice(index, 1);
				}
			},

			removeKeySupport: null
		};
		this.keyboard.setup();
	}
});