'use strict';

/* SettingsPanel */
var SettingsPanel = function(container) {
	this.init();

	this.container = container;

	this.panelTitle = this.label = 'Settings';
	this.autoUpdate = false;
	this.accessLevel = 0;
	this.iconUrl = 'images/icons/settings.svg';
};

MainPanel.extend({
	constructor: SettingsPanel,

	setup: function() {
		this.render();
		this.setupPanelDefault();
	},

	render: function() {
		var saveBind = base.bind(this, this.save);
		var cancelBind = base.bind(this, this.cancel);

		var layout = {
			id: this.id, class: 'main-panel settings-panel',
			header: {
				node: 'header', class: 'panel-settings-container',
				title: {
					id: this.id + '_title_settings_container', class: 'title-settings-container'
				}
			},
			body: {
				id: this.id + '_content_container', class: 'content-container',
				settings: {
					id: this.id + '_settings_container', class: 'settings-container',
					buttons: {
						id: this.id + '_settings_button_container', class: 'button-container'
					}
				},
				mainSettings: this.getBodyLayout()
			},
			footer: {
				node: 'footer', id: this.id + '_button_container', class: 'settings-button-container',
				children: [
					{ node: 'button', class: 'bttn bttn-green', textContent: 'Save', onclick: saveBind },
					{ node: 'button', class: 'bttn bttn-red', textContent: 'Cancel', onclick: cancelBind }
				]
			}
		};

		base.parseLayout(layout, this.container);
	},

	activate: function() {

	},

	deactivate: function() {
	},

	update: function() {
		if (this.model) {
			this.model.xhr.show(base.bind(this, this.updateResponse));
		}
	},

	updateResponse: function() {

	},

	save: function() {
		if (this.model) {
			this.model.xhr.update(base.bind(this, this.saveResponse));
		}
	},

	saveResponse: function(response) {
		if (!response || response.success === false) {
			var flash = new FlashPanel('Error', 'Unable to update Settings', 3000, app.backgroundPanel.id);
			flash.setup();
		} else {
			var flash = new FlashPanel('Success!', 'Settings have been updated!', 3000, app.backgroundPanel.id);
			flash.setup();
		}
	},

	cancel: function() {
		if (this.model) {
			this.model.revert();

			var flash = new FlashPanel('Reverted!', 'Your changes have been reverted', 2000, app.backgroundPanel.id);
			flash.setup();
		}
	}
});
