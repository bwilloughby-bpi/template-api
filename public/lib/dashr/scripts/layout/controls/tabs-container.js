var TabsContainer = function(container) {
	this.init();

	this.container = container;

	this.orientation = 'horizontal';
	this.tabs = [];
};

base.Component.extend({
	constructor: TabsContainer,

	setup: function() {
		this.render();
	},

	render: function() {
		var layout = {
			id: this.id, class: 'tabs-container-' + this.orientation,
			nav: {
				id: this.id + '_nav', class: 'tab-nav'
			},
			content: {
				id: this.id + '_content', class: 'tab-content-container'
			}
		};

		base.parseLayout(layout, this.container);

		this.nav = document.getElementById(this.id + '_nav');
		this.content = document.getElementById(this.id + '_content');
	},

	addTab: function(tab) {
		if (!tab || typeof tab.title === 'undefined') {
			return;
		}

		this.tabs.push(tab);

		this.addNav(tab.title);
		this.addContent(tab);
	},

	addNav: function(title) {
		var self = this;

		var index = this.tabs.length;

		var layout = {
			node: 'a', id: this.id + 'nav_' + index, class: 'tab-button', textContent: title,
			onclick: function(e) { self.selectTab(index - 1); }
		};

		base.parseLayout(layout, this.nav);
	},

	addContent: function(tab) {
		tab.setup(this.content);
	},

	removeAllTabs: function() {
		this.removeAll(this.nav);
		this.removeAll(this.content);
		this.tabs = [];
	},

	selectTab: function(index) {
		for (var i = 0, length = this.tabs.length; i < length; i++) {
			var tab = this.tabs[i];
			var nav = this.nav.childNodes[i];
			var content = this.content.childNodes[i];

			if (i === index) {
				base.addClass(nav, 'selected');
				base.addClass(content, 'selected');

				if (typeof tab.update === 'function') {
					tab.update.call(tab);
				}
			} else {
				base.removeClass(nav, 'selected');
				base.removeClass(content, 'selected');
			}
		}
	}
});
