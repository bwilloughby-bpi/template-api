"use strict";

/* 
	OptionGroup 
	
	this will create an option group that can add and 
	remove options from the list group.  
	 
	@param (array) optionsArray = the array of object options  
	@param (string) container = the container id to add 
	the new panel 
*/ 
var OptionGroupPanel = function(optionsArray, callBackSearch, container)
{ 
	/* this will allow multiple instances of the object 
	in the same project */  
	this.init('OptionGroupPanel');    
	
	/* this array will store all options of the list */ 
	this.optionsArray = optionsArray || []; 
	this.optionNumber = 0; 
	this.callBackSearch = callBackSearch; 
	 
	this.container = container; 
	this.setup(); 
}; 

base.Component.extend( 
{ 
	constructor: OptionGroupPanel, 
	
	render: function() 
	{ 
		var id = this.id; 
		var list = this.panel = this.addElement('div', id + '_option_container','option-group-container left dark','', this.container); 
		
		var addContainer = this.addElement('div','','add-container','', this.container); 
		var callBack = typeof this.callBackSearch === 'function'? this.callBackSearch : ''; 
		var buttonAdd = this.addButton('button', id + '_button_add', 'bttn add circle', '<span></span><span></span>', callBack, addContainer);
	},
	
	setup: function()
	{ 
		this.render();  
	}, 
	
	add: function(opt) 
	{  
		if(opt && typeof opt === 'object') 
		{ 
			var option = opt; 
 			option.panelId = this.id + '_option_' + (++this.optionNumber);  
			
			/* we want to check to block any option that 
			is already added */ 
			if(this.checkToBlock(option) === false) 
			{ 
				this.optionsArray.push(option);  
				this.createOption(option); 
				this.scrollToBottom(); 
			} 
		} 
	}, 
	
	checkToBlock: function(option) 
	{ 
		if(option) 
		{ 
			var options = this.optionsArray;
			for(var i = 0, maxLength = options.length; i < maxLength; i++) 
			{ 
				var checkOption = options[i]; 
				if(checkOption.id === option.id) 
				{ 
					if(typeof FlashPanel !== 'undefined') 
					{ 
						var flash = new FlashPanel('Message Error', 'This option is already added.'); 
						flash.setup();
					}
					return true; 
				} 
			} 
		} 
		return false; 
	}, 
	
	addOptions: function(optionsArray) 
	{ 
		if(optionsArray) 
		{ 
			for(var i = 0, maxLength = optionsArray.length; i < maxLength; i++) 
			{ 
				var option = optionsArray[i];  
				this.add(option);  
			}  
		} 
	}, 
	
	getOptions: function() 
	{ 
		return this.optionsArray; 
	}, 
	
	scrollToBottom: function() 
	{ 
		var panel = document.getElementById(this.container); 
		if(panel) 
		{ 
			panel.scrollTop = panel.scrollHeight; 
		} 
	}, 
	
	createOption: function(option) 
	{ 
		var callback = base.createCallBack(this, this.remove, [option]); 
		var panel = this.addElement('div', option.panelId, 'option-group-item clear','', this.panel); 
		var button = this.addButton('button', '', 'bttn close', '<span></span><span></span>', callback, panel); 
		var label = this.addElement('div','','label', option.label, panel); 
	}, 
	
	getOption: function(option) 
	{ 
		var index = base.inArray(this.optionsArray, option); 
		if(index > -1) 
		{ 
			return this.optionsArray[index]; 
		} 
		return false; 
	}, 
	
	remove: function(option) 
	{ 
		var options = this.optionsArray;
		var index = base.inArray(options, option); 
		if(index > -1) 
		{ 
			options.splice(index, 1); 
			if(option.panelId) 
			{ 
				this.removeChild(option.panelId); 
			} 
		} 
	} 
}); 