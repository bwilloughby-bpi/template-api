"use strict";

/* 
	graphLine 
	
	this will create an animated line graph in canvas. 
	
	@param (mixed) width = the width (int) of the graph 
	container or 'auto'
	@param (mixed) height = the height (int) of the graph 
	container or 'auto'  
	@param (string) container = the id of the container to 
	contain the new graph 
*/ 
var GraphLine = function(textColor, container)
{ 
	this.init('GraphLine');   
 
 	/* this will set the font color */ 
	this.textColor = textColor || '#FFFFFF'; 
	
	/* this will allow the canvas to animate 
	and stop when done */ 
	this.status = 'stopped'; 
	this.timer = null;  
	this.fps = 60;  
	
	this.container = container; 
}; 

Graph.extend(  
{ 
	constructor: GraphLine, 
	
	/* this will create the graph and add the event listener 
	to resize the graph on resize */ 
	setup: function(data)
	{ 
		this.data = data; 
		this.setupAnimationFrame(); 
		
		/* we want to create the graph and 
		start animating the data */   
		this.render(); 
		this.setupEvents(); 
		this.addEvent();     
	},  
	
	/* 
	gridLayer (object)
	
	this child object will setup the grid layer and allow the grid 
	to be drawn and updated */ 
	gridLayer: null, 
	
	setupGridLayer: function() 
	{ 
		var self = this; 
	
		this.gridLayer = 
		{ 
			/* this will create a unique id for the grid canvas 
			and allow us to cache the canvas and ctx */ 
			id: self.id + '_grid', 
			canvas: null, 
			ctx: null, 
			size: { width: 0, height: 0 },  
			
			/* this will set the margin x and y so we can padd the area 
			where the vales and labels will be drawn */ 
			margin: { x: 60, y: 40 }, 
			
			/* this will store the point labels that are 
			added to the grid */ 
			labels: [], 
			values: [], 
			
			/* this will setup the grid layer by creating the canvas, 
			 resizing the canvas, and drawing the grid */ 
			setup: function() 
			{ 
				if(this.canvas === null) 
				{ 
					var grid = this.create(); 
					this.canvas = grid.canvas; 
					this.ctx = grid.ctx;  
				} 
				
				/* we want to resize the layer and draw the
				grid, labels, and values */ 
				this.resize(); 
				this.draw(); 
			}, 
			
			/* this will create the grid layer canvas 
			@return (object) an object with the cached canvas element 
			and ctx element */ 
			create: function() 
			{ 
				var canvas = document.createElement('canvas'); 
				canvas.id = this.id;  
				var ctx = canvas.getContext("2d"); 
				
				document.getElementById(self.container).appendChild(canvas); 
				
				return { 'canvas': canvas, 'ctx': ctx }; 
			}, 
			
			/* this will get the size of each square in the grid. 
			@return (int) the grid size */ 
			getGridSize: function() 
			{ 
				var length = ((this.labels.length <= 0)? 1 : this.labels.length) + 1; 
				return ((this.canvas.width - this.margin.x) / length); 
			}, 
			
			getGridSizeY: function() 
			{ 
				var length = this.getValuesRowCount(); 
				return ((this.canvas.height - this.margin.y) / length); 
			},
			
			getValuesRowCount: function() 
			{ 
				var lineCount = (this.labels.length) || 1; 
				
				/* we want to limit the number of values the 
				grid can have */ 
				if(lineCount > 10) 
				{ 
					lineCount = 10; 
				} 
				else if(lineCount < 3) 
				{ 
					lineCount = 3; 
				} 
				return lineCount; 
			}, 
			
			/* this will resize the gid layer and draw the resized 
			layer */ 
			resize: function() 
			{ 
				var size = self.getContainerSize(); 
				if(size.width > 0) 
				{ 
					this.canvas.width = size.width; 
					this.canvas.height = size.height; 
					
					this.size.width = size.width; 
					this.size.height = size.height; 
					
					this.draw(); 
				}
			}, 
			
			/* this will adda label to the labels array */ 
			addLabel: function(label) 
			{ 
				this.labels.push(label); 
			}, 
			
			/* this will store the highest number in the values 
			array */ 
			highestNumber: 10,  
			
			/* this will check to add a value to the highest number
			if the number is higher than the previous highest number. 
			the values array will build from the highest number to 
			zero when the values are drawn */ 
			addValue: function(value) 
			{ 
				/* we want to get the number of labels as our line count 
				and get the distance between the highest number and 
				the buffer so the highest number does not go to the top 
				of the graph */
				var space = Math.round(((value * 0.40) * 1)), 
				paddedValue = (parseInt(value) + parseInt(space) * 1); 
				
				if(paddedValue > this.highestNumber) 
				{ 
					this.highestNumber = paddedValue;  
				}  
			}, 
			
			/* this will update the values array to setup each value option 
			from the highest number saved to the grid layer */ 
			updateValues: function()
			{ 
				var lineCount = this.getValuesRowCount();   
				
				var valueIncrement = Math.round(this.highestNumber / (lineCount));  
				var incrementStart = 0; 
				this.values = []; 
				
				for(var i = 0; incrementStart <= this.highestNumber; i++)
				{  
					this.values.push(incrementStart);  
					incrementStart += valueIncrement;
				}
			},   
			
			/* this will drawthe grid layer including the values and 
			lables around the grid */ 
			draw: function() 
			{ 
				this.drawGrid(); 
				this.drawValues(); 
				this.drawLabels(); 
			}, 
			
			/* this will reset the grid and start drawing the grid */ 
			drawGrid: function() 
			{ 
				var canvas = this.canvas, 
				ctx = this.ctx, 
				
				size = this.size, 
				width = size.width, 
				height = size.height; 
				
				// clear canvas 
				ctx.clearRect(0, 0, width, height);  
				ctx.globalAlpha = 1; 
				ctx.shadowBlur = 0;
				
				var gridLineX = this.margin.x, 
				gridLineY = height - this.margin.y; 
				
				var gridSize = this.getGridSize(); 
				var gridSizeY = this.getGridSizeY();
				
				// horizontal
				for(var i = 0, maxLength = 0; gridLineY >= maxLength; i++)
				{ 
					ctx.beginPath(); 
					ctx.moveTo(this.margin.x, gridLineY);
					ctx.lineTo(width, gridLineY);
					ctx.lineWidth = 0.5;
					ctx.strokeStyle = '#58595b'; 
					ctx.stroke(); 
					
					gridLineY -= gridSizeY; 
					break; 
				}  
				
				// vertical 
				for( i = 0, maxLength = width; gridLineX <= maxLength; i++)
				{ 
					ctx.beginPath(); 
					ctx.moveTo(gridLineX, 0);
					ctx.lineTo(gridLineX, height - this.margin.y);
					ctx.lineWidth = 0.5;
					ctx.strokeStyle = '#58595b'; 
					ctx.stroke(); 
					
					gridLineX += gridSize; 
					break; 				
				} 
			}, 
			
			/* this will draw the values on the left of the grid */ 
			drawValues: function() 
			{ 
				var canvas = this.canvas, 
				ctx = this.ctx, 
				
				size = this.size, 
				width = size.width, 
				height = size.height; 
				
				this.updateValues(); 
				
				//var gridSize = this.getGridSize();  
				var gridSizeY = this.getGridSizeY();
				
				var values = this.values;
				for(var i = 0, maxLength = values.length; i < maxLength; i++)
				{ 
					var valueY = height - (gridSizeY * i) - this.margin.y, 
					valueX = this.margin.x - 5; 
					
					if((i % 4) === 0) 
					{ 
						var value = values[i]; 
					 
						ctx.font = "14px open_sans_condensed_light"; 
						ctx.fillStyle = self.textColor;  
						ctx.textAlign = 'right'; 
						ctx.fillText(self.intToString(value), valueX - 5, valueY); 
					}
				} 
			}, 
			
			/* this will draw the labels on the bottom of the grid */ 
			drawLabels: function() 
			{ 
				var canvas = this.canvas, 
				ctx = this.ctx, 
				
				size = this.size, 
				width = size.width, 
				height = size.height;
				
				var gridSize = this.getGridSize();  
					
				var labels = this.labels;
				for(var i = 0, maxLength = labels.length; i < maxLength; i++)
				{ 
					/* we need to setup the label y at the bottom of the canvas 
					in the middle of the marginY and set the x at the grid value 
					with the marginX added to the grid */ 
					var valueX = this.margin.x + (gridSize * (i + 1)), 
					valueY = height - (this.margin.y / 2); 
					
					var label = labels[i]; 
				 
					ctx.font = "14px open_sans_condensed_light"; 
					ctx.fillStyle = self.textColor; 
					ctx.textAlign = 'center';
					ctx.fillText(label, valueX, valueY); 		
				} 
			}, 
			
			reset: function() 
			{ 
				this.highestNumber = 10; 
				this.labels = [];  
				this.values = []; 
			} 
		}; 
		
		this.gridLayer.setup(); 
	},  
	
	/* 
	dataLayer 
	
	this will setup the data layer and allow the graph 
	data to animate and update */ 
	dataLayer: null, 
	
	setupDataLayer: function() 
	{ 
		var self = this; 
		
		this.dataLayer = 
		{ 
			/* this will create a unique id for the data canvas 
			and allow us to cache the canvas and ctx */
			id: self.id + '_data_layer', 
			canvas: null, 
			ctx: null, 
			size: { width: 0, height: 0 }, 
			
			/* this will store the data points in the groups that are 
			to be drawn on the graph */
			groups: [],   
			
			/* this will setup the data layer by creating the canvas, 
			 resizing the canvas, and drawing the data */
			setup: function() 
			{ 
				if(this.canvas === null) 
				{ 
					var grid = this.create(); 
					this.canvas = grid.canvas; 
					this.ctx = grid.ctx;  
				} 
				
				this.resize(); 
			}, 
			
			/* this will create the grid layer canvas 
			@return (object) an object with the cached canvas element 
			and ctx element */
			create: function() 
			{ 
				var canvas = document.createElement('canvas');  
				canvas.id = this.id; 
				var ctx = canvas.getContext("2d"); 
				
				document.getElementById(self.container).appendChild(canvas); 
				
				return { 'canvas': canvas, 'ctx': ctx }; 
			}, 
			
			resize: function() 
			{ 
				var size = self.getContainerSize(); 
				if(size.width > 0) 
				{ 
					this.canvas.width = size.width; 
					this.canvas.height = size.height; 
					
					this.size.width = size.width; 
					this.size.height = size.height; 
				} 
			}, 
			
			/* this will create a point object and add it to the 
			points array. The point object knows how to position 
			and draw its point  */ 
			createGroup: function(obj)
			{  
				/* we want to save an instance of the parent 
				object and the parent canvas to use in the child
				point object */ 
				var parent = this, 
				canvas = this.canvas, 
				number = this.groups.length; 
				
				/* this is a point object that can setup the point object 
				by the data object passed tot heparent method. */ 
				var group = 
				{ 
					id: obj.id, 
					number: number,  
					label: obj.label, 
					fillcolor: obj.color || self.getRandomColor(number),  
					value: obj.value, 
					speed: 10, 
					fill: 'yes', 
					dash: false, 
					points: [], 
					
					setupPoints: function() 
					{ 
						var points = obj.points;
						if(obj && typeof obj.points === 'object') 
						{ 
							for(var i = 0, maxLength = points.length; i < maxLength; i++) 
							{ 
								var data = points[i]; 
								data.color = this.fillcolor; 
								var point = self.dataLayer.createPoint(data, i); 
								
								/* we only want to add the labels from the first option */ 
								if(number === 0) 
								{ 
									self.gridLayer.addLabel(point.label); 
								} 
								
								/* we need to check if the new option has higher values */ 
								self.gridLayer.addValue(point.value); 
								
								//add point to points array 
								this.points.push(point);
							} 
						}
					}, 
					
					draw: function(ctx) 
					{ 
						var pause = true; 
						var points = this.points;
						var length = points.length; 
				
						ctx.beginPath(); 
						
						if(length) 
						{ 
							for(var i = 0; i < length; i++) 
							{ 
								var point = points[i]; 
 								
								/* we want to dash the last line to show its not 
								finished yet */ 
								if(i === (length - 1)) 
								{ 
									point.dash = true; 
								} 
								else if(point.dash)
								{ 
									point.dash = false; 
								} 
								
								var pointPaused = point.draw(ctx);
								if(pointPaused === false) 
								{ 
									pause = false; 
								} 
							} 
						} 
						return pause;   
					}
				}; 
				
				group.setupPoints(); 
				
				//add point to points array 
				this.groups.push(group); 
				
				return group; 
			},
			
			/* this will create a point object and add it to the 
			points array. The point object knows how to position 
			and draw its point  */ 
			createPoint: function(obj, number)
			{  
				/* we want to save an instance of the parent 
				object and the parent canvas to use in the child
				point object */ 
				var parent = this, 
				canvas = this.canvas, 
				
				size = this.size, 
				width = size.width, 
				height = size.height; 
				
				/* this is a point object that can setup the point object 
				by the data object passed tot heparent method. */ 
				var point = 
				{ 
					id: obj.pointNumber, 
					number: number, 
					position: { x: (self.gridLayer.getGridSize() * number) + self.gridLayer.margin.x, y: height }, 
					label: obj.label, 
					fillcolor: obj.color || self.getRandomColor(number),  
					value: obj.value, 
					speed: 6, 
					fill: 'yes', 
					dash: 'no', 
					
					getWidth: function() 
					{ 
						//var gridSize = self.gridLayer.getGridSize(); 
						
						var pointDistanceX = ((width - self.gridLayer.margin.x) / (self.gridLayer.labels.length + 1)); 
						return (pointDistanceX / 1.5); 
					}, 
					
					getDestination: function() 
					{ 
						var child = this; 
						var gridSize = self.gridLayer.getGridSize(); 
						//var gridSizeY = self.gridLayer.getGridSizeY();
						
						var getX = function() 
						{ 
							var distanceX = (self.gridLayer.margin.x + (gridSize * (child.number + 1))), 
							width = child.getWidth();  
	 
							return distanceX - (width / 2); 
						}; 
						
						var getY = function() 
						{ 
							var valueArraySize = self.gridLayer.getValuesRowCount(); 
							var highestValue = self.gridLayer.highestNumber;
							
							/*get the value increments by dividing 
							the highestValue from the tmpValueArray size*/ 
							var valueIncrement = Math.round(highestValue / valueArraySize); 
							
							/*get the pointYIncrement by dividing 
							the canvasHeight from the tmpValueArray size*/ 
							var pointYIncrement = ((height - self.gridLayer.margin.y) / valueArraySize); 
							
							/*the point increment is the 
							number divided by the valueIncrement*/ 
							var increments = (child.value / valueIncrement); 
							
							/*the tmpPointY sets the actual height of the point 
							by multiplying the increments to the pointYIncrements*/
							var tmpPointY = (increments * pointYIncrement); 
							
							/*the pointY is set by subtracting 
							the tmpPointY from the canvasHeight*/ 
							var pointY = height - tmpPointY; 
	
							//update position 
							return pointY; 
						}; 
						
						return { x: getX(), y: getY() }; 
					}, 
					
					updatePosition: function() 
					{ 
						var pause = true; 
						/* we need to get the position destination */ 
						var destination = this.getDestination(); 
						
						/*if the position is greater than destination minus margin
						AND position is greater than destination plus margin (margin is 20) */ 
						if(this.position.x > destination.x - 20 && this.position.x < destination.x + 20)
						{ 
							//set position as destination 
							this.position.x = destination.x; 
						}
						else if(this.position.x < destination.x)
						{ 
							/*if the position is less than destination*/ 
							this.position.x += this.speed * 3; 
							pause = false;
						}
						else
						{  
							this.position.x -= this.speed * 3; 
							pause = false;
						}  
						
						/*if the position is greater than destination minus margin
						AND position is greater than destination plus margin (margin is 20) */ 
						if(this.position.y > destination.y - 6 && this.position.y < destination.y + 6)
						{ 
							//set position as destination 
							this.position.y = destination.y; 
						}
						else if(this.position.y < destination.y)
						{ 
							/*if the position is less than destination*/
							this.position.y += this.speed; 
							pause = false;
						}
						else
						{  
							this.position.y -= this.speed; 
							pause = false;
						} 
						
						this.position.x = destination.x; 
						this.position.y = this.position.y; 
						
						return pause; 
					}, 
					
					draw: function(ctx) 
					{ 
						var canvas = parent.canvas, 
						height = parent.size.height;  
  
						ctx.lineWidth = 1.5;  
							
						/* this will update the position of the point 
						and check to pause the draw timer */ 
						var pause = this.updatePosition();  
						
						if(this.value)
						{  
							ctx.fillStyle = this.fillcolor; 
							ctx.shadowColor = this.fillcolor;
							ctx.shadowBlur = 0; 
							ctx.globalAlpha = 1; 
							/* we need to get the width of the point */ 
							var width = this.getWidth();
							
							//var valueY = this.position.y - 15 - self.gridLayer.margin.y; 
							var valueX = this.position.x + (width / 2); 

							ctx.globalAlpha = 0.8; 
	 
							//var itemHeight = -(canvas.height - this.position.y);  
							//ctx.rect(this.position.x, canvas.height - self.gridLayer.margin.y, width, itemHeight); 
							
							var positionY = height - self.gridLayer.margin.y - (height - this.position.y); 
							ctx.lineTo(valueX, positionY);
							//var size = 1;
							//ctx.arc(valueX, positionY, 2, 0, Math.PI*2, true);  
							ctx.strokeStyle = this.fillcolor; 
							
							if(this.dash === true) 
							{ 
								ctx.save(); 
								ctx.setLineDash([4, 2]); 
								ctx.stroke();  
								ctx.restore();
							} 
							else 
							{ 
								ctx.stroke(); 
							}
						}
						
						//reset 
						ctx.shadowBlur = 0; 
						
						return pause; 
					}
				};    
				
				return point; 
			}, 
			
			/* this will reset the data layer */ 
			reset: function() 
			{ 
				this.groups = [];  
			}, 
			
			/* this will rese the grid and data layers 
			and update with the new data */ 
			update: function(data) 
			{ 
				/* we want to reset the grid layer 
				and the data layer */ 
				self.gridLayer.reset(); 
				this.reset(); 
				
				if(data && typeof data === 'object') 
				{ 
					for(var i = 0, maxLength = data.length; i < maxLength; i++) 
					{ 
						var group = this.createGroup(data[i]); 
					} 
				}  
			}, 
			
			/* this will draw all points on the data layer and 
			check to pause the animation timer */ 
			draw: function() 
			{ 
				var canvas = this.canvas, 
				ctx = this.ctx, 
				pause = true, 
				
				size = this.size, 
				width = size.width, 
				height = size.height; 
				
				// clear canvas 
				ctx.clearRect(0, 0, width, height);    
				
				var groups = this.groups, 
				length = groups.length; 
				if(length > 0) 
				{ 
					for(var i = 0; i < length; i++) 
					{ 
						var group = groups[i], 
						groupPaused = group.draw(ctx); 
						if(groupPaused === false) 
						{ 
							pause = false; 
						}
					} 
				} 
				return pause; 
			} 
		}; 
		
		this.dataLayer.setup(); 
		
	}, 
	
	/* this will create a new graph */  
	render: function()
	{ 
		this.setupGridLayer(); 
		this.setupDataLayer();  
	},  
	
	/* this will add apoint tp the graph */ 
	addPoint: function(obj)
	{ 
		this.dataLayer.createPoint(obj); 
		
		/* the grid layer will need to update */ 
		this.gridLayer.draw(); 
		
		/* this will restart the animation timer */ 
		this.startTimer(); 
	}, 
	
	/* this will update the grid layer and data layer from 
	the data array. 
	@param (array) data = the new data array with the point 
	objects */ 
	update: function(data)
	{  
		if(data && typeof data === 'object') 
		{ 
			this.gridLayer.draw(); 
			this.dataLayer.update(data);  
		}
		this.resize();  
	} 
});