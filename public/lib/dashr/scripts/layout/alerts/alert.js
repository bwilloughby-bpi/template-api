'use strict';

var Alert = function(type, title, description, callback, tmpArgArray) {
	this.init();

	this.type = type;
	this.title = title;
	this.description = description;
	this.callback = callback;
	this.arg = tmpArgArray;

	this.response = null;
	this.container = document.body;
	this.returnDecline = false;
};

base.Component.extend({
	constructor: Alert,

	setup: function() {
		this.createShadow();
		this.render();
	},

	createShadow: function() {
		var self = this;
		this.addButton('div', this.id + '_shadow', 'alert-shadow fadeIn', '', function() { self.declineByShadow(); }, this.container);
	},

	render: function() {
		var id = this.id;

		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'alert floatDownZ', '', frag);

		this.createHeader(panel);
		this.addElement('div', id + '_text', 'description center dark', '<p>' + this.description + '</p>', panel);
		var button = this.createButtonContainer(panel);

		this.append(this.container, frag);
		button.focus();
	},

	createHeader: function(container) {
		var id = this.id;
		var titleContainer = this.addElement('header', id + '_title_container', 'title-container', '', container);
		this.addElement('div', id + '_title', 'title left dark', this.title, titleContainer);
	},

	createButtonContainer: function(container) {
		var id = this.id;
		var buttons = this.addElement('footer', id + '_buttons', 'button-container', '', container);
		return this.createButtons(buttons);
	},

	createButtons: function(container) {
		var id = this.id;
		var self = this;
		var button, buttonHolder;

		switch (this.type) {
			case 'confirm':
				buttonHolder = this.addElement('div', 'alert_button_holder', 'button-group', '', container);
				this.addButton('button', id + '_button_1', 'bttn-red', 'No', function() {self.changeResponse(false);}, buttonHolder);
				button = this.addButton('button', id + '_button_2', 'bttn-green', 'Yes', function() {self.changeResponse(true);}, buttonHolder);
				break;
			case 'confirm-cancel':
				buttonHolder = this.addElement('div', 'alert_button_holder', 'button-group', '', container);
				button = this.addButton('button', id + '_button_1', 'bttn', 'Cancel', function() {self.cancel();}, buttonHolder);
				this.addButton('button', id + '_button_2', 'bttn-red', 'No', function() {self.changeResponse(false);}, buttonHolder);
				this.addButton('button', id + '_button_3', 'bttn-green', 'Yes', function() {self.changeResponse(true);}, buttonHolder);
				break;
			case 'alert':
				buttonHolder = this.addElement('div', id + '_button_holder', 'button-group', '', container);
				button = this.addButton('button', id + '_button_1', 'bttn', 'OK', function() {self.changeResponse(true);}, buttonHolder);
				break;
		}
		return button;
	},

	start: function() {
		this.setup();
	},

	declineByShadow: function() {
		if (this.type !== 'confirm-cancel') {
			this.decline();
		} else {
			this.stop();
		}
	},

	remove: function() {
		var panel = document.getElementById(this.id);
		if (panel) {
			this.removeElement(panel);
		}

		panel = document.getElementById(this.id + '_shadow');
		if (panel) {
			this.removeElement(panel);
		}
	},

	stop: function() {
		this.remove();
	},

	getResponse: function() {
		return this.response;
	},

	updateResponse: function(tmpResponse) {
		this.response = tmpResponse;
	},

	changeResponse: function(tmpResponse) {
		this.updateResponse(tmpResponse);

		if (this.response == true) {
			this.accept();
		}
		else {
			this.decline();
		}
	},

	checkResponse: function() {
		return this.response;
	},

	accept: function() {
		if (typeof this.callback === 'function') {
			if (this.arg) {
				//action is determined by the arguments or no arguments
				this.callback(true, this.arg.valueOf());
			}
			else {
				this.callback(true);
			}
		}
		this.stop();
	},

	decline: function() {
		if (this.returnDecline) {
			if (typeof this.callback === 'function') {
				if (this.arg) {
					//action is determined by the arguments or no arguments
					this.callback(false, this.arg.valueOf());
				}
				else {
					this.callback(false);
				}
			}
		}
		this.stop();
	},

	cancel: function() {
		this.stop();
	}
});