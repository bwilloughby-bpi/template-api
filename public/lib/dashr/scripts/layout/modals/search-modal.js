'use strict';

/* SearchModal */
var SearchModal = function(title, multiselect, tmpSearchFunction, callBackFn, container) {
	this.init();

	this.title = title;
	this.multiselect = multiselect;
	this.tmpSearchFunction = tmpSearchFunction;
	this.callBackFn = callBackFn;
	this.container = container;

	this.lastSelectedOption = null;
	this.optionsArray = [];
	this.values = [];
};

Modal.extend({
	constructor: SearchModal,

	removeAll: function() {
		var panel = document.getElementById(this.id);
		if (panel) {
			this.removeElement(panel);
		}

		this.remove();
	},

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'modal modal-search-panel', '', frag);

		this.createHeader(panel);

		var listInput = this.addElement('div', id + '_input', 'list-input', '', panel);
		listInput.autocomplete = 'no';

		var text = this.searchInput = this.addInput('text', id + '_search_field', '', 'Search', '', '', listInput);
		var listContainer = this.listContainer = this.addElement('div', id + '_list_container', 'list-container', '', panel);
		var listUL = this.list = this.addElement('ul', id + '_list_ul', '', '', listContainer);

		this.createFooter(panel);
		this.append(this.container, frag);
	},

	createFooter: function(container) {
		var id = this.id;
		var buttons = this.addElement('footer', id + '_button_container', 'button-container', '', container);
		var button1 = this.addButton('button', id + '_button_1', 'bttn bttn-red', 'Cancel', base.bind(this, this.decline), buttons);
		var button2 = this.addButton('button', id + '_button_2', 'bttn bttn-green', 'Select', base.bind(this, this.accept), buttons);
	},

	getTitle: function() {
		return 'Search ' + this.title;
	},

	setup: function() {
		this.render();
		this.setupMiniLoading();
		this.setupEvents();
		this.createOptions();
	},

	updateTitle: function(title) {
		var titleLabel = document.getElementById(this.id + '_title_label');
		titleLabel.textContent = 'Search ' + title;
	},

	createOptions: function(options) {
		this.resetOptions();
		this.removeMiniLoading();

		var frag = this.createDocFragment();
		if (options && options.length) {
			for (var i = 0, maxLength = options.length; i < maxLength; i++) {
				var option = this.addOption(options[i], frag);

				if (i === 0) {
					//select first option
					this.selectOption(option);
				}
			}
		}

		var container = this.list;
		container.appendChild(frag);

		this.updateListStyle();
	},

	onInputKeyPress: function() {
		if (typeof this.tmpSearchFunction === 'function') {
			this.tmpSearchFunction.call();
		}
	},

	addOption: function(option, container) {
		var options = this.optionsArray;
		var number = options.length;

		option.nameId = this.id + '_list' + '_' + number;
		option.optionNumber = number;
		option.selected = 'no';

		//list
		var button = option.element = this.addButton('li', option.nameId, 'option', '', this.createClickHandler(option), container);
		this.addElement('div', 'option_panel_label' + '_' + number, 'label', option.label, button);

		options.push(option);

		return option;
	},

	resetOptions: function() {
		var obj = this.list;
		if (obj) {
			while (obj.hasChildNodes()) {
				this.removeChild(obj.lastChild);
			}
		}

		this.optionsArray = [];
		this.values = [];
	},

	createClickHandler: function(tmpOption) {
		var self = this;
		return function() {
			self.selectOption(tmpOption);
		};
	},

	updateListStyle: function() {
		var parentId = this.id;
		var options = this.optionsArray;
		for (var i = 0, maxLength = options.length; i < maxLength; i++) {
			var option = options[i],
				panel = option.element;
			if (panel) {
				panel.className = (option.selected === 'yes') ? 'option selected fadeFlipX' : 'option fadeFlipX';
			}
		}
	},

	updateOptions: function(optionsArray) {
		/* we want to check to panel is still created */
		var panel = document.getElementById(this.id);
		if (panel) {
			this.createOptions(optionsArray);
		}
	},

	getNextOption: function() {
		var lastOption = this.lastSelectedOption,
			lastNumber = (lastOption !== null) ? lastOption.optionNumber : 0,
			nextNumber = ++lastNumber;

		var options = this.optionsArray;
		/* we need to check if we are at the end of the list */
		var index = (nextNumber < options.length) ? nextNumber : options.length - 1;
		return options[index];
	},

	selectNextOption: function() {
		var nextOption = this.getNextOption();
		if (nextOption) {
			this.selectOption(nextOption);
		}
	},

	getPreviousOption: function() {
		var lastOption = this.lastSelectedOption,
			lastNumber = (lastOption !== null) ? lastOption.optionNumber : 0,
			previousNumber = --lastNumber;

		if (previousNumber >= 0) {
			return this.optionsArray[previousNumber];
		}
		else {

		}
	},

	selectPreviousOption: function() {
		var previousOption = this.getPreviousOption();
		if (previousOption) {
			this.selectOption(previousOption);
		}
	},

	setLastSelectedOption: function(option) {
		this.lastSelectedOption = option;
	},

	checkToScrollContainer: function(option) {
		if (option) {
			var button = option.element,
				buttonSize = base.getSize(button),
				position = base.position(button);

			var container = this.listContainer,
				size = base.getSize(container),
				scrollPosition = base.getScrollPosition(container);

			if (!container) {
				return;
			}

			if ((position.y + buttonSize.height < (size.height + scrollPosition.top)) && position.y >= scrollPosition.top) {
			} else {
				if (scrollPosition.top < position.y) {
					container.scrollTop = ((position.y + buttonSize.height) - size.height);
				}
				else {
					container.scrollTop = position.y;
				}
			}
		}
	},

	selectOption: function(tmpOption) {
		this.checkToScrollContainer(tmpOption);
		var multiSelect = this.multiselect;

		if (multiSelect === 'yes') {
			if (tmpOption.selected === 'no') {
				tmpOption.selected = 'yes';
			}
			else {
				tmpOption.selected = 'no';
			}
		}
		else {
			tmpOption.selected = 'yes';

			this.unselectOption(tmpOption);
		}

		this.updateListStyle();
		this.setLastSelectedOption(tmpOption);
	},

	unselectOption: function(tmpOption) {
		var options = this.optionsArray;
		for (var j = 0, maxLength = options.length; j < maxLength; j++) {
			var option = options[j];
			if (option !== tmpOption) {
				if (option.selected === 'yes') {
					option.selected = 'no';
				}
			}
		}
	},

	updateValues: function() {
		this.resetValues();

		var options = this.optionsArray;
		for (var j = 0, maxLength = options.length; j < maxLength; j++) {
			var option = options[j];
			if (option.selected === 'yes') {
				this.addValue(option);
			}
		}
	},

	addValue: function(tmpOption) {
		this.values.push(tmpOption);
	},

	resetValues: function() {
		this.values = [];
	},

	accepted: false,

	accept: function() {
		this.updateValues();

		var values = this.values;
		if (values.length > 0) {
			if (this.accepted === false) {
				this.accepted = true;

				var result = (this.multiselect === 'yes') ? values : values[0].value;

				if (typeof this.callBackFn === 'function') {
					this.callBackFn(result);
				}

				this.display();
			}
		} else {
			this.addAlert();
		}
	},

	alertAdded: false,

	addAlert: function() {
		if (this.alertAdded == false) {
			var mpalert = new Alert('alert', 'Search Error', 'You haven\'t selected anything yet.', base.bind(this, this.removeAlert));
			mpalert.start();
			mpalert.returnDecline = true;
			this.alertAdded = true;

			this.removeKeyboardSupport();
		}
	},

	removeAlert: function() {
		this.alertAdded = false;
		this.addKeyboardSupport();
	},

	decline: function() {
		this.display();
	},

	display: function() {
		this.toggleDisplay();

		if (this.toggleMode == 'block') {
			document.getElementById(this.id + '_search_field').focus();
			this.addKeyboardSupport();
		}
		else {
			this.accepted = false;
			this.removeKeyboardSupport();
			this.remove();
		}
	},

	createShadow: function() {
		this.addButton('div', this.id + '_shadow', 'panel-shadow search-shadow fadeIn', '', base.bind(this, this.display), this.container);
	},

	setupEvents: function() {
		var keyPress = base.bind(this, this.keyPress),
			keyPressUp = base.bind(this, this.keyPressUp),
			keyInput = base.bind(this, this.keyPressInput),
			searchInput = document.getElementById(this.id + '_search_field');

		this.addKeyboardSupport = function() {
			base.onKeyUp(keyPressUp, document);
			base.onKeyDown(keyPress, document);
			base.onKeyUp(keyInput, searchInput);
		};

		this.removeKeyboardSupport = function() {
			base.off('keyup', document, keyPressUp);
			base.off('keydown', document, keyPress);
			base.off('keyup', searchInput, keyInput);
		};
	},

	addKeyboardSupport: null,

	removeKeyboardSupport: null,

	keyPressInput: function(keyCode, e) {
		var searchText = this.searchInput.value;

		switch (keyCode) {
			case 38:
				base.preventDefault(e);
				break;
			case 40:
				base.preventDefault(e);
				break;
			default:
				if (typeof this.tmpSearchFunction === 'function') {
					if (searchText.length > 0) {
						this.addMiniLoading();
						this.tmpSearchFunction(searchText);
					}
					else {
						this.removeMiniLoading();
					}
				}
		}
	},

	keyPressUp: function(keyCode, e) {
		switch (keyCode) {
			case 13:
				this.accept();
				base.preventDefault(e);
				break;
			default:
		}
	},

	keyPress: function(keyCode, e) {
		switch (keyCode) {
			case 38:
				this.selectPreviousOption();
				base.preventDefault(e);
				break;
			case 40:
				this.selectNextOption();
				base.preventDefault(e);
				break;
			default:

		}
	}
});