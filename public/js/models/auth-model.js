'use strict';

var AuthModel = BaseModel.extend({
	url: '/api/auth',

	xhr: {
		objectType: 'user',

		getDefaultParams: function() {
		},

		login: function(callback) {
			var params = {
				username: encodeURI(this.model.get('username')),
				password: encodeURI(this.model.get('password'))
			};

			return this._post('/api/auth', params, callback);
		},

		logout: function(callback) {
			return this._delete('/api/auth', {}, callback);
		},

		resume: function(callback) {
			return this._put('/api/auth', {}, callback);
		},

		updateSession: function(instanceParams, callback) {
			// return this.request('/api/auth', { op: 'updateSession' }, instanceParams, callback);
		}
	}
});
