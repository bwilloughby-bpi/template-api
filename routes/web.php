<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function () use ($router) {

    $router->get('/', function () use ($router) {
        return response()->json($router->version(), 101);
    });

    $router->get('/key', function () {
        return str_random(32);
    });

    // auth
    $router->post('/auth', 'AuthController@login');
    $router->delete('/auth', 'AuthController@logout');
    $router->put('/auth', 'AuthController@resume');

    $router->get('/users/search', 'UserController@search');
    resource($router, 'users', 'UserController');
});

function resource($router, $resource, $controller)
{
    $router->get("/{$resource}", $controller . '@index');
    $router->post("/{$resource}", $controller . '@store');
    $router->get("/{$resource}/{id}", $controller . '@show');
    $router->put("/{$resource}/{id}", $controller . '@update');
    $router->delete("/{$resource}/{id}", $controller . '@destroy');
    $router->delete("/{$resource}", $controller . '@destroyAll');
}

function nested_resource($router, $parentResource, $parentId, $resource, $controller)
{
    $baseURL = "/{$parentResource}/{$parentId}/{$resource}";
    $router->get($baseURL, $controller . '@index');
    $router->post($baseURL, $controller . '@store');
    $router->get("$baseURL/{id}", $controller . '@show');
    $router->put("$baseURL/{id}", $controller . '@update');
    $router->delete("/$baseURL/{id}", $controller . '@destroy');
}
