'use strict';

var FileUploadModal = function(title, url, fileName, callback) {
	this.init();

	this.title = title || 'File Upload';
	this.url = url;
	this.fileName = fileName;
	this.callback = callback;

	this.response = null;
	this.container = document.body;
	this.returnDecline = false;
};

base.Component.extend({
	constructor: FileUploadModal,

	setup: function() {
		this.createShadow();
		this.render();
	},

	createShadow: function() {
		this.addButton('div', this.id + '_shadow', 'alert-shadow fadeIn', '', base.bind(this, this.remove), this.container);
	},

	render: function() {
		var uploadBind = base.bind(this, this.upload);

		var layout = {
			id: this.id, class: 'alert floatDownZ',
			header: {
				node: 'header', id: this.id + '_title_container', class: 'title-container',
				text: { id: this.id + '_title', class: 'title left dark', textContent: this.title }
			},
			body: {
				class: 'content-panel',
				input_row: {
					class: 'row light left',
					input: { node: 'input', type: 'file', id: this.id + '_file' }
				}
			},
			buttons: {
				class: 'button-container',
				buttons: {
					class: 'button-group',
					children: [
						{ node: 'button', class: 'bttn', textContent: 'Upload', onclick: uploadBind }
					]
				}
			}
		};

		base.parseLayout(layout, this.container);
	},

	upload: function() {
		var control = document.getElementById(this.id + '_file');
		if (!control || control.files.length === 0) {
			return;
		}

		var file = control.files[0];

		var formData = new FormData();
		formData.append(this.fileName, file);

		base.ajax({
			url: this.url,
			type: 'POST',
			params: formData,
			headers: {},
			completed: base.bind(this, this.uploadResponse)
		});
	},

	uploadResponse: function(response) {
		if (response && response.success === true) {
			if (typeof this.callback === 'function') {
				this.callback();
			}
			this.remove();
		} else {
			var alert = new Alert('text', 'Error', response.message);
			alert.setup();
		}
	},

	remove: function() {
		var panel = document.getElementById(this.id);
		if (panel) {
			this.removeElement(panel);
		}

		panel = document.getElementById(this.id + '_shadow');
		if (panel) {
			this.removeElement(panel);
		}
	}
});