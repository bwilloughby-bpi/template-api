'use strict';

var ModalSubPanel = function(container) {
	this.init();

	this.container = container;
};

Modal.extend({
	constructor: ModalSubPanel,

	createPanel: function() {
		var id = this.id;
		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'modal modal-sub-panel', '', frag);

		var header = this.addElement('header', '', 'panel-settings-container clear', '', panel);
		var titleSettings = this.addElement('div', id + '_title_container', 'title-settings-container', '', header);
		var titleButtonGroup = this.addElement('div', id + '_title_button_group', 'title-button-group', '', titleSettings);

		var container = this.addElement('div', id + '_list_container', 'body-container', '', panel);
		this.append(this.container, frag);
	},

	updateCategory: function() {
		this.layout.selectModule(this.optionCategory.value);
		if (this.pages) {
			this.pages.reset();
		}
		this.update();
	},

	remove: function() {
		var panel = document.getElementById(this.id);
		if (panel) {
			this.removeElement(panel);
		}
	},

	layout: null,

	setupLayoutController: function() {
		var parent = this;

		this.layout = {
			modules: [],

			active: null,

			add: function(module) {
				if (typeof module === 'object') {
					module.selected = false;
				}
				this.modules.push(module);
			},

			remove: function(module) {
				var index = base.inArray(this.modules, module);
				if (index !== -1) {
					this.modules.splice(index, 1);
					return true;
				} else {
					return false;
				}
			},

			setup: function() {
				var modules = this.modules;
				var length = modules.length;

				for (var i = 0; i < length; i++) {
					var module = modules[i];
					if (module && typeof module.setup === 'function') {
						module.setup.call(module, parent.id + '_list_container');
						var panel = document.getElementById(module.id);
						if (panel) {
							panel.style.display = 'none';
						}

						if (i === 0) {
							this.selectModule(module.id);
						}
					}
				}

				// this.setupCategoryOptions();
			},

			setupCategoryOptions: function() {
				var modules = this.modules;
				var length = modules.length;

				var optionArray = [];

				for (var i = 0; i < length; i++) {
					var module = modules[i];
					optionArray.push({ label: module.label, value: module.id });
				}

				parent.optionCategory = new Selector('list', '', optionArray, base.bind(parent, parent.updateCategory), parent.id + '_title_container');
				parent.optionCategory.setup();
			},

			update: function() {
				var modules = this.modules,
					length = modules.length;

				for (var i = 0; i < length; i++) {
					var module = modules[i];
					if (module && module.selected === true && typeof module.update === 'function') {
						module.update.call(module);
						break;
					}
				}
			},

			getModuleById: function(moduleId) {
				var modules = this.modules,
					length = modules.length;

				for (var i = 0; i < length; i++) {
					var module = modules[i];
					if (module && module.id === moduleId) {
						return module;
					}
				}
				return false;
			},

			selectModule: function(moduleId) {
				var module = this.getModuleById(moduleId);
				if (module !== false) {
					if (module.selected !== true) {
						module.selected = true;
					}
					this.active = module;

					this.selectActivePanel();
				}
			},

			selectActivePanel: function() {
				var modules = this.modules;
				var length = modules.length;

				for (var i = 0; i < length; i++) {
					var module = modules[i],
						panel = document.getElementById(module.id);
					if (panel) {
						if (this.active === module) {
							panel.style.display = 'block';
						} else {
							if (module.selected === true) {
								module.selected = false;
							}
							panel.style.display = 'none';
						}
					}
				}
			}
		};
	},

	setupPagePanel: function(pageContainer, returnNewPanel) {
		var parent = this;

		var pages = {
			pagePanel: null,
			rowCount: 0,
			numberPerPage: 50,

			setup: function(container) {
				var container = pageContainer || parent.id + '_page_container';

				if (this.pagePanel == null) {
					/* we needto create a new page panel object */
					this.pagePanel = new PagePanel(0, this.numberPerPage, base.bind(this, this.updatePagePanel), container);
					this.pagePanel.setup();
				}
			},

			updatePagePanel: function() {
				parent.update();
				this.scrollToTop();
			},

			scrollTopId: null,

			scrollToTop: function() {
				var panel = document.getElementById(this.scrollTopId);
				if (panel) {
					panel.scrollTop = 0;
				}
			},

			checkToUpdate: function(rowCount) {
				if (this.rowCount !== null && this.rowCount !== rowCount) {
					this.update(rowCount);
				}
			},

			reset: function() {
				this.pagePanel.resetLastSelection();
				this.update(0);
			},

			update: function(rowCount) {
				this.rowCount = rowCount;
				this.pagePanel.updatePageSettings(this.rowCount, this.numberPerPage);
			},

			getLimit: function(mode) {
				var number = 0;

				if (mode === 'start') {
					number = this.pagePanel.getLimitNumber('start');
				} else {
					number = this.pagePanel.getLimitNumber('stop');
				}

				return number;
			}
		};

		pages.setup();

		if (returnNewPanel) {
			return pages;
		} else {
			this.pages = pages;
		}
	},

	panelOptions: null,

	setupPanelOptions: function(container) {
		var parent = this;

		this.panelOptions = {
			optionsArray: [],

			container: container || this.id + '_settings_button_container',

			add: function(option) {
				if (typeof option === 'object') {
					var callBack = (typeof option.callBackFn === 'function') ? option.callBackFn : '';
					option.object = parent.addButton('button', '', 'bttn light', option.label, callBack, this.container);
					this.optionsArray.push(option);
				}
			},

			reset: function() {
				parent.removeAll(this.container);
				this.optionsArray = [];
			},

			remove: function(option) {
				var index = base.inArray(this.optionsArray, option);
				if (index !== -1) {
					if (typeof option.object === 'object') {
						parent.removeElement(option.object);
					}
					this.optionsArray.splice(index, 1);
					return true;
				} else {
					return false;
				}
			},

			setup: function() {
				if (!parent.optionPanel) {
					this.setupMobileEditPanel();
				}

				this.update();
			},

			getPanelOptions: function() {
				var optionsArray = [];

				if (parent.primaryOptions) {
					optionsArray = optionsArray.concat(parent.primaryOptions);
				}

				var activeModule = parent.layout.active;
				if (activeModule && activeModule.activeOptions) {
					optionsArray = optionsArray.concat(activeModule.activeOptions);
				}

				return optionsArray;
			},

			setupMobileEditPanel: function() {
				parent.setupEditOptionsPanel();
			},

			setupMobileEditOptions: function() {
				var optionsArray = this.optionsArray;
				var length = optionsArray.length;

				var optionArray = [];

				for (var i = 0; i < length; i++) {
					var option = optionsArray[i];
					var callBack = (typeof option.callBackFn === 'function') ? option.callBackFn : '';
					optionArray.push({ label: option.label, tmpFunction: callBack });
				}

				parent.optionPanel.update(optionArray);
			},

			update: function() {
				this.reset();

				var optionsArray = this.getPanelOptions();
				var length = optionsArray.length;

				for (var i = 0; i < length; i++) {
					var option = optionsArray[i];
					if (option) {
						this.add(option);
					}
				}

				this.setupMobileEditOptions();
			}
		};

		// this.panelOptions.setup();
	},

	addSearchPanel: function() {
		var panelControl = function() {};

		panelControl.prototype = {
			constructor: panelControl,

			panel: null,
			callBackFn: null,
			titleLabel: '',

			setup: function(titleLabel, callBackFn) {
				if (typeof callBackFn === 'function') {
					this.callBackFn = callBackFn;
				}

				if (typeof titleLabel === 'string') {
					this.titleLabel = titleLabel;
				}
			},

			createPanel: function() {
				this.panel = null;

				this.panel = new ModalSearchPanel(this.titleLabel, 'no', base.bind(this, this.search), base.bind(this, this.callBackFn), app.backgroundPanel.id);
				this.panel.setup();
			},

			xhr: null,

			search: function(tmpSearch) {
				/* override this function in the extended child with
				an ajax function that has a callback to searchResults.
				example:

				this.cancelPreviousXhr();

				var params = "op=searchClientPatients"+
						 "&search=" + encodeURI(tmpsearch)+
						 "&app=" + encodeURI(app.user.app);

				this.xhr = base.ajax("/api/patient/", params, base.bind(this, this.searchResults));
				*/
			},

			searchResults: function(response) {
				this.xhr = null;
				if (typeof response !== 'string') {
					var result = response;
					this.panel.updateOptions(result);
				}
			},

			cancelPreviousXhr: function() {
				if (this.xhr !== null) {
					this.xhr.abort();
				}
			},

			display: function() {
				this.createPanel();
				this.panel.display();
			}
		};

		return new panelControl();
	},

	setup: function() {
		this.setupLayoutController();

		/* create panel */
		this.createPanel();
		this.setupMinimize();
		this.setupMiniLoading();
		this.layout.setup();
		this.update();
	},

	createShadow: function() {

	},

	toggleDisplay: function() {
		var obj = document.getElementById(this.id);
		var display = obj.style.display;

		if (display === '' || display === 'none') {
			obj.style.display = 'block';
			this.toggleMode = 'block';
		} else {
			base.addClass(obj, 'close');
			this.toggleMode = 'none';
			window.setTimeout(base.bind(this, this.remove), 800);
		}
	}
});

MainPanel.extend(ModalSubPanel.prototype);
