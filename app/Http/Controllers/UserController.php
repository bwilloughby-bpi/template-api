<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UserController extends Controller {

    public function __construct() {
    }

    public function index(Request $request) {
        $response = [
            'success' => true,
        ];

        $filter             = json_decode(urldecode($request->get('filter'))) ?? (object)[];
        $response['filter'] = $filter;
        $skip               = $request->get('skip') ?? 0;
        $take               = $request->get('take') ?? 50;

        // init query
        $users = User::query();

        // filter
        if (isset($filter->role) && $filter->role !== 'all') {
            $users = $users->where('role', $filter->role);
        }

        // rowCount
        $response['rowCount'] = $users->count();

        // fetch
        $users = $users
            ->orderBy('first', 'asc')
            ->skip($skip)->take($take)
            ->get();

        if (!$users) {
            $response['success'] = false;
            $response['message'] = 'Unable to find Users';

            return $this->r_not_found($response);
        }

        foreach ($users as $user) {
            $user->allow_access = ($user->allow_access == 1);
        }

        $response['data'] = $users;

        return $this->r_success($response);
    }

    public function store(Request $request) {
        $response = [
            'success' => true,
        ];

        $this->validateRequest($request);

        $hasher = app()->make('hash');

        $user           = new User;
        $user->username = $request->get('username');
        $user->email    = $request->get('email');
        $user->first    = $request->get('first');
        $user->last     = $request->get('last');
        $user->role     = $request->get('role');

        $password = $request->get('password');
        if ($password && $password !== '') {
            $password       = $hasher->make($password);
            $user->password = $password;
        }

        $user->save();

        $response['id'] = $user->id;

        return $this->r_created($response);
    }

    public function show(Request $request, $id) {
        $response = [
            'success' => true,
        ];

        $user = User::find($id);

        if (!$user) {
            $response['success'] = false;
            $response['message'] = "Unable to find User {$id}";

            return $this->r_not_found($response);
        }

        $response['data'] = $user;

        return $this->r_success($response);
    }

    public function update(Request $request, $id) {
        $response = [
            'success' => true,
        ];

        $user = User::find($id);

        if (!$user) {
            $response['success'] = false;
            $response['message'] = "Unable to find User {$id}";

            return $this->r_not_found($response);
        }

        $this->validateRequest($request, $id);

        $hasher = app()->make('hash');

        $user->username     = $request->get('username');
        $user->email        = $request->get('email');
        $user->first        = $request->get('first');
        $user->last         = $request->get('last');
        $user->role         = $request->get('role');
        $user->allow_access = ($request->get('allow_access') == 'true' ? 1 : 0);

        $password = $request->get('password');
        if ($password && $password !== '') {
            $password       = $hasher->make($password);
            $user->password = $password;
        }

        $user->save();

        return $this->r_success($response);
    }

    public function destroy($id) {
        $response = [
            'success' => true,
        ];

        $user             = User::find($id)->delete();
        $response['user'] = $user;

        return $this->r_success($response);
    }

    public function destroyAll(Request $request) {
        $response = [
            'success' => true,
        ];

        $ids = json_decode($request->get('ids'));

        try {
            // delete users
            $users             = User::whereIn('id', $ids)->delete();
            $response['users'] = $users;

        } catch (\Illuminate\Database\QueryException $e_q) {
            $response['success'] = false;
            $response['message'] = "Unable to delete one or more Users.<br/>" .
                                   "Please check that they are not the Fulfillment Agent on any Campaigns<br/>" .
                                   "Or the Account Manager on any Clients.";
            $response["db_e"]    = $e_q;
        }

        return $this->r_success($response);
    }

    public function search(Request $request) {
        $response = [
            'success' => true,
        ];

        $term = $request->get('term');
        $term = "%{$term}%";

        // init query
        $users = User::query();

        // filter
        $users = $users
            ->where('users.first', 'like', $term)
            ->orWhere('users.last', 'like', $term);

        // rowCount
        $response['rowCount'] = $users->count();

        // fetch
        $users = $users
            ->get();

        if (!$users) {
            $response['success'] = false;
            $response['message'] = 'Unable to find Users';

            return $this->r_not_found($response);
        }

        foreach ($users as $user) {
            $user->allow_access = ($user->allow_access == 1);
        }

        $response['data'] = $users;

        return $this->r_success($response);
    }

    public function validateRequest(Request $request, $id = null) {
        // TODO: Review fields and roles
        $rules = [
            'username' => 'required|max:255',
            'email'    => 'required|email|max:255',
            'first'    => 'required|max:255',
            'last'     => 'required|max:255',
            'role'     => ['required', Rule::in(['restricted', 'user', 'admin'])],
            'password' => 'required|confirmed|max:255',
        ];

        if ($id) {
            $rules['password'] = 'confirmed|max:255';
        }

        $this->validate($request, $rules);
    }
}
