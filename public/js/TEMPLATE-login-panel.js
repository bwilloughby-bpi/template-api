'use strict';

// TODO: Rename to project name
var TEMPLATELoginPanel = function(resume, iconURL, callBackFn, container) {
	this.init();

	this.resume = resume;
	this.iconURL = iconURL;
	this.callBackFn = callBackFn;
	this.container = container;

	this.guestLogin = false;
	this.state = null;
	this.secured = true;

	this.model = new AuthModel();
};

LoginPanel.extend({
	constructor: TEMPLATELoginPanel
});
