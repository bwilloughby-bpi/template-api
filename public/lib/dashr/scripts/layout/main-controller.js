var MainController = function(resume) {
	this.resume = resume;

	this.guid = null;
	this.router = null;
	this.user = null;

	this.loginPanel = null;
	this.backgroundPanel = null;
	this.params = '';
};

base.Class.extend({
	constructor: MainController,

	setupOp: function() {
		this.params = base.getParams();
		this.op = this.params.op;

		this.loadSetup();
	},

	loadSetup: function() {
		this.setupRouter();
		this.getGuid();
		this.setupGlobalModels();
		this.createBackgroundPanel();
		this.createLoginPanel();
	},

	setupRouter: function() {
		var baseUrl = '/';
		this.router = new base.router(baseUrl, 'Dashr');

		this.updateBaseTag(baseUrl);
	},

	getGuid: function() {
		var date = new Date;
		var time = date.getTime();
		this.guid = time - 1260000000 + Math.random() * 1000 + Math.random() * 5000 + Math.random() * 5000;
	},

	setupGlobalModels: function() {
	},

	createBackgroundPanel: function() {
		this.backgroundPanel = new BackgroundPanel('body');
		this.backgroundPanel.setup();
	},

	createLoginPanel: function(guestLogin) {
		this.loginPanel = new LoginPanel(this.resume, '', base.bind(this, this.signIn), this.backgroundPanel.id, guestLogin);
		this.loginPanel.setup();
	},

	resetParams: function() {
		this.params = '';
		this.op = '';
	},

	updateBaseTag: function(baseUrl) {
		var ele = document.getElementsByTagName('base');
		if (ele.length) {
			ele[0].href = baseUrl;
		}
	},

	addRoute: function(uri, template, callBack, title) {
		this.router.add(uri, template, callBack, title);
	},

	removeRoute: function(uri, template, callBack) {
		this.router.remove(uri, template, callBack);
	},

	navigate: function(uri, data, replace) {
		this.router.navigate(uri, data, replace);
	},

	removeLoginPanel: function() {
		var self = this;
		var loginPanel = this.loginPanel;
		window.setTimeout(function() {
			loginPanel.remove();
			self.loginPanel = null;
		}, 2000);
	},

	signIn: function(user, accessToken) {
		this.removeLoginPanel();

		this.updateUser(user);
		this.updateAccessToken(accessToken);

		this.backgroundPanel.signIn();
		this.addModules();
	},

	getUser: function() {
		return this.user;
	},

	updateUser: function(user) {
		this.user = user;
		// appData.user.set(user);
		appData.add('user', user);
	},

	checkToAddOption: function(option) {
		if (!option) {
			return;
		}

		if (option.accessLevel) {
			var level = option.accessLevel;
			if (this.hasAccessLevel(level)) {
				option.setup();
				this.backgroundPanel.addOption(option);
			}
		} else {
			option.setup();
			this.backgroundPanel.addOption(option);
		}
	},

	hasAccessLevel: function(accessLevel) {
		var usr = this.getUser();
		if (typeof usr === 'object' && usr.get('accessLevelNumber') !== null) {
			return usr.get('accessLevelNumber') >= accessLevel;
		}
		return false;
	},

	addModules: function() {
		var settingsPanel = new SettingsPanel(this.backgroundPanel.id);
		this.checkToAddOption(settingsPanel);
		this.backgroundPanel.selectPrimaryOption();
	}
});

var appData = MainController.prototype.data = {
	add: function(property, model) {
		var dataProperty = this[property];
		if (dataProperty === undefined) {
			this[property] = model;
			return model;
		}
		return false;
	},

	set: function(property, model) {
		return this.add(property, model);
	},

	get: function(property) {
		var dataProperty = this[property];
		if (dataProperty !== undefined) {
			return dataProperty;
		}
		return false;
	},

	'delete': function(property) {
		var dataProperty = this[property];
		if (dataProperty !== undefined) {
			delete this[property];
		}
	}
};