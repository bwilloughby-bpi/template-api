<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController {

    public function current_user() {
    }

    public function r_success($response) {
        return response()->json($response, 200);
    }

    public function r_created($response) {
        return response()->json($response, 201);
    }

    public function r_not_found($response) {
        return response()->json($response, 404);
    }
}
