'use strict';

/* DataModal */
var DataModal = function(mode, data, callbackFunction, container) {
	this.init();

	this.mode = mode;
	this.model = new base.Model(data);
	this.callbackFunction = callbackFunction;

	this.container = container;
};

EditModal.extend({
	constructor: DataModal,

	setup: function() {
		this.render();
		this.setupHeaderOptions();
	},

	render: function() {
		var cssClass = this.mainPanelClass || '';

		var layout = {
			id: this.id, class: 'modal modal-edit-panel ' + cssClass,
			header: {
				node: 'header', id: this.id + '_title_container', class: 'title-container',
				title: {
					id: this.id + '_title', class: 'title dark', textContent: this.getTitle()
				},
				buttons: {
					id: this.id + '_title_button_group', class: 'title-button-group'
				}
			},
			body: {
				class: 'body-container', form: this.getBodyLayout()
			},
			footer: {
				node: 'footer', id: this.id + '_button_container', class: 'button-container',
				children: [
					{ node: 'button', id: this.id + '_button_1', class: 'bttn bttn-red', textContent: 'Cancel', onclick: base.bind(this, this.decline) },
					{ node: 'button', id: this.id + '_button_2', class: 'bttn', textContent: 'Save', onclick: base.bind(this, this.accept) }
				]
			}
		};

		base.parseLayout(layout, this.container);
	},

	getBodyLayout: function() {
		return {
			id: this.id + '_list_container', class: 'body-container'
		};
	},

	createRow: function(label, model, prop, container, validate) {
		var className = (validate == true) ? 'val' : '';

		var row = this.addElement('div', '', 'row light left', '', container);
		this.addElement('label', '', '', label, row);

		var id = this.id + '_' + base.uncamelCase(prop, '_');
		var field = this.addInput('text', id, className, '', '', '', row);

		if (model) {
			this.bind(field, model, prop);
		}
		return field;
	},

	createRowLayout: function(label, model, prop, validate, disabled) {
		var className = (validate == true) ? 'val' : '';

		var layout = {
			className: 'row light left',
			label: { node: 'label', textContent: label },
			field: { node: 'input', type: 'text', className: className }
		};

		if (model) {
			var id = this.id + '_' + base.uncamelCase(prop, '_');
			layout.field.id = id;
			layout.field.bind = { model: model, prop: prop };
		}

		if (disabled === true) {
			layout.field.disabled = true;
		}

		return layout;
	},

	createRowTextareaLayout: function(label, model, prop, validate, disabled) {
		var className = (validate == true) ? 'val' : '';

		var layout = {
			className: 'row light left',
			label: { node: 'label', textContent: label },
			field: { node: 'textarea', className: 'small ' + className }
		};

		if (model) {
			var id = this.id + '_' + base.uncamelCase(prop, '_');
			layout.field.id = id;
			layout.field.bind = { model: model, prop: prop };
		}

		if (disabled === true) {
			layout.field.disabled = true;
			layout.field.className += ' disabled-field';
		}

		return layout;
	},

	setupSelectDataOptions: function(element, options, model, prop) {
		this.setupSelectOptions(element, options);
		if (model) {
			this.bind(element, model, prop);
			var value = model.get(prop);
			if (typeof value === 'undefined') {
				model.set(prop, element.value);
			}
		}
	},

	confirmAction: function() {
		var mpalert = new Alert('confirm', 'Confirm Action', 'Do you really want to ' + this.mode + ' this Data?', base.bind(this, this.accept));
		mpalert.start();
	},

	accept: function() {
		switch (this.mode) {
			case 'add':
				this.add();
				break;
			case 'edit':
				this.edit();
				break;
		}
	},

	validateForm: function() {
		var id = this.id;
		var form = document.getElementById(id + '_form');
		if (form) {
			return base.formValidator.validateForm(form, id);
		}
		return false;
	},

	validate: function() {
		var response = {
			status: true,
			message: '',
			error: 'no'
		};

		var result = this.validateForm();
		if (result && result.number !== 0) {
			response.status = false;
			response.message = result.message;
		}

		return response;
	},

	add: function() {
		this.request('add');
	},

	edit: function() {
		this.request('edit');
	},

	request: function(mode) {
		this.addMiniLoading();

		/* this will validate the form before submitting */
		var settings = this.validate();
		if (settings.status === true) {
			var model = this.model;
			switch (mode) {
				case 'edit':
					model.xhr.update('', base.bind(this, this.requestResponse));
					break;
				default:
					model.xhr.add('', base.bind(this, this.requestResponse));
			}
		}
		else {
			this.removeMiniLoading();
			var mpalert = new Alert('alert', 'Error', 'The following corrections need to be made before saving. <br> ' + settings.message);
			mpalert.start();
		}
	},

	requestResponse: function(response) {
		this.removeMiniLoading();

		if (response) {
			var result = response;
			if (result.success === true) {
				this.removeAndCallBack();
			}
			else {
				var mpalert = new Alert('alert', 'Error', 'There was an error editing the Lead.<br>Message: ' + result.message);
				mpalert.start();
			}
		}
	},

	display: function() {
		this.toggleDisplay();

		if (this.toggleMode === 'block') {
			/* select first field */
			var form = document.getElementById(this.id).querySelector('form');
			if (form) {
				var elements = form.elements;
				if (elements.length) {
					elements[0].focus();
				}
			}
		}
	}
});