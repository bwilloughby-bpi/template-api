'use strict';

// TODO: Rename to project name
var TEMPLATEController = function(resume) {
	this.resume = resume;

	this.guid = null;
	this.optionsArray = [];
};

MainController.extend({
	constructor: TEMPLATEController,

	setupRouter: function() {
		var baseUrl = '/';
		this.router = new base.router(baseUrl, 'TEMPLATE');

		this.updateBaseTag(baseUrl);
	},

	createBackgroundPanel: function() {
		this.backgroundPanel = new TEMPLATEBackgroundPanel('body');
		this.backgroundPanel.setup();
	},

	createLoginPanel: function() {
		var logoUrl = '';
		this.loginPanel = new TEMPLATELoginPanel(this.resume, logoUrl, base.bind(this, this.signIn), this.backgroundPanel.id);
		this.loginPanel.setup();
	},

	addPanels: function() {
		var container = this.backgroundPanel.id;

		// TODO: Add individual panels here
		// var clients = new ClientListPanel(container);
		// this.checkToAddOption(clients);

		this.backgroundPanel.selectPrimaryOption();
	},

	signIn: function(user) {
		if (typeof user === 'undefined') {
			return false;
		}

		if (user.restricted !== true) {
			this.removeLoginPanel();
		}

		var userModel = new UserModel(user);
		this.updateUser(userModel);

		this.backgroundPanel.signIn();
		this.addPanels();
	}
});
