var UserListPanel = function(container) {
	this.init();

	this.container = container;

	this.label = this.panelTitle = 'Users';

	this.accessLevel = 0;
	this.autoUpdate = false;
	this.iconUrl = '/images/users.svg';
	this.mainPanelClass = 'user-list-panel';

	this.model = new UserModel();
};

FixedPanel.extend({
	constructor: UserListPanel,
	xhr: null,

	setupList: function(listContainer) {
		this.list = new UserList(this, listContainer);
	},

	setupFilters: function() {
		// TODO: Review roles
		var roleOptions = [
			{ label: 'All Roles', value: 'all' },
			{ label: 'Restricted', value: 'restricted' },
			{ label: 'User', value: 'user' },
			{ label: 'Admin', value: 'admin' }
		];
		this.filterRole = new Selector('list', '', roleOptions, base.bind(this, this.update), this.id + '_settings_container');
		this.filterRole.setup();

		this.collapseSearch = new CollapseSearch(base.bind(this, this.search), base.bind(this, this.updateFilter), this.id + '_settings_container');
		this.collapseSearch.setup();
	},

	setupSettingsButtons: function(container) {
		this.addButton('button', '', 'bttn', 'Add', base.bind(this, this.add), container);
		this.addButton('button', '', 'bttn bttn-red', 'Delete', base.bind(this, this.deleteConfirm), container);
	},

	getFilter: function() {
		var filter = {
			role: this.filterRole.value
		};

		return filter;
	},

	add: function() {
		var userModel = new UserModel();
		var userModal = new UserModal(userModel, base.bind(this, this.update), app.backgroundPanel.id);
		userModal.setup();
		userModal.display();
	},

	edit: function(user) {
		var userModel = new UserModel(user);
		var userModal = new UserModal(userModel, base.bind(this, this.update), app.backgroundPanel.id);
		userModal.setup();
		userModal.display();
	},

	deleteConfirm: function() {
		var message = 'Do you really want to delete these Users?';

		var mpalert = new Alert('confirm', 'Confirm Action', message, base.bind(this, this.delete));
		mpalert.start();
	},

	delete: function() {
		var options = this.list.getSelectedOptions();
		var idArray = [];

		for (var i = 0; i < options.length; i++) {
			idArray.push(options[i].id);
		}

		this.model.xhr.destroyAll(idArray, base.bind(this, this.deleteResponse));
	},

	deleteResponse: function(response) {
		if (response.success === false) {
			var mpalert = new Alert('alert', 'Error', 'There was an error deleting the selected Users.<br> Message: ' + response.message);
			mpalert.start();
		} else {
			this.update();
		}
	},

	search: function(term) {
		if (this.xhr !== null) {
			this.xhr.abort();
		}

		this.xhr = this.model.xhr.search(term, base.bind(this, this.searchResponse));
	},

	searchResponse: function(response) {
		this.xhr = null;
		if (!response || response.success === false) {
			return;
		}

		this.list.setupList(response.data);
	},

	updateFilter: function() {
		this.pages.reset();
		this.update();
	},

	update: function() {
		var filter = this.getFilter();
		var skip = this.pages.getLimit('start');
		var take = this.pages.getLimit('stop');

		if (this.collapseSearch.isSearching() === false) {
			this.model.xhr.index(filter, skip, take, base.bind(this, this.updateResponse));
		}
	},

	updateResponse: function(response) {
		if (response.success === false) {
			return;
		}

		this.pages.checkToUpdate(response.rowCount);
		this.list.setupList(response.data);
	}
});
