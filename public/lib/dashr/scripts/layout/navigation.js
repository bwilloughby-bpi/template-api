'use strict';

var Navigation = function(callBackFn, navContainer, panelContainer) {
	this.init();

	this.callBackFn = callBackFn;
	this.container = navContainer;
	this.panelContainer = panelContainer;

	this.history = null;

	this.groups = [];

	this.optionsArray = [];
	this.lastSelectedOption = null;
	this.loaded = false;

	this.panels = null;

	this.setup();
};

base.Component.extend({
	constructor: Navigation,

	setup: function() {
		this.panels = new NavigationPanels(this.panelContainer);
	},

	reset: function() {
		this.groups = [];
		this.optionsArray = [];
		this.removeAll(this.container);

		this.panels.resetPanels();
	},

	setupOptions: function(optionsArray) {
		if (optionsArray) {
			this.reset();
			for (var i = 0, maxLength = optionsArray.length; i < maxLength; i++) {
				this.addOption(optionsArray[i]);
			}
			this.updateAllOptions();
		}
	},

	selectPrimaryOption: function() {
		app.router.activate();

		if (!this.lastSelectedOption) {
			if (this.optionsArray.length >= 1) {
				this.navigate(this.getNextOption());
			}
		}
	},

	createGroupButton: function(groupLabel, parent, group) {
		var label, iconUrl;
		if (typeof groupLabel === 'object') {
			label = groupLabel.label;
			iconUrl = groupLabel.icon;
		} else {
			label = groupLabel;
			iconUrl = '';
		}

		var option = {
			selected: 'no',
			type: 'group',
			label: label,
			group: group
		};

		var callBack = base.createCallBack(this, this.selectOption, [option]);
		return this.createNavOption(option, callBack, parent, iconUrl);
	},

	addGroup: function(groupLabel, parentGroup) {
		if (groupLabel) {
			var parent = (parentGroup) ? parentGroup.element : this.container;
			var group = {
				'label': (typeof groupLabel === 'object') ? groupLabel.label : groupLabel,
				selected: 'no',
				groups: []
			};

			var navOption = this.createGroupButton(groupLabel, parent, group);

			var element = this.addElement('ul', '', 'group-container', '', parent);
			group.element = element;
			group.button = navOption;

			navOption.group = group;

			var groups = (parentGroup) ? parentGroup.groups : this.groups;
			groups.push(group);
			return group;
		}
		return false;
	},

	getGroup: function(groupLabel, groupsArray) {
		var group = false;
		var label = (typeof groupLabel === 'object') ? groupLabel.label : groupLabel;

		groupsArray = groupsArray || this.groups;
		for (var i = 0, length = groupsArray.length; i < length; i++) {
			var option = groupsArray[i];
			if (option.label === label) {
				group = option;
				break;
			} else if (option.groups.length) {
				group = this.getGroup(groupLabel, option.groups);
				if (group) {
					break;
				}
			}
		}
		return group;
	},

	getParentGroup: function(groupLabel) {
		if (groupLabel && groupLabel !== '') {
			var group = this.getGroup(groupLabel);
			if (group) {
				return group;
			}
			else {
				var parentGroup = (typeof groupLabel === 'object') ? this.getGroup(groupLabel.group) : null;
				return this.addGroup(groupLabel, parentGroup);
			}
		}
		return false;
	},

	activateGroup: function(activeGroup, groupsArray) {
		groupsArray = groupsArray || this.groups;
		for (var i = 0, length = groupsArray.length; i < length; i++) {
			var option = groupsArray[i];
			var element = option.element;

			var active = this.groupIsActive(option, activeGroup);
			if (active === true) {
				if (option.selected !== 'yes') {
					option.selected = 'yes';
					if (element) {
						base.addClass(element, 'opened');
					}
				}
			} else {
				option.selected = 'no';
				if (element) {
					base.removeClass(element, 'opened');
				}
			}

			if (option.groups.length) {
				this.activateGroup(activeGroup, option.groups);
			}
		}
	},

	groupIsActive: function(group, activeGroup) {
		var active = false;
		if (group === activeGroup) {
			active = true;
		}
		else if (group.groups.length) {
			var childGroups = group.groups;
			for (var i = 0, length = childGroups.length; i < length; i++) {
				var option = childGroups[i];
				active = this.groupIsActive(option, activeGroup);
				if (active) {
					break;
				}
			}
		}
		return active;
	},

	addOption: function(navOption) {
		var label = navOption.label || '';
		var option = {
			label: label,
			panelTitle: navOption.panelTitle || label,
			selected: 'no',
			update: (navOption.autoUpdate === true) ? true : false,
			uri: ''
		};

		if (label) {
			var pattern = / /g;
			option.uri = '/' + label.replace(pattern, '-').toLowerCase();
		}

		if (navOption.id !== null) {
			option.panel = navOption.id;
			this.panels.addPanel(option.panel);
		}

		if (typeof navOption.activate === 'function') {
			option.activate = function() {navOption.activate();};
		}

		if (typeof navOption.update === 'function') {
			option.callBackFn = function() {navOption.update();};
		}

		if (typeof navOption.deactivate === 'function') {
			option.deactivate = function() {navOption.deactivate();};
		}

		var parentContainer = this.container;
		if (navOption.group) {
			var group = this.getParentGroup(navOption.group);
			if (group) {
				parentContainer = group.element;
				option.group = group;
			}
		}

		var iconUrl = (navOption.iconUrl) ? navOption.iconUrl : '';
		var callBack = base.createCallBack(this, this.navigate, [option]);
		return this.createNavOption(option, callBack, parentContainer, iconUrl);
	},

	createNavOption: function(option, callback, parent, iconUrl) {
		var options = this.optionsArray;
		var number = (options.length);
		option.optionNumber = number;
		option.id = this.id + '_option_number' + '_' + number;
		options.push(option);

		var navElement = option.element = this.addButton('li', option.id, this.getOptionStyle(option), '', callback, parent);

		var icon = this.addElement('div', '', 'icon', '', navElement);
		if (typeof iconUrl === 'string' && iconUrl !== '') {
			icon.style.backgroundImage = 'url(' + iconUrl + ')';
		}

		var label = this.addElement('div', '', 'label', option.label, navElement);
		return navElement;
	},

	updateAllOptionStyles: function() {
		var options = this.optionsArray;
		for (var i = 0, maxLength = options.length; i < maxLength; i++) {
			var option = options[i];
			var element = option.element;
			if (element) {
				element.className = this.getOptionStyle(option);
			}
		}
	},

	setLastSelectedOption: function(option) {
		this.lastSelectedOption = option;
	},

	getOptionByNumber: function(number) {
		if (number !== null) {
			var options = this.optionsArray;
			for (var i = 0, maxLength = options.length; i < maxLength; i++) {
				var option = options[i];
				if (i === number) {
					return option;
				}
			}
		}
		return false;
	},

	selectByOption: function(option) {
		var index = base.inArray(this.optionsArray, option);
		if (index > -1) {
			this.navigate(option);
		}
	},

	selectByNumber: function(number) {
		var options = this.optionsArray;
		for (var i = 0, maxLength = options.length; i < maxLength; i++) {
			var option = options[i];
			if (i === number) {
				this.navigate(option);
				break;
			}
		}
	},

	selectByLabel: function(label) {
		var options = this.optionsArray;
		for (var i = 0, maxLength = options.length; i < maxLength; i++) {
			var option = options[i];
			if (option.label === label) {
				this.navigate(option);
				break;
			}
		}
	},

	selectByUri: function(uri, suppress) {
		var selected = false;
		var options = this.optionsArray;

		if (typeof uri !== 'undefined' && uri !== '') {
			var formatLabel = function(uri) {
				var pattern = /\//g;
				return (typeof uri === 'string') ? uri.replace(pattern, '') : uri;
			};

			for (var i = 0, maxLength = options.length; i < maxLength; i++) {
				var tmpOption = options[i];
				var optionUri = formatLabel(tmpOption.uri);
				if (uri === optionUri) {
					this.selectOption(tmpOption, suppress);
					selected = true;
					break;
				}
			}
		}

		if (selected === false && this.loaded === false) {
			this.loaded = true;
			if (options.length >= 1) {
				this.selectByNumber(0);
			}
		}
		return selected;
	},

	unselectOptions: function(tmpOption) {
		var options = this.optionsArray;
		for (var j = 0, maxLength = options.length; j < maxLength; j++) {
			var option = options[j];
			if (option !== tmpOption) {
				if (option.selected === 'yes') {
					option.selected = 'no';
				}
			}
		}
		this.updateAllOptionStyles();
	},

	navigate: function(option) {
		if (option) {
			if (option.uri) {
				app.navigate(option.uri);
			}
		}
	},

	selectOption: function(option, suppress) {
		var activate = function(optn) {
			if (optn && typeof optn === 'object') {
				if (typeof optn.activate === 'function') {
					optn.activate.call();
				}
			}
		};

		if (option.type === 'group') {
			this.setLastSelectedOption(option);

			if (option.selected === 'no') {
				option.selected = 'yes';
				if (option.group.selected === 'no') {
					this.selectNextOption();
					return;
				} else {
					this.selectPreviousOption();
					return;
				}
			}

			this.unselectOptions(option);

			return;
		} else {
			this.activateGroup(option.group);
		}

		if (option.selected === 'yes') {
			if (typeof option.callBackFn === 'function' && suppress !== true) {
				option.callBackFn.call();
			}
		} else {
			option.selected = 'yes';

			this.deactivateLastOption();

			activate(option);

			if (typeof option.callBackFn === 'function' && suppress !== true) {
				window.setTimeout(function() { option.callBackFn.call(); }, 700);
			}

			if (option.panel) {
				this.panels.selectPanel(option.panel);
			}

			this.unselectOptions(option);
		}

		if (typeof this.callBackFn === 'function') {
			this.callBackFn(option);
		}

		this.setLastSelectedOption(option);
	},

	deactivateLastOption: function() {
		var previous = this.lastSelectedOption;
		if (previous && typeof previous === 'object') {
			if (typeof previous.deactivate === 'function') {
				previous.deactivate.call();
			}
		}
	},

	getNextOption: function(lastIndex) {
		var lastOption = this.lastSelectedOption;
		var lastNumber = typeof lastIndex !== 'undefined' ? lastIndex : (lastOption !== null) ? lastOption.optionNumber : 0;
		var nextNumber = ++lastNumber;

		var options = this.optionsArray;
		var index = (nextNumber < options.length) ? nextNumber : 0;
		var option = options[index];
		if (option.type === 'group') {
			option = this.getNextOption(index);
		}
		return option;
	},

	selectNextOption: function() {
		var nextOption = this.getNextOption();
		if (nextOption) {
			this.navigate(nextOption);
		}
	},

	getPreviousOption: function(lastIndex) {
		var lastOption = this.lastSelectedOption;
		var lastNumber = typeof lastIndex !== 'undefined' ? lastIndex : (lastOption !== null) ? lastOption.optionNumber : 0;
		var previousNumber = --lastNumber;

		var options = this.optionsArray;
		var index = (previousNumber >= 0) ? previousNumber : (options.length - 1);
		var option = options[index];
		if (option.type === 'group') {
			option = this.getPreviousOption(index);
		}
		return option;
	},

	selectPreviousOption: function() {
		var previousOption = this.getPreviousOption();
		if (previousOption) {
			this.navigate(previousOption);
		}
	},

	getSelectedOption: function() {
		for (var i = 0, maxLength = this.optionsArray.length; i < maxLength; i++) {
			var option = this.optionsArray[i];
			if (option.selected === 'yes') {
				return option;
			}
		}
	},

	refreshSelectedOption: function() {
		var selectedOption = this.getSelectedOption();
		if (selectedOption.callBackFn) {
			selectedOption.callBackFn.call();
		}
	},

	getOptionStyle: function(option) {
		var classList = (option.selected === 'yes') ? 'option selected' : 'option';
		if (option.type === 'group') {
			classList += ' group';
		}
		return classList;
	}
});

var NavigationPanels = function(container) {
	this.init();

	this.container = container;

	this.optionsArray = [];
	this.lastSelectedPanelNumber = null;
};

base.Component.extend({
	constructor: NavigationPanels,

	resetPanels: function() {
		var options = this.optionsArray;
		for (var i = 0, maxLength = options.length; i < maxLength; i++) {
			var panel = options[i].panel;
			if (panel) {
				this.removeElement(panel);
			}
		}
		this.optionsArray = [];
	},

	getPanelNumber: function(id) {
		var options = this.optionsArray;
		for (var i = 0, maxLength = options.length; i < maxLength; i++) {
			var option = options[i];
			if (option.id === id) {
				return i;
			}
		}
		return -1;
	},

	addPanel: function(panelId) {
		if (typeof panelId !== 'undefined') {
			var option = {
				id: panelId,
				panel: document.getElementById(panelId)
			};
			this.optionsArray.push(option);

			var panel = option.panel;
			if (panel) {
				this.changeParent(panel, this.container);
				panel.style.display = 'none';
			}
		}
	},

	getPanelClass: function(lastNum, currentNum) {
		var animation = {
			selecting: 'same',
			removing: ''
		};

		if (lastNum === null) {
			animation.removing = 'no-change';
			animation.selecting = 'no-change';
		} else if (currentNum > lastNum) {
			animation.removing = 'pullLeft';
			animation.selecting = 'pullRightIn';
		} else if (currentNum < lastNum) {
			animation.removing = 'pullRight';
			animation.selecting = 'pullLeftIn';
		}

		return animation;
	},

	selectPanel: function(panelId) {
		var panelNumber = this.getPanelNumber(panelId);
		if (panelNumber === -1) {
			return;
		}

		var lastNumber = this.lastSelectedPanelNumber;
		var lastPanelNumber = (typeof lastNumber !== 'undefined') ? lastNumber : null;
		this.lastSelectedPanelNumber = panelNumber;

		var animationClass;
		var animations = this.getPanelClass(lastPanelNumber, panelNumber);

		var options = this.optionsArray;
		for (var i = 0, maxLength = options.length; i < maxLength; i++) {
			var option = options[i];
			var id = option.id;
			var panel = option.panel;

			if (panel) {
				var style = panel.style;

				if (panelId === id) {
					style.zIndex = 5;
					animationClass = animations.selecting;
					base.animate._in(panel, animationClass, 800);
				} else if (i === lastPanelNumber) {
					style.zIndex = 4;
					animationClass = animations.removing;
					base.animate.out(panel, animationClass, 1000);
				} else if (style.display !== 'none') {
					style.display = 'none';
				}

				style.zIndex = 2;
			}
		}
	}
});
