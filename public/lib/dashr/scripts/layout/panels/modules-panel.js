var ModulesPanel = function(container) {
	this.init();

	this.container = container;

	this.label = 'Modules';
	this.panelTitle = this.label;
	this.autoUpdate = false;
};

MainPanel.extend({
	constructor: ModulesPanel,
	layout: null,

	setup: function() {
		this.setupLayoutController();
		this.setupModules();

		this.createPanel();
		this.setupMiniLoading();
		this.layout.setup();
	},

	setupLayoutController: function() {
		var parent = this;

		this.layout = {
			keys: [],
			modules: [],

			add: function(key, module) {
				this.keys.push(key);
				this.modules.push(module);
			},

			remove: function(module) {
				var index = base.inArray(this.modules, module);
				if (index !== -1) {
					this.keys.splice(index, 1);
					this.modules.splice(index, 1);
					return true;
				} else {
					return false;
				}
			},

			setup: function() {
				var modules = this.modules;
				var length = this.modules.length;

				for (var i = 0; i < length; i++) {
					var module = modules[i];
					if (module && typeof module.setup === 'function') {
						module.setup.call(module, parent.id + '_modules_container');
					}
				}
			},

			update: function(data) {
				for (var attr in data) {
					if (data.hasOwnProperty(attr) === false) {
						continue;
					}

					var index = base.inArray(this.keys, attr);
					if (index !== -1) {
						var module = this.modules[index];
						if (module && typeof module.update === 'function') {
							module.update.call(module, data[attr]);
						}
					}
				}
			}
		};
	},

	createPanel: function() {
		var layout = {
			id: this.id, class: 'main-panel modules-panel ' + this.mainPanelClass,
			wrapper: {
				class: 'main-wrapper',
				header: {
					node: 'header', class: 'panel-settings-container',
					title: { id: this.id + '_title_settings_container', class: 'title-settings-container' }
				},
				content: {
					id: this.id + '_content_container', class: 'content-container',
					body: {
						id: this.id + '_content_container', class: 'content-container scrollbar',
						summary: {
							id: this.id + '_modules_container', class: 'panels-container'
						}
					}
				}
			}
		};

		base.parseLayout(layout, this.container);
	}
});
