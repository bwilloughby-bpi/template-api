"use strict";

var Toggle = function(checked, callBack, container) 
{ 
	/* this will setup to have mutliple instances of the 
	panel in one project without issue */ 
	this.init('Toggle');     
	
	this.checked = checked || false; 
	this.callBack = callBack;       
	
	this.container = container;  
}; 

/* we want to extend the main panel class and html builder 
class and the class methods to the prototype */ 
base.Component.extend( 
{ 
	/* we want to reset the constructor */ 
	constructor: Toggle, 
	
	render: function() 
	{ 
		var frag = this.createDocFragment();
		var panel = this.addElement('div', this.id, 'data-toggle-panel', '', frag); 
		
		var call = base.bind(this, this.change), 
		checkId = this.id + '_checkbox'; 
		var check = this.checkbox = this.addCheckbox(checkId, 'toggle', call, this.checked, panel, true); 
		var label = this.addElement('label', '', 'toggle-bttn', '', panel); 
		label.htmlFor = checkId; 
		
		this.append(this.container, frag);
	}, 
	
	isChecked: function(e) 
	{ 
		var checked = false
		var element = (e)? (e.target || e.srcElement) : document.getElementById(this.id + '_checkbox'); 
		if(element) 
		{ 
			checked = this.checked = element.checked; 
		} 
		return checked; 
	}, 
	
	toggle: function() 
	{ 
		var checked = this.isChecked(); 
		document.getElementById(this.id + '_checkbox').checked = (checked === true)? false : true; 
	}, 			
	
	change: function(e) 
	{ 
		var checked = this.isChecked(e); 
		if(typeof this.callBack === 'function') 
		{ 
			this.callBack(checked); 
		}
	}, 
	
	setup: function() 
	{ 
		this.render(); 
	} 
}); 