'use strict';

/* Selector */
var Selector = function(type, label, optionsArray, callBackFn, container) {
	this.init();

	this.type = type;
	this.label = label;
	this.optionsArray = optionsArray;
	this.callBackFn = callBackFn;
	this.container = container;

	this.field = null;
	this.value = null;
	this.lastSelectedOption = null;
};

base.Component.extend({
	constructor: Selector,

	setup: function() {
		this.render();
		this.setupEvents();
	},

	render: function() {
		var selection = null;

		var frag = this.createDocFragment();
		var panel = this.addElement('div', this.id, 'selector', '', frag);

		if (this.label) {
			this.addElement('li', this.id + '_option_label', 'label', this.label, panel);
		}

		if (this.type === 'button') {
			var callBack = (typeof callBackFn === 'function') ? base.bind(this, this.callBackFn) : '';
			this.addButton('li', this.id + '_option_field', 'field white', this.field, callBack, panel);

			for (var i = 0, maxLength = this.optionsArray.length; i < maxLength; i++) {
				var tmpOption = this.optionsArray[i];
				if (i === 0) {
					selection = tmpOption;
				}
			}
		}
		else if (this.type === 'list') {
			var field = this.addButton('button', this.id + '_option_field', 'field white left bttn', this.field, base.bind(this, this.expandOrCollapse), panel);
			var arrow = this.addElement('div', '', 'arrow', '', panel);

			var optionList = this.addElement('div', this.id + '_option_list', 'option-list fadeFlipX', '', panel);
			var optionContainer = this.addElement('div', this.id + '_option_container', 'option-container light-blue', '', optionList);

			for (var i = 0, maxLength = this.optionsArray.length; i < maxLength; i++) {
				var tmpOption = this.optionsArray[i];
				this.addOption(tmpOption, optionContainer);

				if (i === 0 || tmpOption.selected === true) {
					selection = tmpOption;
				}
			}
		}
		this.append(this.container, frag);

		if (selection !== null) {
			this.selectOption(selection, true);
		}
	},

	getOptions: function() {
		return this.optionsArray;
	},

	optionNumber: 0,

	addOption: function(option, container) {
		var number = this.optionNumber++;

		option.optionNumber = number;
		option.nameId = this.id + '_option_number' + '_' + number;
		if (typeof option.selected === 'undefined') {
			option.selected = false;
		}

		var clickEvent = base.bind(this, this.selectOption);
		this.addButton('li', option.nameId, 'option', option.label, function() { clickEvent(option); }, container);
	},

	remove: function() {
		var panel = document.getElementById(this.id);
		if (panel) {
			this.removeElement(panel);
		}
	},

	createClickHandler: function(tmpOption) {
		var self = this;
		return function() {
			self.selectOption(tmpOption);
		};
	},

	updateSelectedOption: function(optionValue) {
		var tmpOption = this.getOptionByValue(optionValue);
		if (tmpOption) {
			this.updateValue(tmpOption.value);
			this.updateField(tmpOption.label);
			this.updateListStyle(tmpOption);
		}
	},

	getOptionByNumber: function(number) {
		--number;

		for (var i = 0, maxLength = this.optionsArray.length; i < maxLength; i++) {
			var tmpOption = this.optionsArray[i];
			if (i == number) {
				return tmpOption;
			}
		}

		return false;
	},

	selectOptionByNumber: function(number) {
		var tmpOption = this.getOptionByNumber(number);
		if (tmpOption) {
			this.selectOption(tmpOption);
		}
	},

	/* this will select an option by option value.
	@param (string) value = the option value */
	changeCategory: function(optionValue) {
		var optionsArray = this.optionsArray;
		if (optionsArray) {
			for (var i = 0, maxLength = optionsArray.length; i < maxLength; i++) {
				var option = optionsArray[i];
				if (option.value === optionValue) {
					this.selectOption(option, true);
					break;
				}
			}
		}
	},

	/* this will get an option by option value.
	@param (string) value = the option value
	@return (mixed) the option object or false */
	getOptionByValue: function(value) {
		for (var i = 0, maxLength = this.optionsArray.length; i < maxLength; i++) {
			var tmpOption = this.optionsArray[i];
			if (tmpOption.value === value) {
				return tmpOption;
			}
		}

		return false;
	},

	checkToScrollContainer: function(option) {
		if (option) {
			var button = document.getElementById(option.nameId);
			var buttonSize = base.getSize(button);
			var position = base.position(button);

			var container = document.getElementById(this.id + '_option_container');
			if (!container) {
				return;
			}
			var size = base.getSize(container);
			var scrollPosition = base.getScrollPosition(container);

			if ((position.y + buttonSize.height < (size.height + scrollPosition.top)) && position.y >= scrollPosition.top) {
			} else {
				if (scrollPosition.top < position.y) {
					container.scrollTop = ((position.y + buttonSize.height) - size.height);
				}
				else {
					container.scrollTop = position.y;
				}
			}
		}
	},

	selectOption: function(tmpOption, supressCallBack) {
		this.checkToScrollContainer(tmpOption);

		/* we want to check if the option is no the current selected
		option */
		if (tmpOption && tmpOption.value != this.value) {
			this.updateValue(tmpOption.value);
			//update field to show selection
			this.updateField(tmpOption.label);
			//update list style to show selected option
			this.updateListStyle(tmpOption);

			/* we need to check to hold firing the call back function */
			if (typeof supressCallBack === 'undefined') {
				if (typeof this.callBackFn === 'function') {
					this.callBackFn.call();
				}
			}
		}

		this.setLastSelectedOption(tmpOption);
	},

	updateValue: function(tmpValue) {
		this.value = tmpValue;
	},

	updateField: function(tmpOption) {
		this.field = tmpOption;

		var obj = document.getElementById(this.id + '_option_field');
		obj.textContent = this.field;
	},

	updateListStyle: function(tmpOption) {
		for (var i = 0, maxLength = this.optionsArray.length; i < maxLength; i++) {
			var option = this.optionsArray[i];
			var panel = document.getElementById(option.nameId);
			if (panel) {

				if (tmpOption.label === option.label) {
					panel.className = 'option selected';
				}
				else {
					panel.className = 'option';
				}
			}
		}
	},

	getNextOption: function() {
		var number = (this.lastSelectedOption !== null) ? this.lastSelectedOption.optionNumber : 0,
			nextOptionNumber = ++number;

		if (nextOptionNumber < this.optionsArray.length) {
			return this.optionsArray[nextOptionNumber];
		}
		else {
			return false;
		}
	},

	selectNextOption: function() {
		var nextOption = this.getNextOption();
		if (nextOption) {
			this.selectOption(nextOption);
		}
	},

	getPreviousOption: function() {
		var number = (this.lastSelectedOption !== null) ? this.lastSelectedOption.optionNumber : 0,
			previousOptionNumber = --number;

		/* we need to check if we have reached the begining of the list */
		if (previousOptionNumber >= 0) {
			/* return next option */
			return this.optionsArray[previousOptionNumber];
		}
		else {
			return false;
		}
	},

	selectPreviousOption: function() {
		var previousOption = this.getPreviousOption();
		if (previousOption) {
			this.selectOption(previousOption);
		}
	},

	rotateNextOption: function() {
		var previous = (this.lastSelectedOption) ? this.lastSelectedOption.optionNumber : 0,
			nextOptionNumber = ++previous;

		/* we need to check if we are at the end of the list */
		var nextOption = (nextOptionNumber < this.optionsArray.length) ? this.optionsArray[nextOptionNumber] : this.optionsArray[0];
		if (nextOption) {
			this.selectOption(nextOption);
		}
	},

	setLastSelectedOption: function(option) {
		this.lastSelectedOption = option;
	},

	reset: function() {
		for (var i = 0, maxLength = this.optionsArray.length; i < maxLength; i++) {
			var tmpOption = this.optionsArray[i],
				tmpValue = this.valueArray[i];

			//setup first option as default field and value
			if (i == 0) {
				this.updateValue(tmpValue);
				this.updateField(tmpOption);
			}
		}

		this.updateListStyle(this.field);
	},

	expanded: false,

	/*this opens and closes the option list*/
	expandOrCollapse: function(e) {
		if (this.expanded === false) {
			this.updateState('expand');
		}
		else {
			this.updateState('close');
		}
	},

	state: null,

	updateState: function(state) {
		this.state = state;
		this.changeState();
	},

	changeState: function() {
		var obj = document.getElementById(this.id + '_option_list');

		switch (this.state) {
			case 'expand':
				this.expanded = true;
				//show list
				obj.style.display = 'block';
				//add event listerner to close
				this.addEvent();
				/* add keyboard support */
				this.addKeyboardSupport();
				break;
			case 'close':
				this.expanded = false;
				//minimize list
				obj.style.display = 'none';
				//remove event
				this.removeEvent();
				/* remove keyboard */
				this.removeKeyPress();
				break;
		}
	},

	checkEvent: function(e) {
		e = e || window.event;
		var target = e.srcElement || e.target;
		if (target && target.id !== this.id + '_option_field') {
			this.expandOrCollapse(e);
		}
	},

	setupEvents: function() {
		/* we needto bind the callback to the object
		and save if to a var */
		var callBack = base.bind(this, this.checkEvent);
		this.setupKeyboardSupport();

		/* we want to override the remove fn with the
		new event callback */
		this.addEvent = function() {
			/* we want to remove any previous events before
			creating a new event */
			this.removeEvent();
			base.addListener('mouseup', window, callBack);
		};

		/* we want to override the remove fn with the
		new event callback */
		this.removeEvent = function() {
			base.removeListener('mouseup', window, callBack);
		};
	},

	/* this will add the event listener */
	addEvent: null,

	/* this will remove the event listener */
	removeEvent: null,

	setupKeyboardSupport: function() {
		/* we needto bind the callback to the object
		and save if to a var */
		var callBack = base.bind(this, this.keyPress);

		this.addKeyboardSupport = function() {
			/* we want to remove any previous events before
			creating a new event */
			this.removeKeyPress();
			base.onKeyDown(callBack, document);
		};

		/* we want to override the remove fn with the
		new event callback */
		this.removeKeyPress = function() {
			base.removeListener('keydown', document, callBack);
		};
	},

	/* this will add key press event support */
	addKeyboardSupport: null,

	keyPress: function(keyCode, e) {
		/* we want to check if the user is scrolling up or down */
		switch (keyCode) {
			case 38:
				/* scrolling up */
				this.selectPreviousOption();
				base.preventDefault(e);
				break;
			case 40:
				/* scrolling down */
				this.selectNextOption();
				base.preventDefault(e);
				break;
			case 13:
				this.expandOrCollapse();
				base.preventDefault(e);
				break;
			default:
		}
	},

	/* this will remove the key press event */
	removeKeyPress: null
});