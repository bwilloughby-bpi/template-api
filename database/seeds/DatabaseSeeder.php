<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder {

    public function run() {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        // Truncate all tables
        User::truncate();

        // Run seeders/factories
        $this->call(UserSeeder::class);
        factory(User::class, 10)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
