'use strict';

/* MainPanel */
var MainPanel = function(container) {
	this.init();

	this.container = container;

	this.panelTitle = this.label = 'Main Panel';
	this.autoUpdate = true;
};

base.Component.extend({
	constructor: MainPanel,

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();
		var panel = this.addElement('div', id, 'main-panel', '', frag);

		this.createHeader(panel);
		this.createBody(panel);
		this.append(this.container, frag);
	},

	createHeader: function(container) {
		var id = this.id;
		var header = this.addElement('header', '', 'panel-settings-container', '', container);
		var titleSettings = this.titleSettings = this.addElement('div', id + '_title_settings_container', 'title-settings-container', '', header);
	},

	addHeader: function() {
		var id = this.id;
		return {
			class: 'panel-settings-container',
			title: {
				id: id + '_title_settings_container',
				class: 'title-settings-container'
			}
		};
	},

	createBody: function(container) {
		var id = this.id;
		var contentContainer = this.addElement('div', id + '_content_container', 'content-container', '', container);

		this.createSettings(contentContainer);
	},

	createSettings: function(container) {
		var id = this.id;
		var settings = this.addElement('div', id + '_settings_container', 'settings-container', '', container);
		var settingButtons = this.addElement('div', id + '_settings_button_container', 'button-container', '', settings);
		this.setupSettingsButtons(settingButtons);
	},

	setupSettingsButtons: function(container) {

	},

	miniLoading: null,
	loading: false,

	setupMiniLoading: function(listContainer) {
		var self = this;

		self.miniLoading = {
			listContainer: document.getElementById(listContainer),

			count: 0,

			setup: function() {
				this.count++;
				if (self.loading === false) {
					this.setupMiniLoading();
				}

				this.startLoadingMode();
			},

			setupMiniLoading: function() {
				var id = self.id;
				var loadingContainer = this.loadingContainer = self.addElement('div', id + '_loading_container', 'title-loading-container', '', self.titleSettings);
				self.addElement('div', id + '_loading_image', 'loading-circle loading-circle-small-white', '', loadingContainer);
				self.loading = true;
			},

			remove: function() {
				this.count--;

				if (this.count > 0) {
					return;
				}
				else {
					this.count = 0;

					var panel = this.loadingContainer;
					if (panel) {
						self.removeChild(panel);
					}
					self.loading = false;

					this.endLoadingMode();
				}
			},

			startLoadingMode: function() {
				var panel = this.listContainer;
				if (panel) {
					panel.style.opacity = '.6';
				}
			},

			endLoadingMode: function() {
				var panel = this.listContainer;
				if (panel) {
					panel.style.opacity = '1';
				}
			}
		};
	},

	addMiniLoading: function() {
		this.miniLoading.setup();
	},

	removeMiniLoading: function() {
		this.miniLoading.remove();
	},

	scrollChecker: null,

	setupScrollChecker: function() {
		var parent = this;

		this.scrollChecker = {
			titleContainer: document.getElementById(parent.id + '_title_settings_container'),
			container: document.getElementById(parent.id + '_content_container'),

			enabled: false,
			scrolled: false,

			setup: function() {
				this.enabled = true;
				this.setupEvents();
			},

			checkScrollPosition: function() {
				var titleContainer = this.titleContainer;
				var container = this.container;
				var scrollPosition = base.getScrollPosition(container);

				if (scrollPosition.top > 0) {
					if (this.scrolled === false) {
						this.scrolled = true;
						base.addClass(titleContainer, 'scrolled');
					}
				} else {
					if (this.scrolled === true) {
						this.scrolled = false;
						base.removeClass(titleContainer, 'scrolled');
					}
				}
			},

			eventsAdded: false,

			setupEvents: function() {
				var panel = this.container;
				var callBack = base.bind(this, this.checkScrollPosition);

				this.addEvents = function() {
					if (this.eventsAdded !== true) {
						this.eventsAdded = true;
						base.addListener('scroll', panel, callBack);
					}
				};

				this.removeEvents = function() {
					this.eventsAdded = false;
					base.removeEvent('scroll', panel, callBack);
				};

				this.addEvents();
			},

			addEvents: null
		};

		this.scrollChecker.setup();
	},

	updatePagePanel: function() {
		this.update();
		this.scrollToTop();
	},

	scrollToTop: function() {
		var panel = document.getElementById(this.id + '_content_container');
		if (panel) {
			panel.scrollTop = 0;
		}
	},

	pages: null,
	pageRowNumber: 50,

	setupPagePanel: function(pageContainer) {
		var container = pageContainer || this.id + '_page_container';
		this.pages = new MainPageController(0, this.pageRowNumber, base.bind(this, this.updatePagePanel), container);
		this.pages.setup();
	},

	addSearchPanel: function() {
		return new MainSearchPanelController();
	},

	formatNumber: function(num, roundNum) {
		roundNum = (typeof roundNum !== 'undefined') ? roundNum : true;
		var number = this.removeFinancialMarks(num);
		if (number !== false) {
			var pattern = /(\d)(?=(\d{3})+\b)/g;

			var toFixed = (roundNum) ? 2 : 0;
			return number.toFixed(toFixed).replace(pattern, '$1,');
		}
		return num;
	},

	getPanelUrlLabel: function() {
		var label = this.label;
		if (label !== null) {
			var pattern = / /g;
			return label.replace(pattern, '-').toLowerCase();
		}
		else {
			return false;
		}
	},

	removeFinancialMarks: function(num) {
		if (typeof num !== 'undefined') {
			try {
				var number = num.toString();
				var pattern = /[A-Za-z$-,]/g;
				var result = number.replace(pattern, '');

				return Number(result);
			} catch (e) {
				return false;
			}
		} else {
			return false;
		}
	},

	setup: function() {
		this.render();
		this.setupPanelDefault();
	},

	setupPanelDefault: function() {
		this.setupMiniLoading();
		this.setupScrollChecker();
	},

	activate: function() {

	},

	update: function() {

	},

	deactivate: function() {

	}
});

var MainPageController = function(rowCount, numberPerPage, callBackView, container) {
	this.pagePanel = null;
	this.rowCount = rowCount || 0;
	this.numberPerPage = numberPerPage;
	this.callBackView = callBackView;

	this.container = container;
};

base.Class.extend({
	constructor: MainPageController,

	setup: function() {
		var container = this.container;
		if (this.pagePanel === null) {
			var callBack = base.bind(this, this.callBackView);
			this.pagePanel = new PagePanel(0, this.numberPerPage, callBack, container);
			this.pagePanel.setup();
		}
	},

	checkToUpdate: function(rowCount) {
		if (this.rowCount !== null && this.rowCount !== rowCount) {
			if (rowCount > 0) {
				this.update(rowCount);
			} else {
				var pages = this.pagePanel;
				if (pages.getCurrentPage() !== 1) {
					this.reset();

					var callBack = this.callBackView;
					if (typeof callBack === 'function') {
						callBack();
					}
				} else {
					this.update(rowCount);
				}
			}
		}
	},

	reset: function() {
		this.pagePanel.resetLastSelection();
		this.update(0);
	},

	update: function(rowCount) {
		this.rowCount = rowCount;
		this.pagePanel.updatePageSettings(this.rowCount, this.numberPerPage);
	},

	getLimit: function(mode) {
		var direction = (mode === 'start') ? 'start' : 'stop';
		return this.pagePanel.getLimitNumber(direction);
	}
});

var MainSearchPanelController = function() {
	this.panel = null;

	this.callBackFn = null;
	this.titleLabel = '';
	this.xhr = null;
};

base.Class.extend({
	constructor: MainSearchPanelController,

	setup: function(titleLabel, callBackFn) {
		if (typeof callBackFn === 'function') {
			this.callBackFn = callBackFn;
		}

		if (typeof titleLabel === 'string') {
			this.titleLabel = titleLabel;
		}
	},

	createPanel: function() {
		this.panel = null;

		this.panel = new ModalSearchPanel(this.titleLabel, 'no', base.bind(this, this.search), base.bind(this, this.callBackFn), app.backgroundPanel.id);
		this.panel.setup();
	},

	search: function(tmpSearch) {
	},

	searchResults: function(response) {
		this.xhr = null;
		if (typeof response !== 'string') {
			var result = response;
			this.panel.updateOptions(result);
		}
	},

	cancelPreviousXhr: function() {
		if (this.xhr !== null) {
			this.xhr.abort();
		}
	},

	display: function() {
		this.createPanel();
		this.panel.display();
	}
});