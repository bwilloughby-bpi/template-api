"use strict";

/* 
	FlashPanel 
	
	this will create a panel that will display a message 
	on the screen and then remove itself after its done.  
	
	@param (string) title = the panel title 
	@param (string) description = the panel description 
	@param (int) duration = the flash duration in mircoseconds 
	@param (function) callback = the callback functon 
*/  
var FlashPanel = function(title, description, duration, container, callback)
{  
	/* this will setup to have mutliple instances of the 
	panel in one project without issue */ 
	this.init('FlashPanel');   
	
	/* this will save the title and description */  
	this.title = title; 
	this.description = description; 
	this.duration = (typeof duration === 'number')? duration : 4000; 
	this.timer; 
	
	this.container = container || app.backgroundPanel.id; 
	
	/* this will save the callback and args */ 
	this.callback = callback;    
}; 

base.Component.extend( 
{ 
	constructor: FlashPanel, 
	
	setup: function()
	{ 
		this.render(); 
		this.startTimer();     
	}, 
	
	render: function()
	{ 
		var frag = this.createDocFragment();
		var panel = this.addElement('div', this.id, 'flash-panel pullUp', '', frag); 
		var buttonContainer = this.addElement('footer', this.id + '_buttons' + this.number, 'button-container', '', panel); 
		
		var titleContainer = this.addElement('header', this.id + '_title_container' + this.number, 'title-container', '', panel); 
		var title = this.addElement('div', this.id + '_title' + this.number, 'title left dark', this.title, titleContainer);
		var description = this.addElement('div', this.id + '_text' + this.number, 'description center dark', this.description, titleContainer); 
 
		var button = this.addButton('button', this.id + '_button_2', 'bttn close', '<span></span><span></span>', base.bind(this, this.close), buttonContainer); 
		this.append(this.container, frag);
	}, 
	 
	remove: function()
	{ 
		var panel = document.getElementById(this.id); 
		if(panel) 
		{  
			this.removeElement(panel); 
		} 
	},  
	 
	close: function()
	{ 
		var obj = document.getElementById(this.id); 
		if(obj) 
		{ 
			base.addClass(obj, 'close'); 
			window.setTimeout(base.bind(this, this.remove), 800); 
		} 
		this.stopTimer();    
	},    
	
	/* this will return true to the callback if 
	supplied and remove the panel*/
	accept: function()
	{  
		if(typeof this.callback === 'function') 
		{ 
			this.callback.call(); 
		}     
		this.close(); 
	}, 
	
	startTimer: function() 
	{ 
		this.stopTimer(); 
		
		this.timer = window.setTimeout(base.bind(this, this.close), this.duration); 
	}, 
	
	stopTimer: function() 
	{ 
		window.clearTimeout(this.timer); 
	} 
}); 