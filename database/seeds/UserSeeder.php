<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {

    public function run() {
        // TODO: Review fields and roles
        DB::table('users')->insert([
            [
                'username'     => 'brandon.willoughby',
                'email'        => 'brandon.willoughby@businesspromotion.com',
                'first'        => 'Brandon',
                'last'         => 'Willoughby',
                'role'         => 'admin',
                'allow_access' => true,
                'password'     => password_hash('Abc12345*', PASSWORD_DEFAULT),
            ],
        ]);

    }
}
