"use strict";

/* 
	Calendar 
	
	this will create an overlay calendar that will 
	load the current date from the field and return 
	the selected date to the field when saved. 
	
	@param (mixed) field = the date field element or id 
	@param [(mixed)] container = the container or id to add 
	the new date panel 
*/ 
var Calendar = function(field, container) 
{ 
	/* this will allow multiple instances of the same 
	panel class in the same project */ 
	this.init('Calendar');  
	
	/* this will be the field that will be updated when 
	the date is loaded and saved */ 
	this.field = (typeof field !== 'object')? document.getElementById(field) : field; 
	 
	this.weekDayCount = 0; 
	
	/* this will hold all the date option objects */ 
	this.dates = []; 
	
	/* this will store the selected option */ 
	this.selected; 
	
	/* this is the current system date */ 
	this.currentDate = new Date(); 
	this.date = null; 
	
	/* this will setup the parent container */ 
	this.getContainer = function() 
	{ 
		var parent; 
		/* we want to setup the container default or 
		get by id */ 
		if(container == null) 
		{ 
			/* add to body */ 
			parent = document.body; 
		} 
		else 
		{ 
			/* add to container */ 
			parent = (typeof container !== 'object')? document.getElementById(container) : container; 
		} 
		
		return parent; 
	};
	
	this.container = this.getContainer();   
}; 

/* we want to extend the html builder class and the 
class methods to the prototype */ 
base.Component.extend( 
{ 
	/* we want to reset the constructor */ 
	constructor: Calendar,
	
	/* this will remove the panel and the shadow elements and 
	remove any event listeners */ 
	remove: function()
	{ 
		this.removeEvent(); 
		
		var id = this.id; 
		var doc = document; 
		var panel = doc.getElementById(id); 
		if(panel) 
		{  
			this.removeElement(panel); 
		}
		
		panel = doc.getElementById(id + '_shadow'); 
		if(panel) 
		{  
			this.removeElement(panel); 
		}  
	}, 
	
	render: function() 
	{  
		var id = this.id;
		var frag = this.createDocFragment(); 
		var panel = this.addElement('div', id, 'calendar-panel', '', frag); 
		
		
		var title = this.addElement('header', id + '_title_container', 'title-container', '', panel); 
		var titleLabel = this.addElement('div', '', 'title dark', 'Calendar', title); 
		 
		var container = this.addElement('div', id + '_container', 'details-container', '', panel); 
		 
		var dateContainer = this.addElement('div', id + '_date_container', 'date-container', '', container); 
		var buttonPrevious = this.addButton('button', id + '_previous_button', 'direction-buttons bttn previous', '', base.bind(this, this.previous), dateContainer);
		var dateLabel = this.addElement('div', id + '_date_label', 'date-label', '', dateContainer); 
		 
		var optionContainer = this.addElement('div', id + '_option_container', 'options-container', '', dateLabel);
		var optionMonth = this.addSelect(id + '_month','month', '',base.bind(this, this.updateByOption), optionContainer); 
		var optionYear = this.addSelect(id + '_year', 'year', '', base.bind(this, this.updateByOption), optionContainer);  
		
		var buttonNext = this.addButton('button', id + '_next_button', 'direction-buttons bttn next', '', base.bind(this, this.next), dateContainer); 
		var dayLabelContainer = this.addElement('div', id + '_day_label_container',  'day-label-container', '', container); 
		this.setupDayLabels(dayLabelContainer);  
		var calendarContainer = this.addElement('div', id + '_calendar_container', 'calendar-days-container', '', container);
		
		var buttons = this.addElement('footer', id + '_button_container', 'button-container', '', panel); 
		var button1 = this.addButton('button', id + '_button_1', 'bttn-red', 'Cancel', base.bind(this, this.decline), buttons); 
		var button2 = this.addButton('button', id + '_button_2', 'bttn', 'Save', base.bind(this, this.accept), buttons); 
		
		this.append(this.container, frag);
		
		this.setupMonthOptions(); 
		this.setupYearOptions(); 
		
		if(this.field) 
		{ 
			var tmpDate = this.field.value; 
			this.setupMonth(tmpDate); 
		} 
		else 
		{ 
			this.setupMonth(); 
		} 
	}, 
	
	/* this will setup the panel, add events, 
	and display the panel */ 
	setup: function() 
	{ 
		this.render(); 
		this.setupEvents();  
		this.display(); 
	}, 
	
	setupDayLabels: function(container) 
	{ 
		this.addElement('div','','week-day-tl','SUN',container); 
		this.addElement('div','','week-day-tl','MON',container); 
		this.addElement('div','','week-day-tl','TUE',container); 
		this.addElement('div','','week-day-tl','WED',container); 
		this.addElement('div','','week-day-tl','THU',container); 
		this.addElement('div','','week-day-tl','FRI',container); 
		this.addElement('div','','week-day-tl','SAT',container);
	}, 
	
	add: function(area, date, month, day, year, weekday) 
	{ 
		this.dates.push({ 
			area: area, 
			date: date, 
			month: month, 
			day: day, 
			year: year, 
			weekday: weekday 
		}); 
	}, 
	
	/* this will reset the active month day elements
	 from the month container */ 
	reset: function() 
	{ 
		this.dates = []; 
		
		var element = document.getElementById(this.id + '_calendar_container'); 
		this.removeAll(element);  
	}, 
	
	/* this will setup the active month 
	@param [(string)] date = the date string */ 
	setupMonth: function(date) 
	{ 
		this.reset(); 
		
		/* this will setup the date by the date sent or 
		the current date if none setup */ 
		var dateObj = this.setupDate(date), 
		month = dateObj.getMonth(), 
		day = dateObj.getDate(), 
		year = dateObj.getFullYear(); 
		
		this.date = this.setupMonthObject(month, day, year); 
		
		/* this will create the calendar layout days */ 
		this.setupCalendarDays(); 
	}, 
	
	/* this will setup the calendar days */ 
	setupCalendarDays: function() 
	{ 
		this.setupPreviousDays(); 
		this.setupMonthDays(); 
		this.setupNextMonthDays(); 
		this.createMonth(); 
	}, 
	
	/* this will setup a date object by date requested. 
	@param [(string)] date = the date string 
	@return (object) the date object */ 
	setupDate: function(date) 
	{ 
		var dateObj = (date)? new Date(date) : new Date(); 
		if(!dateObj.getFullYear()) // check for valid date 
		{ 
			dateObj = new Date(); 
		} 
		return dateObj; 
	}, 
	
	/* this will update the calendar by the select options */ 
	updateByOption: function() 
	{ 
		var monthOption = document.getElementById(this.id + '_month').value; 
		var yearOption = document.getElementById(this.id + '_year').value; 
		
		var tmpDate = monthOption + '/' + this.date.day + '/' + yearOption; 
		this.setupMonth(tmpDate); 
	}, 
	
	updateOptions: function() 
	{ 
		var date = this.date; 
		var monthInput = document.getElementById(this.id + '_month'); 
		for(var i = 0; i <= 12; i++) 
		{ 
			if(date.month == i) 
			{ 
				monthInput.options[i].selected = true; 
			} 
		} 
		
		var yearOptions = document.getElementById(this.id + '_year'); 
		for(var i = 0, maxLength = yearOptions.options.length; i < maxLength; i++) 
		{ 
			var option = yearOptions.options[i]; 
			if(date.year == option.value) 
			{ 
				option.selected = true; 
			} 
		} 
	}, 
	
	selectDate: function(tmpDate) 
	{ 
		if(tmpDate) 
		{ 
			this.selected = tmpDate; 
			this.setupMonth(tmpDate.date);  
		} 
	}, 
	
	createClickHandler: function(tmpDate)
	{ 
		var self = this; 
		
		return function() 
		{ 
			self.selectDate(tmpDate);  
		}; 
	},
	
	/* get previous month */ 
	getPreviousMonth: function() 
	{ 
		var self = this; 
		
		var date = this.date, 
		previousMonth = ((date.month - 1) < 0)? 11 : date.month - 1, 
		previousYear = (previousMonth == 11)? (date.year - 1) : date.year; 
		
		return this.setupMonthObject(previousMonth, 1, previousYear);  
	}, 
	
	setupMonthObject: function(month, day, year) 
	{ 
		if(!isNaN(month) && !isNaN(year)) 
		{ 
			var self = this; 
			return { 
				month: month, 
				day: day, 
				year: year, 
				date: month + '/' + day + '/' + year,  
				firstDay: self.getFirstDay(month, year),
				days: self.monthLength(month, year), 
				leftOverDays: 0 
			}; 
		} 
	}, 
	
	/* this will select the next month */ 
	next: function() 
	{ 
		var date = this.date, 
		month = ((date.month + 1) == 12)? 0 : date.month + 1, 
		year = (month == 0)? (date.year + 1) : date.year; 
		
		date = this.changeDate(month + '/' + date.day + '/' + year); 
		this.setupMonth(date); 
	}, 
	
	/* this will select the previous month */ 
	previous: function() 
	{ 
		var date = this.date, 
		month = ((date.month - 1) < 0)? 11 : date.month - 1, 
		year = (month == 11)? (date.year - 1) : date.year; 
		
		date = this.changeDate(month + '/' + date.day + '/' + year); 
		this.setupMonth(date); 
	},
	
	/* this will select a date from the calendar day 
	options by sending the date */ 
	changeDate: function(userDate)
	{ 
		var month, day, year, 
		tmpDateArray = userDate.split("/"); 
 
		var standardMonth = (parseInt(tmpDateArray[0]) + 1).toString(); 
		month = (standardMonth.length <= 1)? '0' + standardMonth : standardMonth; 
		 
		var standardDay = tmpDateArray[1];
		day = (standardDay.length <= 1)? '0' + standardDay: standardDay; 
		 
		var standardYear = tmpDateArray[2]; 
		year = standardYear; 
		
		return month + '/' + day + '/' + year;
	}, 
	
	setupPreviousDays: function() 
	{ 
		var previousMonth = this.getPreviousMonth(); 
		for(var i = 0, maxLength = this.weekDay.length; i < maxLength; i++)
		{ 
			//if the week day is not the first day add 1 
			if(this.date.firstDay ===  this.weekDay[i])
			{ 
				break; 
			}
			else
			{ 
				previousMonth.leftOverDays++; 
			}
		} 
		
		var prevDay = (previousMonth.days - previousMonth.leftOverDays + 1), 
		returnDate; 
		
		for(var i = 0, maxLength = previousMonth.leftOverDays; i < maxLength; i++)
		{ 
			this.updateWeekdayCount();  
			
			returnDate = this.changeDate(previousMonth.month + '/' + prevDay + '/' + previousMonth.year); 
			this.add('previous', returnDate, previousMonth.month, prevDay++, previousMonth.year, this.weekDay[this.weekDayCount]);  
		}  
	}, 
	
	setupMonthDays: function() 
	{ 
		var returnDate, 
		tmpDayNumber = 1; 
		
		for(var i = 0, maxLength = this.date.days; i < maxLength; i++)
		{  
			this.updateWeekdayCount();  
			 
			returnDate = this.changeDate(this.date.month + '/' + tmpDayNumber + '/' + this.date.year); 
			this.add('current', returnDate, this.date.month, tmpDayNumber++, this.date.year, this.weekDay[this.weekDayCount]); 
		}  
	}, 
	
	updateWeekdayCount: function() 
	{ 
		this.weekDayCount++; 
		if(this.weekDayCount === 7) 
		{ 
			this.weekDayCount = 0; 
		} 
	}, 
	
	getNextMonth: function() 
	{ 
		var date = this.date, 
		nextMonth = ((date.month + 1) === 12)? 0 : date.month + 1, 
		nextMonthYear = (nextMonth === 0)? (date.year + 1) : date.year; 
		
		return this.setupMonthObject(nextMonth, 1, nextMonthYear);  
	}, 
	
	setupNextMonthDays: function() 
	{ 
		var returnDate; 
		var nextDay = 1; 
		
		var nextMonth = this.getNextMonth(); 
		 
		nextMonth.leftOverDays = (42 - (this.dates.length)); 
		
		for(var i = 0, maxLength = nextMonth.leftOverDays; i < maxLength; i++)
		{  
			this.updateWeekdayCount();  
			 
			returnDate = this.changeDate(nextMonth.month + '/' + nextDay + '/' + nextMonth.year); 
			this.add('next', returnDate, this.date.month, nextDay++, this.date.year, this.weekDay[this.weekDayCount]); 
		}  
	}, 
	
	createMonth: function() 
	{ 
		var selectedDate = this.changeDate(this.date.date), 
		currentDate = this.changeDate(this.currentDate.getMonth() + '/' + this.currentDate.getDate() + '/' + this.currentDate.getFullYear());  
		
		for(var i = 0, maxLength = this.dates.length; i < maxLength; i++) 
		{ 
			var option = this.dates[i]; 
			
			if(!this.selected && this.isSameDate(option.date, selectedDate)) 
			{ 
				this.selected = option; 
			} 
			
			var dateClass = 'week-day';
			//check view date 
			if(this.selected && this.isSameDate(this.selected.date, option.date))
			{ 
				dateClass += ' active-day';  
			} 
			else if(this.isSameDate(option.date, currentDate))
			{ 
				dateClass += ' current-day'; 
			} 
			else if(option.area !== 'current')
			{ 
				dateClass += ' inactive-month'; 
			}   
			
			this.addButton('div', '', dateClass, option.day, this.createClickHandler(option), this.id + '_calendar_container'); 
		} 
		
		this.updateOptions(); 
	}, 
	
	isSameDate: function(date1, date2) 
	{ 
		return date1 === date2? true : false; 
	}, 
	
	weekDay: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"], 
	monthNames: ["January","February","March","April","May","June","July","August","September","October","November","December"], 
	
	/* this will get the week day name of the first day of the month. 
	@param (int) month = the month number 
	@param (int) year = the year number 
	@return (string) the week day name of the first day of the month */ 
	getFirstDay: function(month, year) 
	{
		//start new date object
		var dateObj =  new Date(); 
		
		//set the year of date object
		dateObj.setFullYear(year); 
		var year = dateObj.getFullYear(); 
		
		//set first day of date object
		dateObj.setDate(1); 
 
		//set the month of date object
		dateObj.setMonth(month); 
		
		//save the number of the month (number of the months begins at 0 not 1)
		var m_name = dateObj.getMonth();  
		var monthName = this.monthNames[m_name];
 
		//return first day name
		var first_day = dateObj.getDay();
		return this.weekDay[first_day];
	}, 
	
	/* this will get the length of the month.  
	@param (int) month = the month number 
	@param (int) year = the year number 
	@return (int) the days length */ 
	monthLength: function(month, year)
	{ 
		//check for leap year 
		var leapYear = ((year % 400 === 0) || (year % 100 !== 0 && year % 4 === 0))? 
			true
		:
			false; 
		 
		//setup up last day of each month
		var days = (leapYear === true)? 
			[31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
		:
			[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; 
 
		//get last day 
		return days[month]; 
	}, 
	
	/* this will setup the month select options */ 
	setupMonthOptions: function() 
	{ 
		var selectOptionId = this.id + '_month'; 
		
		var input = document.getElementById(selectOptionId); 
							
		/* create each option then add it to the select */   
		for(var n = 0, maxLength = this.monthNames.length; n < maxLength; n++) 
		{ 
			var name = this.monthNames[n]; 
			input.options[n] = new Option(name, n + 1); 
		} 
	}, 
	
	setupYearOptions: function() 
	{ 		
		var selectOptionId = this.id + '_year'; 
		
		/* we will setup the year then add 15 to it 
		to give our stop point*/ 
		var tmpDate = new Date(), 
		year = tmpDate.getFullYear(), 
		start = year - 100; 
		
		var input = document.getElementById(selectOptionId);    
		
		/* create each year option */ 
		for(var n = 0; n < 120; n++) 
		{ 
			input.options[n] = new Option(start, start); 
			if(year === input.options[n].value) 
			{ 
				input.options[n].selected = true; 
			}
			start++; 
		}
	}, 
	
	/*this accepts the selections */ 
	accept: function()
	{    
		if(this.selected) 
		{  
			this.field.value = this.selected.date;    
			
			//setup timer to remove panel 
			window.setTimeout(base.bind(this, this.display), 200); 
		} 
		else 
		{ 
			var mpalert = new Alert('alert', 'Calendar Error', 'You haven\'t selected anything yet.'); 
			mpalert.start(); 
		}  
	}, 
	
	/*this accepts the selections */ 
	decline: function()
	{    
		//setup timer to remove panel 
		this.display(); 
	},
	
	/*set to dispaly or not display the panel*/ 
	display: function(center)
	{   
		this.toggleDisplay(); 
		
		if(this.toggleMode === 'block') 
		{ 
			this.align(); 
			this.addEvent(); 
		} 	
	}, 
	
	toggleMode: null, 
	
	toggleDisplay: function() 
	{ 
		var obj = document.getElementById(this.id),   
		display = obj.style.display; 
		
		if(display === '' || display === 'none')
		{  
			document.getElementById(this.id + '_month').focus(); 
			obj.style.display = 'block'; 
			this.toggleMode = 'block';   
			this.createShadow();   
		}
		else
		{ 
			obj.style.display = 'none'; 
			this.toggleMode = 'none';
			this.remove(); 
		} 
	}, 
	
	createShadow: function() 
	{ 
		//add shadow 
		this.addButton('div', this.id + '_shadow', 'modal-shadow calendar-shadow fadeIn', '', base.bind(this, this.decline), this.container);
	},
	
	/* align alert panel to center of the window */ 
	align: function()
	{ 
		var panel = document.getElementById(this.id); 
		var windowSize = base.getWindowSize(); 
		
		var top = (windowSize.height / 2) - (panel.offsetHeight / 2);
		var left = (windowSize.width / 2) - (panel.offsetWidth / 2); 
		
		panel.style.top = top + 'px'; 
		panel.style.left = left + 'px'; 
	}, 
	
	setupEvents: function() 
	{ 
		var align = base.bind(this, this.align); 
		
		this.addEvent = function()
		{ 
			base.addListener('resize', window, align); 
		}; 
		
		this.removeEvent = function()
		{ 
			base.removeListener('resize', window, align); 
		}; 
	}, 
	
	/* add and remove events */ 
	addEvent: null, 
	
	removeEvent: null 
	
});