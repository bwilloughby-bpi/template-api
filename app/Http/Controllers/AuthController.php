<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller {

    public function __construct() {
    }

    public function login(Request $request) {
        $response = [
            'success'     => true,
            'allowAccess' => false,
        ];

        $user = User::authenticate($request->get('username'), $request->get('password'));
        if ($user !== false) {
            $user->accessLevelNumber = $this->getAccessLevelNumber($user->role);

            $response['user']        = $user;
            $response['allowAccess'] = true;

            $this->createSessionUser($user);
        }

        return $this->r_success($response);
    }

    public function logout(Request $request) {
        $response = [
            'success'     => true,
            'allowAccess' => false,
        ];

        $this->destroySessionUser();

        return $this->r_success($response);
    }

    public function resume(Request $request) {
        $response = [
            'success'     => true,
            'allowAccess' => false,
        ];

        $user = $this->getSessionUser();
        if ($user !== false) {
            $user->accessLevelNumber = $this->getAccessLevelNumber($user->role);

            $response['user']        = $user;
            $response['allowAccess'] = true;
        }

        return $this->r_success($response);
    }

    protected function createSessionUser($user) {
        $_SESSION['user']              = (object)$user->getAttributes();
        $_SESSION['user']->resumeLogin = true;
    }

    protected function destroySessionUser() {
        $_SESSION['user'] = "";
        $_SESSION         = [];
        session_destroy();
    }

    protected function getSessionUser() {
        $user = $_SESSION['user'] ?? false;
        if ($user !== false && isset($user->id)) {
            return User::find($user->id);
        }

        return false;
    }

    private function getAccessLevelNumber($role) {
        $roles = [
            'admin'      => 5,
            'user'       => 3,
            'restricted' => 1,
        ];

        return $roles[$role];
    }
}
