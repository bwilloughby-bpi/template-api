'use strict';

var UserModal = function(userModel, callback, container) {
	this.init();

	this.model = userModel;
	this.callback = callback;
	this.container = container;

	this.mainPanelClass = 'user-modal';

	this.currentUser = appData.user;
};

ModalDataPanel.extend({
	constructor: UserModal,

	setup: function() {
		this.render();
		this.setupHeaderOptions();
	},

	getBodyLayout: function() {

		var roleOptions = [
			{ node: 'option', value: '', textContent: '' },
			{ node: 'option', value: 'sales', textContent: 'Sales' },
			{ node: 'option', value: 'account manager', textContent: 'Account Manager' },
			{ node: 'option', value: 'fulfillment', textContent: 'Fulfillment' }
		];

		if (this.currentUser.get('role') === 'admin') {
			roleOptions.push({ node: 'option', value: 'admin', textContent: 'Admin' });
		}

		var layout = {
			node: 'form',
			header_main: { node: 'h2', class: 'left dark', textContent: 'User Information' },
			panel_main: {
				class: 'content-panel',
				children: [
					this.createRowLayout('Username', this.model, 'username'),
					this.createRowLayout('Email', this.model, 'email'),
					this.createRowLayout('First Name', this.model, 'first'),
					this.createRowLayout('Last Name', this.model, 'last'),
					this.createRowSelectLayout('Role', this.model, 'role', roleOptions)
				]
			}
		};

		if (this.currentUser.get('role') === 'admin' || this.currentUser.get('id') === this.model.get('id')) {
			layout.panel_main.children.push(this.createRowInputLayout('password', 'Password', this.model, 'password'));
			layout.panel_main.children.push(this.createRowInputLayout('password', 'Password Confirmation', this.model, 'password_confirmation'));
		}

		return layout;
	},

	setupHeaderOptions: function() {
		this.setupMiniLoading();
		this.setupMinimize();
	},

	getTitle: function() {
		return 'User';
	},

	accept: function() {
		var id = this.model.get('id');
		if (typeof id !== 'undefined') {
			this.model.xhr.update(base.bind(this, this.acceptResponse));
		} else {
			this.model.xhr.store(base.bind(this, this.acceptResponse));
		}
	},

	acceptResponse: function(response) {
		if (!response)
			return;

		if (response.success === true) {
			if (typeof this.callback === 'function') {
				this.callback(response);
			}

			window.setTimeout(base.bind(this, this.display), 200);
		} else {
			var message = '';
			for (var attr in response) {
				if (response.hasOwnProperty(attr)) {
					message += '<br>' + response[attr][0];
				}
			}
			var alert = new Alert('alert', 'Error', message, '', '');
			alert.setup();
		}
	}
});
