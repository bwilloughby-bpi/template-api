'use strict';

var TabPanel = function(baseUri, callBackFn, container) {
	this.init();

	this.baseUri = baseUri || '/';
	this.callBackFn = callBackFn;
	this.container = container;
};

base.Component.extend({
	constructor: TabPanel,

	routeBase: '/:panel?/',

	setup: function() {
		this.render();

		this.setupLayoutController();
		this.setupRoutes();
		this.addRoutes();
		this.setupEvents();

		var layout = this.layout;
		layout.setup();
		layout.selectPrimaryModule();
	},

	render: function() {
		this.panel = this.addElement('nav', this.id, 'tab-panel', '', this.container);
	},

	setupLayoutController: function() {
		this.layout = new TabLayoutController(this, this.panel);
	},

	setupRoutes: function() {
		var callBack = base.bind(this, this.selectOptionByRoute);
		var uri = this.baseUri + '/' + ':tab';
		var template = '';

		this.addRoutes = function() {
			app.addRoute(uri, template, callBack, ':tab');
		};

		this.removeRoutes = function() {
			app.removeRoute(uri, template, callBack);
		};
	},

	setupEvents: function() {
		var layout = this.layout;

		var callBack = function() {
			layout.moveSlideTab();
		};

		this.addEvents = function() {
			base.on('resize', window, callBack);
		};
		this.addEvents();

		this.removeEvents = function() {
			base.on('resize', window, callBack);
		};
	},

	destroy: function() {
		var panel = this.panel;
		if (panel) {
			this.removeElement(panel);
		}
		this.removeRoutes();
		this.removeEvents();
	},

	addModules: function(modules) {
		var layout = this.layout;
		layout.empty();

		if (modules) {
			for (var i = 0, length = modules.length; i < length; i++) {
				layout.add(modules[i]);
			}
		}
	},

	getValue: function() {
		var active = this.getActive();
		if (active) {
			return active.value;
		}
		return false;
	},

	getActive: function() {
		var layout = this.layout;
		if (layout) {
			return layout.active;
		}
		return false;
	},

	next: function() {
		this.layout.next();
	},

	previous: function() {
		this.layout.previous();
	},

	update: function() {
		this.layout.update();
	},

	selectOptionByRoute: function(params) {
		if (params) {
			var label = params.tab;
			this.layout.selectByRoute(label);
		}
	},

	navigate: function(uri, replace) {
		uri = this.baseUri + '/' + uri;
		app.navigate(uri, null, replace);
	}
});

var TabLayoutController = function(parent, container) {
	this.init('TabLayoutController');

	this.parent = parent;
	this.container = container;

	this.modules = [];
	this.lastSelectedOption = null;

	this.active = null;
};

base.Component.extend({
	constructor: TabLayoutController,

	render: function() {
		var id = this.id;
		var frag = this.createDocFragment();

		var tabNav = this.addElement('nav', id + '_tab_navigation', 'tab-nav', '', frag);
		var slide = this.slide = this.addElement('div', '', 'slide', '', tabNav);
		var tabContainer = this.tabContainer = this.addElement('ul', id + '_tab_container', 'tab-container', '', tabNav);
		var container = this.addElement('div', id + '_panel_container', 'tab-panel-container', '', frag);
		this.append(this.container, frag);
	},

	getPosition: function(obj) {
		var position = { x: 0, y: 0 };

		if (obj && typeof obj === 'object') {
			var dept = 2;
			while (dept > 0) {
				position.x += (obj.offsetLeft + obj.clientLeft);
				position.y += (obj.offsetTop + obj.clientTop);
				obj = obj.offsetParent;
				dept--;
			}
		}
		return position;
	},

	moveSlideTab: function(element) {
		element = element || this.active.button;
		var offset = this.getPosition(element);

		var slide = this.slide.style;
		var position = offset.x;
		slide.webkitTransform = 'translateX(' + position + 'px)';
		slide.transform = 'translateX(' + position + 'px)';
		slide.width = element.offsetWidth + 'px';
	},

	add: function(module) {
		if (typeof module === 'object') {
			module.selected = false;
			module.created = false;
			module.routeURI = this.createRouteUri(module.title);
		}
		this.modules.push(module);
		this.addTabButton(module);
	},

	addTabButton: function(module) {
		var callBack = base.createCallBack(this, this.navigateToModule, [module]);
		var button = module.button = this.addButton('button', '', 'option', module.title, callBack, this.tabContainer);
	},

	remove: function(module) {
		var index = base.inArray(this.modules, module);
		if (index !== -1) {
			module.remove.call(module);
			this.modules.splice(index, 1);
			return true;
		}
		else {
			return false;
		}
	},

	empty: function() {
		var modules = this.modules,
			length = modules.length;

		for (var i = 0; i < length; i++) {
			var module = modules[i];
			module.remove.call(module);
		}

		this.modules = [];
		this.removeAll(this.tabContainer);
		this.active = null;
		this.lastSelectedOption = null;
	},

	setup: function() {
		this.render();
	},

	setupModules: function() {
		var modules = this.modules,
			length = modules.length;

		for (var i = 0; i < length; i++) {
			var module = modules[i];
			this.setupModule(module);
		}
	},

	setupModule: function(module) {
		if (module) {
			if (typeof module.setup === 'function') {
				module.setup.call(module, this.id + '_panel_container');
				var panel = document.getElementById(module.id);
				if (panel) {
					module.panel = panel;
				}
			}
		}
		module.created = true;
	},

	selectPrimaryModule: function() {
		var module = this.modules[0];
		if (module) {
			this.navigateToModule(module);
		}
	},

	update: function() {
		var modules = this.modules,
			length = modules.length;

		for (var i = 0; i < length; i++) {
			var module = modules[i];
			if (module && module.selected === true && typeof module.update === 'function') {
				module.update.call(module);
				break;
			}
		}
	},

	getActiveTitle: function() {
		var active = this.active;
		if (active) {
			return active.title;
		}
		return '';
	},

	getModuleById: function(moduleId) {
		var modules = this.modules,
			length = modules.length;

		for (var i = 0; i < length; i++) {
			var module = modules[i];
			if (module && module.id === moduleId) {
				return module;
			}
		}
		return false;
	},

	selectModuleById: function(moduleId) {
		var module = this.getModuleById(moduleId);
		if (module !== false) {
			this.navigateToModule(module);
		}
	},

	createRouteUri: function(title) {
		var pattern = /\s+/g;
		return (typeof title === 'string') ? title.replace(pattern, '-').toLowerCase() : title;
	},

	selectByRoute: function(uri) {
		var modules = this.modules,
			length = modules.length;

		for (var i = 0; i < length; i++) {
			var module = modules[i];
			if (module.routeURI === uri) {
				if (this.canRouteToModule(module)) {
					this.selectModule(module, false);
					return true;
				}
			}
		}

		return false;
	},

	canRouteToModule: function(module) {
		var active = this.active;
		if (active) {
			if (this.checkPermission() === true) {
				return true;
			}
		}
		return true;
	},

	navigateToModule: function(module, replace) {
		var parent = this.parent;
		var uri = this.createRouteUri(module.title);
		parent.navigate(uri, replace);
	},

	selectModule: function(module) {
		if (module !== false) {
			var self = this;
			var parent = this.parent;
			var panel = parent.panel;

			var activate = function(optn) {
				if (optn && typeof optn === 'object') {
					var activeClass = optn.activeClass;
					if (activeClass) {
						if (base.hasClass(panel, activeClass) === false) {
							base.addClass(panel, activeClass);
						}
					}

					var button = optn.button;
					if (base.hasClass(button, 'selected') === false) {
						base.addClass(button, 'selected');
					}
					self.moveSlideTab(button);

					var callBack = optn.activate;
					if (typeof callBack === 'function') {
						callBack.call(optn);
					}
				}
			};

			var deactivate = function(previous) {
				if (previous && typeof previous === 'object') {
					var activeClass = previous.activeClass;
					if (activeClass) {
						base.removeClass(panel, activeClass);
					}

					var button = previous.button;
					if (base.hasClass(button, 'selected') === true) {
						base.removeClass(button, 'selected');
					}

					var callBack = previous.deactivate;
					if (typeof callBack === 'function') {
						callBack.call(previous);
					}
				}
			};

			if (module.created === false) {
				this.setupModule(module);
			}

			if (module.selected !== true) {
				module.selected = true;
			}

			var previousActive = this.active;
			deactivate(previousActive);
			this.setLastSelectedOption(previousActive);

			this.active = module;
			this.selectActivePanel();

			activate(module);
		}
	},

	getPanelClass: function(lastNum, currentNum) {
		var animation = {
			selecting: 'same',
			removing: ''
		};

		if (lastNum === -1) {
			animation.removing = '';
			animation.selecting = '';
		}
		else if (currentNum > lastNum) {
			animation.removing = 'pullLeftFast';
			animation.selecting = 'pullRightInFaster';
		}
		else if (currentNum < lastNum) {
			animation.removing = 'pullRightFast';
			animation.selecting = 'pullLeftInFaster';
		}

		return animation;
	},

	selectActivePanel: function() {
		var modules = this.modules,
			length = modules.length;

		var active = this.active;

		for (var i = 0; i < length; i++) {
			var module = modules[i];
			var panel = module.panel;

			if (panel) {
				if (active === module) {
					panel.style.zIndex = 2;
					panel.style.display = 'block';
				} else if (module.selected === true) {
					module.selected = false;

					panel.style.zIndex = 1;
					panel.style.display = 'none';
				} else {
					panel.style.display = 'none';
				}
			}
		}
	},

	setLastSelectedOption: function(option) {
		this.lastSelectedOption = option;
	},

	getNextOption: function() {
		var modules = this.modules;
		var active = this.active;
		var index = (active) ? base.inArray(modules, active) : 0;
		var nextOptionNumber = ++index;

		index = (nextOptionNumber < modules.length) ? nextOptionNumber : false;
		return modules[index];
	},

	checkPermission: function() {
		var active = this.active;
		if (active) {
			var accept = active.accept;
			if (typeof accept === 'function') {
				var accepted = accept.call(active);
				if (accepted === true) {
					return true;
				}
				return false;
			}
		}
		return true;
	},

	next: function() {
		if (this.checkPermission()) {
			var nextOption = this.getNextOption();
			if (nextOption) {
				this.navigateToModule(nextOption);
			}
		}
	},

	getPreviousOption: function() {
		var modules = this.modules;
		var active = this.active;
		var index = (active) ? base.inArray(modules, active) : 0;
		var previousOptionNumber = --index;

		index = (previousOptionNumber >= 0) ? previousOptionNumber : false;
		return modules[index];
	},

	previous: function() {
		var previousOption = this.getPreviousOption();
		if (previousOption) {
			this.navigateToModule(previousOption);
		}
	}
});

var TabModule = function() {
	this.init('TabModule');

	this.title = 'Label';
	this.value = 'label';
};

base.Component.extend({
	constructor: TabModule,

	render: function(container) {

	},

	setup: function(container) {
		this.render(container);
	},

	update: function() {

	},

	activate: function() {

	},

	deactivate: function() {

	},

	removePanel: function() {
		var panel = document.getElementById(this.id);
		if (panel) {
			this.removeElement(panel);
		}
	},

	remove: function() {
		this.removePanel();
	},

	accept: function() {
		return true;
	}
});