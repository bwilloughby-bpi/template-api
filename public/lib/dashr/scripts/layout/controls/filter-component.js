var FilterComponent = function(container) {
	this.init();

	this.container = container;

	this.filters = [];
};

base.Component.extend({
	constructor: FilterComponent,

	setup: function() {
		this.createPanel();
	},

	createPanel: function() {
		var layout = this.getLayout();
		base.parseLayout(layout, this.container);

		this.filterContainer = document.getElementById(this.id + '_filter_container');
		this.filterControls = document.getElementById(this.id + '_filter_controls');
	},

	getLayout: function() {
		var layout = [
			{
				node: 'button', class: 'bttn white', textContent: 'Filter', onclick: base.bind(this, this.toggleFilter)
			},
			{
				id: this.id + '_filter_container', class: 'filter-container fadeFlipX hidden',
				controls: {
					id: this.id + '_filter_controls', class: 'controls'
				},
				buttons: {
					class: 'button-container',
					save: {
						node: 'button', class: 'bttn white', textContent: 'Close', onclick: base.bind(this, this.toggleFilter)
					}
				}
			}
		];

		return layout;
	},

	toggleFilter: function(e) {
		base.toggleClass(this.filterContainer, 'hidden');
	},

	add: function(name, options, callback, type) {
		type = type || 'Selector';

		var selector = new window[type]('list', '', options, callback, this.filterControls);
		selector.setup();

		this.filters.push({
			name: name,
			selector: selector
		});
	},

	addSelector: function(name, selector) {
		if (selector == null) {
			return;
		}

		selector.container = this.filterControls;
		selector.setup();
		this.filters.push({
			name: name,
			selector: selector
		});
	},

	getFilter: function() {
		var filter = {};

		for (var i = 0, len = this.filters.length; i < len; i++) {
			var f = this.filters[i];
			filter[f.name] = f.selector.value;
		}

		return filter;
	},

	getFilterString: function() {
		var filters = [];
		for (var i = 0, len = this.filters.length; i < len; i++) {
			var f = this.filters[i];
			if (f.selector.value !== null) {
				filters.push(f.selector.value);
			}
			else {
				filters.push('all');
			}
		}

		return filters.join(':');
	},

	setFilterString: function(filterString) {
		var digitPattern = /^\d+$/;

		var filterStrings = filterString.split(':');
		for (var i = 0, len = filterStrings.length; i < len && i < this.filters.length; i++) {
			var f = this.filters[i];
			var val = filterStrings[i];
			if (val.match(digitPattern)) {
				val = +val; // screw you, javascript
			}

			f.selector.updateSelectedOption(val);
		}
	},

	updateSelectedOption: function(filterIndex, value) {
		var filter = this.filters[filterIndex];
		filter.selector.updateSelectedOption(value);
	}
});